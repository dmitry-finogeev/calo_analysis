#ifndef EVENT_DATA_STRUCT
#define EVENT_DATA_STRUCT

#include<TTree.h>
#include<TString.h>
#include<TMath.h>
#include<TObject.h>
#include<fstream>

//data processed from waveform
struct event_data_struct
{

    //UInt_t time_stamp;// 32bit
    //UInt_t time_stamp_ns;

    Float_t FIT_MAX_in_gate;
    Float_t FIT_integral_in_gate;
    Float_t FIT_time_max_in_gate;
    Float_t FIT_time_half;
    Float_t FIT_Chi_square;
    Float_t FIT_zero_level;
    Float_t FIT_third_arg;

    Int_t integral_in_gate;

    Float_t mean_in_gate;
    Float_t mean_out_gate;
    Float_t RMS_in_gate;
    Float_t RMS_out_gate;

    Float_t zero_level;
    Float_t zero_level_RMS;

    Float_t time_cross;
    Float_t time_half;

    Short_t MAX_in_gate;//16
    Short_t MIN_in_gate;
    Short_t MAX_out_gate;
    Short_t MIN_out_gate;



    UShort_t time_max_in_gate;//16
    UShort_t time_min_in_gate;

    ULong64_t timestamp;

    Bool_t is_pile_up;


    void reset()
    {
	//time_stamp = 0;
	//time_stamp_ns = 0;


	FIT_MAX_in_gate = 0.;
	FIT_integral_in_gate = 0.;
	FIT_time_max_in_gate = 0.;
	FIT_time_half = 0.;
	FIT_Chi_square = 0.;
	FIT_zero_level = 0.;
	FIT_third_arg = 0.;

	integral_in_gate = 0; //integral / gate_width

	MAX_in_gate = 0;
	MIN_in_gate = 0;
	MAX_out_gate = 0;
	MIN_out_gate = 0;

	time_max_in_gate = 0;
	time_min_in_gate = 0;

	mean_in_gate = 0.;
	mean_out_gate = 0.;
	RMS_in_gate = 0.;
	RMS_out_gate = 0.;

	zero_level = 0.;
	zero_level_RMS = 0.;

	time_cross = 0.;
	time_half = 0.;

        timestamp = 0;

	is_pile_up = false;
    }

    void Print()
    {
//	printf("[time %d:%d; integral_in_gate %i; MAX_in_gate %i; MIN_in_gate %i; MAX_out_gate %i; MIN_out_gate %i; time_max_in_gate %i; time_min_in_gate %i; mean_in_gate %.2f; mean_out_gate %.2f; RMS_in_gate %.2f; RMS_out_gate %.2f; ]",
//	       time_stamp, time_stamp_ns, integral_in_gate, MAX_in_gate, MIN_in_gate, MAX_out_gate, MIN_out_gate, time_max_in_gate, time_min_in_gate, mean_in_gate, mean_out_gate, RMS_in_gate, RMS_out_gate);



	printf("[FIT_MAX_in_gate %.2f; FIT_integral_in_gate %.2f; FIT_time_max_in_gate %.2f; FIT_time_half %.2f; FIT_Chi_square %.2f; FIT_zero_level %.2f; FIT_third_arg %.5f; integral_in_gate %i; MAX_in_gate %i; MIN_in_gate %i; MAX_out_gate %i; MIN_out_gate %i; time_max_in_gate %i; time_min_in_gate %i; mean_in_gate %.2f; mean_out_gate %.2f; zero_level %.2f; zero_level_RMS %.2f; time_cross %.2f; time_half %.2f; RMS_in_gate %.2f; RMS_out_gate %.2f; Timestamp %i; Pule-up %i]",
	       FIT_MAX_in_gate, FIT_integral_in_gate, FIT_time_half, FIT_Chi_square, FIT_zero_level, integral_in_gate, MAX_in_gate, MIN_in_gate, MAX_out_gate, MIN_out_gate, time_max_in_gate, time_min_in_gate, mean_in_gate, mean_out_gate, zero_level, zero_level_RMS, time_cross, time_half, RMS_in_gate, RMS_out_gate, timestamp, is_pile_up);
    }

    static TString GetChName(Int_t channel_num)
    {
	return TString::Format("channel_%i", channel_num);
    }

    TBranch* CreateBranch(TTree *tree, Int_t channel_num)
    {
//	return tree->Branch(GetChName(channel_num).Data(), this,
//			    "time_stamp/i:time_stamp_ns:integral_in_gate:mean_in_gate/F:mean_out_gate:RMS_in_gate:RMS_out_gate:MAX_in_gate/s:MIN_in_gate:MAX_out_gate:MIN_out_gate:time_max_in_gate/b:time_min_in_gate");


	return tree->Branch(GetChName(channel_num).Data(), this,
			"FIT_MAX_in_gate/F:FIT_integral_in_gate/F:FIT_time_max_in_gate/F:FIT_time_half/F:FIT_Chi_square/F:FIT_zero_level/F:FIT_third_arg/F:integral_in_gate/I:mean_in_gate/F:mean_out_gate:RMS_in_gate:RMS_out_gate:zero_level:zero_level_RMS:time_cross:time_half:MAX_in_gate/S:MIN_in_gate:MAX_out_gate:MIN_out_gate:time_max_in_gate/s:time_min_in_gate/s:timestamp/l:is_pile_up/O");
    }


    Int_t SetBranch(TTree *tree, Int_t channel_num)
    {
	return tree->SetBranchAddress(GetChName(channel_num).Data(), this);
    }

};


struct event_log_data_struct
{
    Long64_t timestamp;
    Float_t Common_HV;
    Float_t MPPC_HV[10];
    Int_t temp;
    Int_t LED_HV;
    Int_t PinDiode;

    void reset()
    {
	timestamp = 0;
	temp = 0;
	LED_HV = 0;
	PinDiode = 0;
	Common_HV = 0;
	for(Int_t i = 0; i < 10; i++){
		MPPC_HV[i] = 0;
	}
    }

    void Print()
    {
	printf("[timestamp %lld; temp %.2fC; LED_HV %i; Pin Diode %i]\n",timestamp, (((Float_t)temp)/100.), LED_HV, PinDiode);
	printf("Common voltage: %f\n", Common_HV);
	printf("MPPC voltages");
	for(Int_t i = 0; i < 10; i++){
		printf(" %f;",MPPC_HV[i]);
	}
	printf("\n");
    }


    TBranch* CreateBranch(TTree *tree, Int_t board_id = 0)
    {
	return tree->Branch(Form("log_data_%i", board_id), this,"timestamp/L:Common_HV/F:MPPC_HV[10]/F:temp/I:LED_HV/I:PinDiode/I");
    }


    Int_t SetBranch(TTree *tree, Int_t board_id = 0)
    {
	return tree->SetBranchAddress(Form("log_data_%i", board_id), this);
    }

    event_log_data_struct& operator= (event_log_data_struct const& rth)
    {
	if (this != &rth)
	{
	    this->timestamp = rth.timestamp;
	    this->temp = rth.temp;
	    this->LED_HV = rth.LED_HV;
	    this->PinDiode = rth.PinDiode;
	    this->Common_HV = rth.Common_HV;
	    for(Int_t i = 0; i < 10; i++){
		    this->MPPC_HV[i] = rth.MPPC_HV[i];
	    }
	}

	return *this;
    }

};



class Log_file_reader
{
public:
    Log_file_reader(TString file_path);
    ~Log_file_reader();

    void Search_entry(event_log_data_struct &log_data_lnk, Long64_t timestamp_search, Int_t boardId, Int_t &lastindex);

private:
    event_log_data_struct *log_data_arr;
    Long64_t *log_timestamp_arr;
    Int_t *BoardId_arr;
    Int_t nLogsLines;
};

Log_file_reader::~Log_file_reader()
{
    if(log_data_arr) delete[] log_data_arr;
    if(log_timestamp_arr) delete[] log_timestamp_arr;
    if(BoardId_arr) delete[] BoardId_arr;
}

void Log_file_reader::Search_entry(event_log_data_struct &log_data_lnk, Long64_t timestamp_search, Int_t boardId_search, Int_t &lastindex)
{
	Int_t index = lastindex;
	if(log_timestamp_arr[index] > timestamp_search) {index = -1;}
	if(index == -1) {
		while(BoardId_arr[++index]!=boardId_search){
			if(index>=nLogsLines){return;}
		}
	}

	//printf("%i %i %i %i\n", index,BoardId_arr[index], lastindex,BoardId_arr[lastindex]);
	log_data_lnk = log_data_arr[index];
	lastindex = index;
	while(log_timestamp_arr[index] <= timestamp_search){
		//printf("%i %i %i %i\n", index,BoardId_arr[index], lastindex,BoardId_arr[lastindex]);
		log_data_lnk = log_data_arr[index];
		lastindex = index;

		while(BoardId_arr[++index]!=boardId_search){
			if(index>=nLogsLines){return;}
		}

	}
}

Log_file_reader::Log_file_reader(TString file_path)
{

    log_data_arr = NULL;
    log_timestamp_arr = NULL;
    BoardId_arr = NULL;


    std::ifstream log_file;
    log_file.open(file_path.Data());
    if (!log_file.is_open())
    {
	printf("\nWARING: Log file \"%s\" was NOT opened\n", file_path.Data());
	return;
    }

    printf("\n\n--------------------------\n");
    printf("Loading log file \"%s\"\n", file_path.Data());


    char char_buffer[1000];
    TString *tstring_buffer = new TString(char_buffer);

    //numbers of line
    const Int_t n_first_lines = 1000000;
    std::streampos line_n_size, file_size;
    log_file.seekg( 0, std::ios::beg );
    line_n_size = log_file.tellg();
    file_size = log_file.tellg();

    nLogsLines = -1;
    for(Int_t line = 1; line <= n_first_lines; line++)
    {
	log_file.getline(char_buffer, 1000);
	if(log_file.eof())
	{
	    nLogsLines = line;
	    break;
	}
    }

    if(nLogsLines<0)
    {
	line_n_size = log_file.tellg() - line_n_size;
	log_file.seekg( 0, std::ios::end );
	file_size = log_file.tellg() - file_size;
	nLogsLines = (int)ceil(( (float)file_size / (float)line_n_size )*n_first_lines);
    }

    printf("Estimate log lines: %i\n", nLogsLines);
    //log_file.seekg( 0, std::ios::beg );
    log_file.close();
    log_file.open(file_path.Data());
    if (!log_file.is_open())
    {
	printf("\nWARING: Log file \"%s\" was NOT opened\n", file_path.Data());
	nLogsLines = -1;
	return;
    }

    log_data_arr = new event_log_data_struct[nLogsLines];
    log_timestamp_arr = new Long64_t[nLogsLines];
    BoardId_arr = new Int_t[nLogsLines];


    Int_t line_curr = 0;
    for(Int_t line = 0; line < nLogsLines; line++)
    {
	log_file.getline(char_buffer, 1000);
	if(log_file.eof()) break;

	*tstring_buffer = char_buffer;

	TObjArray *str_words = tstring_buffer->Tokenize(",");

	if(str_words->GetLast() < 15)
	{
	    delete str_words;
	    continue;
	}

	Long64_t readed_timestamp = ((TObjString*)str_words->At(0))->GetString().Atoll();
	log_timestamp_arr[line_curr] = (long long int)(floor(readed_timestamp));
	log_data_arr[line_curr].timestamp = readed_timestamp;
	BoardId_arr[line_curr] =  (Int_t)(((TObjString*)str_words->At(1))->GetString().Atof());
	log_data_arr[line_curr].temp =  (Int_t)(((TObjString*)str_words->At(2))->GetString().Atof()*100.);
	log_data_arr[line_curr].LED_HV =  ((TObjString*)str_words->At(3))->GetString().Atoi();
	log_data_arr[line_curr].PinDiode =  ((TObjString*)str_words->At(4))->GetString().Atoi();
	log_data_arr[line_curr].Common_HV =  ((TObjString*)str_words->At(5))->GetString().Atof();
	for(Int_t mppc_i = 6; mppc_i < 16; mppc_i++){
		log_data_arr[line_curr].MPPC_HV[mppc_i - 6] =  ((TObjString*)str_words->At(mppc_i))->GetString().Atof();
	}


	//printf("%s : %i \n", ((TObjString*)str_words->At(4))->GetString().Data(), log_data_arr[line_curr].PinDiode);
	//log_data_arr[line_curr].Print();
	line_curr++;

	delete str_words;
    }

    nLogsLines = line_curr;

    time_t log_start = log_timestamp_arr[0]/1000.;
    time_t log_end = log_timestamp_arr[nLogsLines-1]/1000.;
    printf("Readed: %i lines\n", nLogsLines+1);
    printf("Log first line: %s", asctime( localtime( &log_start ) ) );
    printf("Log last line: %s", asctime( localtime( &log_end ) ) );
    printf("--------------------------\n\n");

    delete tstring_buffer;
    log_file.close();

}




#endif // EVENT_DATA_STRUCT

