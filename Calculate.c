
#include <TFile.h>
#include <TString.h>
#include <TDirectory.h>
#include <TBrowser.h>


#include "data_calculating.h"

/*
 * Calculate previwe data by data tree
 * with data_calculating class
 *
 */
void Calculate()
{

    TString data_path_source = "root_source/";
    TString data_path_result = "root_results/";

    TString file_name = "Adc64_17_04_06_19_15_overnight_test"; //b-l grease



    TFile source_file((data_path_source+file_name+".root").Data(), "READONLY");
    if(!source_file.IsOpen())
    {
	printf("File \"%s\" was NOT opened\n", (data_path_source+file_name+".root").Data());
	return;
    }

    TTree *data_tree = dynamic_cast<TTree*>(source_file.FindObjectAny("adc64_data"));
    if(data_tree == NULL)
    {
	printf("Data tree was NOT found\n");
	source_file.Close();
	return;
    }


    TFile result_file((data_path_result+file_name+"_result"+".root").Data(), "RECREATE");

    data_calculation Data_calc(data_tree);
    Data_calc.Calculate(1);




    //Saving Histogramms
    result_file.mkdir("Results_hists");
    TIter nx_iter((TCollection*)(Data_calc.Get_Results()));
    TObjArray *hist_arr;
    while ((hist_arr=(TObjArray*)nx_iter()))
    {
	result_file.mkdir(Form("Results_hists/%s",hist_arr->GetName()));
	result_file.cd(Form("Results_hists/%s",hist_arr->GetName()));
	hist_arr->Write();
    }

    //Saving Canvas
    result_file.mkdir("Results_canvas");
    result_file.cd("Results_canvas");
    TObjArray *canv_arr = Data_calc.Get_Canvas();
    canv_arr->Write();

    for(Int_t i = 0; i< canv_arr->GetLast(); i++)
    {
	((TCanvas*)canv_arr->At(i))->SaveAs(Form("%s.pdf(",file_name.Data()));
    }
    ((TCanvas*)canv_arr->At(canv_arr->GetLast()))->SaveAs(Form("%s.pdf)",file_name.Data()));



//    result_file.Close();
//    source_file.Close();


    new TBrowser;
}
