#!/bin/bash  

echo "Runnig calo_test_analysis"

root -l -b -q 'calo_test_analisys_T9.c+("beam_T10_18_10_25_15_09_beam_5GeV_noZS_longCable",-6600.,-6200.,"protons")'
root -l -b -q 'calo_test_analisys_T9.c+("beam_T10_18_10_25_16_40_beam_4GeV_noZS_longCable",-6600.,-6000.,"protons")'
root -l -b -q 'calo_test_analisys_T9.c+("beam_T10_18_10_25_17_51_beam_3GeV_noZS_longCable",-6200.,-5600.,"protons")'
root -l -b -q 'calo_test_analisys_T9.c+("beam_T10_18_10_25_19_27_beam_2GeV_noZS_longCable",-5500.,-4500.,"protons")'
exit 1



root -l -b -q 'calo_test_analisys_T9.c+("beam_T10_18_10_25_15_09_beam_5GeV_noZS_longCable",-6600.,-6200.,"protons")'
root -l -b -q 'calo_test_analisys_T9.c+("beam_T10_18_10_25_16_40_beam_4GeV_noZS_longCable",-6600.,-6000.,"protons")'
root -l -b -q 'calo_test_analisys_T9.c+("beam_T10_18_10_25_17_51_beam_3GeV_noZS_longCable",-6200.,-5600.,"protons")'
root -l -b -q 'calo_test_analisys_T9.c+("beam_T10_18_10_25_19_27_beam_2GeV_noZS_longCable",-5500.,-4500.,"protons")'

root -l -b -q 'calo_test_analisys_T9.c+("beam_T10_18_10_24_22_35_beam_5GeV_noZS",-6600.,-6200.,"protons")'
root -l -b -q 'calo_test_analisys_T9.c+("beam_T10_18_10_24_23_38_beam_4GeV_noZS",-6600.,-6000.,"protons")'
root -l -b -q 'calo_test_analisys_T9.c+("beam_T10_18_10_25_00_42_beam_3GeV_noZS",-6200.,-5600.,"protons")'
root -l -b -q 'calo_test_analisys_T9.c+("beam_T10_18_10_25_10_55_beam_2GeV_noZS",-5500.,-4500.,"protons")'


root -l -q -b calo_test_analisys_T9.c+'("beam_NA61_18_04_30_19_45_10GeV_mod3_noZS")'
root -l -q -b calo_test_analisys_T9.c+'("beam_NA61_18_04_30_19_00_13GeV_mod3_noZS")'
root -l -q -b calo_test_analisys_T9.c+'("beam_NA61_18_04_30_18_55_19GeV_mod3_noZS")'
root -l -q -b calo_test_analisys_T9.c+'("beam_NA61_18_04_30_18_37_30GeV_mod3_noZS")'
root -l -q -b calo_test_analisys_T9.c+'("beam_NA61_18_04_30_18_27_40GeV_mod3_noZS")'
root -l -q -b calo_test_analisys_T9.c+'("beam_NA61_18_04_29_15_50_80GeV_mod3_noZS")'



root -l -q -b calo_test_analisys_T9.c+'("beam_NA61_10GeV_ZS100_27_04_2018")'
root -l -q -b calo_test_analisys_T9.c+'("beam_NA61_19GeV_ZS100_27_04_2018")'

root -l -q -b calo_test_analisys_T9.c+'("beam_NA61_18_04_29_10_14_10GeV_mod5_noZS")'
root -l -q -b calo_test_analisys_T9.c+'("beam_NA61_18_04_29_09_25_13GeV_mod5_noZS_desync")'
root -l -q -b calo_test_analisys_T9.c+'("beam_NA61_18_04_29_08_58_19GeV_mod5_noZS_desync")'
root -l -q -b calo_test_analisys_T9.c+'("beam_NA61_18_04_29_08_31_30GeV_mod5_noZS")'
root -l -q -b calo_test_analisys_T9.c+'("beam_NA61_18_04_28_23_20_40GeV_mod5_noZS")'
root -l -q -b calo_test_analisys_T9.c+'("beam_NA61_18_04_30_20_10_40GeV_mod5_noZS")'
root -l -q -b calo_test_analisys_T9.c+'("beam_NA61_18_04_28_14_50_80GeV_mod5_noZS")'
root -l -q -b calo_test_analisys_T9.c+'("beam_NA61_18_04_28_21_50_120GeV_mod5_noZS_desync")'
root -l -q -b calo_test_analisys_T9.c+'("beam_NA61_18_04_28_20_20_150GeV_mod5_noZS_desync")'






root -l -q -b calo_test_analisys_T9.c+'("beam_NA61_18_04_29_09_52_10GeV_mod5_noZS_HV_minus_800mV")'
root -l -q -b calo_test_analisys_T9.c+'("beam_NA61_18_04_29_09_38_13GeV_mod5_noZS_HV_minus_800mV")'
root -l -q -b calo_test_analisys_T9.c+'("beam_NA61_18_04_29_10_30_19GeV_mod5_noZS_HV_minus_800mV")'
root -l -q -b calo_test_analisys_T9.c+'("beam_NA61_18_04_29_11_01_30GeV_mod5_noZS_HV_minus_800mV")'
root -l -q -b calo_test_analisys_T9.c+'("beam_NA61_18_04_30_20_15_40GeV_mod5_noZS_HV_minus_800mV")'
root -l -q -b calo_test_analisys_T9.c+'("beam_NA61_18_04_28_15_50_80GeV_mod5_noZS_HV_minus_800mV")'
root -l -q -b calo_test_analisys_T9.c+'("beam_NA61_18_04_28_21_30_120Gev_noZS_HV_minus_800mV")'
root -l -q -b calo_test_analisys_T9.c+'("beam_NA61_18_04_28_21_10_150GeV_noZS_HV_minus_800mV")'



root -l -q -b calo_test_analisys_T9.c+'("beam_NA61_18_04_29_23_05_150GeV_mod2_noZS")'
root -l -q -b calo_test_analisys_T9.c+'("beam_NA61_18_04_30_08_40_10GeV_mod2_noZS")'
root -l -q -b calo_test_analisys_T9.c+'("beam_NA61_18_04_29_14_30_80GeV_mod2_noZS")'
root -l -q -b calo_test_analisys_T9.c+'("beam_NA61_18_04_30_16_15_19GeV_mod2_noZS")'
root -l -q -b calo_test_analisys_T9.c+'("beam_NA61_18_04_30_16_35_30GeV_mod2_noZS")'
root -l -q -b calo_test_analisys_T9.c+'("beam_NA61_18_04_30_16_37_40GeV_mod2_noZS")'
root -l -q -b calo_test_analisys_T9.c+'("beam_NA61_18_04_30_16_48_80GeV_mod2_noZS")'
root -l -q -b calo_test_analisys_T9.c+'("beam_NA61_18_04_30_16_59_120GeV_mod2_noZS")'
root -l -q -b calo_test_analisys_T9.c+'("beam_NA61_18_04_30_16_10_13GeV_mod2_noZS")'




root -l -b -q 'calo_test_analisys_T9.c+("beam_18_04_12_19_20_5GeV_mod5",-1725.,-1300.,"protons")'
root -l -b -q 'calo_test_analisys_T9.c+("beam_18_04_12_20_20_4.5GeV_mod5",-1700.,-1400.,"protons")'
root -l -b -q 'calo_test_analisys_T9.c+("beam_18_04_12_19_47_4GeV_mod5",-1700.,-1200.,"protons")'
root -l -b -q 'calo_test_analisys_T9.c+("beam_18_04_12_20_56_3.5GeV_mod5",-1600.,-1000.,"protons")'
root -l -b -q 'calo_test_analisys_T9.c+("beam_18_04_12_21_30_3GeV_mod5",-1400.,-800.,"protons")'
root -l -b -q 'calo_test_analisys_T9.c+("beam_18_04_12_22_10_2.5GeV_mod5",-1200.,-400.,"protons")'
root -l -b -q 'calo_test_analisys_T9.c+("beam_18_04_12_23_06_2GeV_mod5",-500.,500.,"protons")'


root -l -b -q 'calo_test_analisys_T9.c+("beam_18_04_13_12_40_5GeV_mod5_shutter_in",-1725.,-1300.,"protons")'
root -l -b -q 'calo_test_analisys_T9.c+("beam_18_04_13_14_19_5GeV_mod5_triggerALICE",-1725.,-1300.,"protons")'
root -l -b -q 'calo_test_analisys_T9.c+("beam_18_04_13_15_10_5GeV_mod5_triggerALICE_and_TOF",-1725.,-1300.,"protons")'
root -l -b -q 'calo_test_analisys_T9.c+("beam_18_04_13_16_10_5GeV_mod5_triggerTOF_100mV",-1725.,-1300.,"protons")'
root -l -b -q 'calo_test_analisys_T9.c+("beam_18_04_13_16_54_5GeV_mod5_triggerTOF_100mV_narrowBeam",-1725.,-1300.,"protons")'


root -l -b -q 'calo_test_analisys_T9.c+("beam_18_04_13_18_00_5GeV_mod5_triggerTOF_100mV_narrowBeam2",-1725.,-1300.,"protons")'
root -l -b -q 'calo_test_analisys_T9.c+("beam_18_04_14_04_46_4.5GeV_mod5_triggerTOF_100mV_narrowBeam2",-1700.,-1400.,"protons")'
root -l -b -q 'calo_test_analisys_T9.c+("beam_18_04_14_03_24_4GeV_mod5_triggerTOF_100mV_narrowBeam2",-1700.,-1200.,"protons")'
root -l -b -q 'calo_test_analisys_T9.c+("beam_18_04_14_02_05_3.5GeV_mod5_triggerTOF_100mV_narrowBeam2",-1600.,-1000.,"protons")'
root -l -b -q 'calo_test_analisys_T9.c+("beam_18_04_14_00_35_3GeV_mod5_triggerTOF_100mV_narrowBeam2",-1400.,-800.,"protons")'
root -l -b -q 'calo_test_analisys_T9.c+("beam_18_04_13_22_30_2.5GeV_mod5_triggerTOF_100mV_narrowBeam2",-1200.,-400.,"protons")'
root -l -b -q 'calo_test_analisys_T9.c+("beam_18_04_13_19_05_2GeV_mod5_triggerTOF_100mV_narrowBeam2",-500.,500.,"protons")'


root -l -b -q 'calo_test_analisys_T9.c+("beam_18_04_14_19_50_5GeV_mod5_TOF1_TOF2_scint2_narrowBeam2",-1725.,-1300.,"protons")'
//root -l -b -q 'calo_test_analisys_T9.c+("beam_18_04_14_12_35_5GeV_mod5_TOF1_TOF2_scint2_narrowBeam2",-1725.,-1300.,"protons")'
root -l -b -q 'calo_test_analisys_T9.c+("beam_18_04_14_18_30_4GeV_mod5_TOF1_TOF2_scint2_narrowBeam2",-1700.,-1200.,"protons")'
root -l -b -q 'calo_test_analisys_T9.c+("beam_18_04_14_17_00_3GeV_mod5_TOF1_TOF2_scint2_narrowBeam2",-1400.,-800.,"protons")'
root -l -b -q 'calo_test_analisys_T9.c+("beam_18_04_14_14_49_2GeV_mod5_TOF1_TOF2_scint2_narrowBeam2",-500.,500.,"protons")'






#root -l -q -b calo_test_analisys_T9.c+'("24.11.2017_15_28_mod5_10GeV_f7m_cherenkov_Veto")'
#root -l -q -b 'calo_test_analisys_T9.c+("adc64_17_09_08_13_45_protonScan_mod5_6GeV", 300, 700, "protons")'
#root -l -q -b calo_test_analisys_T9.c+'("24.11.2017_16_51_mod5_6GeV_f7m_cherenkov_Veto")'
#root -l -q -b calo_test_analisys_T9.c+'("24.11.2017_22_35_mod5_pions_6GeV_cherenkov_coincidence")'
#root -l -q -b 'calo_test_analisys_T9.c+("adc64_17_09_08_13_45_protonScan_mod5_6GeV", -300, 200, "pions")'
#root -l -q -b 'calo_test_analisys_T9.c+("adc64_17_09_09_22_39_protonScan_mod5_1.5GeV", 6500, 8000, "protons")'



root -l -q -b 'calo_test_analisys_T9.c+("adc64_17_09_08_13_45_protonScan_mod5_6GeV", 300, 700, "protons")'
root -l -q -b 'calo_test_analisys_T9.c+("adc64_17_09_08_16_22_protonScan_mod5_5.5GeV", 400, 800, "protons")'
root -l -q -b 'calo_test_analisys_T9.c+("adc64_17_09_08_18_11_protonScan_mod5_5GeV", 500, 900, "protons")'
root -l -q -b 'calo_test_analisys_T9.c+("adc64_17_09_08_19_30_protonScan_mod5_4.5GeV", 600, 1000, "protons")'
root -l -q -b 'calo_test_analisys_T9.c+("adc64_17_09_08_20_50_protonScan_mod5_4GeV", 800, 1300, "protons")'
root -l -q -b 'calo_test_analisys_T9.c+("adc64_17_09_08_22_55_protonScan_mod5_3.5GeV", 1100, 1600, "protons")'
root -l -q -b 'calo_test_analisys_T9.c+("adc64_17_09_09_02_20_protonScan_mod5_3.0GeV", 1200, 2200, "protons")'
root -l -q -b 'calo_test_analisys_T9.c+("adc64_17_09_09_17_00_protonScan_mod5_2.5GeV", 2300, 2900, "protons")'
root -l -q -b 'calo_test_analisys_T9.c+("adc64_17_09_09_11_15_protonScan_mod5_2.0GeV", 3500, 4500, "protons")'
#root -l -q -b 'calo_test_analisys_T9.c+("adc64_17_09_12_03_54_protonScan_mod5_1.5GeV", 6500, 8000, "protons")'
root -l -q -b 'calo_test_analisys_T9.c+("adc64_17_09_09_22_39_protonScan_mod5_1.5GeV", 6500, 8000, "protons")'




root -l -q -b calo_test_analisys_T9.c+'("24.11.2017_15_09_mod5_7GeV_f7m_cherenkov_Veto")'
root -l -q -b calo_test_analisys_T9.c+'("24.11.2017_15_28_mod5_10GeV_f7m_cherenkov_Veto")'
root -l -q -b calo_test_analisys_T9.c+'("24.11.2017_15_51_mod5_9GeV_f7m_cherenkov_Veto")'
root -l -q -b calo_test_analisys_T9.c+'("24.11.2017_16_11_mod5_8GeV_f7m_cherenkov_Veto")'
root -l -q -b calo_test_analisys_T9.c+'("24.11.2017_16_30_mod5_7GeV_f7m_cherenkov_Veto")'
root -l -q -b calo_test_analisys_T9.c+'("24.11.2017_16_51_mod5_6GeV_f7m_cherenkov_Veto")'
root -l -q -b calo_test_analisys_T9.c+'("24.11.2017_17_15_mod5_5GeV_f7m_cherenkov_Veto")'
root -l -q -b calo_test_analisys_T9.c+'("24.11.2017_17_37_mod5_4.5GeV_f7m_cherenkov_Veto")'
root -l -q -b calo_test_analisys_T9.c+'("24.11.2017_18_01_mod5_4GeV_f7m_cherenkov_Veto")'
root -l -q -b calo_test_analisys_T9.c+'("24.11.2017_18_25_mod5_3.5GeV_f7m_cherenkov_Veto")'
root -l -q -b calo_test_analisys_T9.c+'("24.11.2017_18_50_mod5_3GeV_f7m_cherenkov_Veto")'
root -l -q -b calo_test_analisys_T9.c+'("24.11.2017_19_13_mod5_2.5GeV_f7m_cherenkov_Veto")'
root -l -q -b calo_test_analisys_T9.c+'("24.11.2017_19_39_mod5_2GeV_f7m_cherenkov_Veto")'

exit 1











root -l -q -b calo_test_analisys_T9.c+'("24.11.2017_16_51_mod5_6GeV_f7m_cherenkov_Veto", 0,0, "no_sel", 0)'
root -l -q -b 'calo_test_analisys_T9.c+("adc64_17_09_08_13_45_protonScan_mod5_6GeV", 300, 700, "protons", 0)'



root -l -q -b calo_test_analisys_T9.c+'("26.11.2017_12_23_mod5_6GeV_protons_HV_minus0.8V")'
root -l -q -b calo_test_analisys_T9.c+'("26.11.2017_12_45_mod5_10GeV_protons_HV_minus0.8V")'
root -l -q -b calo_test_analisys_T9.c+'("26.11.2017_13_35_mod5_9GeV_protons_HV_minus0.8V")'
root -l -q -b calo_test_analisys_T9.c+'("26.11.2017_14_00_mod5_8GeV_protons_HV_minus0.8V")'
root -l -q -b calo_test_analisys_T9.c+'("26.11.2017_14_30_mod5_7GeV_protons_HV_minus0.8V")'
root -l -q -b calo_test_analisys_T9.c+'("26.11.2017_15_00_mod5_5GeV_protons_HV_minus0.8V")'
root -l -q -b calo_test_analisys_T9.c+'("26.11.2017_15_20_mod5_4GeV_protons_HV_minus0.8V")'
root -l -q -b calo_test_analisys_T9.c+'("26.11.2017_15_35_mod5_3GeV_protons_HV_minus0.8V")'
root -l -q -b calo_test_analisys_T9.c+'("26.11.2017_15_54_mod5_2GeV_protons_HV_minus0.8V")'

root -l -q -b calo_test_analisys_T9.c+'("26.11.2017_17_00_mod5_9GeV_neg_pions_HV_minus0.8V")'
root -l -q -b calo_test_analisys_T9.c+'("26.11.2017_17_21_mod5_10GeV_neg_pions_HV_minus0.8V")'
root -l -q -b calo_test_analisys_T9.c+'("26.11.2017_17_45_mod5_8GeV_neg_pions_HV_minus0.8V")'
root -l -q -b calo_test_analisys_T9.c+'("26.11.2017_18_15_mod5_7GeV_neg_pions_HV_minus0.8V")'
root -l -q -b calo_test_analisys_T9.c+'("26.11.2017_18_40_mod5_6GeV_neg_pions_HV_minus0.8V")'
root -l -q -b calo_test_analisys_T9.c+'("26.11.2017_19_00_mod5_5GeV_neg_pions_HV_minus0.8V")'
root -l -q -b calo_test_analisys_T9.c+'("26.11.2017_19_30_mod5_4GeV_neg_pions_HV_minus0.8V")'
root -l -q -b calo_test_analisys_T9.c+'("26.11.2017_19_50_mod5_3GeV_neg_pions_HV_minus0.8V")'
root -l -q -b calo_test_analisys_T9.c+'("26.11.2017_20_10_mod5_2GeV_neg_pions_HV_minus0.8V")'
root -l -q -b calo_test_analisys_T9.c+'("26.11.2017_20_36_mod5_1.5GeV_neg_pions_HV_minus0.8V")'
root -l -q -b calo_test_analisys_T9.c+'("26.11.2017_21_05_mod5_1GeV_neg_pions_HV_minus0.8V")'

root -l -q -b calo_test_analisys_T9.c+'("23.11.2017_19_21_mod5_5GeV_focus_7m", 0,0, "no_sel", 1)'
root -l -q -b calo_test_analisys_T9.c+'("23.11.2017_15_55_mod5_6GeV_focus_7m", 0,0, "no_sel", 1)'
root -l -q -b calo_test_analisys_T9.c+'("23.11.2017_16_52_mod5_7GeV_focus_7m", 0,0, "no_sel", 1)'
root -l -q -b calo_test_analisys_T9.c+'("23.11.2017_17_30_mod5_8GeV_focus_7m", 0,0, "no_sel", 1)'
root -l -q -b calo_test_analisys_T9.c+'("23.11.2017_18_02_mod5_9GeV_focus_7m", 0,0, "no_sel", 1)'
root -l -q -b calo_test_analisys_T9.c+'("23.11.2017_18_39_mod5_10GeV_focus_7m", 0,0, "no_sel", 1)'
root -l -q -b calo_test_analisys_T9.c+'("23.11.2017_20_00_mod5_4.5GeV_focus_7m", 0,0, "no_sel", 1)'
root -l -q -b calo_test_analisys_T9.c+'("23.11.2017_20_33_mod5_4Gev_focus_7m", 0,0, "no_sel", 1)'
root -l -q -b calo_test_analisys_T9.c+'("23.11.2017_21_13_mod5_3.5GeV_focus_7m", 0,0, "no_sel", 1)'
root -l -q -b calo_test_analisys_T9.c+'("23.11.2017_21_42_mod5_3GeV_focus_7m", 0,0, "no_sel", 1)'
root -l -q -b calo_test_analisys_T9.c+'("23.11.2017_22_13_mod5_2.5GeV_focus_7m", 0,0, "no_sel", 1)'


root -l -q -b calo_test_analisys_T9.c+'("24.11.2017_20_29_mod5_pions_2GeV_cherenkov_coincidence")'
root -l -q -b calo_test_analisys_T9.c+'("24.11.2017_20_52_mod5_pions_10GeV_cherenkov_coincidence")'
root -l -q -b calo_test_analisys_T9.c+'("24.11.2017_21_11_mod5_pions_9GeV_cherenkov_coincidence")'
root -l -q -b calo_test_analisys_T9.c+'("24.11.2017_21_35_mod5_pions_8GeV_cherenkov_coincidence")'
root -l -q -b calo_test_analisys_T9.c+'("24.11.2017_22_15_mod5_pions_7GeV_cherenkov_coincidence")'
root -l -q -b calo_test_analisys_T9.c+'("24.11.2017_22_35_mod5_pions_6GeV_cherenkov_coincidence")'
root -l -q -b calo_test_analisys_T9.c+'("24.11.2017_22_51_mod5_pions_2GeV_cherenkov_coincedence")'
root -l -q -b calo_test_analisys_T9.c+'("25.11.2017_08_41_mod5_pions_5GeV_cherenkov_coincidence")'
root -l -q -b calo_test_analisys_T9.c+'("25.11.2017_08_59_mod5_pions_4.5GeV_cherenkov_coincidence")'
root -l -q -b calo_test_analisys_T9.c+'("25.11.2017_09_16_mod5_pions_4GeV_cherenkov_coincidence")'
root -l -q -b calo_test_analisys_T9.c+'("25.11.2017_09_33_mod5_pions_3.5GeV_cherenkov_coincidence")'
root -l -q -b calo_test_analisys_T9.c+'("25.11.2017_09_52_mod5_pions_3GeV_cherenkov_coincidence")'
root -l -q -b calo_test_analisys_T9.c+'("25.11.2017_10_41_mod5_pions_2.5GeV_cherenkov_coincedence")'

root -l -q -b 'calo_test_analisys_T9.c+("adc64_17_09_08_13_45_protonScan_mod5_6GeV", -300, 200, "pions")'
root -l -q -b 'calo_test_analisys_T9.c+("adc64_17_09_08_16_22_protonScan_mod5_5.5GeV", -300, 200, "pions")'
root -l -q -b 'calo_test_analisys_T9.c+("adc64_17_09_08_18_11_protonScan_mod5_5GeV", -300, 200, "pions")'
root -l -q -b 'calo_test_analisys_T9.c+("adc64_17_09_08_19_30_protonScan_mod5_4.5GeV", -300, 200, "pions")'
root -l -q -b 'calo_test_analisys_T9.c+("adc64_17_09_08_20_50_protonScan_mod5_4GeV", -300, 200, "pions")'
root -l -q -b 'calo_test_analisys_T9.c+("adc64_17_09_08_22_55_protonScan_mod5_3.5GeV", -300, 200, "pions")'
root -l -q -b 'calo_test_analisys_T9.c+("adc64_17_09_09_02_20_protonScan_mod5_3.0GeV", -300, 200, "pions")'
root -l -q -b 'calo_test_analisys_T9.c+("adc64_17_09_09_17_00_protonScan_mod5_2.5GeV", -300, 200, "pions")'
root -l -q -b 'calo_test_analisys_T9.c+("adc64_17_09_09_11_15_protonScan_mod5_2.0GeV", -300, 200, "pions")'
root -l -q -b 'calo_test_analisys_T9.c+("adc64_17_09_12_03_54_protonScan_mod5_1.5GeV", -300, 200, "pions")'

root -l -q -b 'calo_test_analisys_T9.c+("adc64_17_09_08_13_45_protonScan_mod5_6GeV", 300, 700, "protons")'
root -l -q -b 'calo_test_analisys_T9.c+("adc64_17_09_08_16_22_protonScan_mod5_5.5GeV", 400, 800, "protons")'
root -l -q -b 'calo_test_analisys_T9.c+("adc64_17_09_08_18_11_protonScan_mod5_5GeV", 500, 900, "protons")'
root -l -q -b 'calo_test_analisys_T9.c+("adc64_17_09_08_19_30_protonScan_mod5_4.5GeV", 600, 1000, "protons")'
root -l -q -b 'calo_test_analisys_T9.c+("adc64_17_09_08_20_50_protonScan_mod5_4GeV", 800, 1300, "protons")'
root -l -q -b 'calo_test_analisys_T9.c+("adc64_17_09_08_22_55_protonScan_mod5_3.5GeV", 1100, 1600, "protons")'
root -l -q -b 'calo_test_analisys_T9.c+("adc64_17_09_09_02_20_protonScan_mod5_3.0GeV", 1200, 2200, "protons")'
root -l -q -b 'calo_test_analisys_T9.c+("adc64_17_09_09_17_00_protonScan_mod5_2.5GeV", 2300, 2900, "protons")'
root -l -q -b 'calo_test_analisys_T9.c+("adc64_17_09_09_11_15_protonScan_mod5_2.0GeV", 3500, 4500, "protons")'
root -l -q -b 'calo_test_analisys_T9.c+("adc64_17_09_12_03_54_protonScan_mod5_1.5GeV", 6500, 8000, "protons")'

root -l -q -b calo_test_analisys_T9.c+'("24.11.2017_15_09_mod5_7GeV_f7m_cherenkov_Veto")'
root -l -q -b calo_test_analisys_T9.c+'("24.11.2017_15_28_mod5_10GeV_f7m_cherenkov_Veto")'
root -l -q -b calo_test_analisys_T9.c+'("24.11.2017_15_51_mod5_9GeV_f7m_cherenkov_Veto")'
root -l -q -b calo_test_analisys_T9.c+'("24.11.2017_16_11_mod5_8GeV_f7m_cherenkov_Veto")'
root -l -q -b calo_test_analisys_T9.c+'("24.11.2017_16_30_mod5_7GeV_f7m_cherenkov_Veto")'
root -l -q -b calo_test_analisys_T9.c+'("24.11.2017_16_51_mod5_6GeV_f7m_cherenkov_Veto")'
root -l -q -b calo_test_analisys_T9.c+'("24.11.2017_17_15_mod5_5GeV_f7m_cherenkov_Veto")'
root -l -q -b calo_test_analisys_T9.c+'("24.11.2017_17_37_mod5_4.5GeV_f7m_cherenkov_Veto")'
root -l -q -b calo_test_analisys_T9.c+'("24.11.2017_18_01_mod5_4GeV_f7m_cherenkov_Veto")'
root -l -q -b calo_test_analisys_T9.c+'("24.11.2017_18_25_mod5_3.5GeV_f7m_cherenkov_Veto")'
root -l -q -b calo_test_analisys_T9.c+'("24.11.2017_18_50_mod5_3GeV_f7m_cherenkov_Veto")'
root -l -q -b calo_test_analisys_T9.c+'("24.11.2017_19_13_mod5_2.5GeV_f7m_cherenkov_Veto")'
root -l -q -b calo_test_analisys_T9.c+'("24.11.2017_19_39_mod5_2GeV_f7m_cherenkov_Veto")'

root -l -q -b calo_test_analisys_T9.c+'("23.11.2017_19_21_mod5_5GeV_focus_7m")'
root -l -q -b calo_test_analisys_T9.c+'("23.11.2017_15_55_mod5_6GeV_focus_7m")'
root -l -q -b calo_test_analisys_T9.c+'("23.11.2017_16_52_mod5_7GeV_focus_7m")'
root -l -q -b calo_test_analisys_T9.c+'("23.11.2017_17_30_mod5_8GeV_focus_7m")'
root -l -q -b calo_test_analisys_T9.c+'("23.11.2017_18_02_mod5_9GeV_focus_7m")'
root -l -q -b calo_test_analisys_T9.c+'("23.11.2017_18_39_mod5_10GeV_focus_7m")'
root -l -q -b calo_test_analisys_T9.c+'("23.11.2017_20_00_mod5_4.5GeV_focus_7m")'
root -l -q -b calo_test_analisys_T9.c+'("23.11.2017_20_33_mod5_4Gev_focus_7m")'
root -l -q -b calo_test_analisys_T9.c+'("23.11.2017_21_13_mod5_3.5GeV_focus_7m")'
root -l -q -b calo_test_analisys_T9.c+'("23.11.2017_21_42_mod5_3GeV_focus_7m")'
root -l -q -b calo_test_analisys_T9.c+'("23.11.2017_22_13_mod5_2.5GeV_focus_7m")'

root -l -q -b calo_test_analisys_T9.c+'("25.11.2017_12_16_mod5_neg_pions_10GeV")'
root -l -q -b calo_test_analisys_T9.c+'("25.11.2017_12_54_mod5_neg_pions_9GeV")'
root -l -q -b calo_test_analisys_T9.c+'("25.11.2017_13_30_mod5_neg_pions_8GeV")'
root -l -q -b calo_test_analisys_T9.c+'("25.11.2017_14_15_mod5_neg_pions_7GeV")'
root -l -q -b calo_test_analisys_T9.c+'("25.11.2017_14_40_mod5_neg_pions_6GeV")'
root -l -q -b calo_test_analisys_T9.c+'("25.11.2017_15_28_mod5_neg_pions_5GeV")'
root -l -q -b calo_test_analisys_T9.c+'("25.11.2017_15_49_mod5_neg_pions_4GeV")'
root -l -q -b calo_test_analisys_T9.c+'("25.11.2017_16_20_mod5_neg_pions_3.5GeV")'
root -l -q -b calo_test_analisys_T9.c+'("25.11.2017_16_40_mod5_neg_pions_3GeV")'
root -l -q -b calo_test_analisys_T9.c+'("25.11.2017_17_05_mod5_neg_pions_2.5GeV")'
root -l -q -b-b calo_test_analisys_T9.c+'("25.11.2017_17_28_mod5_neg_pions_2GeV")'
root -l -q -b calo_test_analisys_T9.c+'("25.11.2017_17_55_mod5_neg_pions_1.5GeV")'
root -l -q -b calo_test_analisys_T9.c+'("25.11.2017_18_25_mod5_neg_pions_1GeV")'

