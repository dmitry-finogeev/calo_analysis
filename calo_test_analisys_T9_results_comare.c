#include <TObjArray.h>
#include <TFile.h>
#include <TChain.h>
#include <TObjString.h>
#include<TH2F.h>
#include<TF1.h>

#include<TROOT.h>
#include<TSystem.h>
#include<TSystemDirectory.h>
#include<TSystemFile.h>
#include<TList.h>
#include<TCollection.h>
#include<TMath.h>
#include<TSpectrum.h>
#include<TDirectory.h>
#include<TStopwatch.h>
#include<TGraph.h>
#include<TBrowser.h>
#include<TCanvas.h>
#include<TPad.h>
#include<TProfile.h>
#include<TStyle.h>
#include "TMarker.h"
#include "TLegend.h"

#include "Readme_reader.h"
#include "event_data_struct.h"
#include "bindataformat.h"
#include "../libraries/utilites/utilites.h"





void calo_test_analisys_T9_results_comare()
{
    TObjArray files_names, files_comments;
    TObjArray hists_names, hists_comments;

    TCanvas *canvas = NULL;
    TH1 *th1_hist_ptr = NULL;
    TH2 *th2_hist_ptr = NULL;
    TF1 *tfunc_ptr = NULL;
    Int_t bin;
    Double_t mean, sigma, mean_err, sigma_err;

    gStyle->SetOptFit(1111);


    /////////////////////////////////////////////////
    Readme_reader *readme_reader_ptr = new Readme_reader("");
    readme_reader_ptr->SetInfoFile("/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_source/T9_test_nov_17/Calibration.txt");
    //readme_reader_ptr->Print();

    const Int_t beam_to_module_N = 5;
    Int_t beam_to_module_tcells = readme_reader_ptr->Get_module_info()[beam_to_module_N-1].total_cells;
    TString beam_to_module_name = readme_reader_ptr->Get_module_info()[beam_to_module_N-1].comment;
    Int_t total_modules = readme_reader_ptr->Get_module_num();
    /////////////////////////////////////////////////


    /////////////////////////////////////////////////
    TString source_path = "/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_results/t9_testbeam_nov2017/";

//    files_names.Add(new TObjString("19_noPMT_ampl/adc64_17_09_08_13_45_protonScan_mod5_6GeVprotons_analysis_1.root"));
//    files_comments.Add(new TObjString("T10 protons {old calc no PMTampl}"));

    files_names.Add(new TObjString("13_TOFampl_sel/24.11.2017_16_51_mod5_6GeV_f7m_cherenkov_Veto_analysis_1.root"));
    files_comments.Add(new TObjString("T9 protons {old calc}"));

    files_names.Add(new TObjString("13_TOFampl_sel/adc64_17_09_08_13_45_protonScan_mod5_6GeVprotons_analysis_1.root"));
    files_comments.Add(new TObjString("T10 protons {old calc}"));

    //    files_names.Add(new TObjString("14_newsigcalc/24.11.2017_16_51_mod5_6GeV_f7m_cherenkov_Veto_analysis_1.root"));
    //    files_comments.Add(new TObjString("T9 protons {new calc}"));

    //    files_names.Add(new TObjString("14_newsigcalc/adc64_17_09_08_13_45_protonScan_mod5_6GeVprotons_analysis_1.root"));
    //    files_comments.Add(new TObjString("T10 protons {new calc}"));

        files_names.Add(new TObjString("16_pu_mod5/24.11.2017_16_51_mod5_6GeV_f7m_cherenkov_Veto_analysis_1.root"));
	files_comments.Add(new TObjString("T9 protons {new calc mod 5 sel}"));

    //    files_names.Add(new TObjString("16_pu_mod5/adc64_17_09_08_13_45_protonScan_mod5_6GeVprotons_analysis_1.root"));
    //    files_comments.Add(new TObjString("T10 protons {new calc mod 5 sel}"));

    //    files_names.Add(new TObjString("15_pu_allcels/24.11.2017_16_51_mod5_6GeV_f7m_cherenkov_Veto_analysis_1.root"));
    //    files_comments.Add(new TObjString("T9 protons {new calc allch sel}"));

	files_names.Add(new TObjString("15_pu_allcels/adc64_17_09_08_13_45_protonScan_mod5_6GeVprotons_analysis_1.root"));
	files_comments.Add(new TObjString("T10 protons {new calc allch sel}"));


    /////////////////////////////////////////////////
    hists_names.Add(new TObjString(  Form("%s_module_energy_first_%i",beam_to_module_name.Data(), beam_to_module_tcells)  ));
    hists_comments.Add(new TObjString("Central module energy"));

    hists_names.Add(new TObjString(  Form("calo_energy_first_%i",beam_to_module_tcells)  ));
    hists_comments.Add(new TObjString("Full calo energy"));

    hists_names.Add(new TObjString(  Form("%s_module_trace",beam_to_module_name.Data())  ));
    hists_comments.Add(new TObjString("Central module trace"));

    //    hists_names.Add(new TObjString(  "Calo_trace" ));
    //    hists_comments.Add(new TObjString("Calo trace"));

    /////////////////////////////////////////////////



    Int_t total_files = files_names.GetLast()+1;
    Int_t total_hists = hists_names.GetLast()+1;
    printf("Total files: %i\nTotal hists %i\n", total_files, total_hists);


    TObjArray files_arr;
    for(Int_t ifile = 0; ifile < total_files; ifile++)
    {
	TString iFile_name = ((TObjString*)(files_names.At(ifile)))->GetString();
	TFile *file = new TFile((source_path + iFile_name).Data(), "READONLY");
	files_arr.Add(file);
    }


    TObjArray canv_arr;
    TCanvas *canv_one =  new TCanvas("canv", "canv");


    for(Int_t ifile = 0; ifile < total_files; ifile++)
    {
	TString iFile_name = ((TObjString*)(files_names.At(ifile)))->GetString();
	TString iFile_comment = ((TObjString*)(files_comments.At(ifile)))->GetString();

	gDirectory->cd("Rint:/");
	canvas = new TCanvas(iFile_comment.Data(), iFile_comment.Data());
	canvas->DivideSquare(total_hists);
	canv_arr.Add(canvas);

	for(Int_t ihist = 0; ihist < total_hists; ihist++)
	{
	    TString iHist_name = ((TObjString*)(hists_names.At(ihist)))->GetString();
	    TString iHist_commennts = ((TObjString*)(hists_comments.At(ihist)))->GetString();

	    ((TFile*)files_arr.At(ifile))->cd();
	    th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny(  iHist_name.Data() )));

	    gDirectory->cd("Rint:/");
	    th1_hist_ptr = (TH1F*)(th1_hist_ptr->Clone());

	    if(th1_hist_ptr == NULL)
	    {
		printf("hist %s not found at file %s\n", iHist_name.Data(), iFile_name.Data());
		continue;
	    }
	    else
	    {
		printf("WORKING: hist %s at file %s\n", iHist_name.Data(), iFile_name.Data());
	    }

	    if(ihist != 2)
	    {
		//FitHistogrammSmartInMaxPeak(th1_hist_ptr, mean, sigma, 1., 0., 300.);
		//		            th1_hist_ptr->SetAxisRange(40, 140);
		//			    Float_t hist_mean = th1_hist_ptr->GetMean();
		//			    tfunc_ptr = new TF1("fitfunc", "gaus", hist_mean-20, hist_mean+20);
		//			    tfunc_ptr->SetLineColor(ifile+1);
		//			    th1_hist_ptr->Fit(tfunc_ptr, "RQ");
		//			    mean = tfunc_ptr->GetParameter(1);
		//			    sigma = tfunc_ptr->GetParameter(2);
		//			    th1_hist_ptr->SetAxisRange(-50, 300);
		//			    th1_hist_ptr->SetTitle(Form("%s res %.1f [%s]", iHist_commennts.Data(), (sigma/mean)*100., iFile_comment.Data()));

	    }


	    TVirtualPad *pad_curr = canvas->cd(ihist+1);
	    //th1_hist_ptr->DrawNormalized(  ((ifile==0)?"":"same")  );
	    th1_hist_ptr->Draw( );


	    if(ihist == 2)
	    {
		th1_hist_ptr->SetTitle(Form("%s [%s]", iHist_commennts.Data(), iFile_comment.Data()));
		th1_hist_ptr->SetLineColor(ifile+1);

		canv_one->cd();
		th1_hist_ptr->DrawNormalized(  ((ifile==0)?"":"same")  );

	    }
	}
    }

    /*
    TCanvas *canv =  new TCanvas("canv", "canv");
    canv->DivideSquare(total_hists);

    for(Int_t ifile = 0; ifile < total_files; ifile++)
    {
	TString iFile_name = ((TObjString*)(files_names.At(ifile)))->GetString();
	TString iFile_comment = ((TObjString*)(files_comments.At(ifile)))->GetString();

	for(Int_t ihist = 0; ihist < total_hists; ihist++)
	{
	    TString iHist_name = ((TObjString*)(hists_names.At(ihist)))->GetString();
	    TString iHist_commennts = ((TObjString*)(hists_comments.At(ihist)))->GetString();

	    ((TFile*)files_arr.At(ifile))->cd();
	    th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny(  iHist_name.Data() )));

	    if(th1_hist_ptr == NULL)
	    {
		printf("hist %s not found at file %s\n", iHist_name.Data(), iFile_name.Data());
		continue;
	    }
	    else
	    {
		printf("WORKING: hist %s at file %s\n", iHist_name.Data(), iFile_name.Data());
	    }

	    th1_hist_ptr->SetTitle(Form("%s [%s]", iHist_commennts.Data(), iFile_comment.Data()));
	    th1_hist_ptr->SetLineColor(ifile+1);

	    th1_hist_ptr->Rebin(6);
	    //	    th1_hist_ptr->SetAxisRange(40, 140);
	    //	    Float_t hist_mean = th1_hist_ptr->GetMean();
	    //	    tfunc_ptr = new TF1("fitfunc", "gaus", hist_mean-20, hist_mean+20);
	    //	    tfunc_ptr->SetLineColor(ifile+1);
	    //	    th1_hist_ptr->Fit(tfunc_ptr, "RQN");
	    //	    mean = tfunc_ptr->GetParameter(1);
	    //	    sigma = tfunc_ptr->GetParameter(2);
	    //	    th1_hist_ptr->SetAxisRange(-50, 300);
	    //	    th1_hist_ptr->SetTitle(Form("%s res %.1f [%s]", iHist_commennts.Data(), (sigma/mean)*100., iFile_comment.Data()));



	    TVirtualPad *pad_curr = canv->cd(ihist+1);
	    //th1_hist_ptr->DrawNormalized(  ((ifile==0)?"":"same")  );
	    th1_hist_ptr->Draw(  ((ifile==0)?"":"same")  );

	    //tfunc_ptr->Draw("same");

	    if(ifile == total_files-1)
	    {
		pad_curr->BuildLegend();
	    }

	}
    }
*/






}
