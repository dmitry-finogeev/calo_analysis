
/*
 *
 runna61
  root -l -b -q 'waveform_catcher.c+("/mnt/disk_data/cosmic_data_2017_quarter4/adc64_17_11_07_16_59_NICA_26_overnight_bin/046f451f_20171107_170111.data", 5000,500,"/mnt/disk_data/cosmic_data_2017_quarter4/adc64_17_11_07_16_59_NICA_26_overnight_bin/wave"'
  root -l -b -q 'waveform_catcher.c+("Calib_mod14_conn5_and_mod20_conn6_LED400/030dbfa9_20180319_163901.data", 10,500,"/mnt/disk_data/LED_calib_1q_2018/root_calib1/"'
  root -l -b -q 'waveform_catcher.c+("/mnt/disk_data/LED_calib_1q_2018/Calib_mod14_conn5_and_mod20_conn6_LED400/030dbfa9_20180319_163901.data", 10,500,"/mnt/disk_data/LED_calib_1q_2018/root_calib1/waves_400"'
  root -l -b -q 'waveform_catcher.c+("/mnt/disk_data/LED_calib_1q_2018/calib_8/LED_600/030dbfa9_20180322_152744.data", 10,500,"/mnt/disk_data/LED_calib_1q_2018/root_calib1/waves_600"'
  root -l -b -q 'waveform_catcher.c+("/mnt/disk_data/cosmic_data_2018_quater1/adc64_18_03_26_22_30_INR_5mod_TRSH200_ZS150_MAF8_BLC50_8M/030dbfa9_20180326_223421.data", 10,500,"/mnt/disk_data/cosmic_data_2018_quater1/RESULT/8M"'

 laptop
  root -l -b -q waveform_catcher.c+'("/mnt/hgfs/DETECTORS_DATA/SOURCE/CALO/t9_testbeam_nov2017/24.11.2017_16_51_mod5_6GeV_f7m_cherenkov_Veto/0611cef9_20171124_165142.data", 1,1000,"/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_results/other/waves_T9_6GeV")'

 lxplus
  root -l -b -q 'waveform_catcher.c+("/eos/user/m/morozovs/testbeam_T9_Nov2017/24.11.2017_20_52_mod5_pions_10GeV_cherenkov_coincidence/0611cef9_20171124_205216.data", 1000,1500,"/eos/user/d/dfinogee/RESULTS/CALO/root_results/other/T9_10gevc_pions"' *

 root -l -b -q waveform_catcher.c+'("/mnt/hgfs/DETECTORS_DATA/SOURCE/CALO/JINR_03_2018/04.03.2018_10_30_mod_Dubna_test_noZS/07a8de9a_20180304_102459.data", 1,10,"/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_results/other/test")'

 root -l -b -q waveform_catcher.c+'("/home/runna61/adc64_data_2018_quater1/beam_18_04_11_6GeV_mod5/", 1,10,"/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_results/other/test")'

root -l -b -q waveform_catcher.c+'("/home/runna61/adc64_data_2018_quater1/beam_18_04_13_18_00_5GeV_mod5_triggerTOF_100mV_narrowBeam2/030dbfa9_20180413_180027.data", 1718,1721,"/home/runna61/adc64_data_2018_quater1/beam_results/wawes/wave",31,10)'
root -l -b -q waveform_catcher.c+'("/home/runna61/testbeam_T9_Nov2017/24.11.2017_17_15_mod5_5GeV_f7m_cherenkov_Veto/0611cef9_20171124_171553.data", 1,1000,"/home/runna61/adc64_data_2018_quater1/beam_results/wawes/wave_T9_31",31,0)'
root -l -b -q waveform_catcher.c+'("/home/runna61/testbeam_T9_Nov2017/24.11.2017_17_15_mod5_5GeV_f7m_cherenkov_Veto/07a8de9a_20171124_171553.data", 10000,11000,"/home/runna61/adc64_data_2018_quater1/beam_results/wawes/wave_T9__f1_39",39,0)'
root -l -b -q waveform_catcher.c+'("/home/runna61/adc64_data_2018_quater1/beam_18_04_13_03_15_5GeV_mod1/0611cef9_20180413_032412.data", 10000,11000,"/home/runna61/adc64_data_2018_quater1/beam_results/wawes/wave_T10clb_12",12,0)'
root -l -b -q waveform_catcher.c+'("/home/runna61/adc64_data_2018_quater1/beam_NA61_19GeV_ZS100_27_04_2018/030dbfa9_20180427_220930.data", 0,500,"/home/runna61/adc64_data_2018_quater1/beam_results/wawes/wave_NA61_31",31,1000)'
root -l -b -q waveform_catcher.c+'("/mnt/disk_na61pc008/runna61/adc64_data_2018_quater1/beam_NA61_18_04_29_15_50_80GeV_mod3_noZS/030dbfa9_20180429_154955.data", 0,500,"/mnt/disk_na61pc008/runna61/adc64_data_2018_quater1/beam_results/wawes/wave_NA61_mod3_31",31,1000)'
root -l -b -q waveform_catcher.c+'("/mnt/disk_data/cosmic_data_2018_quater2/adc64_18_06_08_17_12_INR_6mod_CBM_TH200_ZS150/0611cef9_20180608_171034.data", 0,500,"/mnt/disk_data/cosmic_data_2018_quater2/RESULT/cosmic_cbm_31",31,1000)'
root -l -b -q waveform_catcher.c+'("/mnt/disk_data/cosmic_data_2018_quater2/adc64_18_06_04_17_20_INR_6mod_CBM_TH200_ZS150/0611cef9_20180604_172208.data", 0,500,"/mnt/disk_data/cosmic_data_2018_quater2/RESULT/cosmic_cbm_42",42,1000)'

*/


#include <TFile.h>
#include <TObjArray.h>
#include <TGraph.h>
#include <TBrowser.h>
#include <TCanvas.h>
#include <TPad.h>
#include <TObject.h>

#include "event_data_struct.h"

#include "Readme_reader.h"
#include "event_data_struct.h"
#include "bindataformat.h"
//#include "utilites.h"
#include "../libraries/utilites/utilites.h"

void waveform_catcher(TString filename = "", Int_t firsevent = 1, Int_t nevents = 10, TString resultfilename = "waves", Int_t ch_to_show = 31, Float_t charge_trsh = 100.){

    Int_t gate_beg = 70;
    Int_t gate_end = 120;
    const Int_t n_mod_ev_draw = 5;



    const Int_t beam_to_module_N = 5;
    const Float_t RMS_zero_level_trsh = 50.;
    const Float_t pileup_trhd = 150.;



    //mod5 cell 0 = ch 32 in map = 31 in file


    if(filename == "") return;




    BinDataReader reader;
    reader.SetFile(filename.Data());

    //    reader.ReadEvent(0);
    //    return;

    Int_t total_events = reader.events_total;
    printf("Total events: %i\n", reader.events_total);

    TFile *result_file = new TFile((resultfilename+".root").Data(), "recreate");


    Readme_reader *readme_reader_ptr = new Readme_reader("");
    //readme_reader_ptr->SetInfoFile("/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_source/T9_test_nov_17/Calibration.txt");
    //readme_reader_ptr->SetInfoFile("/home/runna61/adc64_data_2018_quater1/beam_root/Calibration.txt");
    readme_reader_ptr->SetInfoFile("/mnt/disk_na61pc008/runna61/adc64_data_2018_quater1/beam_root/Calibration.txt");

    event_data_struct *event_struct = new event_data_struct[reader.channel_total];

    TObjArray *events_array = new TObjArray();
    events_array->SetName("EventsArr");
    events_array->SetOwner();

    TObjArray gr_canv_arr;

    TObjArray *mod_canv_arr = new TObjArray();
    mod_canv_arr->SetName("mod_canv_arr");
    mod_canv_arr->Expand(60*n_mod_ev_draw);


    Int_t total_samples = 200;
    Double_t *X_points, *Y_points;
    X_points = new Double_t[total_samples];
    Y_points = new Double_t[total_samples];
    for(Int_t p=0;p<total_samples;p++)X_points[p] = (double_t)p;


    int mod_canv_ev_iter = 0;
    int events_process = 0;
    int event = 0;
    for(event = firsevent; event < nevents; event++)
    {
	printf("\n\n=====================\n");
	printf("reading event %i\n", event);
	reader.ReadEvent(event);

//	for(Int_t ch = 0; ch < reader.channel_total; ch++)
//	    reader.Calculate_waveform(event_struct[ch], ch, gate_beg, gate_end);

	printf("\nEvent: %i ch %i\n", event, ch_to_show);
	reader.Calculate_waveform(event_struct[ch_to_show], ch_to_show, gate_beg, gate_end);

	if(event_struct[ch_to_show].integral_in_gate < charge_trsh) continue;


	printf("\nEvent: %i ch %i\n", event, ch_to_show);
	printf("read event num %i\n",reader.Event_Num);
	printf("Amplitude %f\n", (event_struct[ch_to_show].MAX_in_gate - event_struct[ch_to_show].zero_level) );
	event_struct[ch_to_show].Print();

	/***/
	printf("\n[");
	for(Int_t p=0;p<total_samples;p++)
	    printf("%i, ", reader.samples_data[ ch_to_show ][p]);
	printf("]\n");

	printf("\n------------\n");
	/***/

	/***
	// Pile-up selection ================================================
	Bool_t is_ev_pileup_good_beammod = 1, is_ev_pileup_good_allcells = 1, is_ch_to_show_no_puleup = 0;
	for(Int_t module_iter = 0; module_iter < 6; module_iter++)
	{
	    Int_t total_cells = readme_reader_ptr->Get_module_info()[module_iter].total_cells;

	    for(Int_t cell_iter = 0; cell_iter < total_cells; cell_iter++)
	    {
		//Int_t tree_ch = readme_reader_ptr->GetContCh_from_CellN(module_iter, cell_iter);
		Int_t tree_ch, board_iter;
		readme_reader_ptr->GetBoardCh_from_CellN(module_iter, cell_iter, board_iter, tree_ch);

		Bool_t is_no_pileup =
			(event_struct[tree_ch].zero_level_RMS < 30) &&
			((event_struct[tree_ch].MAX_out_gate - event_struct[tree_ch].zero_level) < pileup_trhd);
		//		        ((ch_ampl_ratio > .04) || (Amplitude > 1000.));

		if(tree_ch == ch_to_show)
		    is_ch_to_show_no_puleup = is_no_pileup;

		if(!is_no_pileup)
		{
		    printf("event %i; board ch%i; is puleup\n", event, tree_ch);
		}

		is_ev_pileup_good_allcells = is_ev_pileup_good_allcells && is_no_pileup;
		if(module_iter == beam_to_module_N-1) is_ev_pileup_good_beammod = is_ev_pileup_good_beammod && is_no_pileup;
	    }
	}
//	if(is_ev_pileup_good_allcells) continue;
//	if(!is_ev_pileup_good_beammod) continue;
//	if(is_ch_to_show_no_puleup) continue;
	// ==========================================================================
	/***/


	Bool_t ch_to_show_pu = event_struct[ch_to_show].is_pile_up;
	ch_to_show_pu = ch_to_show_pu || (event_struct[ch_to_show].zero_level_RMS > 30.);
	//if(event_struct[ch_to_show].MAX_in_gate - event_struct[ch_to_show].zero_level < 100.)ch_to_show_pu = false;
	if(event_struct[ch_to_show].integral_in_gate < 1500.) ch_to_show_pu = false;



	//if(event_struct[ch_to_show].is_pile_up) continue;


	/***/
	TObjArray *Graph_array = new TObjArray();
	Graph_array->SetName(Form("event_%i", event));
	Graph_array->SetOwner();
	events_array->Add(Graph_array);


	for(Int_t ch = 0; ch < reader.channel_total; ch++)
	{
	    Int_t con_ch = readme_reader_ptr->GetContCh_from_BoardCh(ch, 0);
	    Int_t module_iter, module_cell;
	    readme_reader_ptr->GetCellN_from_ContCh(con_ch, module_iter, module_cell);

	    //	for(Int_t p=0;p<total_samples;p++)
	    //	    X_points[p] = (double_t)reader.samples_data[ch][p];
	    for(Int_t p=0;p<total_samples;p++)
		Y_points[p] = (double_t)reader.samples_data[ ch ][p];

	    TGraph *ch_graph = new TGraph((Int_t)total_samples, X_points, Y_points);
	    ch_graph->SetName(Form("event_%i_ch_%i", event,ch));
	    ch_graph->SetTitle( Form("event_%i ch_%i", event, ch) );
	    ch_graph->SetLineWidth(.1);
	    ch_graph->SetMarkerSize(0.3);
	    ch_graph->SetMarkerStyle(1);
	    if(ch_to_show_pu)
	    {
		ch_graph->SetLineColor(kRed);
		ch_graph->SetMarkerColor(kRed);
	    }





	    Graph_array->Add(ch_graph);




	    if(ch == ch_to_show)
	    {
		//create fit function
		// set fit parameters
		// ch_graph->Fit()
		// delete fit function
		gr_canv_arr.Add(ch_graph);

	    }

	    if(mod_canv_ev_iter < n_mod_ev_draw)
	    {
		TString channel_name = Form("%s_bch%i_ev_%i",
		                            readme_reader_ptr->GetChPrefix(module_iter,module_cell).Data(),
		                            ch, event);
		TGraph *ch_graph_mod = (TGraph*)( ch_graph->Clone(channel_name.Data() ) );
		ch_graph_mod->SetTitle(channel_name.Data());
		mod_canv_arr->AddAt(ch_graph_mod, con_ch + mod_canv_ev_iter*60);

	    }

	}

	/***/
	mod_canv_ev_iter++;
	events_process++;
	if(events_process == nevents) break;



    }

//    for(Int_t i = 0; i <= mod_canv_arr->GetLast(); i++)
//    {
//	printf("%i) %s\n", i, (mod_canv_arr->At(i)->GetName()) );
//    }


    TObjArray *canv_arr_mod = HDraw(mod_canv_arr,"waves_mods", 5,4,1,0,10000);
    if(canv_arr_mod)
    {
	canv_arr_mod->SetName("Canvas_mods");
	events_array->Add(canv_arr_mod);

	for(Int_t i = 0; i <= canv_arr_mod->GetLast(); i++)
	{
	    ((TCanvas*)canv_arr_mod->At(i))->SaveAs(Form("%s_mod.pdf(",   (resultfilename).Data() ));
	}
	((TCanvas*)canv_arr_mod->At(canv_arr_mod->GetLast()))->SaveAs(Form("%s_mod.pdf)",   (resultfilename).Data() ));

    }

    TObjArray *canv_arr = HDraw(&gr_canv_arr,Form("canvas_ch_%i", ch_to_show), 10,10,1,0,10000);
    if(canv_arr)
    {
	canv_arr->SetName("Canvas");
	events_array->Add(canv_arr);

	for(Int_t i = 0; i <= canv_arr->GetLast(); i++)
	{
	    ((TCanvas*)canv_arr->At(i))->SaveAs(Form("%s.pdf(",   (resultfilename).Data() ));
	}
	((TCanvas*)canv_arr->At(canv_arr->GetLast()))->SaveAs(Form("%s.pdf)",   (resultfilename).Data() ));
    }



    SaveArrayStructInFile(events_array, result_file);

    new TBrowser;

}
