#ifndef README_READER_H
#define README_READER_H

#include <iostream>
#include <fstream>
#include <ctime>
#include <Rtypes.h>
#include <string.h>

#include <TFile.h>
#include<TString.h>
#include<TMath.h>
#include<TTree.h>
#include<TObject.h>

#define verbose_dbg 0

struct adc_info {
    TString board_name;
    TString data_file;
    Int_t board_id;

    TString comment;

    int get_hex_name();

    void Clear();
    void Print();

    Int_t Read(FILE *file, Int_t form_ver);
};

void adc_info::Clear()
{
    board_name = "";
    data_file = "";
    board_id = 0;

    comment = "";
}

void adc_info::Print()
{
    printf("\nboard: \"%s\" [hex: %x]; id: %i;\n", board_name.Data(), get_hex_name(), board_id);
    printf("file: %s\n", data_file.Data());

    printf(" %s\n\n", comment.Data());
}

int adc_info::get_hex_name()
{
    if(verbose_dbg) printf("%s\n", board_name.Data());
//    return std::stoi (board_name.Data(),nullptr,16);
    int i;
    sscanf(board_name.Data(), "%x", &i);
    return i;
}

Int_t adc_info::Read(FILE *file, Int_t form_ver)
{
    char buffer[1000]; int read; char *result;

    read = fscanf(file, "%s", buffer);
    if( !read || (read == EOF) || !strlen(buffer)){ Clear(); return -1;}
    if(buffer[0] == '#') {result = fgets(buffer, 1000, file); read = fscanf(file, "%s", buffer);  }
    if(verbose_dbg) printf("a %s\n", buffer);
    board_id = atoi(buffer);

    read = fscanf(file, "%s", buffer);
    if( !read || (read == EOF) || !strlen(buffer)){ Clear(); return -1;}
    if(buffer[0] == '#') {result = fgets(buffer, 1000, file); read = fscanf(file, "%s", buffer);  }
    if(verbose_dbg) printf("a %s\n", buffer);
    board_name = buffer;

    read = fscanf(file, "%s", buffer);
    if( !read || (read == EOF) || !strlen(buffer)){ Clear(); return -1;}
    if(buffer[0] == '#') {result = fgets(buffer, 1000, file); read = fscanf(file, "%s", buffer);  }
    if(verbose_dbg) printf("a %s\n", buffer);
    data_file = buffer;

    read = fscanf(file, "%s", buffer);
    if( !read || (read == EOF) || !strlen(buffer)){ Clear(); return -1;}
    if(buffer[0] == '#') {result = fgets(buffer, 1000, file); read = fscanf(file, "%s", buffer);  }
    if(verbose_dbg) printf("a %s\n", buffer);
    comment = buffer;

    return 1;
}

struct module_info {
    TString name;
    Int_t board_id;

    TString fee_name;
    Int_t sgate_min;
    Int_t sgate_max;
    Float_t ampl_scale;

    Int_t X_mod_pos;
    Int_t Y_mod_pos;
    Int_t slowcntr_num;
    Int_t total_cells;
    Int_t *cell_ch;

    TString comment;

    module_info(){cell_ch = NULL; Clear();}
    ~module_info(){if(cell_ch) delete[] cell_ch;}
    void Clear();
    void Print();

    Int_t Read(FILE *file, Int_t form_ver);
};

void module_info::Clear()
{

    name = "";
    board_id = 0;

    fee_name = "";
    sgate_min = 0;
    sgate_max = 0;
    ampl_scale = 0.;

    X_mod_pos = 0;
    Y_mod_pos = 0;
    slowcntr_num = 0;
    total_cells = 0;
    if(cell_ch) delete[] cell_ch; cell_ch = NULL;

    comment = "";
}

void module_info::Print()
{
    printf("%s ==============\n", name.Data());
    printf("ADC board id: %i;\n", board_id);
    printf("FEE: %s\n", fee_name.Data());
    printf("Signal gate: [%i, %i]\n", sgate_min, sgate_max);
    printf("Amplitude scale: %.3f\n", ampl_scale);
    printf("position: (%i,%i)\n", X_mod_pos, Y_mod_pos);
    printf("slow control id: %i\n", slowcntr_num);
    printf("total cell: %i\n", total_cells);

    printf("ch map: ");
    if(cell_ch)
    {
	for(Int_t ch = 0; ch < total_cells; ch++) printf("%i ", cell_ch[ch]);
	printf("\n");
    }
    else printf("no\n");
    printf(" %s\n\n", comment.Data());

}

Int_t module_info::Read(FILE *file, Int_t form_ver)
{
    char buffer[1000]; int read; char *result;

    read = fscanf(file, "%s", buffer);
    if( !read || (read == EOF) || !strlen(buffer)){ Clear(); return -1;}
    if(buffer[0] == '#') {result = fgets(buffer, 1000, file); read = fscanf(file, "%s", buffer);  }
    name = buffer;

    read = fscanf(file, "%s", buffer);
    if( !read || (read == EOF) || !strlen(buffer)){ Clear(); return -1;}
    if(buffer[0] == '#') {result = fgets(buffer, 1000, file); read = fscanf(file, "%s", buffer);  }
    board_id = atoi(buffer);

    read = fscanf(file, "%s", buffer);
    if( !read || (read == EOF) || !strlen(buffer)){ Clear(); return -1;}
    if(buffer[0] == '#') {result = fgets(buffer, 1000, file); read = fscanf(file, "%s", buffer);  }
    fee_name = buffer;

    if(form_ver == 1)
    {
	sgate_min = 40;
	sgate_max = 130;
	ampl_scale = 1.0;
    }else if (form_ver == 2) {

	read = fscanf(file, "%s", buffer);
	if( !read || (read == EOF) || !strlen(buffer)){ Clear(); return -1;}
	if(buffer[0] == '#') {result = fgets(buffer, 1000, file); read = fscanf(file, "%s", buffer);  }
	sgate_min = atoi(buffer);

	read = fscanf(file, "%s", buffer);
	if( !read || (read == EOF) || !strlen(buffer)){ Clear(); return -1;}
	if(buffer[0] == '#') {result = fgets(buffer, 1000, file); read = fscanf(file, "%s", buffer);  }
	sgate_max = atoi(buffer);

	read = fscanf(file, "%s", buffer);
	if( !read || (read == EOF) || !strlen(buffer)){ Clear(); return -1;}
	if(buffer[0] == '#') {result = fgets(buffer, 1000, file); read = fscanf(file, "%s", buffer);  }
	ampl_scale = atof(buffer);

    }


    read = fscanf(file, "%s", buffer);
    if( !read || (read == EOF) || !strlen(buffer)){ Clear(); return -1;}
    if(buffer[0] == '#') {result = fgets(buffer, 1000, file); read = fscanf(file, "%s", buffer);  }
    X_mod_pos = atoi(buffer);

    read = fscanf(file, "%s", buffer);
    if( !read || (read == EOF) || !strlen(buffer)){ Clear(); return -1;}
    if(buffer[0] == '#') {result = fgets(buffer, 1000, file); read = fscanf(file, "%s", buffer);  }
    Y_mod_pos = atoi(buffer);

    read = fscanf(file, "%s", buffer);
    if( !read || (read == EOF) || !strlen(buffer)){ Clear(); return -1;}
    if(buffer[0] == '#') {result = fgets(buffer, 1000, file); read = fscanf(file, "%s", buffer);  }
    slowcntr_num = atoi(buffer);

    read = fscanf(file, "%s", buffer);
    if( !read || (read == EOF) || !strlen(buffer)){ Clear(); return -1;}
    if(buffer[0] == '#') {result = fgets(buffer, 1000, file); read = fscanf(file, "%s", buffer);  }
    total_cells = atoi(buffer);


    cell_ch = new Int_t[total_cells];
    for(Int_t cell = 0; cell < total_cells; cell++)
    {
	read = fscanf(file, "%s", buffer);
	if( !read || (read == EOF) || !strlen(buffer)){ Clear(); return -1;}
	if(buffer[0] == '#') {result = fgets(buffer, 1000, file); read = fscanf(file, "%s", buffer);  }
	cell_ch[cell] = atoi(buffer);
    }

    read = fscanf(file, "%s", buffer);
    if( !read || (read == EOF) || !strlen(buffer)){ Clear(); return -1;}
    if(buffer[0] == '#') {result = fgets(buffer, 1000, file); read = fscanf(file, "%s", buffer);  }
    comment = buffer;

    return 1;
}



class Readme_reader
{
public:
    Readme_reader(TString runs_folder_path_);
    ~Readme_reader(){Clear();}


    Int_t SetInfoFile(TString info_file_name_){info_file_name = info_file_name_; return OpenInfoFile();}
    void SetRunName(TString run_name_folder_){run_name_folder = run_name_folder_;}
    Int_t Set_runfolder_name(TString Run_name_folder_);

    TString Get_run_name(){return run_name_folder;}
    TString Get_run_comment(){return run_comment;}
    TString Get_info_file_path(){return info_file_name;}
    TString Get_converted_file_name(){return runs_folder_path + run_name_folder + "/" + run_name_folder + ".root";}

    module_info* Get_module_info(){return module_info_arr;}
    adc_info* Get_adc_info(){return adc_info_arr;}
    Int_t Get_module_num(){return total_modules;}
    Int_t Get_boards_num(){return total_boards;}
    TString Get_file_name(Int_t board_iter){return (runs_folder_path + run_name_folder + "/" + adc_info_arr[board_iter].data_file);}
    Int_t Get_boardid(Int_t board_iter){return adc_info_arr[board_iter].board_id;}
    Int_t Get_boarditer(Int_t board_id);
    Int_t Get_total_ch();

    void GetBoardCh_from_ContCh(Int_t cont_ch, Int_t &board_iter, Int_t &board_ch); //continious ch = ch in root file
    void GetBoardCh_from_CellN(Int_t module_iter, Int_t module_cell, Int_t &board_iter, Int_t &board_ch); //continious ch = ch in root file

    void GetCellN_from_ContCh(Int_t cont_ch, Int_t &module_iter, Int_t &module_cell); //continious ch = ch in root file

    Int_t GetContCh_from_CellN(Int_t module_iter, Int_t module_cell);
    Int_t GetContCh_from_BoardCh(Int_t board_ch, Int_t board_iter);

    TString GetChPrefix(Int_t module_iter, Int_t module_cell){
	return run_name_folder + "_" + module_info_arr[module_iter].name + Form("_cell%i", module_cell);
    }

    void Print();


    static const int max_line_len;
    static const char *readme_file_mask;
    static const char *tree_name;
    adc_info *adc_info_arr;

private:

    //service function
    Int_t OpenInfoFile();
    void Clear();

    // private data
    TString runs_folder_path; //main folder with runs (each run in own folder)
    TString run_name_folder; // own folder for run
    TString info_file_name;

    // config info
    Int_t format_version;
    TString run_name;
    TString run_comment;
    Int_t total_modules;
    Int_t total_boards;
    module_info *module_info_arr;


    Int_t *cont_ch_map;
    Int_t *cont_board_map;

};

const int Readme_reader::max_line_len = 1000;
const char* Readme_reader::readme_file_mask = "run_info.txt";
const char* Readme_reader::tree_name = "adc64_data";


// #####################################################################
Readme_reader::Readme_reader(TString runs_folder_path_)
    : runs_folder_path(runs_folder_path_), run_name_folder("void"), info_file_name("void"), module_info_arr(NULL), adc_info_arr(NULL), cont_ch_map(NULL), cont_board_map(NULL)
{
    Clear();
}
// #####################################################################

// #####################################################################
void Readme_reader::Clear()
{
    format_version = 0;
    run_name.Clear();
    run_comment.Clear();
    total_modules = 0;
    total_boards = 0;
    if(module_info_arr) delete[] module_info_arr; module_info_arr = NULL;
    if(adc_info_arr) delete[] adc_info_arr; adc_info_arr = NULL;
    if(cont_board_map) delete[] cont_board_map; cont_board_map = NULL;
    if(cont_ch_map) delete[] cont_ch_map; cont_ch_map = NULL;
}
// #####################################################################

// #####################################################################
Int_t Readme_reader::Get_total_ch()
{
    Int_t tot_ch = 0;

    if(module_info_arr)
    {
	for(Int_t module = 0; module < total_modules; module++)
	    tot_ch += module_info_arr[module].total_cells;
    }

    return tot_ch;
}
// #####################################################################

// #####################################################################
Int_t Readme_reader::Get_boarditer(Int_t board_id)
{
    if(adc_info_arr)
    {
	for(Int_t board = 0; board < total_boards; board++)
	    if(adc_info_arr[board].board_id == board_id)
		return board;
    }
}
// #####################################################################

// #####################################################################
inline void Readme_reader::GetCellN_from_ContCh(Int_t cont_ch, Int_t &module_iter, Int_t &module_cell)
{
    if(cont_board_map && cont_ch_map && module_info_arr)
    {
	Int_t cont_iter = 0;
	for(module_iter = 0; module_iter < Get_module_num(); module_iter++)
	    for(module_cell = 0; module_cell < module_info_arr[module_iter].total_cells; module_cell++)
	    {
		if(cont_ch == cont_iter) return;
		cont_iter++;
	    }
    }else
    {
	module_iter = module_cell = -1;
	return;
    }
}
// #####################################################################

// #####################################################################
inline void Readme_reader::GetBoardCh_from_ContCh(Int_t cont_ch, Int_t &board_iter, Int_t &board_ch)
{
    if(cont_board_map && cont_ch_map)
    {
    board_iter = cont_board_map[cont_ch];
    board_ch = cont_ch_map[cont_ch];
    }
    return;
}
// #####################################################################

// #####################################################################
inline void Readme_reader::GetBoardCh_from_CellN(Int_t module_iter, Int_t module_cell, Int_t &board_iter, Int_t &board_ch)
{
    Int_t cont_ch = GetContCh_from_CellN(module_iter, module_cell);
    if(cont_board_map && cont_ch_map)
    {
    board_iter = cont_board_map[cont_ch];
    board_ch = cont_ch_map[cont_ch];
    }
    return;
}
// #####################################################################

// #####################################################################
Int_t Readme_reader::GetContCh_from_CellN(Int_t module_iter, Int_t module_cell)
{
    Int_t cont_ch = 0;

    if(cont_board_map && cont_ch_map && module_info_arr)
    {
	for(Int_t module = 0; module < module_iter; module++)
	    cont_ch += module_info_arr[module].total_cells;
    }else return -1;

    cont_ch += module_cell;

    return cont_ch;
}
// #####################################################################

// #####################################################################
Int_t Readme_reader::GetContCh_from_BoardCh(Int_t board_ch, Int_t board_iter)
{
    if(cont_board_map && cont_ch_map)
    {
	for(Int_t cont_ch = 0; cont_ch < Get_total_ch(); cont_ch++)
	{
	    if(board_ch == cont_ch_map[cont_ch])
		if(board_iter == cont_board_map[cont_ch])
		    return cont_ch;
	}
    }

    return -1;
}
// #####################################################################


// #####################################################################
void Readme_reader::Print()
{
    printf("run_info file format version: %i\n", format_version);
    printf("\nInfo file: %s%s/%s\n", runs_folder_path.Data(), run_name_folder.Data(), readme_file_mask);
    printf("\nInfo file name: %s\n", info_file_name.Data());
    printf("Name: %s\n", run_name.Data());
    printf("Comment: %s\n", run_comment.Data());
    printf("Boards: %i\n", total_boards);
    printf("Modules: %i\n", total_modules);
    if(adc_info_arr)
    {
	for(Int_t adc = 0; adc < total_boards; adc++)
	{
	    adc_info_arr[adc].Print();
	}
    }
    if(module_info_arr)
    {
	for(Int_t module = 0; module < total_modules; module++)
	{
	    module_info_arr[module].Print();

	}
    }
    printf("Total Channels %i :\n", Get_total_ch());

}
// #####################################################################

// #####################################################################
Int_t Readme_reader::OpenInfoFile()
{

    FILE *info_file = fopen(info_file_name.Data(),"r");
    if(info_file == NULL){
	printf("Readme_reader: Info file \"%s\" was NOT opened\n", info_file_name.Data());
	return -1;
    }

    Clear();


    format_version = 1;

    Int_t read_step = 0, module_readed = 0, adc_readed = 0;
    while(!feof(info_file)) {
	char buffer[max_line_len]; int read; char *result;

	// read a word from the files and skip comments
	read = fscanf(info_file, "%s", buffer);
	if( !read || (read == EOF) || !strlen(buffer)) continue;
	if(buffer[0] == '#') {result = fgets(buffer, 1000, info_file); continue; }

	if(strstr(buffer, "[RUNNAME]")) {read_step = 1; continue;}
	if(strstr(buffer, "[DATACOMMENTS]")) {read_step = 2; continue;}
	if(strstr(buffer, "[ADCSNUM]")) {read_step = 3; continue;}
	if(strstr(buffer, "[ADCSCONF]")) {read_step = 4; continue;}
	if(strstr(buffer, "[MODULESNUM]")) {read_step = 5; continue;}
	if(strstr(buffer, "[MODULESCONF]")) {read_step = 6; continue;}
	if(strstr(buffer, "[FORMATVER]")) {read_step = 7; continue;}

	if(read_step == 1){ run_name += buffer; } //collect name by words
	if(read_step == 2){ run_comment += buffer;} //collect comment by words
	if(read_step == 3){ total_boards = atoi(buffer); adc_info_arr = new adc_info[total_boards]; read_step = 0;}
	if(read_step == 4)
	{
	    if(buffer[0] == '[')
	    {
		Int_t current_adc = atoi(buffer);

		//printf("adc %i\n", current_adc);
		adc_info_arr[adc_readed].Read(info_file, format_version);

		adc_readed++;
	    }
	    if(adc_readed == total_boards) read_step = 0;
	}
	if(read_step == 5){ total_modules = atoi(buffer); module_info_arr = new module_info[total_modules]; read_step = 0;}
	if(read_step == 6)
	{
	    if(buffer[0] == '[')
	    {
		Int_t current_module = atoi(buffer);

		//printf("b %i\n", current_module);
		module_info_arr[module_readed++].Read(info_file, format_version);
	    }
	    if(module_readed == total_modules) read_step = 0;
	}
	if(read_step == 7){ format_version = atoi(buffer); continue;}



	//printf("%s\n", buffer);

    } //while()

    fclose (info_file);


    //construct continious channel map
    cont_ch_map = new Int_t[Get_total_ch()];
    cont_board_map = new Int_t[Get_total_ch()];
    Int_t cont_ch_iter = 0;
    for(Int_t mod=0; mod < Get_module_num(); mod++)
	for(Int_t ch=0; ch<module_info_arr[mod].total_cells;ch++)
	{
	    cont_ch_map[cont_ch_iter] = module_info_arr[mod].cell_ch[ch];
	    cont_board_map[cont_ch_iter] = Get_boarditer( module_info_arr[mod].board_id );
	    cont_ch_iter++;
	}


    return 1;

}

// #####################################################################

// #####################################################################
Int_t Readme_reader::Set_runfolder_name(TString Run_name_folder_)
{
    run_name_folder = Run_name_folder_;
    info_file_name = Form("%s%s/%s", runs_folder_path.Data(), run_name_folder.Data(), readme_file_mask);

    return OpenInfoFile();
}
// #####################################################################


#endif // README_READER_H
