#ifndef READER_H
#define READER_H

#include "types.h"
#include <QFile>
#include <QDebug>

class Reader
{
public:
    Reader();

    void read(QString fn);

private:
    bool findSync();
    void printWord();
    void printWord(QString comment);
    void printRange(int size);
    void reReadBlock();
    void move(qint64 size);
    void adcPayload(qint64 size);

    QString fileName;
    QFile dataFile;
    qint64 fileSize;
    QByteArray dataBlock;

    MpdEventHdr *eventHdr;
    MpdDeviceHdr *deviceHdr;
    MpdMStreamHdr *mstreamHdr;
    AdcMStreamTrigPayload *adcTrigPayload;
    quint32 *curWordPtr;

    int lastReadSize;
    qint64 blockStatPos;
    qint64 curPos;
    qint64 curSize;
    int restSize;
};

#endif // READER_H
