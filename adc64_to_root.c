#include <iostream>
#include <fstream>
#include <vector>

#include <TFile.h>
#include <TNtuple.h>
#include <TGraph.h>
#include <TF1.h>
#include <TTree.h>

#include <TCanvas.h>
#include <TH1F.h>

using namespace std;

typedef struct _Adc64Event {
    int recLength;  //Record Length: 1024
    int boardID;    //BoardID: 31
    int channel;    //Channel: 0
    int eventNumber;//Event Number: 0
    int pattern;    //Pattern: 0x0000
    int timeStamp;  //Trigger Time Stamp: 392909414
    int dcOffset;   //DC offset (DAC): 0x7FFF
    int startIndexCell; //Start Index Cell: 562
    short int data[1024];
    float dataInt[1024];
    float dataIntegral;
    short int max;
    short int min;
    short int minPed;
    short int avg;
    float timeAtThresh;
    float timeOverThresh;
} Adc64Event;

enum _EventHeaderFields {
    REC_LENGTH = 0,
    BOARD_ID,
    CHANNEL,
    EVENT_NUMBER,
    PATTERN,
    TIME_STAMP,
    DC_OFFSET,
    START_INDEX_CELL,

    EVENT_FIELDS_NUMBER
} EventFields;

const char EventHeaderFieldStrings[][100] = {
    "Record Length",
    "BoardID",
    "Channel",
    "Event Number",
    "Pattern",
    "Trigger Time Stamp",
    "DC offset (DAC)",
    "Start Index Cell"
};

typedef std::vector<Adc64Event> VectorOfAdc64Events;
typedef std::vector<VectorOfAdc64Events> VectorOfVectorOfAdc64Events;

void adc64_to_root(char* fileName=0, int nEventsToProcess = -1, const int EVENT_SIZE = 200,
			  const int N_CHANNELS = 5,
			  char* outFileName = "data.root",
			  int signalBegin = 40, int signalEnd = 130,
			  int showData = 0, int eventToShow = -1,
			  const int N_CHANNELS_TO_SHOW = 4) {

    TTree* _bigTree = new TTree( "bigtree", "bigtree");

    int _treeFill_iEvt;
    _bigTree->Branch("iEvt", &_treeFill_iEvt, "iEvt/I");
    int _treeFill_timeStamp;
    _bigTree->Branch("timeStamp", &_treeFill_timeStamp, "timeStamp/I");

    float _treeFill_adc[16];
    float _treeFill_qdc[16];
    float _treeFill_ped[16];
    _bigTree->Branch("adc", _treeFill_adc, "adc[16]/F");
    _bigTree->Branch("qdc", _treeFill_qdc, "qdc[16]/F");
    _bigTree->Branch("ped", _treeFill_ped, "ped[16]/F");

    TCanvas* c1 = 0;
    TH1F* hf1 = 0;

    int signalType[16] = { 1, 1, 1, 1,
			   1, 1, 1, 1,
			   1, 1, 1, 1,
			   1, 1, 1, 1 };

    std::ifstream datafile;
    cout << "Opening file: " << fileName << endl;
    datafile.open(fileName);

    const int MaxStrlen = 1000;
    char buf[MaxStrlen];

    if (datafile.is_open())
    {

      TFile outfile(outFileName, "RECREATE", "data", 5);
      std::vector<std::vector<Adc64Event> > data; // = new std::vector<std::vector<Adc64Event> >();

      int iEvt = 0;
      cout << "Number of events to process= " << nEventsToProcess << endl;

      while(! (datafile.rdstate() & std::ifstream::eofbit ) ) {

	//if (iEvt%1000 == 0) cout << "Event number: " << iEvt << endl;
	cout << "Event number: " << iEvt << endl;

	Adc64Event ev;

	datafile.getline(buf, MaxStrlen);
	const char *pattern = "Event";

	if(!strncmp(pattern, buf, strlen(pattern))) {

	    strtok(buf, " ");

	    char *s_value = strtok(NULL, " ");
	    //ev.eventNumber = atoi(s_value);

	    s_value = strtok(NULL, " ");
	    ev.timeStamp = atoi(s_value);

	 } else {
	   cout << ("Wrong data format");
	 }


       _treeFill_iEvt = iEvt;
       _treeFill_timeStamp = ev.timeStamp;

       for (int k=0; k<16; k++) {
	 _treeFill_adc[k] = 0;
       }

      std::vector<Adc64Event> channelData;

      for(int iCh = 0; iCh < N_CHANNELS; iCh++) {

      const int MaxStrlen2 = 10000;
      char buf2[MaxStrlen2];

      datafile.getline(buf2, MaxStrlen2);

      strtok(buf2, " ");

      char *s_value = strtok(NULL, " ");
      //ev.channel = atoi(s_value);
      ev.channel = iCh;

      int iData=0;

      while (s_value != NULL)
      {
	//std::cout << s_value  << '\n';
	ev.data[iData] = atoi(s_value);
	s_value = strtok(NULL, " ");
	iData++;
      }

      ev.recLength = iData;
      ev.eventNumber = iEvt;

      //cout << "ev.recLength = " << ev.recLength << endl;

      ev.min = SHRT_MAX; // no typo
      ev.max = SHRT_MIN; // no typo
      ev.minPed = SHRT_MAX; // no typo

      int avg = 0;
      float avgPed = 0;

      float dataInt = 0;

      int pedBegin = 10;
      int pedEnd = 30;

      for(int i = 0; i < ev.recLength; i++) {
	  int value = ev.data[i];
	  avg += value;

	  if (i >= pedBegin && i <= pedEnd) {
	    avgPed += value;
	  }

	  if (i == pedEnd) {
	    avgPed = avgPed / (pedEnd - pedBegin + 1);
	    ev.minPed = avgPed;
	    ev.min = avgPed;
	    //std::cout << "avgPed = " << avgPed << std::endl;
	  }

	  if (i >= signalBegin && i <= signalEnd) {
	    if(value > ev.max) ev.max = value;
	    //dataInt += (value - avgPed)/(signalEnd - signalBegin+1);
	    dataInt += (value - avgPed);
	  }

      }

      ev.dataIntegral = dataInt;

      ev.avg = avg / ev.recLength;

      ev.timeOverThresh = -1;
      ev.timeAtThresh = -1;
      channelData.push_back(ev);

      }// for(int iCh = 0; iCh < N_CHANNELS; iCh++)

	//analyse data of one event here

	double adc[16] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	double qdc[16] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	double ped[16] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

	for(int ch = 0; ch < N_CHANNELS; ch++) {
	  adc[ch] = channelData.at(ch).max - channelData.at(ch).min;
	  qdc[ch] = channelData.at(ch).dataIntegral;
	  ped[ch] = channelData.at(ch).minPed;

	  _treeFill_adc[ch] = adc[ch];
	  _treeFill_qdc[ch] = qdc[ch];
	  _treeFill_ped[ch] = ped[ch];

	}

	_bigTree->Fill();

	if (showData==1 && (eventToShow<0 || (eventToShow>0 && iEvt==eventToShow)) ) {


	  if (eventToShow>0 && iEvt==eventToShow) eventToShow = -1;

	  cout << "iEvt = " << iEvt << endl;

	  if (!(c1)) c1 = new TCanvas("c1","c1",640,480);

	  c1->SetGrid();

	  TGraph* gr[N_CHANNELS_TO_SHOW];
	  for (int iGr=0; iGr<N_CHANNELS_TO_SHOW; iGr++) gr[iGr] = new TGraph();

	  for (int iCh=0; iCh<N_CHANNELS_TO_SHOW; iCh++) {
	    cout  << "  ,ped [" << iCh << "] = " << ped[iCh];
	  }
	  cout << endl;

	  for (int iCh=0; iCh<N_CHANNELS_TO_SHOW; iCh++) {
	    cout  << "  ,adc [" << iCh << "] = " << adc[iCh];
	  }
	  cout << endl;

	  for (int iCh=0; iCh<N_CHANNELS_TO_SHOW; iCh++) {
	    for (int iData=0; iData<EVENT_SIZE; iData++) {
	      //gr[iCh]->SetPoint(iData,iData/1024.*204.8,channelData.at(iCh).dataInt[iData]);
	      gr[iCh]->SetPoint(iData,iData,channelData.at(iCh).data[iData]);
	      //if (iCh==0 && iData < 500) cout << "data [" << iData << "] = " << myEvent_ch.data[iData] << endl;
	    }
	  }

	  cout << endl;

	  if (!(hf1)) hf1 = new TH1F("hf1","hf1",100,0,EVENT_SIZE*1.0);

	  hf1->SetStats(0);
	  //hf1->GetYaxis()->SetRangeUser(0,4096);
	  hf1->GetYaxis()->SetRangeUser(-35000,-35000);
	  //hf1->GetYaxis()->SetRangeUser(-25000,2000);
	  hf1->Draw();

	  int colors[16] = { 1, 2, 4, 6, 1, 2, 4, 6, 1, 1, 1, 1, 1, 1, 1, 1 };

	  for (int iGr=0; iGr<N_CHANNELS_TO_SHOW; iGr++) {

	    gr[iGr]->SetMarkerStyle(20);
	    gr[iGr]->SetMarkerSize(0.5);
	    gr[iGr]->SetMarkerColor(colors[iGr]);
	    gr[iGr]->SetLineColor(colors[iGr]);
	    gr[iGr]->Draw("plsame");
	  }

	  c1->Update();
	  char c = getchar();
	  if (c=='q') break;
	  else if (c=='c') showData = 0;
	  else if (c=='p') c1->SaveAs("c1.root");
	}


      iEvt++;
      if (nEventsToProcess>0 && iEvt==nEventsToProcess) break;

      }//while

      _bigTree->Write();

      outfile.Close();

    } else {
	cout << "Unable to open input file" << endl;
    }

}
