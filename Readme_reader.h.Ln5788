#ifndef README_READER_H
#define README_READER_H

#include <iostream>
#include <fstream>
#include <ctime>
#include <Rtypes.h>
#include <string.h>

#include <TFile.h>
#include<TString.h>
#include<TMath.h>
#include<TTree.h>
#include<TObject.h>

struct module_info {
    TString name;
    TString data_file;
    TString board_name;
    TString fee_name;

    Int_t X_mod_pos;
    Int_t Y_mod_pos;
    Int_t slowcntr_num;
    Int_t cells_num;
    Int_t *cell_ch;

    TString comment;

    module_info(){Clear();}
    ~module_info(){if(cell_ch) delete[] cell_ch;}
    void Clear();
    void Print();

    Int_t Read(FILE *file);
};

module_info::Clear()
{

    name = "";
    data_file = "";
    board_name = "";
    fee_name = "";

    X_mod_pos = 0;
    Y_mod_pos = 0;
    slowcntr_num = 0;
    cells_num = 0;
    if(cell_ch) delete[] cell_ch; cell_ch = NULL;

    comment = "";
}

module_info::Print()
{
    printf("%s ==============\n", name.Data());
    printf("file: %s\n", data_file.Data());
    printf("board: %s\n", board_name.Data());
    printf("FEE: %s\n", fee_name.Data());
    printf("position: (%i,%i)\n", X_mod_pos, Y_mod_pos);
    printf("slow control id: %i\n", slowcntr_num);
    printf("total cell: %i\n", cells_num);

    printf("ch map: ");
    if(cell_ch)
    {
	for(Int_t ch = 0; ch < cells_num; ch++) printf("%i ", cell_ch[ch]);
	printf("\n");
    }
    else printf("no\n");
    printf(" %i\n\n", comment.Data());

}

Int_t module_info::Read(FILE *file)
{
    char buffer[1000]; int read; char *result;

    read = fscanf(file, "%s", buffer);
    if( !read || (read == EOF) || !strlen(buffer)){ Clear(); return -1;}
    if(buffer[0] == '#') {result = fgets(buffer, 1000, file); read = fscanf(file, "%s", buffer);  }
    name = buffer;

    read = fscanf(file, "%s", buffer);
    if( !read || (read == EOF) || !strlen(buffer)){ Clear(); return -1;}
    if(buffer[0] == '#') {result = fgets(buffer, 1000, file); read = fscanf(file, "%s", buffer);  }
    data_file = buffer;

    read = fscanf(file, "%s", buffer);
    if( !read || (read == EOF) || !strlen(buffer)){ Clear(); return -1;}
    if(buffer[0] == '#') {result = fgets(buffer, 1000, file); read = fscanf(file, "%s", buffer);  }
    board_name = buffer;

    read = fscanf(file, "%s", buffer);
    if( !read || (read == EOF) || !strlen(buffer)){ Clear(); return -1;}
    if(buffer[0] == '#') {result = fgets(buffer, 1000, file); read = fscanf(file, "%s", buffer);  }
    fee_name = buffer;

    read = fscanf(file, "%s", buffer);
    if( !read || (read == EOF) || !strlen(buffer)){ Clear(); return -1;}
    if(buffer[0] == '#') {result = fgets(buffer, 1000, file); read = fscanf(file, "%s", buffer);  }
    X_mod_pos = atoi(buffer);

    read = fscanf(file, "%s", buffer);
    if( !read || (read == EOF) || !strlen(buffer)){ Clear(); return -1;}
    if(buffer[0] == '#') {result = fgets(buffer, 1000, file); read = fscanf(file, "%s", buffer);  }
    Y_mod_pos = atoi(buffer);

    read = fscanf(file, "%s", buffer);
    if( !read || (read == EOF) || !strlen(buffer)){ Clear(); return -1;}
    if(buffer[0] == '#') {result = fgets(buffer, 1000, file); read = fscanf(file, "%s", buffer);  }
    cells_num = atoi(buffer);


    cell_ch = new Int_t[cells_num];
    for(Int_t cell = 0; cell < cells_num; cell ++)
    {
	read = fscanf(file, "%s", buffer);
	if( !read || (read == EOF) || !strlen(buffer)){ Clear(); return -1;}
	if(buffer[0] == '#') {result = fgets(buffer, 1000, file); read = fscanf(file, "%s", buffer);  }
	cell_ch[cell] = atoi(buffer);
    }

    read = fscanf(file, "%s", buffer);
    if( !read || (read == EOF) || !strlen(buffer)){ Clear(); return -1;}
    if(buffer[0] == '#') {result = fgets(buffer, 1000, file); read = fscanf(file, "%s", buffer);  }
    comment = buffer;
}



class Readme_reader
{
public:
    Readme_reader(TString run_folder_path_);
    ~Readme_reader(){Clear();}


    Int_t Set_runfile_name(TString Run_name_file_);
    TString Get_run_name(){return run_name_file;}

    void Print();

private:

    //service function
    void Clear();

    // private data
    static const int max_line_len;
    static const char *readme_file_mask;
    TString run_folder_path;
    TString run_name_file;

    // config info
    TString run_name;
    TString run_comment;
    Int_t board_num;
    module_info *module_info_arr;

};

const int Readme_reader::max_line_len = 1000;
const char* Readme_reader::readme_file_mask = "run_info.txt";


// #####################################################################
Readme_reader::Readme_reader(TString run_folder_path_)
    : run_folder_path(run_folder_path_), run_name_file("void"), module_info_arr(NULL)
{
    Clear();
}
// #####################################################################

// #####################################################################
void Readme_reader::Clear()
{
    run_name.Clear();
    run_comment.Clear();
    board_num = 0;
    if(module_info_arr) delete[] module_info_arr; module_info_arr = NULL;
}
// #####################################################################

// #####################################################################
void Readme_reader::Print()
{
    printf("\nInfo file: %s/%s/%s\n", run_folder_path.Data(), run_name_file.Data(), readme_file_mask);
    printf("Name: %s\n", run_name.Data());
    printf("Comment: %s\n", run_comment.Data());
    printf("Boards %i :\n", board_num);
    if(module_info_arr)
    {
	for(Int_t board = 0; board < board_num; board++)
	{
	    module_info_arr[board].Print();

	}
    }
}
// #####################################################################

// #####################################################################
Int_t Readme_reader::Set_runfile_name(TString Run_name_file_)
{
    run_name_file = Run_name_file_;

    TString info_file_name = Form("%s/%s/%s", run_folder_path.Data(), run_name_file.Data(), readme_file_mask);

    FILE *info_file = fopen(info_file_name.Data(),"rt");
    if(info_file == NULL){
	printf("Readme_reader:Info file \"%s\" was NOT opened\n", info_file_name.Data());
	return -1;
    }

    Clear();


    Int_t read_step = 0, board_readed = 0;
    while(!feof(info_file)) {
	char buffer[max_line_len]; int read; char *result;

	// read a word from the files and skip comments
	read = fscanf(info_file, "%s", buffer);
	if( !read || (read == EOF) || !strlen(buffer)) continue;
	if(buffer[0] == '#') {result = fgets(buffer, 1000, info_file); continue; }

	if(strstr(buffer, "[RUNNAME]")) {read_step = 1; continue;}
	if(strstr(buffer, "[DATACOMMENTS]")) {read_step = 2; continue;}
	if(strstr(buffer, "[BOARDNUM]")) {read_step = 3; continue;}
	if(strstr(buffer, "[BOARDSCONF]")) {read_step = 4; continue;}

	if(read_step == 1){ run_name += buffer;} //collect name by words
	if(read_step == 2){ run_comment += buffer;} //collect comment by words
	if(read_step == 3){ board_num = atoi(buffer); module_info_arr = new module_info[board_num]; read_step = 0;}
	if(read_step == 4)
	{
	    if(buffer[0] == '[')
	    {
		Int_t current_board = atoi(buffer);

		printf("b %i\n", current_board);
		module_info_arr[board_readed].Read(info_file);
		board_readed++;
	    }
	    if(board_readed == board_num) read_step = 0;
	}



	//printf("%s\n", buffer);

    } //while()

    fclose (info_file);

    return 1;
}
// #####################################################################






















//// #####################################################################
//TString Readme_reader::Read_module_name(Int_t board_ch)
//{
//    if(run_name == "void")
//    {
//	printf("Waring run name is not set\n");
//	return "void";
//    }

//    TString run_readme_name = Form(readme_file_mask, run_name.Data());

//    std::ifstream file;
//    file.open( (readme_folder_path + run_readme_name).Data() );
//    if (file.is_open())
//    {
//	//printf("File \"%s\" was opened for reading\n", run_readme_name.Data());
//    }
//    else
//    {
//	printf("\nERROR: Readme file \"%s\" was NOT opened for reading\n", (readme_folder_path + run_readme_name).Data() );
//	return "void";
//    }


//    char char_buffer[1000];
//    file.getline(char_buffer, 1000);


//    file.close();

//    TString name = char_buffer;

//    Int_t s_pos = 10;
//    Int_t p_pos = name.Length();

//    //if(name.Contains("module"))s_pos =

//    TString new_name = name(s_pos,p_pos-s_pos-1);


//    return new_name;



//}
//// #####################################################################


#endif // README_READER_H



// readme file example
/*

# module record
[1]
#comment sign

module	    # name [string]
data_file   # data file name [string]
ADC64_board # ADC64 board identificator [string]
15_v16	    # FEE name [string]

2   4	    # X Y module position [int, int]
1	    # cell number in slow control [int]
0   1	2   3	4   5	6   7	8   9	10  # cells number - ch numbers [int, int, ... int]

comment	    # comment




[2]

--- // ---

[human]
module №: 12,14,15 CBM PSD module
FEE: 2,7,9
orientation: horizontal
FEE attached without optical grease
MAPD voltage: FEE2 all channels 71.1 V; FEE7 all channels 71.3 V; FEE9 all channels 71.4 V;
thermal compensation: absent
PCB temperature: FEE2 27.08 FEE7 27.12 FEE9 27.06 deg. C from SLowControl at start; FEE2 26.95 FEE7 27.00 FEE9 26.95 deg. C from SLowControl at stop
ADC64S2_v5
ADC64 readout count: 200
ADC64 after trigger readout count: 140
ADC64 trigger condition: threshold on rising edge - all channels 200; ch 13 300 (noisy)
noise levels: TODO
LED flash calibration: TODO
LED flash disabled;
*/
