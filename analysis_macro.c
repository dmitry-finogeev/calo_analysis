#define isLOCAL 0

#include <iostream>
#include <fstream>
//#include <conio.h>
#include <ctime>

#include <TFile.h>
#include <TGraph.h>
#include <TF1.h>
#include <TTree.h>
#include <TObjArray.h>
#include <TString.h>
#include <TDirectory.h>
#include <TCanvas.h>
#include <TH1F.h>
#include <TSystemFile.h>
#include <TSystemDirectory.h>
#include <TBrowser.h>
#include "langaus.hh"
#include "data_calculating.h"

#if isLOCAL
#include "../libraries/utilites/utilites.h"
#else
#include "utilites.h"
#endif




float makeCalib(TH1 *hsum) {


TF1 *fit = new TF1("langau",langaufun,5000,40000.,4);
fit->SetParNames("Width","MP","Area","GSigma");
fit->SetParameters(1000,7000,1500000,5000.);


//fit->SetParLimits(0,5.,100.); commented for small mods
//fit->SetParLimits(1,20.,200.);
//fit->SetParLimits(3,0.5,200.);


hsum->Fit(fit,"","",5000,40000);

Double_t maxx, FWHM;
langaupro(fit->GetParameters(), maxx, FWHM);

cout << "maxx = " << maxx << endl;

return (float)maxx;

}




void analysis_macro()
{
    //input data
#if isLOCAL
    TString source_path = "/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_results/cosmic_autumn_2017/temp_runs/";
//  TString source_path = "/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_source/";
    TString result_path = "/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_results/cosmic_autumn_2017/";
#else
    TString source_path = "/media/sf_ADC64/data_processing/ADC64/PROCESSING/root_converted_new/";
    TString result_path = "/media/sf_ADC64/data_processing/ADC64/PROCESSING/root_results/";
#endif

    TString result_file_name = "CALO_analysis_004";

    TObjArray files_names;
    files_names.SetOwner();


    // == Source files management ==
    /****/ // Tate files list from directory
    FilesListFromDir(source_path, &files_names, 0, ".root");

    //remove extension
    for(Int_t f=0; f<=files_names.GetLast();f++)
    {
	TObjString *iFile_name = (TObjString*)(files_names.At(f));

//        if(iFile_name->GetString().Contains(".pdf"))
//        {
//            files_names.RemoveAt(f);
//            f--;
//            continue;
//        }

	Int_t p_pos = ((TObjString*)(files_names.At(f)))->GetString().Index(".root", 4);
	TString new_name = iFile_name->GetString()(0,p_pos);
	iFile_name->SetString( new_name );
    }
    /*** //Print files list for manual array
    for(Int_t f=0; f<=files_names.GetLast();f++)
	current_out << Form("files_names.Add(new TObjString(\"%s\"));\n", ((TObjString*)(files_names.At(f)))->GetString().Data());
    /*** //manual arry
    files_names.Add(new TObjString("adc64_17_02_17_16_55_overweekend"));
    //files_names.Add(new TObjString("adc64_17_04_21_17_04_overweekend"));
	    //files_names.Add(new TObjString("adc64_17_02_16_17_00_overnight"));
	    //files_names.Add(new TObjString("adc64_17_02_15_18_10_overnight"));
    /****/
    // =============================



    // Calculation CONSTANTS =======
    #if isLOCAL
	Int_t draw_first_entery = 0;
	Int_t precalculation_n_events = 1000000;
	Int_t calculation_n_events = 1000000;
    #else
	Int_t draw_first_entery = 0;
	Int_t precalculation_n_events = 1000000;
	Int_t calculation_n_events = 10000000;
    #endif


    const Int_t max_charge = 60000;
    const Int_t max_charge_bin = 1000;
    const Double_t timeselection_nsigma = 0.7;
    const Double_t timeselection_near_nsigma = 3;
    const Double_t timeselection_hor_nsigma = 3.;

    const Double_t pedescut_trsh = 2000.;
    const Double_t pedescut_trsh_hor = 1000.;
    const Double_t pedescut_nsigma = 5.;
    const Double_t pedescut_near_nsigma = 4;
    const Double_t pedescut_hor_nsigma = 1.;

    const Double_t ampl_max_near_cut = -5000.;
    // =============================


    current_out << Form("\n\n\n######################################################\n");
    current_out << Form("###################  DATA ANALYSER  ##################\n");
    current_out << Form("######################################################\n\n\n");

    current_out << Form("--------------------------\n");
    current_out << Form("Source path: %s\n", source_path.Data());
    current_out << Form("Result filename: %s\n", result_file_name.Data());
    current_out << Form("Result path: %s\n", result_path.Data());
    current_out << Form("File list to analyse:\n");
    for(Int_t f=0; f<=files_names.GetLast();f++)
	current_out << Form("%s\n",  ((TObjString*)(files_names.At(f)))->GetString().Data());
    current_out << Form("--------------------------\n\n");


    current_out << Form("--------------------------\n");
    current_out << Form("precalculation_n_events    = %i\n", precalculation_n_events);
    current_out << Form("calculation_n_events    = %i\n", calculation_n_events);
    current_out << Form("max_charge		    = %i\n", max_charge);
    current_out << Form("max_charge_bin		    = %i\n", max_charge_bin);
    current_out << Form("timeselection_nsigma	    = %f\n", timeselection_nsigma);
    current_out << Form("timeselection_near_nsigma	    = %f\n", timeselection_near_nsigma);
    current_out << Form("timeselection_hor_nsigma   = %f\n", timeselection_hor_nsigma);
    current_out << Form("pedescut_trsh		    = %f\n", pedescut_trsh);
    current_out << Form("pedescut_trsh_hor	    = %f\n", pedescut_trsh_hor);
    current_out << Form("pedescut_nsigma	    = %f\n", pedescut_nsigma);
    current_out << Form("pedescut_near_nsigma	    = %f\n", pedescut_near_nsigma);
    current_out << Form("pedescut_hor_nsigma	    = %f\n", pedescut_hor_nsigma);
    current_out << Form("ampl_max_near_cut	    = %f\n", ampl_max_near_cut);
    current_out << Form("--------------------------\n") << std::endl;

    //----------------------------  0  1  2  3  4  5  6  7  8  9
    //Int_t get_ch_for_section[10] = {4, 3, 2, 1, 0, 7, 6, 9, 8, 5};
    //Int_t get_ch_for_section[7] = {0,4,1,5,2,6,3}; //NICA wrong
    //Int_t get_ch_for_section[7] = {4,3,6,5,2,1,0}; //NICA
    Int_t get_ch_for_section[7] = {0,1,2,3,4,5,6}; //NICA bin

    // =============================


    // Result file =================
    TFile *result_file = new TFile((result_path + result_file_name + ".root").Data(), "RECREATE");
    gDirectory->cd("Rint:/");
    TObjArray *result_array = new TObjArray;
    result_array->SetName("main_results");
    result_array->SetOwner();
    TCanvas *temp_canv = new TCanvas("temp", "temp");
    // =============================




    time_t start_time = time(NULL);
    current_out << Form("Starting analysis at %s", asctime( localtime( &start_time ) )) << std::endl;

    Int_t nfiles_skiped = 0;
    Int_t total_files = files_names.GetLast()+1;
    for(Int_t iFile=0; iFile<total_files;iFile++)
    {
	TString iFile_name = ((TObjString*)(files_names.At(iFile)))->GetString();
	current_out << std::endl << Form("### %i of %i ###", iFile+1, total_files) << std::endl;
	TFile *source_file = new TFile(source_path+iFile_name+".root", "READONLY");
	TTree *data_tree = dynamic_cast<TTree*>(source_file->FindObjectAny("adc64_data"));


	if(!data_tree)
	{
	    current_out << Form("ERROR: Data tree not found !!!") << std::endl;
	    source_file->Close();
	    delete source_file;
	    nfiles_skiped++;
	    continue;
	}

	Int_t total_Channels = data_tree->GetNbranches();
	if(data_tree->GetBranch("log_data") != NULL) total_Channels--;

	//if(total_Channels < 10) //temp solution
	if(total_Channels < 7) //temp solution
	{
	    current_out << Form("ERROR: Too low branches: %i, wron module !!!", total_Channels) << std::endl;
	    source_file->Close();
	    delete source_file;
	    nfiles_skiped++;
	    continue;
	}

	//11 channels in ADC; but in module 10 sectors
	//use other ch to sec table for other module type
	//total_Channels = 10; //
	total_Channels = 7; //





	time_t current_time = time(NULL);
	Int_t proc_sec = difftime(current_time,start_time);
	Float_t percents = (float)iFile/(total_files);
	Float_t proc_rate = (float)proc_sec/(iFile-nfiles_skiped)/60.;
	Int_t time_est = (percents == 0)?0:proc_sec/percents*(1. - percents);
	current_out << Form("Calculating file: \"%s\" %i/%i [skiped %i] (%5.1f%%) [total events: %lld]; [pas %3.0dm %2.0is] [est %3.0dm %2.0is] [%.1f min/file]",
			    iFile_name.Data(), iFile+1, total_files, nfiles_skiped,
			    (percents*100.),
			    data_tree->GetEntries(),
			    (proc_sec/60), proc_sec%60,
			    (time_est/60), time_est%60,
			    proc_rate)  << std::endl << std::endl;

	if(OUTINFILE)
	    printf("Calculating file: \"%s\" %i/%i [skiped %i] (%5.1f%%) [total events: %lld]; [pas %3.0dm %2.0is] [est %3.0dm %2.0is] [%.1f min/file]\n",
		   iFile_name.Data(), iFile+1, total_files, nfiles_skiped,
		   (percents*100.),
		   data_tree->GetEntries(),
		   (proc_sec/60), proc_sec%60,
		   (time_est/60), time_est%60,
		   proc_rate);




	// Calclating data for each file ################################################
	TObjArray *file_data_array = new TObjArray();
	file_data_array->SetName((iFile_name + "").Data());
	file_data_array->SetOwner();
	result_array->Add(file_data_array);

	result_file->cd("/");

	TH1 *th1_hist_ptr = NULL;
	Double_t mean, sigma;

	// preCalculating for each channel *********************************************
	TObjArray *onemip_draw_array = new TObjArray();
	TObjArray *onemiphor_draw_array = new TObjArray();
	TObjArray *onemipnear_draw_array = new TObjArray();
	TObjArray *onemipnear2_draw_array = new TObjArray();

	Double_t *mean_time_value = new Double_t[total_Channels];
	Double_t *sigma_time_value = new Double_t[total_Channels];
	Double_t *mean_pedestal_value = new Double_t[total_Channels];
	Double_t *sigma_pedestal_value = new Double_t[total_Channels];
	Double_t *mean_onemip_value = new Double_t[total_Channels];
	Double_t *sigma_onemip_value = new Double_t[total_Channels];

	for(Int_t channel_num = 0; channel_num < total_Channels; channel_num++)
	{
	    TString channel_name = event_data_struct::GetChName(channel_num);
	    TObjArray *channel_data_array = new TObjArray();
	    channel_data_array->SetName(channel_name.Data());
	    channel_data_array->SetOwner();
	    file_data_array->Add(channel_data_array);

	    current_out << Form("----- preCalculating channel %s -----", channel_name.Data()) << std::endl;
	    if(OUTINFILE) printf("----- preCalculating for channel %s -----\n", channel_name.Data());

	    // Mean signal time ============================
	    th1_hist_ptr = new TH1F("temp", "temp", 200, 0, 200);

	    data_tree->Draw(Form("%s.time_max_in_gate>>temp", channel_name.Data()), "", "", 10000);
	    FitHistogrammInMaxPeak(th1_hist_ptr, mean, sigma, 4.);

	    th1_hist_ptr->SetName(Form("%s_%s_meansignaltime", iFile_name.Data(), channel_name.Data()));
	    th1_hist_ptr->SetTitle(Form("Signal mean time [file: %s] [ch: %s] [mean: %.1f]", iFile_name.Data(), channel_name.Data(), mean));
	    th1_hist_ptr->GetXaxis()->SetRangeUser(mean-20, mean+20);
	    channel_data_array->Add(th1_hist_ptr);

	    TString time_pedes_selection = Form("(%s.time_max_in_gate > %f + 20.)",
						channel_name.Data(), mean);
	    // =============================================


	    // Pedestals ============================
	    th1_hist_ptr = new TH1F("temp", "temp", 400, -2000, 2000);

//	    data_tree->Draw(Form("(%s.integral_in_gate - %s.mean_out_gate)>>temp",
//				 channel_name.Data(), channel_name.Data()), time_pedes_selection.Data(), "", 10000);
	    data_tree->Draw(Form("(%s.integral_in_gate)>>temp",
				 channel_name.Data()), time_pedes_selection.Data(), "", 10000);
	    //FitHistogrammInMaxPeak(th1_hist_ptr, mean, sigma, 20.);
//	    mean_pedestal_value[channel_num] = mean;
//	    sigma_pedestal_value[channel_num] = sigma;
	    mean_pedestal_value[channel_num] = 0.;
	    sigma_pedestal_value[channel_num] = 0.;

	    th1_hist_ptr->SetName(Form("%s_%s_pedestal", iFile_name.Data(), channel_name.Data()));
//	    th1_hist_ptr->SetTitle(Form("Pedestal [file: %s] [ch: %s] [mean: %.1f, sigma: %.1f, nsigma_cut: %.1f trhd: %.1f, hor trhd: %.1f]",
//					iFile_name.Data(), channel_name.Data(), mean, sigma, pedescut_nsigma, mean + pedescut_nsigma*sigma, mean + pedescut_hor_nsigma*sigma));
	    th1_hist_ptr->SetTitle(Form("Pedestal [file: %s] [ch: %s] [trhd: %.1f, hor trhd: %.1f]",
					iFile_name.Data(), channel_name.Data(), pedescut_trsh, pedescut_trsh));
	    //th1_hist_ptr->GetXaxis()->SetRangeUser(mean-20, mean+20);
	    channel_data_array->Add(th1_hist_ptr);

	    //pedescut_trsh =  mean + pedescut_nsigma*sigma;

//	    TString amplitude_pedescut_selection = Form("((%s.integral_in_gate - %s.mean_out_gate) > %f)",
//							channel_name.Data(), channel_name.Data(), pedescut_trsh);
//	    TString amplitude_maxcharge_selection = Form("((%s.integral_in_gate - %s.mean_out_gate) < %i)",
//							 channel_name.Data(), channel_name.Data(), max_charge);
	    TString amplitude_pedescut_selection = Form("((%s.integral_in_gate) > %f)",
							channel_name.Data(), pedescut_trsh);
	    TString amplitude_maxcharge_selection = Form("((%s.integral_in_gate) < %i)",
							 channel_name.Data(), max_charge);
	    // =============================================



	    // Signal time ============================
	    th1_hist_ptr = new TH1F("temp", "temp", 200, 0, 200);

	    data_tree->Draw(Form("%s.time_max_in_gate>>temp", channel_name.Data()),
			    amplitude_pedescut_selection.Data(), "", 10000);
	    FitHistogrammInMaxPeak(th1_hist_ptr, mean, sigma, 5.);
	    mean_time_value[channel_num] = mean;
	    sigma_time_value[channel_num] = sigma;
	    //	    Double_t time_max = th1_hist_ptr->GetBinCenter( th1_hist_ptr->GetMaximumBin() );


	    th1_hist_ptr->SetName(Form("%s_%s_signaltime", iFile_name.Data(), channel_name.Data()));
	    th1_hist_ptr->SetTitle(Form("Signal mean time [pedestal selection] [file: %s] [ch: %s] [nsigma: %.1f, selection: (%.1f, %.1f)]",
					iFile_name.Data(), channel_name.Data(), timeselection_nsigma, mean - timeselection_nsigma*sigma, mean + timeselection_nsigma*sigma));
	    th1_hist_ptr->GetXaxis()->SetRangeUser(mean-20, mean+20);
	    channel_data_array->Add(th1_hist_ptr);

	    TString time_selection = Form("(%s.time_max_in_gate > %f)&&(%s.time_max_in_gate < %f)",
					  channel_name.Data(), 55., channel_name.Data(),60.);
//	    TString time_selection = Form("(%s.time_max_in_gate > %f)&&(%s.time_max_in_gate < %f)",
//					  channel_name.Data(), mean - timeselection_nsigma*sigma, channel_name.Data(), mean + timeselection_nsigma*sigma );
	    //	    TString time_selection = Form("(%s.time_max_in_gate > %f)&&(%s.time_max_in_gate < %f)",
	    //					  channel_name.Data(), time_max - 1., channel_name.Data(), time_max + 1. );
	    //	    TString time_selection = Form("(%s.time_max_in_gate == %f)",
	    //					  channel_name.Data(), time_max);
	    // =============================================

	    // Signal RMS in gate ==========================
	    th1_hist_ptr = new TH1F("temp", "temp", 500, 0, 500);

//	    data_tree->Draw(Form("%s.RMS_in_gate>>temp", channel_name.Data()),
//			    //(amplitude_pedescut_selection+"&&"+time_selection+"&&"+amplitude_maxcharge_selection).Data(),
//			    Form("((%s.integral_in_gate - %s.mean_out_gate) > %f)&&((%s.integral_in_gate - %s.mean_out_gate) < %f)",
//				 channel_name.Data(), channel_name.Data(), 25., channel_name.Data(), channel_name.Data(), 100.),
//			    "", precalculation_n_events);
	    data_tree->Draw(Form("%s.RMS_in_gate>>temp", channel_name.Data()),
			    //(amplitude_pedescut_selection+"&&"+time_selection+"&&"+amplitude_maxcharge_selection).Data(),
			    Form("((%s.integral_in_gate) > %f)&&((%s.integral_in_gate) < %f)",
				 channel_name.Data(), 25., channel_name.Data(), 100.),
			    "", precalculation_n_events);
	    //FitHistogrammInMaxPeak(th1_hist_ptr, mean, sigma, 5.);
	    //	    Double_t time_max = th1_hist_ptr->GetBinCenter( th1_hist_ptr->GetMaximumBin() );


	    th1_hist_ptr->SetName(Form("%s_%s_signalRMS", iFile_name.Data(), channel_name.Data()));
	    th1_hist_ptr->SetTitle(Form("RMS in gate [pedestal, time, maxcharge selection] [file: %s] [ch: %s]",
					iFile_name.Data(), channel_name.Data()));
	    //th1_hist_ptr->GetXaxis()->SetRangeUser(mean-20, mean+20);
	    channel_data_array->Add(th1_hist_ptr);

	    TString signal_RMS_selection = Form("(%s.RMS_in_gate > 70.)", channel_name.Data());
	    signal_RMS_selection = Form("(1)");
	    // =============================================

	    // One mip amplitude ===========================
	    th1_hist_ptr = new TH1F("temp", "temp", max_charge_bin, 0, max_charge);

//	    data_tree->Draw(Form("(%s.integral_in_gate - %s.mean_out_gate)>>temp", channel_name.Data(), channel_name.Data()),
//			    (time_selection+"&&"+signal_RMS_selection).Data(), "", precalculation_n_events);
	    data_tree->Draw(Form("(%s.integral_in_gate)>>temp", channel_name.Data()),
			    (time_selection+"&&"+signal_RMS_selection).Data(), "", precalculation_n_events);



	    //TF1 *fit_funk = new TF1("fit_funk_onemip", "gaus");
	    //FitHistogrammSmartInMaxPeak(th1_hist_ptr, mean, sigma, 0.5, pedescut_trsh+20., max_charge);
	    //FitHistogramm(th1_hist_ptr, mean, sigma, pedescut_trsh, max_charge, "landau");
	    //FitHistogramm(th1_hist_ptr, mean, sigma, pedescut_trsh, max_charge, "gaus");
	    //FitHistogrammInRMSRange(th1_hist_ptr, mean, sigma, 1.5, pedescut_trsh, max_charge);
	    mean = makeCalib(th1_hist_ptr); sigma = 0.;
	    //mean = 0.; sigma = 0.;

	    th1_hist_ptr->GetXaxis()->SetRangeUser(0, max_charge);
	    mean_onemip_value[channel_num] = mean;
	    sigma_onemip_value[channel_num] = sigma;


	    th1_hist_ptr->SetName(Form("%s_%s_onemipamplitude", iFile_name.Data(), channel_name.Data()));
	    th1_hist_ptr->SetTitle(Form("One mip amplitude [file: %s] [ch: %s]", iFile_name.Data(), channel_name.Data()));
	    channel_data_array->Add(th1_hist_ptr);
	    onemip_draw_array->Add(th1_hist_ptr);
	    // =============================================
	}

	// ***************************************************************************

	current_out << Form("\n----- CALCULATION -----\n\n");

	// Calculation result ********************************************************
	// One mip amplitude horizontal events =========
	for(Int_t channel_num = 0; channel_num < total_Channels; channel_num++)
	{
	    TString channel_name = event_data_struct::GetChName(channel_num);
	    current_out << Form("----- Calculating for channel %s -----", channel_name.Data()) << std::endl;
	    if(OUTINFILE) printf("----- Calculating for channel %s -----\n", channel_name.Data());


	    // selections -------------------------------------------
	    TString amplitude_hor_selection = "(1)";
	    TString time_hor_selection = "(1)";
	    for(Int_t iCh = 0; iCh < total_Channels; iCh++)
	    {
		TString iCh_name = event_data_struct::GetChName(iCh);

		if(iCh != channel_num)
		{
//		    amplitude_hor_selection += Form("&&((%s.integral_in_gate - %s.mean_out_gate) > %f)",
//						    iCh_name.Data(), iCh_name.Data(),mean_pedestal_value[iCh] + pedescut_hor_nsigma*sigma_pedestal_value[iCh]);
//		    amplitude_hor_selection += Form("&&((%s.integral_in_gate) > %f)",
//						    iCh_name.Data(), mean_pedestal_value[iCh] + pedescut_hor_nsigma*sigma_pedestal_value[iCh]);
		    amplitude_hor_selection += Form("&&((%s.integral_in_gate) > %f)",
						    iCh_name.Data(), pedescut_trsh_hor);
//		    time_hor_selection += Form("&&(%s.time_max_in_gate > %f)&&(%s.time_max_in_gate < %f)",
//					       iCh_name.Data(), mean_time_value[iCh] - timeselection_hor_nsigma*sigma_time_value[iCh],
//					       iCh_name.Data(), mean_time_value[iCh] + timeselection_hor_nsigma*sigma_time_value[iCh]);
		}

		if(0)
		    //if(iCh != channel_num)
		    if(iCh == channel_num)
		    {
			time_hor_selection += Form("&&(%s.time_max_in_gate > %f)&&(%s.time_max_in_gate < %f)",
						   iCh_name.Data(), mean_time_value[iCh] - timeselection_hor_nsigma*sigma_time_value[iCh],
						   iCh_name.Data(), mean_time_value[iCh] + timeselection_hor_nsigma*sigma_time_value[iCh]);
		    }
	    }


	    Int_t section_num = -1, section_left, channel_left, section_right, channel_right,  section_near, channel_near;
	    for(Int_t sec = 0; sec < total_Channels; sec++)
		if(get_ch_for_section[sec] == channel_num)
		    section_num = sec;

	    section_left = section_num - 1;
	    section_right = section_num + 1;
	    section_near = (section_num == total_Channels-1) ? section_left: section_right; //R
	    //section_near = (section_num == 0) ? section_right:section_left;		    //L

	    channel_left = (section_left < 0)? -1 : get_ch_for_section[section_left];
	    channel_right = (section_right > total_Channels-1)? -1 : get_ch_for_section[section_right];
	    channel_near = get_ch_for_section[section_near];

	    current_out << Form("ch %i sec %i;  lch %i lsec %i; rch %i rsec %i; nch %i nsec %i \n",
				channel_num, section_num, channel_left, section_left, channel_right, section_right,channel_near,  section_near);

	    TString channel_near_name = event_data_struct::GetChName(channel_near);
//	    TString nearby_selection = Form("((%s.integral_in_gate - %s.mean_out_gate) > %f)&&((%s.integral_in_gate - %s.mean_out_gate) < %f)&&(%s.time_max_in_gate > %f)&&(%s.time_max_in_gate < %f)&&(%s.time_max_in_gate > %f)&&(%s.time_max_in_gate < %f)",
//					    channel_near_name.Data(), channel_near_name.Data(), mean_pedestal_value[channel_near] + pedescut_near_nsigma*sigma_pedestal_value[channel_near],
//					    channel_near_name.Data(), channel_near_name.Data(),mean_onemip_value[channel_num] + ampl_max_near_cut,
//					    channel_near_name.Data(), mean_time_value[channel_near] - timeselection_near_nsigma*sigma_time_value[channel_near],
//					    channel_near_name.Data(), mean_time_value[channel_near] + timeselection_near_nsigma*sigma_time_value[channel_near],
//					    channel_name.Data(), mean_time_value[channel_num] - timeselection_nsigma*sigma_time_value[channel_num],
//					    channel_name.Data(), mean_time_value[channel_num] + timeselection_nsigma*sigma_time_value[channel_num]);
//	    TString nearby_selection = Form("((%s.integral_in_gate) > %f)&&((%s.integral_in_gate) < %f)&&(%s.time_max_in_gate > %f)&&(%s.time_max_in_gate < %f)&&(%s.time_max_in_gate > %f)&&(%s.time_max_in_gate < %f)",
//					    channel_near_name.Data(), mean_pedestal_value[channel_near] + pedescut_near_nsigma*sigma_pedestal_value[channel_near],
//					    channel_near_name.Data(), mean_onemip_value[channel_num] + ampl_max_near_cut,
//					    channel_near_name.Data(), mean_time_value[channel_near] - timeselection_near_nsigma*sigma_time_value[channel_near],
//					    channel_near_name.Data(), mean_time_value[channel_near] + timeselection_near_nsigma*sigma_time_value[channel_near],
//					    channel_name.Data(), mean_time_value[channel_num] - timeselection_nsigma*sigma_time_value[channel_num],
//					    channel_name.Data(), mean_time_value[channel_num] + timeselection_nsigma*sigma_time_value[channel_num]);
	    TString nearby_selection = Form("((%s.integral_in_gate) > %f)&&((%s.integral_in_gate) < %f)&&(%s.time_max_in_gate > %f)&&(%s.time_max_in_gate < %f)&&(%s.time_max_in_gate > %f)&&(%s.time_max_in_gate < %f)",
					    channel_near_name.Data(), pedescut_trsh,
					    channel_near_name.Data(), mean_onemip_value[channel_num] + ampl_max_near_cut,
					    //channel_near_name.Data(), 10000.,
									    channel_near_name.Data(), mean_time_value[channel_near] - timeselection_near_nsigma*sigma_time_value[channel_near],
					    channel_near_name.Data(), mean_time_value[channel_near] + timeselection_near_nsigma*sigma_time_value[channel_near],
					    channel_name.Data(), mean_time_value[channel_num] - timeselection_nsigma*sigma_time_value[channel_num],
					    channel_name.Data(), mean_time_value[channel_num] + timeselection_nsigma*sigma_time_value[channel_num]);

	    TString nearby2_selection = "(0)";
	    if((channel_left != -1)&&(channel_right != -1))
	    {
		TString channel_left_name = event_data_struct::GetChName(channel_left);
		TString channel_right_name = event_data_struct::GetChName(channel_right);
		nearby2_selection = Form("(%s.time_max_in_gate > %f)&&(%s.time_max_in_gate < %f)&&(%s.time_max_in_gate > %f)&&(%s.time_max_in_gate < %f)&&(%s.time_max_in_gate > %f)&&(%s.time_max_in_gate < %f)",
					 //channel_near_name.Data(), channel_near_name.Data(), mean_pedestal_value[channel_near] + pedescut_nsigma*sigma_pedestal_value[channel_near],
					 channel_left_name.Data(), mean_time_value[channel_left] - timeselection_nsigma*sigma_time_value[channel_left],
					 channel_left_name.Data(), mean_time_value[channel_left] + timeselection_nsigma*sigma_time_value[channel_left],
					 channel_right_name.Data(), mean_time_value[channel_right] - timeselection_nsigma*sigma_time_value[channel_right],
					 channel_right_name.Data(), mean_time_value[channel_right] + timeselection_nsigma*sigma_time_value[channel_right],
					 channel_name.Data(), mean_time_value[channel_num] - timeselection_nsigma*sigma_time_value[channel_num],
					 channel_name.Data(), mean_time_value[channel_num] + timeselection_nsigma*sigma_time_value[channel_num]);

//		nearby2_selection = Form("((%s.integral_in_gate - %s.mean_out_gate) > %f)&&((%s.integral_in_gate - %s.mean_out_gate) > %f)",
//					 channel_left_name.Data(), channel_left_name.Data(), 25.,
//					 channel_right_name.Data(), channel_right_name.Data(), 25.);
		nearby2_selection = Form("((%s.integral_in_gate) > %f)&&((%s.integral_in_gate) > %f)",
					 channel_left_name.Data(), pedescut_trsh,
					 channel_right_name.Data(), pedescut_trsh);
	    }

	    current_out << Form("amplitude selection: %s\n\n", amplitude_hor_selection.Data());
	    current_out << Form("time selection: %s\n\n", time_hor_selection.Data());
	    current_out << Form("nearby selection: %s\n\n", nearby_selection.Data());
	    current_out << Form("nearby2 selection: %s\n", nearby2_selection.Data()) << std::endl;
	    // ------------------------------------------------------




	    th1_hist_ptr = new TH1F("temp", "temp", max_charge_bin, 0, max_charge);
//	    data_tree->Draw(Form("(%s.integral_in_gate - %s.mean_out_gate)>>temp", channel_name.Data(), channel_name.Data()),
//			    (amplitude_hor_selection + "&&" + time_hor_selection).Data(), "", calculation_n_events, draw_first_entery);
	    data_tree->Draw(Form("(%s.integral_in_gate)>>temp", channel_name.Data()),
			    (amplitude_hor_selection + "&&" + time_hor_selection).Data(), "", calculation_n_events, draw_first_entery);

	    th1_hist_ptr->SetName(Form("%s_%s_onemiphoramplitude", iFile_name.Data(), channel_name.Data()));
//	    th1_hist_ptr->SetTitle(Form("One mip amplitude [horizontal pedesns: %.1f, timens: %.1f] [file: %s] [ch: %s] [hor pdes cut nsigma: %.1f]",
//					pedescut_hor_nsigma, timeselection_hor_nsigma,  iFile_name.Data(), channel_name.Data(), pedescut_hor_nsigma));
	    th1_hist_ptr->SetTitle(Form("One mip amplitude [horizontal] [file: %s] [ch: %s]",
					iFile_name.Data(), channel_name.Data()));
	    ((TObjArray*)file_data_array->FindObject(channel_name.Data()))->Add(th1_hist_ptr);
	    onemiphor_draw_array->Add(th1_hist_ptr);



	    th1_hist_ptr = new TH1F("temp", "temp", max_charge_bin, 0, max_charge);
//	    data_tree->Draw(Form("(%s.integral_in_gate - %s.mean_out_gate)>>temp", channel_name.Data(), channel_name.Data()),
//			    (nearby_selection).Data(), "", calculation_n_events, draw_first_entery);
	    data_tree->Draw(Form("(%s.integral_in_gate)>>temp", channel_name.Data()),
			    (nearby_selection).Data(), "", calculation_n_events, draw_first_entery);

	    th1_hist_ptr->SetName(Form("%s_%s_onemipnearamplitude", iFile_name.Data(), channel_name.Data()));
//	    th1_hist_ptr->SetTitle(Form("One mip amplitude [nearby pedesns: %.1f timens: %.1f timenearns: %.1f amplmaxcut: %.f] [file: %s] [ch: %s] [hor pdes cut nsigma: %.1f]",
//					pedescut_near_nsigma, timeselection_nsigma, timeselection_near_nsigma, ampl_max_near_cut,iFile_name.Data(), channel_name.Data(), pedescut_hor_nsigma));
	    th1_hist_ptr->SetTitle(Form("One mip amplitude [nearby] [file: %s] [ch: %s]", iFile_name.Data(), channel_name.Data()));
	    ((TObjArray*)file_data_array->FindObject(channel_name.Data()))->Add(th1_hist_ptr);
	    onemipnear_draw_array->Add(th1_hist_ptr);




	    th1_hist_ptr = new TH1F("temp", "temp", max_charge_bin, 0, max_charge);
//	    data_tree->Draw(Form("(%s.integral_in_gate - %s.mean_out_gate)>>temp", channel_name.Data(), channel_name.Data()),
//			    (nearby2_selection).Data(), "", calculation_n_events, draw_first_entery);
	    data_tree->Draw(Form("(%s.integral_in_gate)>>temp", channel_name.Data()),
			    (nearby2_selection).Data(), "", calculation_n_events, draw_first_entery);

	    th1_hist_ptr->SetName(Form("%s_%s_onemipnear2amplitude", iFile_name.Data(), channel_name.Data()));
//	    th1_hist_ptr->SetTitle(Form("One mip amplitude [nearby two sides pedesns: %.1f timens: %.1f] [file: %s] [ch: %s] [hor pdes cut nsigma: %.1f]",
//					pedescut_nsigma, timeselection_nsigma, iFile_name.Data(), channel_name.Data(), pedescut_hor_nsigma));
	    th1_hist_ptr->SetTitle(Form("One mip amplitude [nearby two sides] [file: %s] [ch: %s]", iFile_name.Data(), channel_name.Data()));
	    ((TObjArray*)file_data_array->FindObject(channel_name.Data()))->Add(th1_hist_ptr);
	    onemipnear2_draw_array->Add(th1_hist_ptr);
	}
	// =============================================



	// Build canvas ================================
	TObjArray draw_array;
	for(Int_t channel_num = 0; channel_num < total_Channels; channel_num++)
	{
	    draw_array.Add( onemip_draw_array->At(channel_num) );
	    draw_array.Add( onemiphor_draw_array->At(channel_num) );
	    draw_array.Add( onemipnear_draw_array->At(channel_num) );
	    draw_array.Add( onemipnear2_draw_array->At(channel_num) );
	}

	file_data_array->AddAll(
		    HDraw(&draw_array, Form("Canv_onemip_%i", iFile), 3, 4, 4, 0,1000,1)
		    );
	// =============================================



	// ***************************************************************************




	delete[] mean_pedestal_value;
	delete[] sigma_pedestal_value;
	delete[] mean_time_value;
	delete[] sigma_time_value;
	delete[] mean_onemip_value;
	delete[] sigma_onemip_value;

	delete onemip_draw_array;
	delete onemiphor_draw_array;
	delete onemipnear_draw_array;
	delete onemipnear2_draw_array;
	//###############################################################################


	source_file->Close();
	delete source_file;
    }


    time_t end_time = time(NULL);
    current_out << Form("Analysis finished at %s", asctime( localtime( &end_time ) )) << std::endl;



    // Saving results ===============
    SaveArrayStructInFile(result_array, result_file);
    //===============================

    //copy log file
    if(OUTINFILE)
	gROOT->ProcessLine( Form(".cp CurrentLog.txt %s",(result_path + result_file_name + "_log.txt").Data()) );

    gROOT->ProcessLine( Form(".cp analysis_macro.c %s", (result_path + result_file_name + "_code.c").Data()) );

    delete temp_canv;
    delete result_array;
    result_file->Close();
    delete result_file;

    new TBrowser;
}











