#ifndef DATA_CALCULATING
#define DATA_CALCULATING

#include <TTree.h>
#include <TObjArray.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TF1.h>
#include <TCanvas.h>

#include "event_data_struct.h"

/*
 * calculate preview historgamms and canvas
 */

struct result_calculation
{
    Float_t ZeroLevelRMS_mean;
    Float_t ZeroLevelRMS_sigma;
    Float_t ZeroLevelMean_mean;
    Float_t ZeroLevelMean_sigma;
    Float_t Time_peak_mean;
    Float_t Time_peak_sigma;
    Float_t Pedestal_mean;
    Float_t Pedestal_sigma;
    Float_t LED_mean;
    Float_t LED_sigma;
};


class data_calculation
{
public:
    data_calculation(TTree *data_tree_);
    ~data_calculation();

    void Calculate(Int_t ch_to_calc = 0);

    TObjArray *Get_Results(){return Res_Collection;}
    TObjArray *Get_Canvas(){return Canv_Collection;}

private:
    TTree *data_tree;
    Int_t channels_total;

    event_data_struct *event_struct;
    result_calculation *result_struct;

    TObjArray *Res_Collection;
    TObjArray *Canv_Collection;

    void PrevCalc(Int_t channel_num);

    void BuildCanvas();

    void TH2AxisNorm(TH2* hist);
};

// #####################################################################
data_calculation::data_calculation(TTree *data_tree_)
    :data_tree(data_tree_)
{
    Res_Collection = new TObjArray();
    Res_Collection->SetOwner();

    Canv_Collection = new TObjArray();
    Canv_Collection->SetOwner();

    channels_total = data_tree->GetNbranches()-1; //log branch
    event_struct = new event_data_struct[channels_total];
    result_struct = new result_calculation[channels_total];

    for(Int_t ch=0; ch<channels_total; ch++)
    {
	event_struct[ch].SetBranch(data_tree, ch);

	TObjArray *curr_arr = new TObjArray();
	curr_arr->SetName(event_struct[ch].GetChName(ch).Data());
	curr_arr->SetOwner();
	Res_Collection->Add(curr_arr);
    }
}

// #####################################################################
data_calculation::~data_calculation()
{
    delete[] event_struct;
    delete[] result_struct;
    delete Res_Collection;
    delete Canv_Collection;

}

// #####################################################################
void data_calculation::Calculate(Int_t ch_to_calc)
{
    if(ch_to_calc == 0)ch_to_calc = channels_total;

    for(Int_t channel=0; channel<ch_to_calc; channel++)
    {
	Printf("Preview calculation for channel %i\n", channel);
	PrevCalc(channel);
    }

    BuildCanvas();
}

// #####################################################################
void data_calculation::PrevCalc(Int_t channel_num)
{
    event_data_struct &event_struct_curr = event_struct[channel_num];
    TString ch_name = event_struct_curr.GetChName(channel_num);
    TObjArray *hist_arr = (TObjArray*)(Res_Collection->FindObject(ch_name.Data()));




    TH1 *TH1_ptr = NULL;
    TH2 *TH2_ptr = NULL;
    Double_t hist_max, hist_RMS, hist_mean;

    TF1 *fit_gaus = new TF1("gaus_func","gaus");
    TCanvas *work_canv = new TCanvas("work", "work");


    // Parameters ===============
    Float_t TimeStamp_max = 500000;//data_tree->GetMaximum(Form("%s.time_stamp", ch_name.Data()));
    Float_t Tot_Charge_max = 1000000;
    Float_t Tot_Charge_min = -100;
    Float_t LED_Charge_min = 400000;
    // ==========================

    // temp trend ------------------------------
    TH2_ptr = new TH2F("name_temp", "title_temp", 1000, 0, TimeStamp_max, 1000, 0, 300);
    data_tree->Draw(Form("(log_data.temp*0.01):%s.time_stamp>>name_temp", ch_name.Data()));

    TH2_ptr->SetName(Form("temp_trend_%s",ch_name.Data()));
    TH2_ptr->SetTitle( Form("Temperature trend [%s];time, sec; C", ch_name.Data()) );
    TH2AxisNorm(TH2_ptr);
    hist_arr->Add(TH2_ptr);

    // LED_HV trend ------------------------------
    TH2_ptr = new TH2F("name_temp", "title_temp", 1000, 0, TimeStamp_max, 1000, 0, 5000);
    data_tree->Draw(Form("log_data.LED_HV:%s.time_stamp>>name_temp", ch_name.Data()));

    TH2_ptr->SetName(Form("LEDHV_trend_%s",ch_name.Data()));
    TH2_ptr->SetTitle( Form("LED HV trend [%s];time, sec; C", ch_name.Data()) );
    TH2AxisNorm(TH2_ptr);
    hist_arr->Add(TH2_ptr);

    // PinDiode trend ------------------------------
    TH2_ptr = new TH2F("name_temp", "title_temp", 1000, 0, TimeStamp_max, 100, 0, 100);
    data_tree->Draw(Form("log_data.PinDiode:%s.time_stamp>>name_temp", ch_name.Data()));

    TH2_ptr->SetName(Form("PinDiode_trend_%s",ch_name.Data()));
    TH2_ptr->SetTitle( Form("PinDiode trend [%s];time, sec; C", ch_name.Data()) );
    TH2AxisNorm(TH2_ptr);
    hist_arr->Add(TH2_ptr);


    // level RMS trend ------------------------------
    TH2_ptr = new TH2F("name_temp", "title_temp", 1000, 0, TimeStamp_max, 1000, 0, 300);
    data_tree->Draw(Form("%s.RMS_out_gate:%s.time_stamp>>name_temp", ch_name.Data(), ch_name.Data()));

    TH2_ptr->SetName(Form("zero_lev_RMS_trend_%s",ch_name.Data()));
    TH2_ptr->SetTitle( Form("Zero level RMS Trend [%s];time, sec; RMS", ch_name.Data()) );
    TH2AxisNorm(TH2_ptr);
    hist_arr->Add(TH2_ptr);


    // level Mean trend ------------------------------
    TH2_ptr = new TH2F("name_temp", "title_temp", 1000, 0, TimeStamp_max, 1000, -33000, -30000);
    data_tree->Draw(Form("%s.mean_out_gate:%s.time_stamp>>name_temp", ch_name.Data(), ch_name.Data()));

    TH2_ptr->SetName(Form("zero_lev_mean_trend_%s",ch_name.Data()));
    TH2_ptr->SetTitle( Form("Zero level mean Trend [%s];time, sec; zero level, ch", ch_name.Data()) );
    TH2AxisNorm(TH2_ptr);
    hist_arr->Add(TH2_ptr);

    // Level mean fit ------------------------------
    TH1_ptr = new TH1F("name_temp", "title_temp", 1000, -33000, -30000);
    data_tree->Draw(Form("%s.mean_out_gate>>name_temp", ch_name.Data()));

    hist_max = TH1_ptr->GetBinCenter( TH1_ptr->GetMaximumBin() );
    TH1_ptr->GetXaxis()->SetRangeUser(hist_max -10., hist_max +10.);
    hist_RMS = TH1_ptr->GetRMS();
    fit_gaus->SetRange(hist_max -hist_RMS*2., hist_max +hist_RMS*2.);
    TH1_ptr->Fit(fit_gaus, "QR");
    result_struct[channel_num].ZeroLevelMean_mean = fit_gaus->GetParameter(1);
    result_struct[channel_num].ZeroLevelMean_sigma = fit_gaus->GetParameter(2);

    TH1_ptr->SetName(Form("zero_lev_mean_%s",ch_name.Data()));
    TH1_ptr->SetTitle( Form("Zero level mean [%s] [mean %.1f sigma %.1f];zero level, ch",
			    ch_name.Data(), result_struct[channel_num].ZeroLevelMean_mean, result_struct[channel_num].ZeroLevelMean_sigma) );
    hist_arr->Add(TH1_ptr);


    // Level RMS fit------------------------------
    TH1_ptr = new TH1F("name_temp", "title_temp", 1000, 0, 1000);
    data_tree->Draw(Form("%s.RMS_out_gate>>name_temp", ch_name.Data()));

    hist_max = TH1_ptr->GetBinCenter( TH1_ptr->GetMaximumBin() );
    TH1_ptr->GetXaxis()->SetRangeUser(hist_max -10., hist_max +10.);
    hist_RMS = TH1_ptr->GetRMS();
    fit_gaus->SetRange(hist_max -hist_RMS*2., hist_max +hist_RMS*2.);
    TH1_ptr->Fit(fit_gaus, "QR");
    result_struct[channel_num].ZeroLevelRMS_mean = fit_gaus->GetParameter(1);
    result_struct[channel_num].ZeroLevelRMS_sigma = fit_gaus->GetParameter(2);
//    printf("ZeroLevelRMS_mean %f\n", result_struct[channel_num].ZeroLevelRMS_mean);
//    printf(" ZeroLevelRMS_sigma %f\n", result_struct[channel_num].ZeroLevelRMS_sigma);

    TH1_ptr->SetName(Form("zero_lev_RMS_%s",ch_name.Data()));
    TH1_ptr->SetTitle( Form("Zero level RMS [%s] [mean %.1f sigma %.1f]; zero level RMS",
			    ch_name.Data(), result_struct[channel_num].ZeroLevelRMS_mean, result_struct[channel_num].ZeroLevelRMS_sigma) );
    hist_arr->Add(TH1_ptr);


    // charge vs time ------------------------------
    TH2_ptr = new TH2F("name_temp", "title_temp", 1000, Tot_Charge_min, Tot_Charge_max, 200, 0, 200);
    //data_tree->Draw(Form("%s.time_max_in_gate:(%s.integral_in_gate - %s.mean_out_gate)>>name_temp", ch_name.Data(), ch_name.Data(), ch_name.Data()));
    data_tree->Draw(Form("%s.time_max_in_gate:(%s.integral_in_gate)>>name_temp", ch_name.Data(), ch_name.Data()));

    TH2_ptr->SetName(Form("charge_time_%s",ch_name.Data()));
    TH2_ptr->SetTitle( Form("Signal charge vs time [%s];charge; time", ch_name.Data()) );
    TH2AxisNorm(TH2_ptr);
    hist_arr->Add(TH2_ptr);


    // charge vs amplitude ------------------------------
    TH2_ptr = new TH2F("name_temp", "title_temp", 1000, Tot_Charge_min, Tot_Charge_max, 3200, -32000, 32000);
    //data_tree->Draw(Form("%s.MAX_in_gate:(%s.integral_in_gate - %s.mean_out_gate)>>name_temp", ch_name.Data(), ch_name.Data(), ch_name.Data()));
    data_tree->Draw(Form("%s.MAX_in_gate:(%s.integral_in_gate)>>name_temp", ch_name.Data(), ch_name.Data()));

    TH2_ptr->SetName(Form("charge_ampl_%s",ch_name.Data()));
    TH2_ptr->SetTitle( Form("Signal charge vs amplitude [%s];charge; time", ch_name.Data()) );
    TH2AxisNorm(TH2_ptr);
    hist_arr->Add(TH2_ptr);


    // charge vs level Mean ------------------------------
    TH2_ptr = new TH2F("name_temp", "title_temp", 1000, Tot_Charge_min, Tot_Charge_max, 3200, -33000, -30000);
    //data_tree->Draw(Form("%s.mean_out_gate:(%s.integral_in_gate - %s.mean_out_gate)>>name_temp", ch_name.Data(), ch_name.Data(), ch_name.Data()));
    data_tree->Draw(Form("%s.mean_out_gate:(%s.integral_in_gate)>>name_temp", ch_name.Data(), ch_name.Data()));

    TH2_ptr->SetName(Form("charge_zero_level_%s",ch_name.Data()));
    TH2_ptr->SetTitle( Form("Signal charge vs zero level [%s];charge; zero level", ch_name.Data()) );
    TH2AxisNorm(TH2_ptr);
    hist_arr->Add(TH2_ptr);


    // charge vs level RMS------------------------------
    TH2_ptr = new TH2F("name_temp", "title_temp", 1000, Tot_Charge_min, Tot_Charge_max, 1000, 0, 300);
    //data_tree->Draw(Form("%s.RMS_out_gate:(%s.integral_in_gate - %s.mean_out_gate)>>name_temp", ch_name.Data(), ch_name.Data(), ch_name.Data()));
    data_tree->Draw(Form("%s.RMS_out_gate:(%s.integral_in_gate)>>name_temp", ch_name.Data(), ch_name.Data()));

    TH2_ptr->SetName(Form("charge_lvlRMS_%s",ch_name.Data()));
    TH2_ptr->SetTitle( Form("Signal charge vs zero level RMS [%s];charge; time", ch_name.Data()) );
    TH2AxisNorm(TH2_ptr);
    hist_arr->Add(TH2_ptr);



    // Time fit------------------------------
    TH1_ptr = new TH1F("name_temp", "title_temp", 1000, 0, 1000);
    data_tree->Draw(Form("%s.time_max_in_gate>>name_temp", ch_name.Data()));

    hist_max = TH1_ptr->GetBinCenter( TH1_ptr->GetMaximumBin() );
    TH1_ptr->GetXaxis()->SetRangeUser(hist_max -10., hist_max +10.);
    hist_RMS = TH1_ptr->GetRMS();
    fit_gaus->SetRange(hist_max -hist_RMS*2., hist_max +hist_RMS*2.0);
    TH1_ptr->Fit(fit_gaus, "QR");
    result_struct[channel_num].Time_peak_mean = fit_gaus->GetParameter(1);
    result_struct[channel_num].Time_peak_sigma = fit_gaus->GetParameter(2);

//    printf("Time_peak_mean %f\n", result_struct[channel_num].Time_peak_mean);
//    printf("Time_peak_sigma %f\n", result_struct[channel_num].Time_peak_sigma);

    TH1_ptr->SetName(Form("signal_time_%s",ch_name.Data()));
    TH1_ptr->SetTitle( Form("Time of peak [%s] [mean %.1f sigma %.1f];time",
			    ch_name.Data(), result_struct[channel_num].Time_peak_mean, result_struct[channel_num].Time_peak_sigma) );
    hist_arr->Add(TH1_ptr);


    // Pedestal ------------------------------
    TH1_ptr = new TH1F("name_temp", "title_temp", 1000, Tot_Charge_min, LED_Charge_min);
//    data_tree->Draw(Form("(%s.integral_in_gate - %s.mean_out_gate)>>name_temp", ch_name.Data(), ch_name.Data()),
//		    Form("(%s.time_max_in_gate > %f)||(%s.time_max_in_gate < %f)",
//			 ch_name.Data(), result_struct[channel_num].Time_peak_mean + result_struct[channel_num].Time_peak_sigma,
//			 ch_name.Data(), result_struct[channel_num].Time_peak_mean - result_struct[channel_num].Time_peak_sigma));

    data_tree->Draw(Form("(%s.integral_in_gate)>>name_temp", ch_name.Data()),
		    Form("(%s.time_max_in_gate > %f)||(%s.time_max_in_gate < %f)",
			 ch_name.Data(), result_struct[channel_num].Time_peak_mean + result_struct[channel_num].Time_peak_sigma,
			 ch_name.Data(), result_struct[channel_num].Time_peak_mean - result_struct[channel_num].Time_peak_sigma));

    hist_max = TH1_ptr->GetBinCenter( TH1_ptr->GetMaximumBin() );
    TH1_ptr->GetXaxis()->SetRangeUser(hist_max -30., hist_max +30.);
    hist_RMS = TH1_ptr->GetRMS();
    hist_mean = TH1_ptr->GetMean();
    fit_gaus->SetRange(hist_mean -hist_RMS*2., hist_mean +hist_RMS*2.);
    TH1_ptr->Fit(fit_gaus, "QR");
    result_struct[channel_num].Pedestal_mean = fit_gaus->GetParameter(1);
    result_struct[channel_num].Pedestal_sigma = fit_gaus->GetParameter(2);

    TH1_ptr->SetName(Form("charge_pedestal_%s",ch_name.Data()));
    TH1_ptr->SetTitle( Form("Charge Pedestal [%s] [selection: time] [mean %.1f sigma %.1f]",
			    ch_name.Data(), result_struct[channel_num].Pedestal_mean, result_struct[channel_num].Pedestal_sigma) );
    hist_arr->Add(TH1_ptr);


    // Charge ------------------------------
    TH1_ptr = new TH1F("name_temp", "title_temp", 1000, Tot_Charge_min, LED_Charge_min);
//    data_tree->Draw(Form("(%s.integral_in_gate - %s.mean_out_gate)>>name_temp", ch_name.Data(), ch_name.Data()),
//		    Form("(%s.time_max_in_gate < %f)&&(%s.time_max_in_gate > %f)",
//			 ch_name.Data(), (result_struct[channel_num].Time_peak_mean + result_struct[channel_num].Time_peak_sigma),
//			 ch_name.Data(), (result_struct[channel_num].Time_peak_mean - result_struct[channel_num].Time_peak_sigma)));

    data_tree->Draw(Form("(%s.integral_in_gate)>>name_temp", ch_name.Data()),
		    Form("(%s.time_max_in_gate < %f)&&(%s.time_max_in_gate > %f)",
			 ch_name.Data(), (result_struct[channel_num].Time_peak_mean + result_struct[channel_num].Time_peak_sigma),
			 ch_name.Data(), (result_struct[channel_num].Time_peak_mean - result_struct[channel_num].Time_peak_sigma)));

//    printf("(%s.time_max_in_gate < %f)&&(%s.time_max_in_gate > %f)\n",
//	   ch_name.Data(), (result_struct[channel_num].Time_peak_mean + result_struct[channel_num].Time_peak_sigma),
//	   ch_name.Data(), (result_struct[channel_num].Time_peak_mean - result_struct[channel_num].Time_peak_sigma));

    TH1_ptr->SetName(Form("charge_%s",ch_name.Data()));
    TH1_ptr->SetTitle( Form("Charge [%s] [selection: time]", ch_name.Data()) );
    hist_arr->Add(TH1_ptr);


    // LED ampl trend ------------------------------
    TH2_ptr = new TH2F("name_temp", "title_temp", 1000, 0, TimeStamp_max, 1000, LED_Charge_min, Tot_Charge_max);
//    data_tree->Draw(Form("(%s.integral_in_gate - %s.mean_out_gate):%s.time_stamp>>name_temp", ch_name.Data(), ch_name.Data(), ch_name.Data()),
//		    Form("(%s.time_max_in_gate < %f)&&(%s.time_max_in_gate > %f)&&(%s.RMS_out_gate < %f)",
//			 ch_name.Data(), (result_struct[channel_num].Time_peak_mean + result_struct[channel_num].Time_peak_sigma),
//			 ch_name.Data(), (result_struct[channel_num].Time_peak_mean - result_struct[channel_num].Time_peak_sigma),
//			 ch_name.Data(), (result_struct[channel_num].ZeroLevelRMS_mean + result_struct[channel_num].ZeroLevelRMS_sigma)	 ));

    data_tree->Draw(Form("(%s.integral_in_gate):%s.time_stamp>>name_temp", ch_name.Data(), ch_name.Data()),
		    Form("(%s.time_max_in_gate < %f)&&(%s.time_max_in_gate > %f)&&(%s.RMS_out_gate < %f)",
			 ch_name.Data(), (result_struct[channel_num].Time_peak_mean + result_struct[channel_num].Time_peak_sigma),
			 ch_name.Data(), (result_struct[channel_num].Time_peak_mean - result_struct[channel_num].Time_peak_sigma),
			 ch_name.Data(), (result_struct[channel_num].ZeroLevelRMS_mean + result_struct[channel_num].ZeroLevelRMS_sigma)	 ));

    TH2_ptr->SetName(Form("LED_mean_trend_%s",ch_name.Data()));
    TH2_ptr->SetTitle( Form("LED charge Trend [%s] [selection: time, RMS];time, sec; LED ampl, ch", ch_name.Data()) );
    TH2AxisNorm(TH2_ptr);
    hist_arr->Add(TH2_ptr);


    //    //che/ch = mean/sigma^2
    //LED ------------------------------
    TH1_ptr = new TH1F("name_temp", "title_temp", 1000, LED_Charge_min, Tot_Charge_max);
//    data_tree->Draw(Form("(%s.integral_in_gate - %s.mean_out_gate)>>name_temp", ch_name.Data(), ch_name.Data()),
//		    Form("(%s.time_max_in_gate < %f)&&(%s.time_max_in_gate > %f)&&(%s.RMS_out_gate < %f)",
//			 ch_name.Data(), (result_struct[channel_num].Time_peak_mean + result_struct[channel_num].Time_peak_sigma),
//			 ch_name.Data(), (result_struct[channel_num].Time_peak_mean - result_struct[channel_num].Time_peak_sigma),
//			 ch_name.Data(), (result_struct[channel_num].ZeroLevelRMS_mean + result_struct[channel_num].ZeroLevelRMS_sigma)	 ));

    data_tree->Draw(Form("(%s.integral_in_gate)>>name_temp", ch_name.Data()),
		    Form("(%s.time_max_in_gate < %f)&&(%s.time_max_in_gate > %f)&&(%s.RMS_out_gate < %f)",
			 ch_name.Data(), (result_struct[channel_num].Time_peak_mean + result_struct[channel_num].Time_peak_sigma),
			 ch_name.Data(), (result_struct[channel_num].Time_peak_mean - result_struct[channel_num].Time_peak_sigma),
			 ch_name.Data(), (result_struct[channel_num].ZeroLevelRMS_mean + result_struct[channel_num].ZeroLevelRMS_sigma)	 ));

    hist_max = TH1_ptr->GetBinCenter( TH1_ptr->GetMaximumBin() );
    TH1_ptr->GetXaxis()->SetRangeUser(hist_max -300., hist_max +300.);
    hist_RMS = TH1_ptr->GetRMS();
    hist_mean = TH1_ptr->GetMean();
    fit_gaus->SetRange(hist_mean -hist_RMS*2., hist_mean +hist_RMS*2.);
    TH1_ptr->Fit(fit_gaus, "QR");
    result_struct[channel_num].LED_mean = fit_gaus->GetParameter(1);
    result_struct[channel_num].LED_sigma = fit_gaus->GetParameter(2);

    TH1_ptr->SetName(Form("LED_%s",ch_name.Data()));
    TH1_ptr->SetTitle( Form("LED charge [%s] [selection: time, RMS] [mean %.1f, sigma %.1f, (m-p)/s/s %.1f]",
			    ch_name.Data(), result_struct[channel_num].LED_mean, result_struct[channel_num].LED_sigma,
			    ((result_struct[channel_num].LED_mean-result_struct[channel_num].Pedestal_mean)/result_struct[channel_num].LED_sigma/result_struct[channel_num].LED_sigma)) );
    hist_arr->Add(TH1_ptr);



    delete fit_gaus;
    delete work_canv;
}

// #####################################################################
void data_calculation::BuildCanvas()
{
    for(Int_t channel = 0; channel < channels_total; channel++)
    {
	TString ch_name = event_struct[channel].GetChName(channel);
	TObjArray *hist_arr_ch = (TObjArray*)(Res_Collection->FindObject( ch_name.Data()));
	if(hist_arr_ch == NULL) continue;
	if(hist_arr_ch->GetLast() < 0) continue;
	TCanvas *Canv_curr = new TCanvas(Form("%s_prev", ch_name.Data()), Form("%s_prev", ch_name.Data()));
	Canv_curr->Divide(4, 4);
	Canv_Collection->Add(Canv_curr);

	TH1* hist_curr;
	Int_t cc = 1;


	hist_curr = (TH1*)(hist_arr_ch->FindObject(Form("temp_trend_%s",ch_name.Data())));
	if(hist_curr == NULL) continue;
	Canv_curr->cd(cc++);
	hist_curr->Draw("col");

	hist_curr = (TH1*)(hist_arr_ch->FindObject(Form("LEDHV_trend_%s",ch_name.Data())));
	if(hist_curr == NULL) continue;
	Canv_curr->cd(cc++);
	hist_curr->Draw("col");

	hist_curr = (TH1*)(hist_arr_ch->FindObject(Form("PinDiode_trend_%s",ch_name.Data())));
	if(hist_curr == NULL) continue;
	Canv_curr->cd(cc++);
	hist_curr->Draw("col");

	hist_curr = (TH1*)(hist_arr_ch->FindObject(Form("zero_lev_mean_trend_%s",ch_name.Data())));
	if(hist_curr == NULL) continue;
	Canv_curr->cd(cc++);
	hist_curr->Draw("col");

	hist_curr = (TH1*)(hist_arr_ch->FindObject(Form("zero_lev_mean_%s",ch_name.Data())));
	if(hist_curr == NULL) continue;
	Canv_curr->cd(cc++);
	hist_curr->Draw();

	hist_curr = (TH1*)(hist_arr_ch->FindObject(Form("zero_lev_RMS_trend_%s",ch_name.Data())));
	if(hist_curr == NULL) continue;
	Canv_curr->cd(cc++);
	hist_curr->Draw("col");

	hist_curr = (TH1*)(hist_arr_ch->FindObject(Form("zero_lev_RMS_%s",ch_name.Data())));
	if(hist_curr == NULL) continue;
	Canv_curr->cd(cc++);
	hist_curr->Draw();




	hist_curr = (TH1*)(hist_arr_ch->FindObject(Form("charge_time_%s",ch_name.Data())));
	if(hist_curr == NULL) continue;
	Canv_curr->cd(cc++);
	hist_curr->Draw("col");

	hist_curr = (TH1*)(hist_arr_ch->FindObject(Form("charge_ampl_%s",ch_name.Data())));
	if(hist_curr == NULL) continue;
	Canv_curr->cd(cc++);
	hist_curr->Draw("col");

	hist_curr = (TH1*)(hist_arr_ch->FindObject(Form("charge_zero_level_%s",ch_name.Data())));
	if(hist_curr == NULL) continue;
	Canv_curr->cd(cc++);
	hist_curr->Draw("col");

	hist_curr = (TH1*)(hist_arr_ch->FindObject(Form("charge_lvlRMS_%s",ch_name.Data())));
	if(hist_curr == NULL) continue;
	Canv_curr->cd(cc++);
	hist_curr->Draw("col");

	hist_curr = (TH1*)(hist_arr_ch->FindObject(Form("charge_%s",ch_name.Data())));
	if(hist_curr == NULL) continue;
	Canv_curr->cd(cc++);
	hist_curr->Draw();




	hist_curr = (TH1*)(hist_arr_ch->FindObject(Form("charge_pedestal_%s",ch_name.Data())));
	if(hist_curr == NULL) continue;
	Canv_curr->cd(cc++);
	hist_curr->Draw();

	hist_curr = (TH1*)(hist_arr_ch->FindObject(Form("signal_time_%s",ch_name.Data())));
	if(hist_curr == NULL) continue;
	Canv_curr->cd(cc++);
	hist_curr->Draw();

	hist_curr = (TH1*)(hist_arr_ch->FindObject(Form("LED_mean_trend_%s",ch_name.Data())));
	if(hist_curr == NULL) continue;
	Canv_curr->cd(cc++);
	hist_curr->Draw("col");


	hist_curr = (TH1*)(hist_arr_ch->FindObject(Form("LED_%s",ch_name.Data())));
	if(hist_curr == NULL) continue;
	Canv_curr->cd(cc++);
	hist_curr->Draw();
    }



}


// #####################################################################
void data_calculation::TH2AxisNorm(TH2 *hist)
{
    Int_t X_min = hist->FindFirstBinAbove(1,1);
    Int_t X_max = hist->FindLastBinAbove(1,1);
    Int_t Y_min = hist->FindFirstBinAbove(1,2);
    Int_t Y_max = hist->FindLastBinAbove(1,2);

    hist->GetXaxis()->SetRange(X_min, X_max);
    hist->GetYaxis()->SetRange(Y_min, Y_max);
}

#endif //DATA_CALCULATING
