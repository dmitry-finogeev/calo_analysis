

#include <fstream>
#include <iostream>
#include <TNtuple.h>
#include <TFile.h>
#include <Rtypes.h>
#include <vector>
#include <TString.h>

#include "Readme_reader.h"
#include "event_data_struct.h"
#include "bindataformat.h"



#include "../digdataprocessing/DigMonitor.cpp"
#include "../digdataprocessing/DigVariables.cpp"
#include "../digdataprocessing/DigData.cpp"
#include "../digdataprocessing/DigDataRecipe.cpp"
#include "../digdataprocessing/DigEvent.cpp"
#include "../digdataprocessing/DigSignals.cpp"
#include "../digdataprocessing/DigChannel.cpp"
#include "../digdataprocessing/DigDataAnalysis.cpp"


// !!! last version see in BinChainConverter.c, where possible to convert severals files

void BinToTreeDataConverter(TString run_name, Int_t TotEv = 0, TString source_path = "/mnt/hgfs/DETECTORS_DATA/SOURCE/CALO/T10_RUNS/", TString result_path = "/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_source/T10_test_sep_17/")
{
    ////////////////////////////////////////////////////////////////////////////////////////

    TString log_file_name = source_path + "slowControl.csv";

    TString result_file_prefix = "";

    Int_t gate_beg = 40;
    Int_t gate_end = 130;
    ////////////////////////////////////////////////////////////////////////////////////////


    TString result_file_name = result_path + run_name + result_file_prefix + ".root";


    Readme_reader *readme_reader_ptr = new Readme_reader(source_path);
    Int_t is_readme_opened = readme_reader_ptr->Set_runfile_name(run_name);
    Int_t total_channels = readme_reader_ptr->Get_total_ch();
    Int_t total_boards = readme_reader_ptr->Get_boards_num();
    readme_reader_ptr->Print();

    if(is_readme_opened < 0)
    {
	printf("readme file %s not opened\n", readme_reader_ptr->Get_info_file_path().Data());
	return;
    }

    Log_file_reader *Log_reader_ptr = new Log_file_reader(log_file_name);


    GlobalProcessStatus *globalProcessState = new GlobalProcessStatus();
    DigData *DigData_ptr = new DigData(globalProcessState);
    DigDataRecipe *DataRecipe_ptr = DigData_ptr->GetDataRecipe();
    DataRecipe_ptr->GetMainDetault()->minAmplitude = 0.; // set minimal amplitude no ampl selection)\n
    Int_t Is_DigDataConfigured = DataRecipe_ptr->Configure((source_path + run_name).Data()); //open configuration file, and configure data processing parameters\n
    if(Is_DigDataConfigured)
    {
	DigData_ptr->OpenData();
	DataRecipe_ptr->PrintOut();
    }

    event_log_data_struct *event_log_struct = new event_log_data_struct[total_boards];
    event_data_struct *event_struct = new event_data_struct[total_channels];








    //create result root file, create tree
    TFile *result_file = new TFile(result_file_name.Data(), "RECREATE");

    TTree *data_tree = new TTree("adc64_data", "adc64_data");
    for(Int_t ch = 0; ch < total_channels; ch++)
	(event_struct+ch)->CreateBranch(data_tree, ch);

    Int_t *log_search_index = new Int_t[total_boards];
    for(Int_t brd = 0; brd < total_boards; brd++)
    {
	(event_log_struct+brd)->CreateBranch(data_tree, readme_reader_ptr->Get_boardid(brd));
	log_search_index[brd] = 0;
    }
    if(Is_DigDataConfigured)
	DigData_ptr->GetEvent(0)->SetTreeParam(data_tree);








    //opening source file
    Int_t events_to_process = 0;
    BinDataReader *bin_reader_arr = new BinDataReader[readme_reader_ptr->Get_boards_num()];
    for(Int_t ifile = 0; ifile < total_boards; ifile++)
    {
	bin_reader_arr[ifile].SetFile(readme_reader_ptr->Get_file_name(ifile));
	if(Is_DigDataConfigured)
	    if(bin_reader_arr[ifile].events_total != DigData_ptr->GetEvent(0)->GetChannel(0)->GetTotalEvents())
		printf("WARNING: Adc64 and Digitizer data has different numgers of events\n############################################################################\n");
	if(TotEv == 0)
	{
	    if( (events_to_process == 0) ||
		    (events_to_process >  bin_reader_arr[ifile].events_total) )
		events_to_process = bin_reader_arr[ifile].events_total;
	} else events_to_process = TotEv;
    }







    //reading events
    time_t start_time = time(NULL);

    for(Int_t nevent = 1; nevent <= ( (events_to_process < 1)?bin_reader_arr[0].events_total:events_to_process ); nevent++)
    {

	for(Int_t ifile = 0; ifile < total_boards; ifile++)
	{
	    bin_reader_arr[ifile].ReadEvent(nevent);
	}





	for(Int_t channel = 0; channel < total_channels; channel++)
	{
	    Int_t ch_num, ch_board;
	    readme_reader_ptr->GetBoardCh_from_ContCh(channel, ch_board, ch_num);
	    ch_num--; //in info file map channel 1-128; in file 0-127


	    bin_reader_arr[ch_board].Calculate_waveform(event_struct[channel], ch_num, gate_beg, gate_end);

	    /****
	    printf("Event %i channel %i board %i ch_board %i\n", nevent, channel, ch_board, ch_num);
	    event_struct[channel].Print();
	    /****/
	}




	for(Int_t brd = 0; brd < total_boards; brd++)
	{
	    Long64_t search_timestamp = bin_reader_arr->TimeStamp_date*1000;
	    search_timestamp += bin_reader_arr->TimeStamp_ns*0.000001;
	    //	    Long64_t search_timestamp = bin_reader_arr->TimeStamp_date;
	    Log_reader_ptr->Search_entry(*(event_log_struct+brd), search_timestamp, readme_reader_ptr->Get_boardid(brd), *(log_search_index+brd));
	}




	if(Is_DigDataConfigured)
	    DigData_ptr->GetEvent(nevent-1)->CalculateEvent();



	data_tree->Fill();



	if((nevent%100) == 0)
	{
	    time_t current_time = time(NULL);
	    Int_t proc_sec = difftime(current_time,start_time);
	    Float_t percents = (float)nevent/events_to_process;
	    Int_t time_est = (percents == 0)?0:proc_sec/percents*(1. - percents);
	    Float_t proc_rate = (float)proc_sec/nevent*1000000./60.;

	    printf("Processed events: %i (%5.1f%%); [pas %3.0dm %2.0is] [est %3.0dm %2.0is] [%.1f min/1M ev]\r",
		   nevent, (percents*100.),
		   (proc_sec/60), proc_sec%60,
		   (time_est/60), time_est%60,
		   proc_rate);
	    cout<<flush;
	}

    }

    //copy info file
    gROOT->ProcessLine( Form(".cp %s %s.txt",readme_reader_ptr->Get_info_file_path().Data(), (result_path+run_name+result_file_prefix).Data()) );


    printf("%i events writed in %s.root file\n", events_to_process, (result_path+run_name+result_file_prefix).Data());
    data_tree->Write();
    result_file->Close();

    delete readme_reader_ptr;
    delete Log_reader_ptr;
    delete[] event_struct;
    delete[] bin_reader_arr;
    delete[] log_search_index;


}



#ifdef __CINT__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;

#pragma link C++ class DigDataRecipe+;
#pragma link C++ struct DataConfiguration+;
#pragma link C++ struct ChConfiguration+;
#pragma link C++ struct DataProcParameters+;
#pragma link C++ struct Main_Processing_Param+;
#pragma link C++ struct Fitting_Processing+;
#pragma link C++ struct Amplitude_Processing_Param+;
#pragma link C++ class ParametersFrame+;
#pragma link C++ class MyMainFrame+;
#endif

