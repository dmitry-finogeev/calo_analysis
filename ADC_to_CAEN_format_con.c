#include <TString.h>
#include <TFile.h>
#include <TChain.h>
#include <TTree.h>


struct event_data_struct
{

    Int_t integral_in_gate;

    Float_t mean_in_gate;
    Float_t mean_out_gate;
    Float_t RMS_in_gate;
    Float_t RMS_out_gate;

    Short_t MAX_in_gate;//16
    Short_t MIN_in_gate;
    Short_t MAX_out_gate;
    Short_t MIN_out_gate;

    UShort_t time_max_in_gate;//16
    UShort_t time_min_in_gate;


    void reset()
    {
	integral_in_gate = 0; //integral / gate_width

	MAX_in_gate = 0;
	MIN_in_gate = 0;
	MAX_out_gate = 0;
	MIN_out_gate = 0;

	time_max_in_gate = 0;
	time_min_in_gate = 0;

	mean_in_gate = 0.;
	mean_out_gate = 0.;
	RMS_in_gate = 0.;
	RMS_out_gate = 0.;
    }

    void Print()
    {
	printf("[integral_in_gate %i; MAX_in_gate %i; MIN_in_gate %i; MAX_out_gate %i; MIN_out_gate %i; time_max_in_gate %i; time_min_in_gate %i; mean_in_gate %.2f; mean_out_gate %.2f; RMS_in_gate %.2f; RMS_out_gate %.2f; ]",
	       integral_in_gate, MAX_in_gate, MIN_in_gate, MAX_out_gate, MIN_out_gate, time_max_in_gate, time_min_in_gate, mean_in_gate, mean_out_gate, RMS_in_gate, RMS_out_gate);
    }

    static TString GetChName(Int_t channel_num)
    {
	return TString::Format("channel_%i", channel_num);
    }

    TBranch* CreateBranch(TTree *tree, Int_t channel_num)
    {
	return tree->Branch(GetChName(channel_num).Data(), this,
			    "integral_in_gate/I:mean_in_gate/F:mean_out_gate:RMS_in_gate:RMS_out_gate:MAX_in_gate/S:MIN_in_gate:MAX_out_gate:MIN_out_gate:time_max_in_gate/s:time_min_in_gate/s");
    }


    Int_t SetBranch(TTree *tree, Int_t channel_num)
    {
	return tree->SetBranchAddress(GetChName(channel_num).Data(), this);
    }

};



struct MAIN_signal{
    float Amplitude;
    float Time;
    float Time_pol1;
    float Time_pol3;
    float ZeroLevel;
    float ZeroLevelRMS;

    //                    0 peak_max
    // peak_front_end  0      0   peak_back_start
    float time_peak;						//               *           *
    float time_begin;						//             *                *
    float time_end;							//            *                     *
    float time_front_end;// point of front end of peak		//           *                          *
    float time_back_begin;// point of start back of peak		//          *                               *
    float Charge_GATE;                                              //         0  peak_start                           0   peak_end
    //     * *                                               *           *
    void Reset();
//    void PrintOut();
//    bool IsVoid();
};

#define VOID_VALUE -1
void MAIN_signal::Reset(){
    Amplitude = VOID_VALUE;
    Time = VOID_VALUE;
    Time_pol1 = VOID_VALUE;
    Time_pol3 = VOID_VALUE;
    ZeroLevel = VOID_VALUE;
    ZeroLevelRMS = VOID_VALUE;
    time_peak = VOID_VALUE;
    time_begin = VOID_VALUE;
    time_end = VOID_VALUE;
    time_front_end = VOID_VALUE;
    time_back_begin = VOID_VALUE;
    Charge_GATE = VOID_VALUE;
}



void ADC_to_CAEN_format_con(TString input_file_name, TString output_file_name = "void")
{
    
    // ############################################################
    const int first_ch = 40;
    const int total_ch = 10;
    // ############################################################
    
    
    
    if(output_file_name == "void")
    {
	int point_pos = input_file_name.Last('.');
	output_file_name = input_file_name(0, point_pos) + "_ddp.root";
    }
    
    printf("Result file name: %s\n", output_file_name.Data());
    
    TChain *input_data_tree = new TChain;
    input_data_tree->AddFile(input_file_name + "/adc64_data");   
	
    
    event_data_struct *event_struct = new event_data_struct[total_ch];
    for(Int_t ch = 0; ch < total_ch; ch++)
    {
	input_data_tree->SetBranchAddress((event_struct+ch)->GetChName(ch+first_ch).Data(), (event_struct+ch));
    }    
    
    TFile *result_file = new TFile(output_file_name.Data(), "RECREATE");
    TTree *result_tree = new TTree();
    
    MAIN_signal *ddp_struct = new MAIN_signal[total_ch];
    for(Int_t ch = 0; ch < total_ch; ch++)
    {
	result_tree->Branch(Form("main_CELL%i",ch), (ddp_struct+ch), "Amplitude/F:Time:Time_pol1:Time_pol3:ZeroLevel:ZeroLevelRMS:time_peak:time_begin:time_end:time_front_end:time_back_begin:Charge_GATE");
    }    
    
    for(Int_t entry = 0; entry <= input_data_tree->GetEntries(); entry++)
    {
	input_data_tree->GetEntry(entry);
	
	for(Int_t ch = 0; ch < total_ch; ch++)
	{
	    (ddp_struct+ch)->Reset();
	    (ddp_struct+ch)->Amplitude = (event_struct+ch)->MAX_in_gate;
	    (ddp_struct+ch)->Charge_GATE = (event_struct+ch)->integral_in_gate;
	    (ddp_struct+ch)->Time = (event_struct+ch)->time_max_in_gate;
	}  
	
	result_tree->Fill();
    }
    
    
    
    
    result_tree->Write("RawDataTree");
    
    result_file->Close();
    
    delete [] event_struct;
    delete [] ddp_struct;
    delete input_data_tree;
}
