
#include "Reader.h"
#include <QByteArray>

namespace {

const int BLOCK_SIZE = 1000;
}

Reader::Reader():
    lastReadSize(0),
    blockStatPos(0),
    curPos(0),
    curSize(0),
    restSize(0)
{
    dataBlock.resize(BLOCK_SIZE*sizeof(quint32));
    QByteArray d1("1234567890");
    int s1 = d1.size();
    int c1 = d1.capacity();
    qDebug()<<s1<<c1;
    d1.resize(100);
    s1 = d1.size();
    c1 = d1.capacity();
    qDebug()<<s1<<c1;
    d1.reserve(1000);
    s1 = d1.size();
    c1 = d1.capacity();
    qDebug()<<s1<<c1;
    d1.remove(0,3);
    s1 = d1.size();
    c1 = d1.capacity();
    qDebug()<<s1<<c1;
    qDebug()<<d1;
}

bool Reader::findSync()
{
    while(blockStatPos+curSize<fileSize){
	if(curPos+sizeof(quint32)>curSize)
	    reReadBlock();
	curWordPtr = reinterpret_cast<quint32*>(dataBlock.data()+curPos);

	while(curPos+sizeof(quint32)<curSize){
	    if(*curWordPtr == 0x2A502A50 || *curWordPtr == 0x4A624A62){
		printWord("Sync found");
		return true;
	    }
	    printWord("Sync missing");
	    curPos += sizeof(quint32);
	    ++curWordPtr;
	}
	break;
    }
    return false;
}

void Reader::read(QString fn)
{
    fileName = fn;
    dataFile.setFileName(fileName);
    if(!dataFile.open(QIODevice::ReadOnly))
	qFatal(QString("Can't open file:").append(fileName).toStdString().c_str());
    else
	qDebug()<<"Start reading file:"<<fileName;
    fileSize = dataFile.size();
    if(!findSync()){
	qDebug()<<"Can't find sync word";
	return;
    }
    while(true){
	if(curPos+sizeof(MpdEventHdr) > curSize)
	    reReadBlock();
	eventHdr = reinterpret_cast<MpdEventHdr*>(curWordPtr);
	if(eventHdr->sync != 0x2A502A50 && eventHdr->sync != 0x4A624A62){
	    qDebug()<<QString("sync lost curPos:%1").arg(blockStatPos+curPos,4,16);
	    if(!findSync()){
		qDebug()<<"Failed to find next sync";
		exit(0);
	    } else {
		qDebug()<<QString("sync found curPos:%1").arg(blockStatPos+curPos,4,16);
		eventHdr = reinterpret_cast<MpdEventHdr*>(curWordPtr);
	    }
	}
	printRange(sizeof(MpdEventHdr)/4);
	qDebug()<<"\n\n"<<QString("MpdEvent: Sync 0x%1 evNum=0x%2 len=0x%3")
		  .arg(eventHdr->sync,8,16,QChar('0'))
		  .arg(eventHdr->evNum,8,16,QChar('0'))
		  .arg(eventHdr->length,8,16,QChar('0'));
	int restEventLen = eventHdr->length;


	if(curPos+sizeof(MpdEventHdr)+eventHdr->length > curSize)
	    reReadBlock();
	while(restEventLen){
	    Q_ASSERT(restEventLen>=sizeof(MpdDeviceHdr));
	    deviceHdr = reinterpret_cast<MpdDeviceHdr*>(curWordPtr);
	    printRange(sizeof(MpdDeviceHdr)/4);
	    qDebug()<<QString("==== MpdDevice: deviceId=0x%1 serial=0x%2 len=0x%3 ====")
		      .arg(deviceHdr->deviceId,8,16,QChar('0'))
		      .arg(deviceHdr->deviceSerial,8,16,QChar('0'))
		      .arg(deviceHdr->length,8,16,QChar('0'));
	    restEventLen -= sizeof(MpdDeviceHdr);

	    switch (deviceHdr->deviceId) {
	    case 0xd1:
		qDebug()<<"\t\t[VME Payload]";
		if(false) {
		    printRange(deviceHdr->length/4);
		} else {
		    qDebug()<<"Skip VME payload";
		    printWord("First word of payload");
		    curPos += deviceHdr->length-sizeof(quint32);
		    curWordPtr += deviceHdr->length/4-1;
		    printWord("Last word of payload");
		    curPos += sizeof(quint32);
		    ++curWordPtr;
		}
		break;
	    case 0xca:
		qDebug()<<"\t\t[ADC64WR Payload]";
		adcPayload(deviceHdr->length);
		break;
	    default:
		qDebug()<<"\t\t[Unknown Payload]";
		printRange(deviceHdr->length/4);
		break;
	    }

	    qDebug()<<"\t\t[End of Device Payload]";
	    restEventLen -= deviceHdr->length;
	}
    }
}

void Reader::printWord(QString comment)
{
    qDebug()<<QString("%1: %2\t%3")
	      .arg(blockStatPos+(reinterpret_cast<qint64>(curWordPtr)-reinterpret_cast<qint64>(dataBlock.data())), 4, 16)
	      .arg(*curWordPtr, 8, 16, QChar('0'))
	      .arg(comment);
}

void Reader::printRange(int size)
{
    for(int i=0;i<size;++i){
	printWord();
	curPos += sizeof(quint32);
	++curWordPtr;
    }
}

void Reader::reReadBlock()
{
//    blockStatPos<fileSize
    int s1 = dataBlock.size();
    int c1 = dataBlock.capacity();
//    if(curPos){
//        Q_ASSERT(curWordPtr == reinterpret_cast<quint32*>(dataBlock.data()+curPos));
//        curWordPtr -= curPos/4;
//        lastReadSize = dataBlock.size();
//        blockStatPos += curPos;

//        qDebug()<<"reReadBlock:"<<curPos<<dataBlock.size();
//        s1 = dataBlock.size();
//        c1 = dataBlock.capacity();
//        dataBlock.remove(0, curPos);
//        restSize=dataBlock.size();

//        s1 = dataBlock.size();
//        c1 = dataBlock.capacity();
//        dataBlock.reserve(BLOCK_SIZE*sizeof(quint32));
//        dataBlock.resize(BLOCK_SIZE*sizeof(quint32));
//        s1 = dataBlock.size();
//        c1 = dataBlock.capacity();
//        qDebug()<<"reReadBlock:"<<curPos+dataBlock.size()<<dataBlock.size();
//        curPos = 0;
//    }
    if(curPos){
	Q_ASSERT(curWordPtr == reinterpret_cast<quint32*>(dataBlock.data()+curPos));
	curWordPtr -= curPos/4;
	lastReadSize = dataBlock.size();
	blockStatPos += curPos;

	dataBlock.remove(0, curPos);
	restSize=dataBlock.size();

	dataBlock.reserve(BLOCK_SIZE*sizeof(quint32));
	dataBlock.resize(BLOCK_SIZE*sizeof(quint32));
	curPos = 0;
	qDebug()<<QString("reReadBlock:0x%1-0x%2 (restSize=0x%3)")
		  .arg(blockStatPos, 5, 16, QChar('0'))
		  .arg(blockStatPos+restSize, 5, 16, QChar('0'))
		  .arg(restSize, 0, 16);
    }
//    if(blockStatPos){
//        curPos = 0;
//        curWordPtr = reinterpret_cast<quint32*>(dataBlock.data()+curPos);
//        printWord();
//        curPos = restSize-4;
//        curWordPtr = reinterpret_cast<quint32*>(dataBlock.data()+curPos);
//        printWord();
//        curPos = restSize;
//        curWordPtr = reinterpret_cast<quint32*>(dataBlock.data()+curPos);
//        printWord();
//        curPos = restSize+4;
//        curWordPtr = reinterpret_cast<quint32*>(dataBlock.data()+curPos);
//        printWord();
//    }
    lastReadSize = dataFile.read(dataBlock.data()+restSize, dataBlock.size()-restSize);
    if(lastReadSize==0){
	qCritical()<<"seems to be end of file: lastReadSize="<<lastReadSize;
    }

//    if(blockStatPos){
//        curPos = 0;
//        curWordPtr = reinterpret_cast<quint32*>(dataBlock.data()+curPos);
//        printWord();
//        curPos = restSize-4;
//        curWordPtr = reinterpret_cast<quint32*>(dataBlock.data()+curPos);
//        printWord();
//        curPos = restSize;
//        curWordPtr = reinterpret_cast<quint32*>(dataBlock.data()+curPos);
//        printWord();
//        curPos = restSize+4;
//        curWordPtr = reinterpret_cast<quint32*>(dataBlock.data()+curPos);
//        printWord();
//    }
//    s1 = dataBlock.size();
//    c1 = dataBlock.capacity();
    curSize = lastReadSize+restSize;

//    curPos = 0;
//    while(curPos < curSize){
//        curWordPtr = reinterpret_cast<quint32*>(dataBlock.data()+curPos);
//        printWord();
//        curPos += sizeof(quint32);
//    }
    curPos = 0;
    curWordPtr = reinterpret_cast<quint32*>(dataBlock.data()+curPos);
    Q_ASSERT(curWordPtr == reinterpret_cast<quint32*>(dataBlock.data()+curPos));
}

void Reader::move(qint64 size)
{
    curWordPtr += size/4;
    curPos += size;
}

void Reader::adcPayload(qint64 size)
{
    if(false) {
	printRange(size/4);
    } else {
	quint64 restSize = size;
	while(restSize){
	    mstreamHdr = reinterpret_cast<MpdMStreamHdr*>(curWordPtr);
	    printRange(sizeof(MpdMStreamHdr)/4);
	    qDebug()<<QString("MStream: subtype=%1 userDefBits=0x%2 words=0x%3")
		      .arg(mstreamHdr->subtype, 1, 16, QChar('0'))
		      .arg(mstreamHdr->usrDefBits, 2, 16, QChar('0'))
		      .arg(mstreamHdr->words32b, 6, 16, QChar('0'));
	    restSize -= sizeof(MpdMStreamHdr);
	    if(mstreamHdr->subtype==0){
		Q_ASSERT(mstreamHdr->words32b == sizeof(AdcMStreamTrigPayload)/4);
		adcTrigPayload = reinterpret_cast<AdcMStreamTrigPayload*>(curWordPtr);
		printRange(sizeof(AdcMStreamTrigPayload)/4);
		qDebug()<<QString("chMask=%1:%2 taiSec=0x%3 taiNSecFlag=0x%4")
			  .arg(adcTrigPayload->hiCh, 8, 16, QChar('0'))
			  .arg(adcTrigPayload->lowCh, 8, 16, QChar('0'))
			  .arg(adcTrigPayload->taiSec, 8, 16, QChar('0'))
			  .arg(adcTrigPayload->taiNSecFlag, 8, 16, QChar('0'));
		restSize -= sizeof(AdcMStreamTrigPayload);
	    } else if(mstreamHdr->subtype==1){
		qDebug()<<QString("Adc Ch%1 Payload").arg(mstreamHdr->usrDefBits);
		if(true) {
		    printRange(mstreamHdr->words32b);
		} else {
		    qDebug()<<"Skip adc channal payload";
		    curPos += mstreamHdr->words32b*4;
		    curWordPtr += mstreamHdr->words32b;
		}
		restSize -= mstreamHdr->words32b * 4;
		qDebug()<<QString("End of Adc Ch%1 Payload").arg(mstreamHdr->usrDefBits);
	    } else {
		qCritical()<<"unknown MStream subtype:"<<mstreamHdr->subtype;
	    }
	    Q_ASSERT(restSize>=0);
	}
    }
}

void Reader::printWord()
{
    qDebug()<<((curPos<restSize) ? "****" : "    ")<<"\t"<<QString("%1: %2")
	      .arg(blockStatPos+(reinterpret_cast<qint64>(curWordPtr)-reinterpret_cast<qint64>(dataBlock.data())), 4, 16)
	      .arg(*curWordPtr, 8, 16, QChar('0'));
}
