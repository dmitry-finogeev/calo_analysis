#include <fstream>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <cstring>

using namespace std;

int main(int argc, char* argv[])
{

    int total_events = 0;
      FILE  *DataFile = fopen(argv[0],"rb");
	if(DataFile == NULL){
	    std::cout<< "File not found" <<std::endl;

	return -1;
	}else{
	    fseek(DataFile, 0L, SEEK_END);
	total_events = ftell(DataFile)/(1024*4);
	fclose(DataFile);

	std::cout<< "Events : "<< total_events << " (1024*4byte each)" <<std::endl;

	};
    return total_events;
}

