#include <fstream>
#include <iostream>
#include <TNtuple.h>
#include <TFile.h>
#include <TTree.h>

//using namespace std;
#define MSTREAM_TRIGGER_DATA 0
#define MSTREAM_CHANNEL_DATA 1



struct EventHeader {
    UInt_t magic; // 0x2A502A50
    UInt_t payloadLength;
    UInt_t eventNumber;
};
struct DeviceEventHeader {
    UInt_t deviceSerialNo;
    UInt_t devicePayloadLength;
    UInt_t deviceId;

};
struct MStreamHeader {
    UInt_t mStreamSubtype;
    UInt_t mStreamPayloadLength;
    UInt_t subtypeDefinedBits;
};




class MStreamBlock {
    friend class DeviceEventBlock;
    Int_t *payload;
public:
    MStreamBlock() : payload(0) {}
    struct MStreamHeader header;
    void print();  // diagnostic output
    Int_t getTypeId() { return header.mStreamSubtype; }
    Int_t getChannelId() { return header.subtypeDefinedBits; }

    short* getData() { return ((short*) payload) +8; } // skip mstream header  e540b 7e578222
    Int_t getDataLength() { return header.mStreamPayloadLength*2 - 12; } // skip mstream header  e540b 7e578222
    void getMinMax(int *min, int *max);

};


class DeviceEventBlock {
    int payloadCounter;
public:
    DeviceEventBlock() : payloadCounter(0) {}
    struct DeviceEventHeader header;
    std::vector<MStreamBlock> blocks;
    void readMStreamBlock(ifstream &file);
};


class Adc64Event {
public:
    void readBinary(ifstream &file);

    string getEventType();
    int getEventTypeId();
    Adc64Event() { reset(); }
    void reset();
    std::vector<DeviceEventBlock> devBlocks;
protected:
    struct EventHeader eveh;
    int payloadCounter;

    void readDeviceEventBlock(ifstream &file);
};


void MStreamBlock::getMinMax(int *min, int *max)
{
    *min = 1000000;
    *max = -1;
    short *data = getData();
    //    cout << "#######################"<<endl;
    //TH1D h1("h1", "event 1 channel 1", 1000, 0, 1000);
    for(int i = 0; i < getDataLength(); i++) {
	//        cout << data[i] << " " << *min << " " << *max << endl;
	if(data[i] > *max) {
	    *max = data[i];
	}
	if(data[i] < *min) {
	    *min = data[i];
	}
	//if(h1.GetEntries() < 100) h1.Fill(data[i]);
    }

    //if(h1.GetEntries() < 100)  h1.Write();
}

void Adc64Event::reset() {
    payloadCounter = 0;
    eveh.magic = 0;
    eveh.payloadLength = 0;
    eveh.eventNumber = 0;
    /*
    for(int i = 0; i < devBlocks.size(); i++) {
	for(int j = 0; j < devBlocks[i].blocks[j].size(); j++) {
	     TODO clear memory
	    if (devBlocks[i].blocks[j].payload != 0) {
		delete[] devBlocks[i].blocks[j].payload;
	    }
	}
	devBlocks[i].blocks.clear(); // remove MStreams
    }
    */
    devBlocks.clear();
}

void DeviceEventBlock::readMStreamBlock(ifstream &file) {
    MStreamBlock block;

    std::cout << "DeviceEventBlock::readMStreamBlock: " << endl;
    while( payloadCounter < header.devicePayloadLength ) { // for all the blocks
	//file.read( (char*) &(block.header), sizeof(block.header));
	//    UInt_t mStreamSubtype:2;
	//    UInt_t mStreamPayloadLength:22;
	//    UInt_t subtypeDefinedBits:8;

	uint32_t buffer;
	file.read( (char*) &(buffer), 4);
	std::cout << "buffer: " << std::hex << buffer << endl;

	block.header.subtypeDefinedBits = buffer & 0x000000ff;
	buffer = buffer >> 8;
	block.header.mStreamPayloadLength = buffer & 0x3fffff;
	buffer = buffer >> 22;
	block.header.mStreamSubtype = buffer & 0x00000003;


		std::cout << "MStream block found!" << endl;
		std::cout << "\theader.subtypeDefinedBits: " << block.header.subtypeDefinedBits << endl;
		std::cout << "\theader.mStreamPayloadLength, words: " << block.header.mStreamPayloadLength << endl;
		std::cout << "\theader.mStreamPayloadLength, bytes: " << block.header.mStreamPayloadLength*4 << endl;
		std::cout << "\theader.mStreamSubtype: " << block.header.mStreamSubtype << endl;

	if(block.header.mStreamPayloadLength > 0) {
	    if(block.payload != 0) delete[] block.payload;
	    block.payload = new Int_t[block.header.mStreamPayloadLength];
	    // TODO remove this extra memory juggling
	    file.read((char*)block.payload, block.header.mStreamPayloadLength*4);

	}

	blocks.push_back(block);
	payloadCounter += block.header.mStreamPayloadLength*4 + 4; // add 4 bytes for mstream header

	block.print();

	std::cout << "\tpayloadCounter now: " << payloadCounter << endl;
	//        int pp;
	//        cin >> pp;

    }
    std::cout << " End DeviceEventBlock::readMStreamBlock: " << blocks.size() << " blocks decoded" << endl;
}

void MStreamBlock::print() {
    int *data = (int*) payload;
    std::cout << "MStream (" << header.mStreamPayloadLength << " words): "<< std::hex;

    if(header.mStreamPayloadLength < 8) {
	for(int i = 0; i < header.mStreamPayloadLength; i++) {
	    std::cout << "  " << data[i];
	}
    } else {
	for(int i = 0; i < 6; i++) {
	    std::cout << "  " << data[i];
	}
	std::cout << "  <...>";
	for(int i = header.mStreamPayloadLength-2; i < header.mStreamPayloadLength; i++) {
	    std::cout << "  " << data[i];
	}
    }
    std::cout << "\n" << std::dec;
}

void Adc64Event::readDeviceEventBlock(ifstream &file) {
    int nDevBlocks = devBlocks.size();
    DeviceEventBlock block;

    //file.read( (char*) &(block.header), sizeof(block.header));
    //    uint32_t deviceSerialNo;
    //    uint32_t devicePayloadLength:24;
    //    uint32_t deviceId:8;

    uint32_t buffer;
    file.read( (char*) &(buffer), 4);
    block.header.deviceSerialNo = buffer;


    file.read( (char*) &(buffer), 4);
    std::cout << "buffer: " << std::hex << buffer << endl;


    block.header.deviceId = buffer & 0x000000ff;
    buffer = buffer >> 8;
    block.header.devicePayloadLength = buffer;


    std::cout << "Adc64Event::readDeviceEventBlock: " << endl;
    std::cout << "\tdeviceSerialNo: " << block.header.deviceSerialNo;
    std::cout << "\tdeviceId: " << block.header.deviceId;
    std::cout << "\tpayload, bytes: " << block.header.devicePayloadLength << endl;

    block.readMStreamBlock(file);

    devBlocks.push_back(block);
    payloadCounter += block.header.devicePayloadLength + 8; // 8 for dev header
}

void Adc64Event::readBinary(ifstream& file) {

    //file.read( (char*) &(this->eveh), sizeof(this->eveh));
    file.read( (char*) &(this->eveh.magic), 4);
    file.read( (char*) &(this->eveh.payloadLength), 4);
    file.read( (char*) &(this->eveh.eventNumber), 4);

    std::cout << "Magic: " << std::hex << eveh.magic
	      << "\n size (bytes): " << std::dec << eveh.payloadLength
	      << "\n evNo: " << eveh.eventNumber << endl;

    std::cout << "Event type: " << getEventType() << endl;

    for(int nReadDeviceBlocks = 1; payloadCounter < eveh.payloadLength; nReadDeviceBlocks++) {
	readDeviceEventBlock(file);
	std::cout << "End readDeviceEventBlock #" << nReadDeviceBlocks
		  <<", now at " << payloadCounter << " of " << eveh.payloadLength << endl;
    }
}

string Adc64Event::getEventType() {
    switch(eveh.magic) {
    case 0x2A502A50:
	return "simple";
    case 0x4A624A62:
	return "end-of-burst";
    default:
	return "undefined";
    };
}

int Adc64Event::getEventTypeId() {
    switch(eveh.magic) {
    case 0x2A502A50:
	return 1;
    case 0x4A624A62:
	return 2;
    default:
	return -1;
    };
}

void adctr_(char *filename = 0) {
    ifstream data_infile(filename ? filename : "adc64_test1.data");

    Adc64Event ev;
    int nEvents = 0;

    TFile *f = new TFile("data.root", "NEW");
    TNtuple *nt = new TNtuple("nt", "ADC64 events", "deviceId:eventId:channelId:min:max");
    do {
	ev.reset();
	ev.readBinary(data_infile);
	++nEvents;
	std::cout << "<<<EVENT "<< nEvents << " COMPLETE>>>" << endl;
	std::cout << "<<< "<< ev.devBlocks.size() << " device blocks decoded >>>" << endl;

	cout << "nEvents\tdevId\tchanId\tmin - max " << endl;

	if(ev.getEventTypeId() < 0) break;
	for(int i = 0; i < ev.devBlocks.size(); i++) {
	    int deviceId = ev.devBlocks[i].header.deviceId;
	    int serialNo = ev.devBlocks[i].header.deviceSerialNo;
	    for(int j = 0; j < ev.devBlocks[i].blocks.size(); j++) {

		if (ev.devBlocks[i].blocks[j].getTypeId() == MSTREAM_CHANNEL_DATA) {
		    int channelId = ev.devBlocks[i].blocks[j].getChannelId();
		    int min = -1, max = -1;
		    ev.devBlocks[i].blocks[j].getMinMax(&min, &max);

		    cout << nEvents << "\t" << deviceId << "\t" << channelId << "\t"
			 << min << " - " << max << endl;
		    //        char c;
		    //        cin >> c;
		    nt->Fill(deviceId, nEvents, channelId, min, max);
		}
	    }
	}

	//    } while ( ev.getEventTypeId() > 0 );
    } while ( nEvents < 10 );

    nt->Write();
    f->Close();
    //std::cout << "read: " << token << endl;
}
