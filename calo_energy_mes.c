

#include <fstream>
#include <iostream>
#include <TNtuple.h>
#include <TFile.h>
#include <TChain.h>
#include <Rtypes.h>
#include <vector>
#include <TBrowser.h>

#include "Readme_reader.h"
#include "event_data_struct.h"
#include "bindataformat.h"
#include "../libraries/utilites/utilites.h"


void calo_energy_mes()
{
    ////////////////////////////////////////////////////////////////////////////////////////

    TString source_path = "/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_source/T10_test_sep_17/";
    TString result_path = "/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_results/T10_test_sep_17/";

    TString result_file_name = "protons_6Gevc_0";

    ////////////////////////////////////////////////////////////////////////////////////////


    /************************************************/
    const Int_t max_evergy = 200000000000;
    const Int_t max_energy_bin = 100;
    Int_t pedestal_cut_trsh = 2000;

    Float_t part_time_sel_val[2] = {-.2, .2};
    TString part_sel_comment = "pions";
    /************************************************/


    result_file_name += "_" + part_sel_comment;


    Readme_reader *readme_reader_ptr = new Readme_reader("/mnt/hgfs/DETECTORS_DATA/SOURCE/CALO/T10_RUNS/");
    readme_reader_ptr->Set_runfile_name("adc64_17_09_07_14_22_protonScan_mod5_6GeV");
    readme_reader_ptr->Print();


    Int_t total_modules = readme_reader_ptr->Get_module_num();



    TChain *data_tree = new TChain;

    data_tree->AddFile(source_path + "adc64_17_09_07_14_22_protonScan_mod5_6GeV.root/adc64_data");


    TFile *result_file = new TFile((result_path + result_file_name + ".root").Data(), "RECREATE");
    gDirectory->cd("Rint:/");
    // =============================



    TObjArray *result_array = new TObjArray;
    result_array->SetName(readme_reader_ptr->Get_run_name().Data());
    result_array->SetOwner();

    TObjArray *commhist_array = new TObjArray;
    commhist_array->SetName("calo_results");
    commhist_array->SetOwner();
    result_array->Add(commhist_array);

    TObjArray *canv_array = new TObjArray;
    canv_array->SetName("result_canvas");
    canv_array->SetOwner();
    result_array->Add(canv_array);
    TCanvas *temp_canv = new TCanvas("temp", "temp");

    TCanvas *canvas_evergy = new TCanvas(readme_reader_ptr->Get_run_name().Data(), readme_reader_ptr->Get_run_name().Data());
    canvas_evergy->DivideSquare(total_modules+2);
    canv_array->Add(canvas_evergy);




    TString hist_comment = readme_reader_ptr->Get_run_comment() + " {" + part_sel_comment + "}";

    TString calo_summ_amplitude_eq = "0";
    TString part_time_sel_eq = Form("(  ((main_PMT02.Time_pol1-main_PMT01.Time_pol1) >= %f)&&((main_PMT02.Time_pol1-main_PMT01.Time_pol1) >= %f)  )",
				    part_time_sel_val[0], part_time_sel_val[1]);

    for(Int_t module_iter = 0; module_iter < total_modules; module_iter++)
    {
	TString module_name = readme_reader_ptr->Get_module_info()[module_iter].comment;
	TObjArray *module_data_array = new TObjArray();
	module_data_array->SetName(module_name.Data());
	module_data_array->SetOwner();
	result_array->Add(module_data_array);

	Int_t total_cells = readme_reader_ptr->Get_module_info()[module_iter].total_cells;

	printf("\n\nCalculating %s =============================\n", module_name.Data());



	// Amplitude equetions ==============================
	TString module_summ_amplitude_eq = "0";
	for(Int_t sel_cell = 0; sel_cell < total_cells; sel_cell++)
	{
	    Int_t sel_ch = readme_reader_ptr->GetContCh_from_CellN(module_iter, sel_cell);
	    TString sel_ch_name = event_data_struct::GetChName(sel_ch);

	    module_summ_amplitude_eq += Form("+((%s.integral_in_gate)*(  (%s.time_max_in_gate > %i)&&(%s.time_max_in_gate < %i)  ))",
					  sel_ch_name.Data(), sel_ch_name.Data(), 70,  sel_ch_name.Data(), 90);

	    //if(sel_cell < 9)
	    calo_summ_amplitude_eq += Form("+((%s.integral_in_gate)*(  (%s.time_max_in_gate > %i)&&(%s.time_max_in_gate < %i)  ))",
					  sel_ch_name.Data(), sel_ch_name.Data(), 70,  sel_ch_name.Data(), 90);
//	    calo_summ_amplitude_eq += Form("+((%s.integral_in_gate)*(%s.integral_in_gate < %i)*(  (%s.time_max_in_gate > %i)  ))",
//					  sel_ch_name.Data(), sel_ch_name.Data(), max_evergy, sel_ch_name.Data(), 70);

	}
	printf("module_summ_amplitude: %s\n", module_summ_amplitude_eq.Data());
	// ==================================================


	// Module Evergy draw =============================================
	TH1 *th1_hist_ptr = NULL;
	th1_hist_ptr = new TH1F("temp", "temp", max_energy_bin, 0, max_evergy);
	canvas_evergy->cd(module_iter+1);

	data_tree->Draw(Form("(%s)>>temp", module_summ_amplitude_eq.Data()),part_time_sel_eq.Data());

	th1_hist_ptr->SetName(Form("%s_summampl",module_name.Data()));
	th1_hist_ptr->SetTitle(Form("Amplitude summ %s %s", module_name.Data(), hist_comment.Data()));
	module_data_array->Add(th1_hist_ptr);
	// =============================================


    }


    printf("\n\ncalo_summ_amplitude_eq: %s\n", calo_summ_amplitude_eq.Data());



    // Calo Evergy draw =============================================
    TH1 *th1_hist_ptr = NULL;
    th1_hist_ptr = new TH1F("temp", "temp", max_energy_bin, 0, 10*max_evergy);
    canvas_evergy->cd(total_modules+1);

    data_tree->Draw(Form("(%s)>>temp", calo_summ_amplitude_eq.Data()),part_time_sel_eq.Data());

    th1_hist_ptr->SetName(Form("calo_summampl"));
    th1_hist_ptr->SetTitle(Form("Calo Amplitude summ %s", hist_comment.Data()));
    commhist_array->Add(th1_hist_ptr);
    // =============================================

    // Partical times =============================================
    th1_hist_ptr = new TH1F("temp", "temp", 2000, -10, 10);
    canvas_evergy->cd(total_modules+2);

    data_tree->Draw(Form("(main_PMT02.Time_pol1-main_PMT01.Time_pol1)>>temp"));

    th1_hist_ptr->SetName(Form("part_time"));
    th1_hist_ptr->SetTitle(Form("Particles start TOF %s [%.1f, %.1f]", hist_comment.Data(), part_time_sel_val[0], part_time_sel_val[1]));
    commhist_array->Add(th1_hist_ptr);
    // =============================================




    if(1){
	for(Int_t i = 0; i < canv_array->GetLast(); i++)
	{
	    ((TCanvas*)canv_array->At(i))->SaveAs(Form("%s.pdf(",  (result_path + result_file_name + "_pdf.pdf").Data() ));
	}
	((TCanvas*)canv_array->At(canv_array->GetLast()))->SaveAs(Form("%s.pdf)",  (result_path + result_file_name + "_pdf.pdf").Data() ));
    }



    SaveArrayStructInFile(result_array, result_file);


    new TBrowser;


}





