
#include <TFile.h>
#include <TString.h>
#include <TDirectory.h>
#include <TBrowser.h>


#include "data_calculating.h"

/*
 * test analysis macro
 *
 */

void Analysis()
{

    TString data_path_source = "root_source/";
    TString data_path_result = "root_results/";

    TString file_name = "adc64_17_04_24_17_27_overnight";



    TFile source_file((data_path_source+file_name+".root").Data(), "READONLY");
    if(!source_file.IsOpen())
    {
	printf("File \"%s\" was NOT opened\n", (data_path_source+file_name+".root").Data());
	return;
    }

    TTree *data_tree = dynamic_cast<TTree*>(source_file.FindObjectAny("adc64_data"));
    if(data_tree == NULL)
    {
	printf("Data tree was NOT found\n");
	source_file.Close();
	return;
    }


    TFile result_file((data_path_result+file_name+"_result"+".root").Data(), "RECREATE");


    Int_t channels_total = 10;//data_tree->GetNbranches()-1; //log branch
    event_data_struct *event_struct = new event_data_struct[channels_total];
    event_log_data_struct *event_log_struct = new event_log_data_struct;

    TObjArray *Res_Collection = new TObjArray();
    Res_Collection->SetOwner();

    event_log_struct->SetBranch(data_tree);
    for(Int_t ch=0; ch<channels_total; ch++)
    {
	event_struct[ch].SetBranch(data_tree, ch);

	TObjArray *curr_arr = new TObjArray();
	curr_arr->SetName(event_struct[ch].GetChName(ch).Data());
	curr_arr->SetOwner();
	Res_Collection->Add(curr_arr);
    }



    TObjArray *hist_arr;
    TCanvas *canv_temp = new TCanvas("temp", "temp");


    // ##############################################################
    Int_t ch_curr = 3;
    TString ch_name = event_data_struct::GetChName(ch_curr);
    hist_arr = (TObjArray*)(Res_Collection->FindObject(ch_name.Data()));
    TH1 *TH1_ptr;


    // Time -----------------------------------------------
    TH1_ptr = new TH1F("name_temp", "title_temp", 200, 0, 200);
    data_tree->Draw(Form("(%s.time_max_in_gate)>>name_temp", ch_name.Data()));

    TH1_ptr->SetName(Form("Time_%s",ch_name.Data()));
    TH1_ptr->SetTitle( Form("Time [%s]", ch_name.Data()) );
    hist_arr->Add(TH1_ptr);

    TString time_cut = Form("(%s.time_max_in_gate < %f)&&(%s.time_max_in_gate > %f)",
			    ch_name.Data(), 67.,
			    ch_name.Data(), 54.);


    Int_t Charge_min = -100;
    Int_t Charge_max = 1000;
    Int_t Charge_bin = 110;

    // Charge no CUT -----------------------------------------------
    TH1_ptr = new TH1F("name_temp", "title_temp", Charge_bin, Charge_min, Charge_max);
    data_tree->Draw(Form("(%s.integral_in_gate - %s.mean_out_gate)>>name_temp", ch_name.Data(), ch_name.Data()));

    TH1_ptr->SetName(Form("Charge_nosel_%s",ch_name.Data()));
    TH1_ptr->SetTitle( Form("Charge no selection [%s]", ch_name.Data()) );
    hist_arr->Add(TH1_ptr);



    // Charge time CUT -----------------------------------------------
    TH1_ptr = new TH1F("name_temp", "title_temp", Charge_bin, Charge_min, Charge_max);
    data_tree->Draw(Form("(%s.integral_in_gate - %s.mean_out_gate)>>name_temp", ch_name.Data(), ch_name.Data()),
		    time_cut.Data());

    TH1_ptr->SetName(Form("Charge_timecut_%s",ch_name.Data()));
    TH1_ptr->SetTitle( Form("Charge time cut [%s]", ch_name.Data()) );
    hist_arr->Add(TH1_ptr);

    // Charge hor sel -----------------------------------------------
    Float_t charge_treshold = 30.;
    TString hor_sel = "";
    for(Int_t ch=0; ch <channels_total; ch++)
    {
	TString ch_name = event_data_struct::GetChName(ch);
	hor_sel += Form("((%s.integral_in_gate - %s.mean_out_gate) >= %f)", ch_name.Data(), ch_name.Data(), charge_treshold);
	if(ch <channels_total-1) hor_sel += "&&";
    }
    //printf("hor_sel: %s\n", hor_sel.Data());

    TH1_ptr = new TH1F("name_temp", "title_temp", Charge_bin, Charge_min, Charge_max);
    data_tree->Draw(Form("(%s.integral_in_gate - %s.mean_out_gate)>>name_temp", ch_name.Data(), ch_name.Data()),
		    hor_sel.Data());

    TH1_ptr->SetName(Form("Charge_horsel_%s",ch_name.Data()));
    TH1_ptr->SetTitle( Form("Charge hor sel [%s]", ch_name.Data()) );
    hist_arr->Add(TH1_ptr);

    // Charge nearby sel -----------------------------------------------
    TString nby_sel = "";
    Int_t ch_left = 4;//get_ch_MPPC_by_CBM(MPPC_to_CBM_ch_map[ch_curr] - 1);
    Int_t ch_right = 2;//get_ch_MPPC_by_CBM(MPPC_to_CBM_ch_map[ch_curr] + 1);

    nby_sel += Form("((%s.integral_in_gate - %s.mean_out_gate) >= %f)",
		    event_data_struct::GetChName(ch_left).Data(), event_data_struct::GetChName(ch_left).Data(), charge_treshold);
    nby_sel += Form("&&((%s.integral_in_gate - %s.mean_out_gate) >= %f)",
		    event_data_struct::GetChName(ch_right).Data(), event_data_struct::GetChName(ch_right).Data(), charge_treshold);

    printf("ch_r %i; ch_l %i; nby_sel: %s\n",ch_right, ch_left, nby_sel.Data());

    TH1_ptr = new TH1F("name_temp", "title_temp", Charge_bin, Charge_min, Charge_max);
    data_tree->Draw(Form("(%s.integral_in_gate - %s.mean_out_gate)>>name_temp", ch_name.Data(), ch_name.Data()),
		    nby_sel.Data());

    TH1_ptr->SetName(Form("Charge_nbysel_%s",ch_name.Data()));
    TH1_ptr->SetTitle( Form("Charge nearby sel [%s]", ch_name.Data()) );
    hist_arr->Add(TH1_ptr);

    // Charge neaby & time CUT -----------------------------------------------
    TH1_ptr = new TH1F("name_temp", "title_temp", Charge_bin, Charge_min, Charge_max);
    data_tree->Draw(Form("(%s.integral_in_gate - %s.mean_out_gate)>>name_temp", ch_name.Data(), ch_name.Data()),
		    Form("%s&&%s",time_cut.Data(), nby_sel.Data()));

    TH1_ptr->SetName(Form("Charge_timecut_nbysel_%s",ch_name.Data()));
    TH1_ptr->SetTitle( Form("Charge [time cut] [nearby sel] [%s]", ch_name.Data()) );
    hist_arr->Add(TH1_ptr);

    // Time nearby sel -----------------------------------------------
    TH1_ptr = new TH1F("name_temp", "title_temp", 200, 0, 200);
    data_tree->Draw(Form("(%s.time_max_in_gate)>>name_temp", ch_name.Data()), nby_sel.Data());

    TH1_ptr->SetName(Form("Time_nby_sel_%s",ch_name.Data()));
    TH1_ptr->SetTitle( Form("Time nearby selection [%s]", ch_name.Data()) );
    hist_arr->Add(TH1_ptr);

    // Charge nearby oneside sel -----------------------------------------------
    TString nby_os_sel = "";
    Int_t ch_right2 = 1;//get_ch_MPPC_by_CBM(MPPC_to_CBM_ch_map[ch_curr] - 1);

    nby_os_sel += Form("((%s.integral_in_gate - %s.mean_out_gate) >= %f)",
		    event_data_struct::GetChName(ch_right2).Data(), event_data_struct::GetChName(ch_right2).Data(), charge_treshold);
    nby_os_sel += Form("&&((%s.integral_in_gate - %s.mean_out_gate) >= %f)",
		    event_data_struct::GetChName(ch_right).Data(), event_data_struct::GetChName(ch_right).Data(), charge_treshold);

    printf("ch_r %i; ch_l %i; nby_os_sel: %s\n",ch_right, ch_right2, nby_os_sel.Data());

    TH1_ptr = new TH1F("name_temp", "title_temp", Charge_bin, Charge_min, Charge_max);
    data_tree->Draw(Form("(%s.integral_in_gate - %s.mean_out_gate)>>name_temp", ch_name.Data(), ch_name.Data()),
		    nby_os_sel.Data());

    TH1_ptr->SetName(Form("Charge_nbysel_os_%s",ch_name.Data()));
    TH1_ptr->SetTitle( Form("Charge [nearby one side] sel [%s]", ch_name.Data()) );
    hist_arr->Add(TH1_ptr);

    // Charge nearby oneside sel -----------------------------------------------
    TH1_ptr = new TH1F("name_temp", "title_temp", Charge_bin, Charge_min, Charge_max);
    data_tree->Draw(Form("(%s.integral_in_gate - %s.mean_out_gate)>>name_temp", ch_name.Data(), ch_name.Data()),
		    Form("%s&&%s",time_cut.Data(), nby_os_sel.Data()));

    TH1_ptr->SetName(Form("Charge_nbysel_os_timecut_%s",ch_name.Data()));
    TH1_ptr->SetTitle( Form("Charge [nearby one side][time cut] sel [%s]", ch_name.Data()) );
    hist_arr->Add(TH1_ptr);


    // ##############################################################




    //Saving Histogramms
    result_file.mkdir("Results_hists");
    TIter nx_iter((TCollection*)(Res_Collection));
    while ((hist_arr=(TObjArray*)nx_iter()))
    {
	if(hist_arr->GetLast() >=0 )
	{
	    result_file.mkdir(Form("Results_hists/%s",hist_arr->GetName()));
	    result_file.cd(Form("Results_hists/%s",hist_arr->GetName()));
	    hist_arr->Write();
	}
    }

    delete canv_temp;
    delete event_struct;
    delete event_log_struct;
    delete Res_Collection;


    result_file.Close();
    source_file.Close();


    new TBrowser;
}
