#include <TObjArray.h>
#include <TFile.h>
#include <TChain.h>
#include <TObjString.h>
#include<TH2F.h>
#include<TF1.h>

#include<TROOT.h>
#include<TSystem.h>
#include<TSystemDirectory.h>
#include<TSystemFile.h>
#include<TList.h>
#include<TCollection.h>
#include<TMath.h>
#include<TSpectrum.h>
#include<TDirectory.h>
#include<TStopwatch.h>
#include<TGraph.h>
#include<TBrowser.h>
#include<TCanvas.h>
#include<TPad.h>
#include<TProfile.h>
#include<TStyle.h>
#include "TMarker.h"
#include "TLegend.h"

#include "Readme_reader.h"
#include "event_data_struct.h"
#include "bindataformat.h"
#include "../libraries/utilites/utilites.h"





void calo_test_analisys_T9_results_comare2()
{
    TObjArray files_names, files_comments;
    TObjArray hists_names, hists_comments;

    TCanvas *canvas = NULL;
    TH1 *th1_hist_ptr = NULL;
    TH2 *th2_hist_ptr = NULL;
    TF1 *tfunc_ptr = NULL;
    Int_t bin;
    Double_t mean, sigma, mean_err, sigma_err;

    gStyle->SetOptFit(1111);

    /////////////////////////////////////////////////
    TString result_path = "/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_results/t9_testbeam_nov2017/pictures/";

    /////////////////////////////////////////////////
    Readme_reader *readme_reader_ptr = new Readme_reader("");
    readme_reader_ptr->SetInfoFile("/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_source/T9_test_nov_17/Calibration.txt");
    //readme_reader_ptr->Print();

    const Bool_t print_log = 1;
    const Int_t beam_to_module_N = 5;
    Int_t beam_to_module_tcells = readme_reader_ptr->Get_module_info()[beam_to_module_N-1].total_cells;
    TString beam_to_module_name = readme_reader_ptr->Get_module_info()[beam_to_module_N-1].comment;
    Int_t total_modules = readme_reader_ptr->Get_module_num();
    /////////////////////////////////////////////////


    /////////////////////////////////////////////////
    TString source_path = "/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_results/t9_testbeam_nov2017/";

    //    files_names.Add(new TObjString("19_noPMT_ampl/adc64_17_09_08_13_45_protonScan_mod5_6GeVprotons_analysis_1.root"));
    //    files_comments.Add(new TObjString("T10 protons {old calc no PMTampl}"));

    //    files_names.Add(new TObjString("13_TOFampl_sel/24.11.2017_16_51_mod5_6GeV_f7m_cherenkov_Veto_analysis_1.root"));
    //    files_comments.Add(new TObjString("T9 protons {old calc}"));

    //    files_names.Add(new TObjString("13_TOFampl_sel/adc64_17_09_08_13_45_protonScan_mod5_6GeVprotons_analysis_1.root"));
    //    files_comments.Add(new TObjString("T10 protons {old calc}"));

    //    files_names.Add(new TObjString("14_newsigcalc/24.11.2017_16_51_mod5_6GeV_f7m_cherenkov_Veto_analysis_1.root"));
    //    files_comments.Add(new TObjString("T9 protons {new calc}"));

    //    files_names.Add(new TObjString("14_newsigcalc/adc64_17_09_08_13_45_protonScan_mod5_6GeVprotons_analysis_1.root"));
    //    files_comments.Add(new TObjString("T10 protons {new calc}"));

    files_names.Add(new TObjString("16_pu_mod5/24.11.2017_15_28_mod5_10GeV_f7m_cherenkov_Veto_analysis_1.root"));
    files_comments.Add(new TObjString("T9_protons_10GeV_c"));

    files_names.Add(new TObjString("16_pu_mod5/24.11.2017_16_51_mod5_6GeV_f7m_cherenkov_Veto_analysis_1.root"));
    files_comments.Add(new TObjString("T9_protons_6GeV_c"));

    files_names.Add(new TObjString("16_pu_mod5/24.11.2017_18_50_mod5_3GeV_f7m_cherenkov_Veto_analysis_1.root"));
    files_comments.Add(new TObjString("T9_protons_3GeV_c"));

//    files_names.Add(new TObjString("16_pu_mod5/adc64_17_09_08_13_45_protonScan_mod5_6GeVprotons_analysis_1.root"));
//    files_comments.Add(new TObjString("T10 protons {new calc mod 5 sel}"));

    //    files_names.Add(new TObjString("15_pu_allcels/24.11.2017_16_51_mod5_6GeV_f7m_cherenkov_Veto_analysis_1.root"));
    //    files_comments.Add(new TObjString("T9 protons {new calc allch sel}"));

    //	files_names.Add(new TObjString("15_pu_allcels/adc64_17_09_08_13_45_protonScan_mod5_6GeVprotons_analysis_1.root"));
    //	files_comments.Add(new TObjString("T10 protons {new calc allch sel}"));


    //    files_names.Add(new TObjString("13_TOFampl_sel/24.11.2017_16_51_mod5_6GeV_f7m_cherenkov_Veto_analysis_1.root"));
    //    files_comments.Add(new TObjString("T9 protons {old calc}"));


    /////////////////////////////////////////////////
//    hists_names.Add(new TObjString(  "%s_cell_%i_energy"  ));
//    hists_comments.Add(new TObjString("energy"));

    hists_names.Add(new TObjString(  "%s_module_energy_first_%i"  ));
    hists_comments.Add(new TObjString("Fist_i_cell"));

    /////////////////////////////////////////////////



    Int_t total_files = files_names.GetLast()+1;
    Int_t total_hists = hists_names.GetLast()+1;
    printf("Total files: %i\nTotal hists %i\n", total_files, total_hists);


    TObjArray files_arr;
    for(Int_t ifile = 0; ifile < total_files; ifile++)
    {
	TString iFile_name = ((TObjString*)(files_names.At(ifile)))->GetString();
	TFile *file = new TFile((source_path + iFile_name).Data(), "READONLY");
	files_arr.Add(file);
    }


    TObjArray canv_arr;
    TCanvas *canv_one =  new TCanvas("canv", "canv");


    for(Int_t ifile = 0; ifile < total_files; ifile++)
    {
	TString iFile_name = ((TObjString*)(files_names.At(ifile)))->GetString();
	TString iFile_comment = ((TObjString*)(files_comments.At(ifile)))->GetString();


	for(Int_t ihist = 0; ihist < total_hists; ihist++)
	{
	    TString iHist_commennts = ((TObjString*)(hists_comments.At(ihist)))->GetString();

	    for(Int_t module_iter = 4; module_iter < 5; module_iter++)
	    {
		TString module_name = readme_reader_ptr->Get_module_info()[module_iter].comment;


		TString canv_name = Form("%s_%s_%s", iFile_comment.Data(), module_name.Data(), iHist_commennts.Data());
		Int_t total_cells = readme_reader_ptr->Get_module_info()[module_iter].total_cells;
		total_cells = 9;

		gDirectory->cd("Rint:/");
		canvas = new TCanvas(canv_name.Data(), canv_name.Data(), 1400, 1000);
		canvas->DivideSquare(total_cells);
		canv_arr.Add(canvas);

		for(Int_t cell_iter = 0; cell_iter < total_cells; cell_iter++)
		{
		    TString iHist_name = Form( (((TObjString*)(hists_names.At(ihist)))->GetString()).Data(), module_name.Data(), cell_iter+1) ;

		    ((TFile*)files_arr.At(ifile))->cd();
		    th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny(  iHist_name.Data() )));


		    if(th1_hist_ptr == NULL)
		    {
			printf("hist %s not found at file %s\n", iHist_name.Data(), iFile_name.Data());
			continue;
		    }
		    else
		    {
			printf("WORKING: hist %s at file %s\n", iHist_name.Data(), iFile_name.Data());
		    }

		    gDirectory->cd("Rint:/");
		    th1_hist_ptr = (TH1F*)(th1_hist_ptr->Clone());


		    TVirtualPad *pad_curr = canvas->cd(cell_iter+1);
		    pad_curr->SetLogy(print_log);
		    //th1_hist_ptr->DrawNormalized(  ((ifile==0)?"":"same")  );

		    Float_t x_off = 0.1;
		    pad_curr->SetLeftMargin(x_off);
		    pad_curr->SetRightMargin(x_off);
		    pad_curr->SetTopMargin(x_off);
		    pad_curr->SetBottomMargin(0.2);


		    th1_hist_ptr->SetTitle(Form("first %i sections;E [MeV]",cell_iter+1));
		    th1_hist_ptr->GetXaxis()->SetRangeUser(-10., 500.);

		    gStyle->SetTitleFontSize(0.09);

		    Float_t text_size = 0.07;
		    th1_hist_ptr->GetXaxis()->SetLabelSize(text_size);
		    th1_hist_ptr->GetYaxis()->SetLabelSize(text_size);

		    th1_hist_ptr->GetXaxis()->SetTitleSize(text_size);
		    th1_hist_ptr->GetYaxis()->SetTitleSize(text_size);
		    th1_hist_ptr->Draw( );


//		    if(ihist == 2)
//		    {
//			th1_hist_ptr->SetTitle(Form("%s [%s]", iHist_commennts.Data(), iFile_comment.Data()));
//			th1_hist_ptr->SetLineColor(ifile+1);

//			canv_one->cd();
//			th1_hist_ptr->DrawNormalized(  ((ifile==0)?"":"same")  );

//		    }

		} //cell iter

	    } // module iter
	} // hist iter
    } // file iter


    for(Int_t canv_iter = 0; canv_iter <= canv_arr.GetLast(); canv_iter++)
    {
	TString iHist_commennts = ((TObjString*)(hists_comments.At(0)))->GetString();
	TString iFile_comment = ((TObjString*)(files_comments.At(canv_iter)))->GetString();
	if(print_log) iFile_comment += "_log";

	TCanvas *icanv = (TCanvas*)(canv_arr.At(canv_iter));
	icanv->SaveAs(Form("%spic_%s_%s.png", result_path.Data(), iFile_comment.Data(), iHist_commennts.Data() ));
	icanv->SaveAs(Form("%spic_%s_%s.eps", result_path.Data(), iFile_comment.Data(), iHist_commennts.Data() ));
    }




    /*
    TCanvas *canv =  new TCanvas("canv", "canv");
    canv->DivideSquare(total_hists);

    for(Int_t ifile = 0; ifile < total_files; ifile++)
    {
	TString iFile_name = ((TObjString*)(files_names.At(ifile)))->GetString();
	TString iFile_comment = ((TObjString*)(files_comments.At(ifile)))->GetString();

	for(Int_t ihist = 0; ihist < total_hists; ihist++)
	{
	    TString iHist_name = ((TObjString*)(hists_names.At(ihist)))->GetString();
	    TString iHist_commennts = ((TObjString*)(hists_comments.At(ihist)))->GetString();

	    ((TFile*)files_arr.At(ifile))->cd();
	    th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny(  iHist_name.Data() )));

	    if(th1_hist_ptr == NULL)
	    {
		printf("hist %s not found at file %s\n", iHist_name.Data(), iFile_name.Data());
		continue;
	    }
	    else
	    {
		printf("WORKING: hist %s at file %s\n", iHist_name.Data(), iFile_name.Data());
	    }

	    th1_hist_ptr->SetTitle(Form("%s [%s]", iHist_commennts.Data(), iFile_comment.Data()));
	    th1_hist_ptr->SetLineColor(ifile+1);

	    th1_hist_ptr->Rebin(6);
	    //	    th1_hist_ptr->SetAxisRange(40, 140);
	    //	    Float_t hist_mean = th1_hist_ptr->GetMean();
	    //	    tfunc_ptr = new TF1("fitfunc", "gaus", hist_mean-20, hist_mean+20);
	    //	    tfunc_ptr->SetLineColor(ifile+1);
	    //	    th1_hist_ptr->Fit(tfunc_ptr, "RQN");
	    //	    mean = tfunc_ptr->GetParameter(1);
	    //	    sigma = tfunc_ptr->GetParameter(2);
	    //	    th1_hist_ptr->SetAxisRange(-50, 300);
	    //	    th1_hist_ptr->SetTitle(Form("%s res %.1f [%s]", iHist_commennts.Data(), (sigma/mean)*100., iFile_comment.Data()));



	    TVirtualPad *pad_curr = canv->cd(ihist+1);
	    //th1_hist_ptr->DrawNormalized(  ((ifile==0)?"":"same")  );
	    th1_hist_ptr->Draw(  ((ifile==0)?"":"same")  );

	    //tfunc_ptr->Draw("same");

	    if(ifile == total_files-1)
	    {
		pad_curr->BuildLegend();
	    }

	}
    }
*/






}
