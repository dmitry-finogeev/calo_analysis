#ifndef EVENT_DATA_STRUCT
#define EVENT_DATA_STRUCT

#include<TTree.h>
#include<TString.h>

const Int_t MPPC_to_CBM_ch_map[10] = {10, 8, 6, 4, 2, 9, 7, 5, 3, 1};
Int_t get_ch_MPPC_by_CBM(const Int_t cbm_ch)
{
    for(Int_t mppc_ch = 0; mppc_ch < 10; mppc_ch++)
	if(MPPC_to_CBM_ch_map[mppc_ch] == cbm_ch) return mppc_ch;

    return -1;
}

//data processed from waveform
struct event_data_struct
{
    Int_t time_stamp;

    Int_t integral_in_gate;

    Int_t MAX_in_gate;
    Int_t MIN_in_gate;
    Int_t MAX_out_gate;
    Int_t MIN_out_gate;

    Int_t time_max_in_gate;
    Int_t time_min_in_gate;

    Float_t mean_in_gate;
    Float_t mean_out_gate;
    Float_t RMS_in_gate;
    Float_t RMS_out_gate;


    void reset()
    {
	time_stamp = 0;

	integral_in_gate = 0; //integral / gate_width

	MAX_in_gate = 0;
	MIN_in_gate = 0;
	MAX_out_gate = 0;
	MIN_out_gate = 0;

	time_max_in_gate = 0;
	time_min_in_gate = 0;

	mean_in_gate = 0.;
	mean_out_gate = 0.;
	RMS_in_gate = 0.;
	RMS_out_gate = 0.;
    }

    void Print()
    {
	printf("[time %d; integral_in_gate %i; MAX_in_gate %i; MIN_in_gate %i; MAX_out_gate %i; MIN_out_gate %i; time_max_in_gate %i; time_min_in_gate %i; mean_in_gate %.2f; mean_out_gate %.2f; RMS_in_gate %.2f; RMS_out_gate %.2f; ]",
	       time_stamp, integral_in_gate, MAX_in_gate, MIN_in_gate, MAX_out_gate, MIN_out_gate, time_max_in_gate, time_min_in_gate, mean_in_gate, mean_out_gate, RMS_in_gate, RMS_out_gate);
    }

    static TString GetChName(Int_t channel_num)
    {
	return TString::Format("channel_%i", channel_num);
    }

    TBranch* CreateBranch(TTree *tree, Int_t channel_num)
    {
	return tree->Branch(GetChName(channel_num).Data(), this,
	                    "time_stamp/I:integral_in_gate:MAX_in_gate:MIN_in_gate:MAX_out_gate:MIN_out_gate:time_max_in_gate:time_min_in_gate:mean_in_gate/F:mean_out_gate:RMS_in_gate:RMS_out_gate");
    }


    Int_t SetBranch(TTree *tree, Int_t channel_num)
    {
	return tree->SetBranchAddress(GetChName(channel_num).Data(), this);
    }

};


struct event_log_data_struct
{
    Int_t temp;
    Int_t LED_HV;
    Int_t PinDiode;

    void reset()
    {
	temp = 0;
	LED_HV = 0;
	PinDiode = 0;
    }

    void Print()
    {
	printf("[temp %.2fC; LED_HV %i; Pin Diode %i]", (((Float_t)temp)/100.), LED_HV, PinDiode);
    }


    TBranch* CreateBranch(TTree *tree)
    {
	return tree->Branch("log_data", this,"temp/I:LED_HV:PinDiode");
    }


    Int_t SetBranch(TTree *tree)
    {
	return tree->SetBranchAddress("log_data", this);
    }

    event_log_data_struct& operator= (event_log_data_struct const& rth)
    {
	if (this != &rth)
	{
	    this->temp = rth.temp;
	    this->LED_HV = rth.LED_HV;
	    this->PinDiode = rth.PinDiode;
	}

	return *this;
    }

};


#endif // EVENT_DATA_STRUCT
