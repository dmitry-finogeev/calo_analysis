#include <iostream>
#include <fstream>
//#include <conio.h>
#include <ctime>

#include <TFile.h>
#include <TGraph.h>
#include <TF1.h>
#include <TTree.h>

#include <TString.h>

#include <TCanvas.h>
#include <TH1F.h>


#include "adc64_text_data.h"




void signal_viewer()
{
    //input data
    TString source_path = "/mnt/hgfs/DETECTORS_DATA/CALO/";

    Int_t events_to_process = 2000000;
    Int_t gate_beg = 40;
    Int_t gate_end = 130;


    /***/
    TString source_file_name = "adc64_17_04_06_19_15_overnight"; //2190402 readed 26284823 lines
    /***/


    printf("\n\n\n######################################################\n");
    printf("#################  SIGNAL VIEWER  ##################\n");
    printf("######################################################\n\n\n");


    adc64_text_data data_class(source_path + source_file_name + ".txt");
    Int_t channel_total = data_class.Get_Channel_total();
    Int_t sample_total = data_class.Get_Samples_total();
    event_data_struct *event_struct = new event_data_struct[channel_total];


    Int_t Channel_to_show = 0;

    TCanvas *canv = new TCanvas("canv", "canv");
    TGraph *waveform_graph = new TGraph();
    waveform_graph->SetTitle(Form("Channel [%i]", Channel_to_show));

    Int_t start_time_stamp = -1;
    for(Int_t nEvent = 0; nEvent <= events_to_process; nEvent++)
    {
	if((data_class.read_event()) != 0) break;
	data_class.Calculate_waveform(event_struct[Channel_to_show], Channel_to_show, gate_beg, gate_end);

	if(start_time_stamp < 0)
	{
	    start_time_stamp = event_struct[Channel_to_show].time_stamp;
	    printf("first timestamp %i\n", start_time_stamp);
	}
	event_struct[Channel_to_show].time_stamp -= start_time_stamp;


	if(event_struct[Channel_to_show].time_stamp > 200)
	{
	    for(Int_t s=0;s<sample_total;s++)
		waveform_graph->SetPoint(s, s, data_class.Get_samples_data()[Channel_to_show][s]);

	    printf("\n\n-------------------------\n");
	    event_struct[Channel_to_show].Print(); printf("\n");

	    waveform_graph->Draw("ALP*");
	    canv->Update();
	    canv->Modified();

	    printf("INPUT 'q' TO QUIT -------------------------\n");

	    char key = getchar();
	    if(key == 'q')break;
	}


	Int_t **sample_data = data_class.Get_samples_data();
    }


    delete [] event_struct;

}


