/*
 * Calorimeter module cosmic test analysis
 * Open root data file converted from bin data
 * with run_info.txt file and process results
 * as amplitude peaks with
 *
 * !!!!!!!!!!!!!!!!!!!!!
 * Run macro in bash mode:
 * root -b -q -l cosmic_analysis.c+

 * string to change for analysis:
 *
 *  -- #define isLOCAL 1
 *
 *  -- TString runs_path
 *  -- TString result_path
 *  -- TString result_file_postfix = "supermodule_6";
 *
 *  -- runs_names.Add(new TObjString("adc64_17_09_21_16_31_overnight_bin"));
 *
 *  -- #define calc_cons_ver 2
 *
 *
 *
 * Dmitry Finogeev
 * dmitry-finogeev@yandex.ru
 *
 *
 * scp /mnt/disk_data_8TB/cosmic_data_2018_quater3/RESULTS/NICA_results_sep_all_sel_fitsignals__timensig_2_sa2000.pdf dfinogee@lxplus.cern.ch:/eos/user/d/dfinogee/TEMP/
 */

#define isLOCAL 0 // local - at my laptop
#define is_one_dir 0

#include <iostream>
#include <fstream>
//#include <conio.h>
#include <ctime>

#include <TFile.h>
#include <TGraph.h>
#include <TF1.h>
#include <TTree.h>
#include <TObjArray.h>
#include <TString.h>
#include <TDirectory.h>
#include <TCanvas.h>
#include <TH1F.h>
#include <TSystemFile.h>
#include <TSystemDirectory.h>
#include <TBrowser.h>
#include <TAxis.h>
#include "langaus.hh"

#include "Readme_reader.h"
//#include "data_calculating.h"
#include "event_data_struct_v0.h"

#include "../libraries/utilites/utilites.h"

Double_t
makeCalib(TH1 *hsum, Double_t x_min = 4000., Double_t x_max = 10000.)
{

	TF1 *fit = new
	TF1 ("langau", langaufun, x_min, x_max, 4);
	fit->SetParNames ("Width", "MP", "Area", "GSigma");
	fit->SetParameters (1000, 7000, 1500000, 5000.);

	//fit->SetParLimits(0,5.,100.); commented for small mods
	//fit->SetParLimits(1,20.,200.);
	//fit->SetParLimits(3,0.5,200.);
	fit->SetParameters (500, 2000, 7815397, 800.);
	//fit->SetParLimits(0,5.,100.); commented for small mods
	//fit->SetParLimits(1,20.,200.);
	//fit->SetParLimits(3,0.5,200.);

	hsum->Fit (fit, "Q", "", x_min, x_max);

	Double_t maxx, FWHM;
	langaupro (fit->GetParameters (), maxx, FWHM);

	//cout << "maxx = " << maxx << endl;
	printf ("widtg: %f; MP: %f; Area: %f; GSigma %f\n", fit->GetParameter (0), fit->GetParameter (1), fit->GetParameter (2), fit->GetParameter (3));

	return maxx;

}


void cosmic_analysis_fit()
{
	// input data path ########################################################################
#if isLOCAL
	//    TString runs_path = "/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_source/";
	//    TString result_path = "/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_results/cosmic_autumn_2017/";
	//    TString runs_path = "/mnt/hgfs/DETECTORS_DATA/SOURCE/CALO/t9_testbeam_nov2017/";
	//    TString result_path = "/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_results/t9_testbeam_nov2017/";
	//    TString runs_path = "/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_source/T9_test_nov_17/";
	TString runs_path = "/mnt/hgfs/DETECTORS_DATA/SOURCE/CALO/T9_cosmic/";
	TString result_path = "/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_results/cosmic_T9/";
#else
	//TString runs_path = "/mnt/disk_data/beam_data_2017_sep/";
	//TString runs_path = "/mnt/disk_data/cosmic_data_2017_quarter4/";
	//TString result_path = "/mnt/disk_data/cosmic_data_2017_quarter4/RESULTS/";
	//    TString runs_path = "/home/runna61/cosmic_data_2018_quater1/";
	//    TString result_path = "/home/runna61/cosmic_data_2018_quater1/RESULTS/";
	//    TString runs_path = "/mnt/disk_data/cosmic_data_2018_quater1/";
//      TString runs_path = "/mnt/disk_data/cosmic_data_2018_quater2/";

//        TString result_path = "/mnt/disk_data/cosmic_data_2018_quater2/RESULT/";
	//    TString runs_path = "/afs/cern.ch/work/a/aizvestn/public/";
	//    TString result_path = "/eos/user/d/dfinogee/RESULTS/CALO/root_results/cosmic_T9/";

	//TString runs_path = "/home/runna61/cosmic_data_2018_quater2/";
	//TString result_path = "/home/runna61/adc64_data_2018_quater1/cosmic_results/";

//    TString runs_path = "/mnt/disk_data/cosmic_data_2018_quater2/Optical_contact_tests/setting3/";
//    TString result_path = "/mnt/disk_data/cosmic_data_2018_quater2/Optical_contact_tests/setting3/";

//    TString runs_path = "/mnt/disk_data/cosmic_data_2018_quater2/Optical_contact_tests/commroots/";
//    TString result_path = "/mnt/disk_data/cosmic_data_2018_quater2/Optical_contact_tests/commroots/";

//    TString runs_path = "/mnt/disk_data/cosmic_data_2018_quater2/";
//    TString result_path = "/mnt/disk_data/cosmic_data_2018_quater2/RESULT/";
	TString runs_path = "/mnt/disk_data/cosmic_data_2018_quater3/";
	TString result_path = "/mnt/disk_data/cosmic_data_2018_quater3/RESULT/";

#endif

	//TString result_file_name = "CBM_6modules_TH_200_ZS100_TC_3_2M";
	//TString result_file_name = "CMB_INR_cosmic_compare2";
	TString result_file_name = "NICA_OptConn_all";
	//TString result_file_name = "beam_analysis";

	TString result_file_postfix = "_timensig_2_sa2000";
	//TString result_file_postfix = "NICA_test_final_1";
	//TString result_file_postfix = "super_module_test";
	// ########################################################################################

	// list of runs to process ################################################################
	TObjArray runs_names;
	runs_names.SetOwner ();
	/**** // Tate files list from directory
	 FilesListFromDir(runs_path, &runs_names, 0, ".root");

	 /*** //Print files list for manual array
	 for(Int_t f=0; f<=runs_names.GetLast();f++)
	 current_out << Form("files_names.Add(new TObjString(\"%s\"));\n", ((TObjString*)(runs_names.At(f)))->GetString().Data());
	 /***/ //manual arry
	if (isLOCAL) {

		//runs_names.Add(new TObjString("adc64_17_08_28_15_23_fullSuperModule_cosmic"));
		//runs_names.Add(new TObjString("11.11.2017_100mv_thresh_sum"));
		//runs_names.Add(new TObjString("15.11.2017_30mv_thresh_sum"));
		//runs_names.Add(new TObjString("20.11.2017_muons_trg500"));
		//runs_names.Add(new TObjString("20.11.2017_mod5_beamTrigger"));

		//runs_names.Add(new TObjString("adc64_18_02_11_15_00_CBM_6modules_TH_200_ZS100_TC_3_2M"));
	} else {
		/********************
		runs_path = "/home/runna61/calo_analysis_2018_niko/root_files/";
		result_path = "/mnt/disk_data_8TB/cosmic_data_2018_quater3/RESULTS/";
		result_file_name = "NICA_results_test_bsight";


		//runs_names.Add(new TObjString("18_09_11_16_20_INR_12mod_NICA_TH250_ZS150_DSPoff"));
		runs_names.Add(new TObjString("18_09_20_01_13_INR_12mod_NICA_TH200_ZS100_DSPoff"));
		runs_names.Add(new TObjString("18_09_22_18_08_INR_12mod_NICA_TH200_ZS100_DSPoff"));
		runs_names.Add(new TObjString("18_10_06_19_55_INR_12mod_NICA_TH200_ZS100_DSPoff"));
		runs_names.Add(new TObjString("18_10_09_18_27_INR_12mod_NICA_TH200_ZS100_DSPoff"));
		/********************
		runs_path = "/home/runna61/calo_analysis_2018_niko/root_files/";
		result_path = "/mnt/disk_data_8TB/cosmic_data_2018_quater3/RESULTS/";
		result_file_name = "NICA_results_test_bsight";


		//runs_names.Add(new TObjString("18_09_11_16_20_INR_12mod_NICA_TH250_ZS150_DSPoff"));
		runs_names.Add(new TObjString("18_09_20_01_13_INR_12mod_NICA_TH200_ZS100_DSPoff"));
		runs_names.Add(new TObjString("18_09_22_18_08_INR_12mod_NICA_TH200_ZS100_DSPoff"));
		runs_names.Add(new TObjString("18_10_06_19_55_INR_12mod_NICA_TH200_ZS100_DSPoff"));
		runs_names.Add(new TObjString("18_10_09_18_27_INR_12mod_NICA_TH200_ZS100_DSPoff"));
		/********************/

}
// #######################################################################################

// Calculation constans ##################################################################

#define calc_cons_ver 4

// NICA cosmic
#if (calc_cons_ver == 1)
const Int_t min_charge = -100;
const Int_t max_charge = 100000;
const Int_t charge_bins = 1000;

const UChar_t pedestal_time_gate[2] = {40, 70}; // x1], [x2
const Double_t pedes_width_fit = 5000.;
const Double_t One_mip_max_fit = 40000.;

const Double_t pedes_cut_nsig = 3.;
const Double_t signal_time_sel_nsig = 1.5;

const Double_t pedescut_shift_hor = -1500.;
const Double_t max_ample_shift = -1000.;
const Double_t ampl_compare = 1000.;
const Double_t ampl_max_bsight = 9000;
const Double_t ampl_compare_bsight = 5000;

//const Long64_t EntriesCalc = 100000000;
const Long64_t EntriesCalc = 100000000;

const Double_t use_const_pedestal = 2000; //if use_const_pedestal != -1.0 this values used as constan pedestal mean
const Double_t time_gate_expand_hor = 1.;
const Int_t back_sign_base = 2;
TString Ampl_eq = "FIT_integral_in_gate";
TString Ampl_ped_eq = "integral_in_gate";
TString Time_eq = "FIT_time_half";
result_file_postfix = "_bsb2_bma9000_bsa5000";

#elif (calc_cons_ver == 2)
// supermodule cosmic CERN
const Int_t min_charge = -100;
const Int_t max_charge = 40000;
const Int_t charge_bins = 200;

const UChar_t pedestal_time_gate[2] =
{	40, 70}; // x1], [x2
const Double_t pedes_width_fit = 5000.;
const Double_t use_const_pedestal = -5000.;//if use_const_pedestal != -1.0 this values used as constan pedestal mean
const Double_t One_mip_max_fit = 12000.;

const Double_t pedes_cut_nsig = 4.;
const Double_t signal_time_sel_nsig = 1.5;

const Double_t max_ample_shift = -1000;
const Double_t ampl_compare = 1000.;

const Long64_t EntriesCalc = 10000000;
#elif (calc_cons_ver == 3)
// supermodule cosmic common treshold CERN
const Int_t min_charge = -1000;
const Int_t max_charge = 10000;
const Int_t charge_bins = 200;

const UChar_t pedestal_time_gate[2] =
{	40, 70}; // x1], [x2
const Double_t pedes_width_fit = 5000.;
const Double_t One_mip_max_fit = 12000.;

const Double_t pedes_cut_nsig = 2.;
const Double_t signal_time_sel_nsig = 1.5;

const Double_t pedescut_shift_hor = -1500.;
const Double_t max_ample_shift = -1000;
const Double_t ampl_compare = 1000.;

const Long64_t EntriesCalc = 100000000;
#elif (calc_cons_ver == 4)
// supermodule cosmic 6mod zero sup
const Int_t min_charge = -100;
const Int_t max_charge = 200000;
const Int_t charge_bins = 1000;

const UChar_t pedestal_time_gate[2] = {40, 70}; // x1], [x2 
const Double_t pedes_width_fit = 5000.;
const Double_t One_mip_max_fit = 10000.;

const Double_t pedes_cut_nsig = 3.;
const Double_t signal_time_sel_nsig = 2;

const Double_t pedescut_shift_hor = -1500.;
const Double_t max_ample_shift = 10000.;
const Double_t ampl_compare = 1000.;
const Double_t ampl_max_bsight = 9000;
const Double_t ampl_compare_bsight = 5000;

const Long64_t EntriesCalc = 100000000;
//const Long64_t EntriesCalc = 10000;

const Double_t use_const_pedestal = 2000; //if use_const_pedestal != -1.0 this values used as constan pedestal mean
const Double_t use_fixed_max_mean = 4000; //if  != -1.0 this values used else value from fit
const Double_t time_gate_expand_hor = 1.;
const Int_t back_sign_base = 3;
TString Ampl_eq = "integral_in_gate";
TString Ampl_ped_eq = "integral_in_gate";
TString Time_eq = "time_half";
result_file_postfix = "_bsb3_bma9000_bsa5000";

runs_path = "/mnt/disk_data_8TB/cosmic_data_2018_quater2/";
result_path = "/mnt/disk_data_8TB/cosmic_data_2018_quater2/RESULTS/";
result_file_name = "CBM_results_test_bsight_fixmaxmean";


//runs_names.Add(new TObjString("adc64_18_06_04_17_20_INR_6mod_CBM_TH200_ZS150"));
runs_names.Add(new TObjString("adc64_18_06_05_18_37_INR_6mod_CBM_TH200_ZS150"));
runs_names.Add(new TObjString("adc64_18_06_05_23_24_INR_6mod_CBM_TH200_ZS150"));
runs_names.Add(new TObjString("adc64_18_06_06_16_23_INR_6mod_CBM_TH200_ZS150"));
runs_names.Add(new TObjString("adc64_18_06_08_17_12_INR_6mod_CBM_TH200_ZS150"));
runs_names.Add(new TObjString("adc64_18_06_13_16_15_INR_6mod_CBM_TH200_ZS150"));
//runs_names.Add(new TObjString("adc64_18_06_14_15_20_INR_6mod_CBM_TH200_ZS150"));
runs_names.Add(new TObjString("adc64_18_06_15_17_06_INR_6mod_CBM_TH200_ZS150"));



#elif (calc_cons_ver == 5)
const Int_t min_charge = -100;
const Int_t max_charge = 100000;
const Int_t charge_bins = 1000;

const UChar_t pedestal_time_gate[2] =
{	40, 130}; // x1], [x2
//const Double_t use_const_pedestal = -1.; //if use_const_pedestal != -1.0 this values used as constan pedestal mean
const Double_t use_const_pedestal = 2000.;//if use_const_pedestal != -1.0 this values used as constan pedestal mean

const Double_t One_mip_max_fit = 10000.;
const Double_t pedes_width_fit = 5000.;
const Double_t pedes_cut_nsig = 2;
const Double_t signal_time_sel_nsig = 2.;

const Double_t time_gate_expand_hor = 1.;
const Double_t pedescut_shift_hor = -1000.;
const Double_t max_ample_shift = 10000;
const Double_t ampl_compare = 2000.;

const Long64_t EntriesCalc = 100000000;
//const Long64_t EntriesCalc = 100000;

#endif
// #######################################################################################

printf ("\n\n\n########################################################################################\n");
printf ("#################################   COSMIC ANALYSIS   ##################################\n");
printf ("########################################################################################\n\n");

// Initialisation ########################################################################
Int_t total_runs = runs_names.GetLast () + 1;
Int_t nfiles_skiped = 0;
time_t start_time = time (NULL);
current_out << Form ("Starting cosmic analysis at %s", asctime (localtime (&start_time))) << std
::endl;

TString result_file_path = result_path + result_file_name + "_" + result_file_postfix;

Readme_reader *rmreader_ptr = new
Readme_reader (runs_path);

TObjArray *result_array = new
TObjArray;
result_array->SetName ("analysis_results");
result_array->SetOwner ();

TObjArray *canvas_array = new
TObjArray;
canvas_array->SetName ("Canvas");
canvas_array->SetOwner ();
result_array->Add (canvas_array);

//temp canvas, will be deleted at finish
TCanvas *temp_canv = new
TCanvas ("temp", "temp");

Double_t mean, sigma;
TH1 *th1_hist_ptr = NULL;
TH2 *th2_hist_ptr = NULL;
TF1 *tf1_func_ptr = NULL;
TCanvas *tcanv_ptr = NULL;
// #######################################################################################

// Open result file #######################################################################
TFile *result_file = new
TFile ((result_file_path + ".root").Data (), "RECREATE");
result_file->cd ();
// #######################################################################################

// #######################################################################################
// ######################   MAIN LOOP   ##################################################
// #######################################################################################
for (Int_t iFile = 0; iFile < total_runs; iFile++) {

	// Initialisation *****************************************
	TString iRun_name = ((TObjString*) (runs_names.At (iFile)))->GetString ();
	current_out << std
	::endl << Form("######### RUN %s %i of %i (skiped %i) #########",
		iRun_name.Data(), iFile+1, total_runs, nfiles_skiped) << std::endl;

	// Readme reader ///////////////////////////////////////////////////////////////////////
	TString run_data_path = "";
	if (is_one_dir) {
		printf ("Opening info file %s\n", (runs_path + iRun_name + ".txt").Data ());
		if (rmreader_ptr->SetInfoFile (runs_path + iRun_name + ".txt") < 0) return;
		rmreader_ptr->SetRunName(iRun_name);
		rmreader_ptr->Print ();
		run_data_path = runs_path + iRun_name + ".root";
	} else {

		if (rmreader_ptr->Set_runfolder_name (iRun_name.Data ()) < 0) {
			current_out << Form ("ERROR: Info file %s not found !!!", rmreader_ptr->Get_info_file_path ().Data ()) << std
			::endl;
			nfiles_skiped++;
			continue;
		}
		run_data_path = rmreader_ptr->Get_converted_file_name ().Data ();
	}
	////////////////////////////////////////////////////////////////////////////////////////

	TFile *source_file = new
	TFile (run_data_path.Data (), "READONLY");
	if (!source_file) {
		current_out << Form ("ERROR: File %s not found !!!", run_data_path.Data ()) << std
		::endl;
		nfiles_skiped++;
		continue;
	}

	TTree *data_tree = dynamic_cast<TTree*>(source_file->FindObjectAny(Readme_reader::tree_name));

	if (!data_tree) {
		current_out << Form ("ERROR: Data tree not found !!!") << std
		::endl;
		source_file->Close ();
		delete source_file;
		nfiles_skiped++;
		continue;
	}
	gDirectory->cd("Rint:/");

	Int_t total_modules = rmreader_ptr->Get_module_num ();

	TObjArray *file_data_array = new
	TObjArray ();
	file_data_array->SetName ((iRun_name + "").Data ());
	file_data_array->SetOwner ();
	result_array->Add (file_data_array);

	time_t current_time = time (NULL);
	Int_t proc_sec = difftime (current_time, start_time);
	Float_t percents = (float) iFile / (total_runs);
	Float_t proc_rate = (float) proc_sec / (iFile - nfiles_skiped) / 60.;
	Int_t time_est = (percents == 0) ? 0 : proc_sec / percents * (1. - percents);
	current_out
		<< Form ("Calculating file: \"%s\" %i/%i [skiped %i] (%5.1f%%) [total events: %lld]; [pas %3.0dm %2.0is] [est %3.0dm %2.0is] [%.1f min/file]", iRun_name.Data (), iFile + 1, total_runs,
							nfiles_skiped, (percents * 100.), data_tree->GetEntries (), (proc_sec / 60), proc_sec % 60, (time_est / 60), time_est % 60, proc_rate) << std
	::endl << std::endl;

	if (OUTINFILE) printf ("Calculating file: \"%s\" %i/%i [skiped %i] (%5.1f%%) [total events: %lld]; [pas %3.0dm %2.0is] [est %3.0dm %2.0is] [%.1f min/file]\n", iRun_name.Data (), iFile + 1,
													total_runs, nfiles_skiped, (percents * 100.), data_tree->GetEntries (), (proc_sec / 60), proc_sec % 60, (time_est / 60), time_est % 60, proc_rate);
	// ********************************************************

	// Module loop ********************************************
	for (Int_t module_iter = 0; module_iter < total_modules; module_iter++)
	//    for(Int_t module_iter = 4; module_iter < 5; module_iter++)
		{
		// Initialisation ---------------------------------
		TString module_name = rmreader_ptr->Get_module_info ()[module_iter].name;
		Int_t total_cells = rmreader_ptr->Get_module_info ()[module_iter].total_cells;

		current_out << std
		::endl << Form("######### [module %i] : \"%s\" #########",
			module_iter, module_name.Data()) << std::endl;

		TObjArray *module_data_array = new
		TObjArray ();
		module_data_array->SetName (Form ("%s", module_name.Data ()));
		module_data_array->SetOwner ();
		file_data_array->Add (module_data_array);

		Double_t *Pedestal_edge = new
		Double_t[total_cells];
		Double_t *Signal_time_min = new
		Double_t[total_cells];
		Double_t *Signal_time_max = new
		Double_t[total_cells];
		Double_t *OneMip_mean = new
		Double_t[total_cells];
		// ------------------------------------------------

		// Cell loop ******************************************
		for (Int_t cell_iter = 0; cell_iter < total_cells; cell_iter++) {
			// Initialisation ---------------------------------
			TObjArray *cell_data_array = new
			TObjArray ();
			cell_data_array->SetName (Form ("cell_%i", cell_iter));
			cell_data_array->SetOwner ();
			module_data_array->Add (cell_data_array);

			Int_t tree_ch = rmreader_ptr->GetContCh_from_CellN (module_iter, cell_iter);
			TString channel_name = event_data_struct
			::GetChName(tree_ch);
			TString hist_name_id = rmreader_ptr->GetChPrefix (module_iter, cell_iter);

			current_out << std
			::endl << Form("--------- pass 1 cell %i %s ---------",
				cell_iter, hist_name_id.Data()) << std::endl;
			// ------------------------------------------------

			// Pedestal -----------------------------
			th1_hist_ptr = new
			TH1F ("temp", "temp", 300, -10000, 20000);

			data_tree->Draw (Form ("(%s.%s)>>temp", channel_name.Data (), Ampl_ped_eq.Data ()),
												Form ("(%s.%s <= %i)||(%s.%s >= %i)", channel_name.Data (), Time_eq.Data (), pedestal_time_gate[0], channel_name.Data (), Time_eq.Data (), pedestal_time_gate[1]), "",
												10000);

			FitHistogrammInMaxPeak (th1_hist_ptr, mean, sigma, pedes_width_fit, -4000, 4000);
			if (use_const_pedestal != -1.0) {
				mean = use_const_pedestal;
				sigma = 0.;
			}
			Pedestal_edge[cell_iter] = mean + sigma * pedes_cut_nsig;

			th1_hist_ptr->GetXaxis()->SetRangeUser(mean-sigma * (pedes_cut_nsig+2), mean+sigma * (pedes_cut_nsig+2));
			th1_hist_ptr->SetName (Form ("%s_pedestal", hist_name_id.Data ()));
			th1_hist_ptr->SetTitle (Form ("Pedestal, [pedestal cut %.1f] {%s};ch", Pedestal_edge[cell_iter], hist_name_id.Data ()));
			cell_data_array->Add (th1_hist_ptr);

			// ------------------------------------------------

			// Signal time pedestal cut -----------------------
			th1_hist_ptr = new
			TH1F ("temp", "temp", 200, 0, 200);

			data_tree->Draw (Form ("%s.%s>>temp", channel_name.Data (), Time_eq.Data ()), Form ("((%s.%s) > %f)", channel_name.Data (), Ampl_eq.Data (), Pedestal_edge[cell_iter]/*+1000.*/), "",
												EntriesCalc);

			FitHistogrammInMaxPeak (th1_hist_ptr, mean, sigma, 5., 30, 80);
			Signal_time_min[cell_iter] = mean - signal_time_sel_nsig * sigma;
			Signal_time_max[cell_iter] = mean + signal_time_sel_nsig * sigma;

			tf1_func_ptr = th1_hist_ptr->GetFunction (fit_funk_name);
			if (tf1_func_ptr) tf1_func_ptr->SetRange (Signal_time_min[cell_iter], Signal_time_max[cell_iter]);

			th1_hist_ptr->SetName (Form ("%s_signaltime_pedestal", hist_name_id.Data ()));
			th1_hist_ptr->SetTitle (Form ("Signal time, pedestal cut  [%.1f <= time <= %.1f] {%s}", Signal_time_min[cell_iter], Signal_time_max[cell_iter], hist_name_id.Data ()));
			th1_hist_ptr->GetXaxis()->SetRangeUser(Signal_time_min[cell_iter]-20., Signal_time_max[cell_iter]+20.);
			th1_hist_ptr->GetXaxis ()->SetRangeUser (0, 200);
			cell_data_array->Add (th1_hist_ptr);
			// ------------------------------------------------

			// One mip amplitude -----------------------------
			th1_hist_ptr = new
			TH1F ("temp", "temp", charge_bins, min_charge, max_charge);

			data_tree->Draw (
				Form ("(%s.%s)>>temp", channel_name.Data (), Ampl_eq.Data ()),
				Form ("(%s.%s >= %f)&&(%s.%s <= %f)", channel_name.Data (), Time_eq.Data (), Signal_time_min[cell_iter], channel_name.Data (), Time_eq.Data (), Signal_time_max[cell_iter]), "",
				EntriesCalc);
			mean = makeCalib (th1_hist_ptr, Pedestal_edge[cell_iter], One_mip_max_fit);
			
			if (use_fixed_max_mean != -1.0) {
				OneMip_mean[cell_iter] = use_fixed_max_mean;
			}
			else
			{
			    OneMip_mean[cell_iter] = mean;
			}
			

			th1_hist_ptr->SetName (Form ("%s_ample_timesel", hist_name_id.Data ()));
			th1_hist_ptr->SetTitle (Form ("Amplitude, {%s};ch", hist_name_id.Data ()));
			cell_data_array->Add (th1_hist_ptr);

			// ------------------------------------------------

		}
		// cell loop ******************************************

		/******************************************************/
		// Cell loop 2 ****************************************
		for (Int_t cell_iter = 0; cell_iter < total_cells; cell_iter++) {
			// Initialisation ---------------------------------
			TObjArray *cell_data_array = (TObjArray*) (module_data_array->At (cell_iter));

			Int_t tree_ch = rmreader_ptr->GetContCh_from_CellN (module_iter, cell_iter);
			Int_t tree_ch_left = (cell_iter > 0) ? rmreader_ptr->GetContCh_from_CellN (module_iter, cell_iter - 1) : -1;
			Int_t tree_ch_right = (cell_iter < total_cells - 1) ? rmreader_ptr->GetContCh_from_CellN (module_iter, cell_iter + 1) : -1;

			TString channel_name = event_data_struct::GetChName(tree_ch);
			TString channel_name_left = event_data_struct::GetChName(tree_ch_left);
			TString channel_name_right = event_data_struct::GetChName(tree_ch_right);

			TString hist_name_id = rmreader_ptr->GetChPrefix (module_iter, cell_iter);

			current_out << std
			::endl << Form("--------- pass 2 cell %i %s ---------",
				cell_iter, hist_name_id.Data()) << std::endl;

			// Selections -------------------------------------
			TString selection_eq_signal_time = Form ("(%s.%s >= %f)&&(%s.%s <= %f)", channel_name.Data (), Time_eq.Data (), Signal_time_min[cell_iter], channel_name.Data (), Time_eq.Data (),
																								Signal_time_max[cell_iter]);
			TString selection_eq_signal_time_right =
				(tree_ch_right >= 0) ?
					Form ("(%s.%s >= %f)&&(%s.%s <= %f)", channel_name_right.Data (), Time_eq.Data (), Signal_time_min[cell_iter + 1], channel_name_right.Data (), Time_eq.Data (),
								Signal_time_max[cell_iter + 1]) : "(0)";
			TString selection_eq_signal_time_left =
				(tree_ch_left >= 0) ?
					Form ("(%s.%s >= %f)&&(%s.%s <= %f)", channel_name_left.Data (), Time_eq.Data (), Signal_time_min[cell_iter - 1], channel_name_left.Data (), Time_eq.Data (),
								Signal_time_max[cell_iter - 1]) : "(0)";

			TString selection_eq_pedescut_right = (tree_ch_right >= 0) ? Form ("((%s.%s) > %f)", channel_name_right.Data (), Ampl_eq.Data (), Pedestal_edge[cell_iter + 1]) : "(0)";
			TString selection_eq_pedescut_left = (tree_ch_left >= 0) ? Form ("((%s.%s) > %f)", channel_name_left.Data (), Ampl_eq.Data (), Pedestal_edge[cell_iter - 1]) : "(0)";

			TString selection_eq_ampl_max_right = (tree_ch_right >= 0) ? Form ("((%s.%s) < %f)", channel_name_right.Data (), Ampl_eq.Data (), OneMip_mean[cell_iter + 1] + max_ample_shift) : "(0)";
			TString selection_eq_ampl_max_left = (tree_ch_left >= 0) ? Form ("((%s.%s) < %f)", channel_name_left.Data (), Ampl_eq.Data (), OneMip_mean[cell_iter - 1] + max_ample_shift) : "(0)";

			TString selection_eq_ampl_equal_right =
				(tree_ch_right >= 0) ? Form ("(abs(%s.%s - %s.%s) < %f)", channel_name.Data (), Ampl_eq.Data (), channel_name_right.Data (), Ampl_eq.Data (), ampl_compare) : "(0)";
			TString selection_eq_ampl_equal_left =
				(tree_ch_left >= 0) ? Form ("(abs(%s.%s - %s.%s) < %f)", channel_name.Data (), Ampl_eq.Data (), channel_name_left.Data (), Ampl_eq.Data (), ampl_compare) : "(0)";

			TString selection_one_left = selection_eq_signal_time_left + "&&" + selection_eq_pedescut_left + "&&" + selection_eq_signal_time + "&&" + selection_eq_ampl_max_left;
			printf ("one left selection :%s\n\n", selection_one_left.Data ());

			TString selection_one_rignt = selection_eq_signal_time_right + "&&" + selection_eq_pedescut_right + "&&" + selection_eq_signal_time + "&&" + selection_eq_ampl_max_right;
			printf ("one rignt selection :%s\n\n", selection_one_rignt.Data ());

			TString selection_one_eqamp_right = selection_eq_signal_time_right + "&&" + selection_eq_pedescut_right + "&&" + selection_eq_signal_time + "&&" + selection_eq_ampl_equal_right;
			printf ("one rignt eqamp selection :%s\n\n", selection_one_eqamp_right.Data ());

			TString selection_one_eqamp_left = selection_eq_signal_time_left + "&&" + selection_eq_pedescut_left + "&&" + selection_eq_signal_time + "&&" + selection_eq_ampl_equal_left;
			printf ("one left eqamp selection :%s\n\n", selection_one_eqamp_left.Data ());

			TString selection_rignt_or_left_eqamp = Form ("(%s)||(%s)", selection_one_eqamp_right.Data (), selection_one_eqamp_left.Data ());
			printf ("rignt or left eqamp selection :%s\n\n", selection_rignt_or_left_eqamp.Data ());

			TString selection_rignt_or_left = Form ("(%s)||(%s)", selection_one_left.Data (), selection_one_rignt.Data ());
			printf ("rignt or left selection :%s\n\n", selection_rignt_or_left.Data ());

			TString selection_three_cells = selection_eq_signal_time_right + "&&" + selection_eq_signal_time_left + "&&" + selection_eq_pedescut_left + "&&" + selection_eq_pedescut_right + "&&"
				+ selection_eq_signal_time;
			printf ("three selection :%s\n\n", selection_three_cells.Data ());

			TString selection_eq_hor_time = "(1)";
			for (Int_t icell = 0; icell < total_cells; icell++) {
				Int_t tree_chi = rmreader_ptr->GetContCh_from_CellN (module_iter, icell);
				TString channeli_name = event_data_struct
				::GetChName(tree_chi);

				if (icell != cell_iter) selection_eq_hor_time += Form ("&&(%s.%s >= %f)&&(%s.%s <= %f)", channeli_name.Data (), Time_eq.Data (), Signal_time_min[icell] - time_gate_expand_hor,
																																channeli_name.Data (), Time_eq.Data (), Signal_time_max[icell] + time_gate_expand_hor);

				if (icell != cell_iter) {
					selection_eq_hor_time += Form ("&&(((%s.%s) > %f))", channeli_name.Data (), Ampl_eq.Data (), Pedestal_edge[icell] + pedescut_shift_hor);
					// 						      channeli_name.Data(), 500.);
				}
			}
			
			printf ("hor selection :%s\n\n", selection_eq_hor_time.Data ());
			// ------------------------------------------------


			
			// back sight selection
			Int_t cell_1 = (cell_iter<total_cells*.5)? cell_iter+1 : cell_iter-1;
			Int_t tree_ch_cell1 = rmreader_ptr->GetContCh_from_CellN (module_iter, cell_1);
			TString channel_name_cell1 = event_data_struct::GetChName(tree_ch_cell1);
			
			Int_t cell_2 = (cell_iter<total_cells*.5)? cell_iter+1+back_sign_base : cell_iter-1-back_sign_base;
			Int_t tree_ch_cell2 = rmreader_ptr->GetContCh_from_CellN (module_iter, cell_2);
			TString channel_name_cell2 = event_data_struct::GetChName(tree_ch_cell2);
			
			
			TString selection_eq_signal_time_cell1 = Form ("(%s.%s >= %f)&&(%s.%s <= %f)",
			      channel_name_cell1.Data (), Time_eq.Data (), Signal_time_min[cell_1], channel_name_cell1.Data (), Time_eq.Data (), Signal_time_max[cell_1]);
			TString selection_eq_signal_time_cell2 = Form ("(%s.%s >= %f)&&(%s.%s <= %f)",
			      channel_name_cell2.Data (), Time_eq.Data (), Signal_time_min[cell_2], channel_name_cell2.Data (), Time_eq.Data (), Signal_time_max[cell_2]);
			
			TString selection_eq_pedescut_cell1 = Form ("((%s.%s) > %f)", channel_name_cell1.Data (), Ampl_eq.Data (), Pedestal_edge[cell_1]);
			TString selection_eq_pedescut_cell2 = Form ("((%s.%s) > %f)", channel_name_cell2.Data (), Ampl_eq.Data (), Pedestal_edge[cell_2]);
			
			TString selection_eq_ampl_max_cell1 = Form ("((%s.%s) < %f)", channel_name_cell1.Data (), Ampl_eq.Data (), ampl_max_bsight);
			TString selection_eq_ampl_max_cell2 = Form ("((%s.%s) < %f)", channel_name_cell2.Data (), Ampl_eq.Data (), ampl_max_bsight);
			
			TString selection_eq_ampl_equal_cell12 = Form ("(abs(%s.%s - %s.%s) < %f)",
			                                               channel_name_cell1.Data (), Ampl_eq.Data (), channel_name_cell2.Data (), Ampl_eq.Data (), ampl_compare_bsight);
			
//			TString selection_back_sight = Form ("(%s)&&(%s)&&(%s)&&(%s)&&(%s)&&(%s)&&(%s)&&(%s)",
//			                                     selection_eq_signal_time_cell1.Data (), selection_eq_signal_time_cell2.Data (),
//			                                     selection_eq_ampl_max_cell1.Data (), selection_eq_ampl_max_cell2.Data (),
//			                                     selection_eq_pedescut_cell1.Data (), selection_eq_pedescut_cell2.Data (),
//			                                     selection_eq_ampl_equal_cell12.Data(), selection_eq_signal_time.Data());
			
			TString selection_back_sight =
			                 selection_eq_signal_time_cell1 + "&&" + selection_eq_signal_time_cell2
			        + "&&" + selection_eq_pedescut_cell1 + "&&" + selection_eq_pedescut_cell2
			        + "&&" + selection_eq_ampl_max_cell1 + "&&" + selection_eq_ampl_max_cell2
			        + "&&" + selection_eq_ampl_equal_cell12 
			        + "&&" + selection_eq_signal_time
			        ;
			        
			
			printf ("back sight selection :%s\n\n", selection_back_sight.Data ());
			
			

			// Amplitude horizontal cut -----------------------
			th1_hist_ptr = new
			TH1F ("temp", "temp", charge_bins, min_charge, max_charge);

			data_tree->Draw (Form ("(%s.%s)>>temp", channel_name.Data (), Ampl_eq.Data ()), selection_eq_hor_time.Data (), "", EntriesCalc);

			th1_hist_ptr->SetName (Form ("%s_ampl_hor", hist_name_id.Data ()));
			th1_hist_ptr->SetTitle (Form ("Amplitude, [horizontal] {%s};ch", hist_name_id.Data ()));
			printf ("horizontal: selected %f events\n", th1_hist_ptr->GetEntries ());
			cell_data_array->Add (th1_hist_ptr);
			// ------------------------------------------------

			// Amplitude two near cut -----------------------
			th1_hist_ptr = new
			TH1F ("temp", "temp", charge_bins, min_charge, max_charge);

			data_tree->Draw (Form ("(%s.%s)>>temp", channel_name.Data (), Ampl_eq.Data ()), selection_three_cells.Data (), "", EntriesCalc);

			th1_hist_ptr->SetName (Form ("%s_near_two", hist_name_id.Data ()));
			th1_hist_ptr->SetTitle (Form ("Amplitude, [thee module time selection] {%s};ch", hist_name_id.Data ()));
			printf ("near two: selected %f events\n", th1_hist_ptr->GetEntries ());
			cell_data_array->Add (th1_hist_ptr);
			// ------------------------------------------------

			// Amplitude near left -----------------------
			th1_hist_ptr = new
			TH1F ("temp", "temp", charge_bins, min_charge, max_charge);

			data_tree->Draw (Form ("(%s.%s)>>temp", channel_name.Data (), Ampl_eq.Data ()), selection_one_left.Data (), "", EntriesCalc);

			th1_hist_ptr->SetName (Form ("%s_near_left", hist_name_id.Data ()));
			th1_hist_ptr->SetTitle (Form ("Amplitude, [left cell selection] {%s};ch", hist_name_id.Data ()));
			printf ("near left: selected %f events\n", th1_hist_ptr->GetEntries ());
			cell_data_array->Add (th1_hist_ptr);
			// ------------------------------------------------

			// Amplitude near right -----------------------
			th1_hist_ptr = new
			TH1F ("temp", "temp", charge_bins, min_charge, max_charge);

			data_tree->Draw (Form ("(%s.%s)>>temp", channel_name.Data (), Ampl_eq.Data ()), selection_one_rignt.Data (), "", EntriesCalc);

			th1_hist_ptr->SetName (Form ("%s_near_right", hist_name_id.Data ()));
			th1_hist_ptr->SetTitle (Form ("Amplitude, [right cell selection] {%s};ch", hist_name_id.Data ()));
			printf ("near right: selected %f events\n", th1_hist_ptr->GetEntries ());
			cell_data_array->Add (th1_hist_ptr);
			// ------------------------------------------------

			// Amplitude near right or left equal amplitude ---
			th1_hist_ptr = new
			TH1F ("temp", "temp", charge_bins, min_charge, max_charge);

			data_tree->Draw (Form ("(%s.%s)>>temp", channel_name.Data (), Ampl_eq.Data ()), selection_rignt_or_left_eqamp.Data (), "", EntriesCalc);

			th1_hist_ptr->SetName (Form ("%s_near_eqamp", hist_name_id.Data ()));
			th1_hist_ptr->SetTitle (Form ("Amplitude, [near two cell eqamp selection] {%s};ch", hist_name_id.Data ()));
			printf ("near eqamp: selected %f events\n", th1_hist_ptr->GetEntries ());
			cell_data_array->Add (th1_hist_ptr);
			// ------------------------------------------------

			// Amplitude near right or left --------------
			th1_hist_ptr = new
			TH1F ("temp", "temp", charge_bins, min_charge, max_charge);

			data_tree->Draw (Form ("(%s.%s)>>temp", channel_name.Data (), Ampl_eq.Data ()), selection_rignt_or_left.Data (), "", EntriesCalc);

			th1_hist_ptr->SetName (Form ("%s_right_left", hist_name_id.Data ()));
			th1_hist_ptr->SetTitle (Form ("Amplitude, [right or left cell selection] {%s};ch", hist_name_id.Data ()));
			printf ("right or left: selected %f events\n", th1_hist_ptr->GetEntries ());
			cell_data_array->Add (th1_hist_ptr);
			// ------------------------------------------------

			// Back sight selection --------------
			th1_hist_ptr = new
			TH1F ("temp", "temp", charge_bins, min_charge, max_charge);

			data_tree->Draw (Form ("(%s.%s)>>temp", channel_name.Data (), Ampl_eq.Data ()), selection_back_sight.Data (), "", EntriesCalc);

			th1_hist_ptr->SetName (Form ("%s_bsight", hist_name_id.Data ()));
			th1_hist_ptr->SetTitle (Form ("Amplitude, [back sight selection] {%s};ch", hist_name_id.Data ()));
			printf ("back sight selection: selected %f events\n", th1_hist_ptr->GetEntries ());
			cell_data_array->Add (th1_hist_ptr);
			// ------------------------------------------------

		}
		// cell loop 2 ****************************************

		// Canvas draw ****************************************
		/******************************************************/
		TString canv_name_prefix = rmreader_ptr->Get_run_name () + "_" + rmreader_ptr->Get_module_info ()[module_iter].name;

		// ------------------------------------------------
		tcanv_ptr = new
		TCanvas ((canv_name_prefix + "_pedestals").Data (), "pedestal");
		canvas_array->Add (tcanv_ptr);
		tcanv_ptr->DivideSquare (total_cells + 1);
		for (Int_t cell_iter = 0; cell_iter < total_cells; cell_iter++) {
			TObjArray *cell_data_array = (TObjArray*) (module_data_array->At (cell_iter));
			TString hist_name_id = rmreader_ptr->GetChPrefix (module_iter, cell_iter);
			//th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny(  Form("%s_pedestal", hist_name_id.Data()) )));
			th1_hist_ptr = ((TH1*) (cell_data_array->At (0)));
			tcanv_ptr->cd (cell_iter + 1);
			if (th1_hist_ptr) {
				th1_hist_ptr->Draw ("");
			}

		}
		// ------------------------------------------------

		// ------------------------------------------------
		tcanv_ptr = new
		TCanvas ((canv_name_prefix + "_time").Data (), "time");
		canvas_array->Add (tcanv_ptr);
		tcanv_ptr->DivideSquare (total_cells + 1);
		for (Int_t cell_iter = 0; cell_iter < total_cells; cell_iter++) {
			TObjArray *cell_data_array = (TObjArray*) (module_data_array->At (cell_iter));
			TString hist_name_id = rmreader_ptr->GetChPrefix (module_iter, cell_iter);
			th1_hist_ptr = ((TH1*) (cell_data_array->At (1)));
			tcanv_ptr->cd (cell_iter + 1)->SetLogy (1);
			if (th1_hist_ptr) {
				th1_hist_ptr->Draw ("");
			}

		}
		// ------------------------------------------------

		// ------------------------------------------------
		tcanv_ptr = new
		TCanvas ((canv_name_prefix + "_mip").Data (), "mip");
		canvas_array->Add (tcanv_ptr);
		tcanv_ptr->DivideSquare (total_cells + 1);
		for (Int_t cell_iter = 0; cell_iter < total_cells; cell_iter++) {
			TObjArray *cell_data_array = (TObjArray*) (module_data_array->At (cell_iter));
			TString hist_name_id = rmreader_ptr->GetChPrefix (module_iter, cell_iter);
			th1_hist_ptr = ((TH1*) (cell_data_array->At (2)));
			tcanv_ptr->cd (cell_iter + 1)->SetLogy (1);
			if (th1_hist_ptr) {
				th1_hist_ptr->Draw ("");
			}

		}
		// ------------------------------------------------

		temp_canv->cd ();
		th1_hist_ptr->Draw ();
		/*********/
		// ****************************************************
	delete [] OneMip_mean;
	delete [] Pedestal_edge;
	delete [] Signal_time_min;
	delete [] Signal_time_max;
}
// module loop ********************************************

source_file->Close ();
delete source_file;
}
 // #######################################################################################
 // #######################################################################################
 // #######################################################################################

 // Drawing pdf ###########################################################################
if (1) {

for (Int_t i = 0; i < canvas_array->GetLast (); i++) {
	((TCanvas*) canvas_array->At (i))->SaveAs ((result_file_path + ".pdf(").Data ());
}
((TCanvas*) canvas_array->At (canvas_array->GetLast ()))->SaveAs ((result_file_path + ".pdf)").Data ());
}
 // #######################################################################################

 // Saving results ########################################################################
SaveArrayStructInFile (result_array, result_file);
result_file->Close ();
current_out << std
::endl << Form("Result file: %s was writed\n", (result_file_path + ".root").Data()) << std::endl;
gROOT->ProcessLine (Form (".cp cosmic_analysis.c %s", (result_file_path + "_code.c").Data ()));
 // #######################################################################################

 // Terminating ###########################################################################
delete result_file;
delete rmreader_ptr;
delete temp_canv;

if (isLOCAL) {
result_file = new
TFile ((result_file_path + ".root").Data (), "READONLY");
TBrowser *br = new
TBrowser;
br->BrowseObject (result_file);
}
 // #######################################################################################

}
