#include <fstream>
#include <iostream>
#include <TNtuple.h>
#include <TFile.h>
#include <TChain.h>
#include <Rtypes.h>
#include <vector>
#include <TBrowser.h>
#include <TH1.h>
#include <TMath.h>
#include <TROOT.h>
#include <TLatex.h>
#include <TProfile.h>
#include <TStyle.h>
#include <TVirtualPad.h>
#include <TPaveText.h>
#include <TString.h>
#include "langaus.hh"


#include "Readme_reader.h"
#include "event_data_struct.h"
#include "bindataformat.h"
#include "../libraries/utilites/utilites.h"
#include "../digdataprocessing/DigVariables.cpp"


//scp -r ./* dfinogee@lxplus.cern.ch:/eos/user/d/dfinogee/RESULTS/CALO/root_results/beam_results_2018_apr/


void calo_test_analisys_T9(TString input_run_name = "void", Float_t tm_sel_min = 0., Float_t tm_sel_max = 0., TString part_name = "no_sel", Int_t IsElectrons = 0)
{
    Bool_t is_time_sel = (part_name != "no_sel");
    TString source_path;
    Float_t part_time_sel_val[2]; part_time_sel_val[0] = tm_sel_min; part_time_sel_val[1] = tm_sel_max;
    
    
    // Folder path /////////////////////////////////////////////////////////////////////////
    TString result_path = "/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_results/t9_testbeam_nov2017/";
    result_path = "/home/runna61/adc64_data_2018_quater1/beam_results/";
    result_path = "/mnt/disk_na61pc008/runna61/adc64_data_2018_quater1/beam_results/";
    result_path = "/home/runna61/adc64_data_2018_quater4/root_results/";


    //result_path += "01_test/";
    //result_path += "02_firstresults/";
    //result_path += "03_Scalib/";
    //result_path += "04_multihit/";
    //result_path += "05_narrow/";
    //result_path += "06_T9_multhit/";
    //result_path += "07_scint2/";
    //result_path += "08_scont_no_mod3/";
    //result_path += "09_T9_no_mod3/";
    //result_path += "10_scint_calib/";
    //result_path += "11_nom3_multht/";
    //result_path += "12_nom3/";
    //result_path += "13_narrow_nom3/";
    //result_path += "14_fbeam_nom3/";
    //result_path += "15_fbeam_nom3_multihit/";
    //    result_path += "16_scint_calib20/";
    //result_path += "17_new_pu_scint_calib20/";
    //    result_path += "18_new_pu_scint_calib20_el/";
    //    result_path += "19_T9_nom3_newpu_esel/";
    //   result_path += "20_T9_nmod3_newpu/";
    //result_path += "22_newpu_nmod3/";
    //result_path += "23_NA61_first/";
    //result_path += "24_NA61_calib/";
        //result_path += "25_NA61_800mV/";
         //   result_path += "26_NA61_mod2/";
         //   result_path += "27_NA61_electrons/";
        //    result_path += "28_NA61_mod3/";
	//    result_path += "29_NA61_nmod3/";
	 //   result_path += "30_NA61_nmod3_tomod5/";
	 //   result_path += "31_NA61_mod3_fastfeefit/";
	 //   result_path += "32_NA61_mod3_fastfeefit_ampl/";


    //result_path += "01_firstrun/";
    //result_path += "02_nofit/";
    result_path += "03_longcables/";
    //result_path += "15_pu_allcels/";
    //result_path += "16_pu_mod5/";
    //result_path += "17_noTOFnoPMTampl/";
    //result_path += "18_noTOF_noPMTampl/";
    //result_path += "19_noPMT_ampl/";
    //result_path += "20_Amplitude/";
    //result_path += "21_Ampl_pedsel/";
    //result_path += "22_Charge_pedsel/";
    //result_path += "23_Custom/";
    //result_path += "20_Amplitude/";


    IsElectrons = 0;
#define Is_T10_2 1
#define Is_T10_fastfee 1
#define Is_NA61 0 //Is_T10_18 must be 0

#define Is_MultiHit_selection 0
#define exclude_module 20
#define Is_Calibration 0

#define IsGetCurrCalib 0
#define IsPedestal_rejection 0
#define IsNoiseCalib 0
#define IsNewCalculation 1
#define IsUseAmplitudes 0
#define IsUseFitSignals 1
#define Calib_TOF_selection 1
    
    const Int_t beam_to_module_N = 1;
    TString run_file_name;
    
#if (!Is_Calibration)
    //    run_file_name = "21.11.2017_mod5_shutter1_in_shutter2_out";
    //    run_file_name = "22.11.2017_17.53_mod5_5GeV_focus__7m";
    //    run_file_name = "22.11.2017_mod5_6GeV_focus_7m";
    //    run_file_name = "23.11.2017_15_55_mod5_6GeV_focus_7m";
    //    run_file_name = "25.11.2017_08_41_mod5_pions_5GeV_cherenkov_coincidence";
    //    run_file_name = "25.11.2017_15_28_mod5_neg_pions_5GeV";
    //    run_file_name = "26.11.2017_21_40_mod5_6GeV_neg_pions_black_front";
    //    run_file_name = "27.11.2017_09_30_mod5_6GeV_neg_pions_black_front";
    //    run_file_name = "27.11.2017_09_56_mod5_6GeV_neg_pions_2layers_black_front";
    //    run_file_name = "27.11.2017_10_17_mod5_6GeV_neg_pions_2layers_black_back";
    //    run_file_name = "27.11.2017_10_34_mod5_6GeV_neg_pions_2layers_back_no_front";
    //    run_file_name = "27.11.2017_11_42_mod5_6GeV_neg_pions_all_closed";
    
    //    run_file_name = "26.11.2017_21_40_mod5_6GeV_neg_pions_black_front";
    
    //    run_file_name = "26.11.2017_09_40_mod5_6GeV_shutter1_in_shutter2_out_nominalHV";
    //    run_file_name = "26.11.2017_10_24_mod5_6GeV_shutter1_in_shutter2_out_HV_minus0.5V";
    //    run_file_name = "26.11.2017_10_52_mod5_6GeV_shutter1_in_shutter2_out_HV_minus0.8V";
    //    run_file_name = "26.11.2017_12_23_mod5_6GeV_protons_HV_minus0.8V";
    //    run_file_name = "26.11.2017_12_05_mod5_6GeV_protons_HV_minus0.5V";
    
    
    //    run_file_name = "27.11.2017_15_20_mod5_6GeV_neg_pions_after_HV_low";
    //    run_file_name = "27.11.2017_16_50_mod5_6GeV_neg_pions_exchangeCable_from_mod5";
    // run_file_name = "01.12.2017_11_00_mod5_6GeV_shutter1_in_shutter2_out";
    // run_file_name = "01.12.2017_12_00_mod5_6GeV_cherenkov_Veto";
    run_file_name = "01.12.2017_20_35_mod5_6GeV_chernkov_Veto_fullCalo_changed_SlowControl";
    //   run_file_name = "01.12.2017_17_47_mod5_6GeV_shutter1_in_shutter2_out";
    //    run_file_name = "qqq";
    //    run_file_name = "qqq";
    //    run_file_name = "qqq";
    
    
    if(is_time_sel)
    {
	source_path = "/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_source/T10_test_sep_17/";

    }
    else
    {
	source_path = "/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_source/T9_test_nov_17/";
    }
    //new calculation
    if(IsNewCalculation)
	source_path = "/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_source/T10_T9_beamtests_17_newcalc2/";

    source_path = "/home/runna61/adc64_data_2018_quater1/T10_T9_beamtests_17_newcalc2/";
    if(Is_T10_2)
	source_path = "/home/runna61/adc64_data_2018_quater1/beam_root_pu/";

    source_path = "/home/runna61/adc64_data_2018_quater1/beam_root_pu/";
    source_path = "/mnt/disk_na61pc008/runna61/adc64_data_2018_quater1/beam_root_pu/";
    source_path = "/mnt/disk_na61pc008/runna61/adc64_data_2018_quater1/beam_root_fit/";
    source_path = "/home/runna61/adc64_data_2018_quater4/root_files/";


    if(input_run_name != "void") run_file_name = input_run_name;
    //TString result_file_name = run_file_name + ((part_name == "no_sel")?"":part_name)+ ((IsElectrons)?"_elsel" : "")
    TString result_file_name = run_file_name + ((part_name == "no_sel")?"":part_name)+ ((IsElectrons)?"" : "")
            + "_analysis_2";
    
    
    TChain *data_tree = new TChain;
    data_tree->AddFile(source_path + run_file_name + ".root/adc64_data");
    
    
    
#else
    run_file_name = "Calibration";
    //
    
    TChain *data_tree = new TChain;
    
    /********
    is_time_sel = false;
    source_path = "/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_source/T9_test_nov_17/";

    run_file_name = "27.11.2017_11_15_noiseOnly_back_and_top";
    TString result_file_name = run_file_name+"_T9" + (IsNoiseCalib?"_noisecalib":"");
    data_tree->AddFile(source_path + "27.11.2017_11_15_noiseOnly_back_and_top.root/adc64_data");
    /********
    is_time_sel = false;
    //source_path = "/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_source/T9_test_nov_17/";
    //new calculation
    source_path = "/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_source/T10_T9_beamtests_17_newcalc2/";
    source_path = "/home/runna61/adc64_data_2018_quater1/beam_root_pu/";

    TString result_file_name = run_file_name+"_T9";
    data_tree->AddFile(source_path + "21.11.2017_mod7_shutter1_in_shutter2_out.root/adc64_data");
    data_tree->AddFile(source_path + "21.11.2017_mod8_shutter1_in_shutter2_out.root/adc64_data");
    data_tree->AddFile(source_path + "21.11.2017_mod9_shutter1_in_shutter2_out.root/adc64_data");
    data_tree->AddFile(source_path + "22.11.2017_mod4_shutter1_in_shutter2_out.root/adc64_data");
    data_tree->AddFile(source_path + "22.11.2017_mod1_shutter1_in_shutter2_out.root/adc64_data");
    data_tree->AddFile(source_path + "21.11.2017_mod2_shutter1_in_shutter2_out.root/adc64_data");
    data_tree->AddFile(source_path + "21.11.2017_mod5_shutter1_in_shutter2_out.root/adc64_data");
    data_tree->AddFile(source_path + "21.11.2017_mod6_shutter1_in_shutter2_out.root/adc64_data");
    data_tree->AddFile(source_path + "22.11.2017_mod4_shutter1_in_shutter2_out.root/adc64_data");
    data_tree->AddFile(source_path + "22.11.2017_21_33_mod3_shutter1_in_shutter2_out_resetADC64.root/adc64_data");
    data_tree->AddFile(source_path + "23.11.2017_mod9_shutter1_in_shutter2_out.root/adc64_data");


    //    TString result_file_name = run_file_name+"_HVnominal";
    //    data_tree->AddFile(source_path + "26.11.2017_09_40_mod5_6GeV_shutter1_in_shutter2_out_nominalHV.root/adc64_data");

    //    TString result_file_name = run_file_name+"_HV0.5";
    //    data_tree->AddFile(source_path + "26.11.2017_10_24_mod5_6GeV_shutter1_in_shutter2_out_HV_minus0.5V.root/adc64_data");

    //    TString result_file_name = run_file_name+"_HV0.8";
    //    data_tree->AddFile(source_path + "26.11.2017_10_52_mod5_6GeV_shutter1_in_shutter2_out_HV_minus0.8V.root/adc64_data");


    /********
    TString result_file_name = run_file_name+"_T10" + (IsNoiseCalib?"_noisecalib":"");
    is_time_sel = true;
    //source_path = "/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_source/T10_test_sep_17/";
    //new calculation
    source_path = "/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_source/T10_T9_beamtests_17_newcalc2/";


    //        data_tree->AddFile(source_path + "adc64_17_09_11_15_18_muonScan_mod3_6GeV.root/adc64_data");
    //        data_tree->AddFile(source_path + "adc64_17_09_08_02_59_muonScan_mod4_6GeV.root/adc64_data");
    //        data_tree->AddFile(source_path + "adc64_17_09_11_20_09_muonScan_mod1_6GeV.root/adc64_data");
    //        data_tree->AddFile(source_path + "adc64_17_09_11_22_45_muonScan_mod2_6GeV.root/adc64_data");
    //        data_tree->AddFile(source_path + "adc64_17_09_12_01_20_muonScan_mod7_6GeV.root/adc64_data");
    //        data_tree->AddFile(source_path + "adc64_17_09_08_11_31_muonScan_mod8_6GeV.root/adc64_data");
    //        data_tree->AddFile(source_path + "adc64_17_09_08_08_10_muonScan_mod9_6GeV.root/adc64_data");
    //        data_tree->AddFile(source_path + "adc64_17_09_08_05_10_muonScan_mod6_6GeV.root/adc64_data");
    //        data_tree->AddFile(source_path + "adc64_17_09_11_12_10_muonScan_mod5_6GeV_test.root/adc64_data");

    data_tree->AddFile(source_path + "adc64_17_09_08_13_45_protonScan_mod5_6GeV.root/adc64_data");
    data_tree->AddFile(source_path + "adc64_17_09_10_16_38_protonScan_mod1_6GeV.root/adc64_data");
    data_tree->AddFile(source_path + "adc64_17_09_10_18_01_protonScan_mod2_6GeV.root/adc64_data");
    data_tree->AddFile(source_path + "adc64_17_09_10_19_40_protonScan_mod3_6GeV.root/adc64_data");
    data_tree->AddFile(source_path + "adc64_17_09_10_21_07_protonScan_mod6_6GeV.root/adc64_data");
    data_tree->AddFile(source_path + "adc64_17_09_10_22_33_protonScan_mod7_6GeV.root/adc64_data");
    data_tree->AddFile(source_path + "adc64_17_09_10_23_59_protonScan_mod8_6GeV.root/adc64_data");
    data_tree->AddFile(source_path + "adc64_17_09_11_01_28_protonScan_mod9_6GeV.root/adc64_data");
    data_tree->AddFile(source_path + "adc64_17_09_11_17_45_protonScan_mod4_6GeV.root/adc64_data");

    /********
    //T10_18
    TString result_file_name = run_file_name+"_T10_2" + (IsNoiseCalib?"_noisecalib":"");
    is_time_sel = true;
    //source_path = "/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_source/T10_test_sep_17/";
    //new calculation
    //source_path = "/mnt/hgfs/DETECTORS_DATA/RESULTS/CALO/root_source/T10_T9_beamtests_17_newcalc2/";
    source_path = "/home/runna61/adc64_data_2018_quater1/beam_root_pu/";


    //    data_tree->AddFile(source_path + "beam_18_04_12_19_20_5GeV_mod5.root/adc64_data");
    //    data_tree->AddFile(source_path + "beam_18_04_12_6GeV_mod4.root/adc64_data");

    //data_tree->AddFile(source_path + "beam_18_04_12_23_06_2GeV_mod5.root/adc64_data");
    data_tree->AddFile(source_path + "beam_18_04_13_12_40_5GeV_mod5_shutter_in.root/adc64_data");

    data_tree->AddFile(source_path + "beam_18_04_13_00_11_5GeV_mod4.root/adc64_data");
    data_tree->AddFile(source_path + "beam_18_04_13_00_38_5GeV_mod6.root/adc64_data");
    data_tree->AddFile(source_path + "beam_18_04_13_01_48_5GeV_mod9.root/adc64_data");
    data_tree->AddFile(source_path + "beam_18_04_13_02_12_5GeV_mod8.root/adc64_data");
    data_tree->AddFile(source_path + "beam_18_04_13_02_35_5GeV_mod7.root/adc64_data");
    data_tree->AddFile(source_path + "beam_18_04_13_03_15_5GeV_mod1.root/adc64_data");
    data_tree->AddFile(source_path + "beam_18_04_13_03_51_5GeV_mod2.root/adc64_data");
    data_tree->AddFile(source_path + "beam_18_04_13_07_52_5GeV_mod3.root/adc64_data");

    //    data_tree->AddFile(source_path + "beam_18_04_14_12_35_5GeV_mod5_TOF1_TOF2_scint2_narrowBeam2.root/adc64_data");
    //    data_tree->AddFile(source_path + "beam_18_04_14_19_50_5GeV_mod5_TOF1_TOF2_scint2_narrowBeam2.root/adc64_data");



    //        data_tree->AddFile(source_path + "beam_18_04_12_20_56_3.5GeV_mod5.root/adc64_data");
    //	data_tree->AddFile(source_path + "beam_18_04_12_21_30_3GeV_mod5.root/adc64_data");
    //	data_tree->AddFile(source_path + "beam_18_04_13_08_33_5GeV_surface_scan_point_1.root/adc64_data");
    //	data_tree->AddFile(source_path + "beam_18_04_13_09_01_5GeV_surface_scan_point_2.root/adc64_data");
    //	data_tree->AddFile(source_path + "beam_18_04_13_09_26_5GeV_surface_scan_point_3.root/adc64_data");


    /********/
    //NA61
    //TString result_file_name = run_file_name+"_NA61_80GeV_mod4_fit" + (IsNoiseCalib?"_noisecalib":"");
    TString result_file_name = run_file_name+"_T10_fast_fee" + (IsNoiseCalib?"_noisecalib":"");
    is_time_sel = true;

    source_path = "/home/runna61/adc64_data_2018_quater1/beam_root_na61_/";
    source_path = "/mnt/disk_na61pc008/runna61/adc64_data_2018_quater1/beam_root_pu/";
    source_path = "/mnt/disk_na61pc008/runna61/adc64_data_2018_quater1/beam_root_fit/";

    source_path = "/home/runna61/adc64_data_2018_quater4/root_files/";
    //data_tree->AddFile(source_path + "beam_NA61_18_04_28_20_20_150GeV_mod5_noZS_desync.root/adc64_data");


    //data_tree->AddFile(source_path + "beam_NA61_80GeV_shutter_mouns_26_04_2018.root/adc64_data");
    //data_tree->AddFile(source_path + "beam_NA61_18_04_28_23_44_40GeV_mod5_shutter_muons_noZS.root/adc64_data");



    //data_tree->AddFile(source_path + "beam_NA61_18_04_29_13_57_80GeV_mod1_noZS.root/adc64_data");
     //   data_tree->AddFile(source_path + "beam_NA61_18_04_29_14_30_80GeV_mod2_noZS.root/adc64_data");


    //T10_fastfee
 //   data_tree->AddFile(source_path + "beam_T10_18_10_24_21_53_beam_5GeV_noZS.root/adc64_data");
 //   data_tree->AddFile(source_path + "beam_T10_18_10_24_22_35_beam_5GeV_noZS.root/adc64_data");

    //T10_fastfee long cables
    data_tree->AddFile(source_path + "beam_T10_18_10_25_15_09_beam_5GeV_noZS_longCable.root/adc64_data");


    //data_tree->AddFile(source_path + "beam_NA61_18_04_29_15_50_80GeV_mod3_noZS.root/adc64_data");
    //data_tree->AddFile(source_path + "beam_NA61_18_04_30_18_27_40GeV_mod3_noZS.root/adc64_data");


       // data_tree->AddFile(source_path + "beam_NA61_18_04_29_12_40_80GeV_mod4_noZS_desync.root/adc64_data");
      //  data_tree->AddFile(source_path + "beam_NA61_18_04_28_14_50_80GeV_mod5_noZS.root/adc64_data");
    //    data_tree->AddFile(source_path + "beam_NA61_18_04_29_11_55_80GeV_mod6_noZS.root/adc64_data");
     //   data_tree->AddFile(source_path + "beam_NA61_18_04_29_19_00_80GeV_mod7_noZS.root/adc64_data");
     //   data_tree->AddFile(source_path + "beam_NA61_18_04_29_18_30_80GeV_mod8_noZS.root/adc64_data");
    //    data_tree->AddFile(source_path + "beam_NA61_18_04_29_17_10_80GeV_mod9_noZS.root/adc64_data");


     //   data_tree->AddFile(source_path + "beam_NA61_18_04_29_13_30_80GeV_mod1_noZS_HV_minus_800mV.root/adc64_data");
     //   data_tree->AddFile(source_path + "beam_NA61_18_04_29_14_50_80GeV_mod2_noZS_HV_minus_800mV.root/adc64_data");
    //    data_tree->AddFile(source_path + "beam_NA61_18_04_29_15_30_80GeV_mod3_noZS_HV_minus_800mV.root/adc64_data");
    //    data_tree->AddFile(source_path + "beam_NA61_18_04_29_12_55_80GeV_mod4_noZS_HV_minus_800mV_desync.root/adc64_data");
    //    data_tree->AddFile(source_path + "beam_NA61_18_04_28_15_50_80GeV_mod5_noZS_HV_minus_800mV.root/adc64_data");
     //   data_tree->AddFile(source_path + "beam_NA61_18_04_29_11_40_80GeV_mod6_noZS_HV_minus_800mV.root/adc64_data");
     //   data_tree->AddFile(source_path + "beam_NA61_18_04_29_19_15_80GeV_mod7_noZS_HV_minus_800mV.root/adc64_data");
      //  data_tree->AddFile(source_path + "beam_NA61_18_04_29_18_15_80GeV_mod8_noZS_HV_minus_800mV.root/adc64_data");
      //  data_tree->AddFile(source_path + "beam_NA61_18_04_29_17_40_80GeV_mod9_noZS_HV_minus_800mV.root/adc64_data");



    /********/

#endif
    
    ////////////////////////////////////////////////////////////////////////////////////////
    
    
    
    // Calculation constants ///////////////////////////////////////////////////////////////
    
#if IsUseAmplitudes
    const Int_t min_charge = 0;
    const Int_t max_charge = 1500;
    const Int_t max_charge_bin = 50;
#else
    const Int_t min_charge = -1000;
    const Int_t max_charge = 20000;
    const Int_t max_charge_bin = 50;
#endif
    

#if Is_NA61
    const Int_t min_energy = -50;
    //const Int_t max_energy = 1000;
    const Int_t max_energy = 4000;
    const Int_t max_energy_bin =  (max_energy-min_energy);
#else
    const Int_t min_energy = -50;
    const Int_t max_energy = 500;
    const Int_t max_energy_bin = 1100;
#endif


    Float_t pedestal_cut_mip = 0.5;
    const Bool_t is_custom_ped_trsh = false;
    const Float_t custom_ped_nsgm = 1.5;
    
    const Int_t n_module_noize_rej = 9;

    const Float_t RMS_zero_level_trsh = 50.;
    const Float_t pileup_trhd = 150.;
    const Float_t TOF_ampl_rad = 150.;
    
    Float_t muoncutfirst_min;
    Float_t muoncutfirst_max;
    Float_t muoncutlast_min;
    Float_t muoncutlast_max;
    Float_t muoncut_cmp;
    
    const Float_t mev_in_mip = 5.;
    
    UChar_t signal_time_gate[2] = {70, 90};
    //UChar_t signal_time_gate[2] = {10, 60};

    
    
    
    /****/
    if(Is_NA61)
    {
	muoncutfirst_min = 10.;//x
	muoncutfirst_max = 55.;
	muoncutlast_min = 15.;//y
	muoncutlast_max = 30.;
	muoncut_cmp = 20.;
    }
    else if(Is_T10_2)
    {
	muoncutfirst_min = 10.;//x
	muoncutfirst_max = 55.;
	muoncutlast_min = 15.;//y
	muoncutlast_max = 30.;
	muoncut_cmp = 20.;
    }
    else if(is_time_sel)
    {
	muoncutfirst_min = 10.;
	muoncutfirst_max = 45.;
	muoncutlast_min = 10.;
	muoncutlast_max = 40.;
	muoncut_cmp = 25.;
    }
    else
    {
	muoncutfirst_min = 15.;
	muoncutfirst_max = 50.;
	muoncutlast_min = 12.;
	muoncutlast_max = 46.;
	muoncut_cmp = 25.;
    }
    /**** //HV low
    muoncutfirst_min = 5.;
    muoncutfirst_max = 40.;
    muoncutlast_min = 5.;
    muoncutlast_max = 50.;
    muoncut_cmp = 25.;
    /****/
    
    Float_t calib_noise_T10_mean_array[90] = {55.983994, 86.892815, 26.425106, 42.507465, -13.257971, -11.337613, -50.018578, -42.181889, -55.667435, -67.450867, 87.290611, 111.126915, 140.774170, 131.279022, 65.740318, -4.675683, -41.756233, -42.784859, -41.123604, -62.065353, 74.484940, 111.786034, 57.730927, 68.874367, 28.180731, -21.091618, -49.865864, -19.750175, -29.888914, -38.658257, 110.516411, 145.637665, 196.665894, 184.531784, 150.281464, -41.638004, -50.835289, -6.825617, 33.158165, 6.289832, 168.405777, 238.094955, 240.079849, 195.802292, 115.130486, -11.347405, -29.679512, -40.921486, -31.389328, -32.015762, 99.001213, 157.391708, 121.587326, 117.704430, 51.026875, -15.229918, -34.465405, -30.075146, -34.176151, -46.483437, 45.375568, 36.964001, 33.398960, -14.747524, -80.068375, -17.993471, -57.182636, -111.773201, -123.361198, -125.538132, 119.440582, 140.965393, 121.871262, 98.212372, 5.086172, -5.518027, -28.396500, -75.589844, -79.150574, -94.807045, 78.203102, 98.294121, 72.413383, 34.792492, -5.157246, -18.788168, -25.675772, -62.644028, -65.742531, -71.489616};
    Float_t calib_noise_T10_sigma_array[90] = {616.365967, 760.296814, 561.906799, 829.313660, 621.178101, 656.162720, 681.433044, 699.540527, 609.691833, 706.218933, 638.790283, 662.212646, 694.351379, 930.129333, 733.537476, 659.055908, 681.713928, 733.472961, 646.493225, 601.992188, 629.010864, 802.742798, 622.125793, 744.622314, 562.640320, 615.451111, 636.324768, 744.307678, 581.804443, 554.929565, 629.544373, 644.639099, 717.943604, 826.476990, 610.865845, 677.802063, 626.257996, 617.629211, 667.568237, 536.165100, 640.183777, 825.492004, 736.517944, 912.202820, 664.680664, 659.380554, 626.941956, 595.256104, 633.070557, 616.634094, 632.458069, 713.406128, 671.782471, 791.322205, 582.983582, 734.173889, 682.805786, 692.366455, 596.332764, 534.759766, 724.874146, 712.023865, 641.559570, 870.679504, 1078.051514, 798.197266, 619.596985, 833.469055, 651.181030, 622.545654, 745.351318, 723.761292, 728.050598, 1039.700073, 898.341919, 770.139099, 702.055420, 795.405579, 672.143372, 690.261353, 656.433960, 824.486938, 649.870117, 749.035217, 623.630005, 695.583191, 656.317993, 767.721985, 676.852722, 577.482544};
    
    Float_t calib_noise_T9_mean_array[90] = {23.639700, 29.081966, -8.836606, 8.563286, 18.328587, 1.132293, -13.336116, 13.636755, 9.488007, -4.886619, -18.628540, -17.994238, -15.886780, -20.969936, 33.082764, 6.853848, -17.127914, -42.507427, -12.087902, 5.066625, -13.133057, 10.220142, -26.004759, 39.484322, -0.801957, 4.483300, -8.281135, -10.670261, -14.419660, 26.206726, 13.259528, -2.245700, -15.431497, 38.370934, 34.662678, 19.117708, -22.229359, 8.147587, 5.352398, -13.971908, 8.683086, -14.468011, -8.350795, 18.468357, -16.156485, -23.530458, -6.291051, 2.581675, -3.423581, -10.408801, -10.574389, 6.294055, 1.875466, -5.758702, -58.148857, 5.171666, -1.140003, -16.120716, -0.257629, 13.828116, -7.492298, 4.899879, -16.340307, 7.123511, -7.840694, 11.455421, -10.127605, -11.637474, -9.291282, 3.917283, -0.406390, -20.728630, -23.114676, -42.231945, -18.962475, -4.238063, 0.585184, -19.867853, -16.772541, -12.774047, -6.079286, -12.806444, 20.228231, -20.683956, -6.415242, -26.651201, 27.346901, -13.641907, -2.693639, 5.944688};
    Float_t calib_noise_T9_sigma_array[90] = {571.976074, 690.903320, 511.055908, 674.206421, 602.411133, 605.073853, 608.061646, 614.864319, 561.431396, 556.379272, 610.135071, 581.428162, 557.026855, 671.961365, 625.527771, 659.529663, 641.522583, 621.676514, 630.794617, 624.494873, 533.755371, 710.978638, 581.400757, 812.878357, 542.197815, 694.981445, 592.823975, 336.572327, 331.290161, 555.537415, 532.971069, 573.842590, 538.100098, 572.014771, 610.889526, 631.396301, 489.619843, 528.177368, 604.758606, 509.103424, 545.498230, 548.165527, 547.129883, 790.320435, 623.167480, 563.925781, 564.872864, 559.671997, 680.929749, 480.693298, 528.980408, 538.793945, 543.793152, 736.770569, 485.093475, 642.389160, 596.957703, 575.068604, 510.032440, 516.364746, 697.498718, 702.261597, 604.247803, 936.028503, 861.958008, 737.869263, 659.473145, 767.640625, 649.111267, 615.001953, 585.386719, 592.179077, 562.937256, 553.523621, 693.751709, 920.410767, 592.712524, 566.881775, 618.153625, 928.538086, 967.467102, 651.297424, 574.623535, 713.031677, 519.800964, 620.305664, 837.085510, 552.498596, 575.289062, 700.059265};
    
    Float_t *calib_noise_mean_array = (is_time_sel)? calib_noise_T10_mean_array : calib_noise_T9_mean_array;
    Float_t *calib_noise_sigma_array = (is_time_sel)? calib_noise_T10_sigma_array : calib_noise_T9_sigma_array;
    
#if IsNewCalculation
    //new calc
    //Float_t calib_coef_T10_array[90] = {3585.421143, 3871.444092, 3958.224854, 3334.972168, 3857.760254, 3495.224121, 3812.574707, 4038.783447, 4217.118164, 4279.309082, 3760.677490, 3977.735840, 3952.387939, 3799.897217, 4051.361328, 5204.214844, 4296.872559, 4082.555176, 4948.617188, 5507.988770, 3456.798828, 3430.368408, 3232.586670, 2973.795654, 3081.638184, 4032.977295, 3924.249512, 3779.237793, 3996.336426, 3995.801758, 3235.408447, 2213.727783, 3061.674561, 3761.674561, 3337.083984, 3206.713867, 2877.265869, 3855.334473, 3492.523438, 3833.800537, 3129.223145, 2425.143555, 2462.437744, 2882.531738, 3070.937012, 3573.152344, 3054.188232, 3418.130127, 3344.047852, 3212.935791, 3312.118896, 2564.304443, 2034.932495, 2640.040527, 2717.311523, 3078.100098, 2568.027344, 3106.338867, 2825.549072, 2402.216064, 3417.898682, 3776.375488, 3545.830566, 3855.604492, 4217.000488, 4096.080078, 3954.239990, 4137.545410, 3757.994629, 4579.072754, 3021.693359, 2923.613037, 3256.372559, 2950.251221, 3057.335205, 4112.597656, 3934.089111, 3737.155273, 3227.246826, 4045.304199, 2847.596191, 3009.813965, 3027.216309, 2886.026123, 3026.817627, 4015.869629, 4728.189941, 4370.943848, 4566.589844, 4527.018066};
    //soft pilepu sel
#if Is_T10_2
    //Float_t calib_coef_T10_array[90] = {3873.970459, 4093.480713, 3961.550293, 3595.134277, 3915.184814, 3656.765869, 3905.302246, 4755.529785, 4553.720215, 4792.314453, 4155.436523, 3994.150635, 4121.429688, 3672.071777, 4329.466797, 5608.199707, 4277.279297, 4248.824707, 5039.862793, 5489.740723, 1834.282959, 1823.877441, 1592.526489, 1718.453613, 1722.101318, 2253.388428, 1882.046021, 2091.537354, 2002.757080, 1945.541382, 3884.226562, 3020.138916, 3489.009766, 3942.244629, 3634.643555, 3753.057617, 3601.943359, 4194.484863, 4040.974854, 4179.648438, 3061.994873, 1637.626221, 1824.315063, 2708.590332, 2563.555664, 3438.807861, 3164.368652, 4322.875488, 3245.071777, 4303.755859, 4763.864746, 3647.561768, 3383.491455, 3978.322754, 3912.377930, 4350.427734, 4211.321289, 5076.668457, 4109.136719, 3839.715332, 3277.928955, 3949.199707, 3441.610107, 3970.880371, 4262.612793, 3774.339111, 3833.311523, 3720.333252, 3419.434814, 5091.398438, 3075.090332, 3546.864990, 2723.138184, 2883.952881, 2639.497803, 3014.491455, 3123.983643, 2688.490234, 2463.356445, 2838.864990, 3174.909668, 3185.321045, 3723.329834, 3197.949951, 3364.618652, 4142.251953, 5073.884277, 4832.696777, 5368.495117, 4965.780273};
    //calib_coef_T10_array[40] = calib_coef_T10_array[40] * 1.2;

    //new pu
    Float_t calib_coef_T10_array[90] = {3977.420410, 4121.415527, 4141.809082, 3893.018555, 4019.340088, 3981.787842, 3905.066406, 5045.558105, 4626.477051, 5095.598633, 4308.050781, 4231.133301, 4238.747559, 3742.928467, 4464.853027, 5663.217285, 4294.812012, 4148.129395, 5004.895020, 5490.752441, 2600.825195, 2588.792969, 2263.278809, 2102.485352, 2266.903076, 3136.638916, 2686.712402, 3073.464111, 2968.934814, 2886.072266, 4037.792969, 3245.595703, 3630.038574, 4198.372559, 3828.259033, 3838.608643, 3658.279297, 4251.454590, 4264.515625, 4363.390625, 3098.579346, 2081.093750, 2029.356445, 2772.395020, 2881.536621, 3346.115234, 2839.473877, 4171.196289, 3764.881104, 4218.612305, 4913.548828, 3776.679688, 3490.371094, 4214.726562, 4045.541992, 4425.739258, 4149.607422, 5181.718262, 4262.381348, 3923.714355, 3265.403320, 4016.701904, 3567.649170, 4040.272949, 4385.716309, 3789.767822, 3858.719727, 3739.551025, 3474.382080, 5055.111328, 3136.407471, 3733.234619, 2896.481934, 3010.292969, 2729.794189, 3017.116699, 3272.786133, 2690.987061, 2623.914062, 3107.010986, 3463.606445, 3395.822266, 3802.711914, 3437.150635, 3553.379395, 4223.194336, 5021.766602, 4913.917480, 5354.277832, 5080.682129};


    //shutter Sergey
    calib_coef_T10_array[40] = 3460.283936/1.2;
    calib_coef_T10_array[41] = 2096.850342;
    calib_coef_T10_array[42] = 1950.428223;
    calib_coef_T10_array[43] = 2528.640137;
    calib_coef_T10_array[44] = 2685.658691;
    calib_coef_T10_array[45] = 3210.465088;
    calib_coef_T10_array[46] = 2669.266357;
    calib_coef_T10_array[47] = 4205.971191;
    calib_coef_T10_array[48] = 3653.506348;
    calib_coef_T10_array[49] = 3833.762695;

    //fast fee adc v3
    calib_coef_T10_array[0] = 2886.195068;
    calib_coef_T10_array[1] = 2793.242188;
    calib_coef_T10_array[2] = 2779.393311;
    calib_coef_T10_array[3] = 2797.260742;
    calib_coef_T10_array[4] = 3196.483154;
    calib_coef_T10_array[5] = 4109.025391;
    calib_coef_T10_array[6] = 3065.973877;
    calib_coef_T10_array[7] = 3071.411621;
    calib_coef_T10_array[8] = 3678.282959;
    calib_coef_T10_array[9] = 4224.7;

    //fast fee adc v3 long cables
    calib_coef_T10_array[0] = 2794.706543;
    calib_coef_T10_array[1] = 2714.547363;
    calib_coef_T10_array[2] = 2994.454346;
    calib_coef_T10_array[3] = 2920.050781;
    calib_coef_T10_array[4] = 3393.484375;
    calib_coef_T10_array[5] = 3845.089844;
    calib_coef_T10_array[6] = 3389.170410;
    calib_coef_T10_array[7] = 3227.226807;
    calib_coef_T10_array[8] = 3863.318604;
    calib_coef_T10_array[9] = 4302.6;

    //shutter new pu
    //    calib_coef_mean_array[40] = 3139.956299;
    //    calib_coef_mean_array[41] = 2097.376465;
    //    calib_coef_mean_array[42] = 2033.845825;
    //    calib_coef_mean_array[43] = 2765.148193;
    //    calib_coef_mean_array[44] = 2851.850342;
    //    calib_coef_mean_array[45] = 3412.271729;
    //    calib_coef_mean_array[46] = 2849.544434;
    //    calib_coef_mean_array[47] = 4152.022461;
    //    calib_coef_mean_array[48] = 3764.705811;
    //    calib_coef_mean_array[49] = 4221.978516;

#else
    Float_t calib_coef_T10_array[90] = {3642.077637, 3789.011963, 4006.321045, 3397.457031, 3925.212646, 3546.288818, 3964.979492, 4076.806885, 4247.652344, 4294.667480, 3938.616455, 3903.081543, 4063.903809, 3888.749023, 4126.805176, 5234.443848, 4332.414062, 4119.253418, 4950.773926, 5507.342773, 3653.138184, 3645.431152, 3343.263184, 3153.840576, 3072.946777, 4052.280762, 3779.966309, 3885.020996, 4000.660156, 4011.748291, 3146.802246, 2198.095947, 3165.215576, 3923.469482, 3320.780762, 3117.664551, 3174.295898, 4106.753418, 3505.197266, 3922.438232, 3316.702637, 2462.117188, 2537.647705, 3204.286621, 2868.579834, 3719.839355, 2974.656494, 3445.686279, 3431.234375, 3233.950439, 3465.041504, 2608.042969, 2140.458252, 2721.406738, 2747.065430, 2854.358154, 2619.644287, 3127.895020, 2846.147461, 2418.317871, 3605.493164, 3987.839600, 3572.641357, 4168.173828, 4095.749756, 4194.027832, 3962.549316, 4181.138184, 3771.331055, 4586.330078, 3210.733887, 3414.597656, 3393.403809, 3211.026123, 2909.182617, 3939.711914, 3967.244141, 3838.977539, 3095.915283, 3892.253174, 2928.049316, 3200.228760, 3117.052490, 3156.947266, 2891.228516, 4035.983154, 4744.992188, 4429.530273, 4566.974121, 4533.924805};
#endif

    //    Float_t calib_coef_T9_array[90] = {
    //        4078.862549, 4405.585938, 4575.482910, 3832.785400, 4247.193848, 4670.833496, 4753.293945, 5667.749512, 5020.571777, 5307.924805,
    //        5036.793945, 5405.000000, 5403.117188, 4580.207520, 5445.171387, 6949.497559, 5405.000000, 5540.647949, 7059.627930, 7006.018066,
    //        4168.392090, 4558.722656, 3635.833252, 3760.658447, 3708.611328, 5046.182617, 4659.461426, 4804.741211, 3206.583008, 5276.968750,
    //        4228.919434, 3636.270020, 4416.788086, 5094.445801, 4311.268555, 4712.837402, 4415.657227, 5658.632324, 5148.392578, 5284.519531,
    //        4137.052246, 3012.095703, 3099.355713, 3710.074219, 3745.490723, 4519.034668, 3931.458008, 5358.987793, 5395.296387, 5241.137207,
    //        4414.908691, 3573.950928, 2970.518799, 3686.361816, 3534.961914, 4013.786621, 3587.492920, 4617.906738, 4029.712402, 3609.557129,
    //        4420.347656, 4891.221191, 4824.579590, 4835.147949, 5162.984375, 5358.456055, 5590.303711, 5571.962402, 4795.303711, 5977.821289,
    //        3716.259277, 4370.007324, 4497.434570, 3946.989502, 4189.361816, 5091.888184, 5819.684570, 5473.451172, 4791.077148, 6481.239746,
    //        3623.433105, 3845.909912, 4247.372070, 4464.138184, 4445.795898, 5883.323242, 6660.334473, 6504.312012, 7108.781738, 6617.249512};



#if Is_NA61
    //new pu
    Float_t calib_coef_T9_array[90] = {3977.420410, 4121.415527, 4141.809082, 3893.018555, 4019.340088, 3981.787842, 3905.066406, 5045.558105, 4626.477051, 5095.598633, 4308.050781, 4231.133301, 4238.747559, 3742.928467, 4464.853027, 5663.217285, 4294.812012, 4148.129395, 5004.895020, 5490.752441, 2600.825195, 2588.792969, 2263.278809, 2102.485352, 2266.903076, 3136.638916, 2686.712402, 3073.464111, 2968.934814, 2886.072266, 4037.792969, 3245.595703, 3630.038574, 4198.372559, 3828.259033, 3838.608643, 3658.279297, 4251.454590, 4264.515625, 4363.390625, 3098.579346, 2081.093750, 2029.356445, 2772.395020, 2881.536621, 3346.115234, 2839.473877, 4171.196289, 3764.881104, 4218.612305, 4913.548828, 3776.679688, 3490.371094, 4214.726562, 4045.541992, 4425.739258, 4149.607422, 5181.718262, 4262.381348, 3923.714355, 3265.403320, 4016.701904, 3567.649170, 4040.272949, 4385.716309, 3789.767822, 3858.719727, 3739.551025, 3474.382080, 5055.111328, 3136.407471, 3733.234619, 2896.481934, 3010.292969, 2729.794189, 3017.116699, 3272.786133, 2690.987061, 2623.914062, 3107.010986, 3463.606445, 3395.822266, 3802.711914, 3437.150635, 3553.379395, 4223.194336, 5021.766602, 4913.917480, 5354.277832, 5080.682129};


    //    //shutter Sergey
    //    calib_coef_T9_array[40] = 3460.283936/1.2;
    //    calib_coef_T9_array[41] = 2096.850342;
    //    calib_coef_T9_array[42] = 1950.428223;
    //    calib_coef_T9_array[43] = 2528.640137;
    //    calib_coef_T9_array[44] = 2685.658691;
    //    calib_coef_T9_array[45] = 3210.465088;
    //    calib_coef_T9_array[46] = 2669.266357;
    //    calib_coef_T9_array[47] = 4205.971191;
    //    calib_coef_T9_array[48] = 3653.506348;
    //    calib_coef_T9_array[49] = 3833.762695;



    //adc64_data->Draw("channel_40.integral_in_gate/3419>>(1000,-100,2000)","channel_40.integral_in_gate/3419 / ( channel_40.integral_in_gate/3419 + channel_41.integral_in_gate/2744 + channel_42.integral_in_gate/2869+channel_43.integral_in_gate/3245+channel_44.integral_in_gate/3471+channel_45.integral_in_gate/3941+channel_46.integral_in_gate/3363 + channel_47.integral_in_gate/4050 +channel_48.integral_in_gate/3935+channel_49.integral_in_gate/4253)<0.8","colz")

    // 80Gev
    /***/
    calib_coef_T9_array[40] = 3419.790527;
    calib_coef_T9_array[41] = 2744.521240;
    calib_coef_T9_array[42] = 2869.559082;
    calib_coef_T9_array[43] = 3245.322266;
    calib_coef_T9_array[44] = 3471.155518;
    calib_coef_T9_array[45] = 3941.775635;
    calib_coef_T9_array[46] = 3363.513916;
    calib_coef_T9_array[47] = 4050.125000;
    calib_coef_T9_array[48] = 3935.532227;
    calib_coef_T9_array[49] = 4253.373535;


    calib_coef_T9_array[0] = 3720.088867;
    calib_coef_T9_array[1] = 3991.100586;
    calib_coef_T9_array[2] = 3566.106689;
    calib_coef_T9_array[3] = 3788.910645;
    calib_coef_T9_array[4] = 3983.575195;
    calib_coef_T9_array[5] = 4016.353760;
    calib_coef_T9_array[6] = 3642.060791;
    calib_coef_T9_array[7] = 4537.302246;
    calib_coef_T9_array[8] = 4461.733887;
    calib_coef_T9_array[9] = 5389.553711;

    calib_coef_T9_array[10] = 3644.265381;
    calib_coef_T9_array[11] = 3856.463867;
    calib_coef_T9_array[12] = 3507.275635;
    calib_coef_T9_array[13] = 3075.304688;
    calib_coef_T9_array[14] = 3806.739258;
    calib_coef_T9_array[15] = 4703.682617;
    calib_coef_T9_array[16] = 4259.963379;
    calib_coef_T9_array[17] = 3849.650146;
    calib_coef_T9_array[18] = 4714.102051;
    calib_coef_T9_array[19] = 5305.203613;

//    calib_coef_T9_array[20] = 4000.265382;
//    calib_coef_T9_array[21] = 4000.463867;
//    calib_coef_T9_array[22] = 4000.275635;
//    calib_coef_T9_array[23] = 4000.304688;
//    calib_coef_T9_array[24] = 4000.739258;
//    calib_coef_T9_array[25] = 4000.682627;
//    calib_coef_T9_array[26] = 4000.963379;
//    calib_coef_T9_array[27] = 4000.650246;
//    calib_coef_T9_array[28] = 4000.202052;
//    calib_coef_T9_array[29] = 4000.203623;

    calib_coef_T9_array[20] = 4288.878418;
    calib_coef_T9_array[21] = 4101.934570;
    calib_coef_T9_array[22] = 3403.818604;
    calib_coef_T9_array[23] = 3643.245117;
    calib_coef_T9_array[24] = 3090.196289;
    calib_coef_T9_array[25] = 4168.503418;
    calib_coef_T9_array[26] = 3821.916504;
    calib_coef_T9_array[27] = 4266.141113;
    calib_coef_T9_array[28] = 3970.018311;
    calib_coef_T9_array[29] = 4089.209229;

    //from fit signals
    calib_coef_T9_array[20] = 3727.870361;
    calib_coef_T9_array[21] = 3900.864746;
    calib_coef_T9_array[22] = 2842.094482;
    calib_coef_T9_array[23] = 3117.296387;
    calib_coef_T9_array[24] = 3062.752930;
    calib_coef_T9_array[25] = 4787.875977;
    calib_coef_T9_array[26] = 3763.925049;
    calib_coef_T9_array[27] = 4340.034180;
    calib_coef_T9_array[28] = 4560.321289;
    calib_coef_T9_array[29] = 4300.430664;


    calib_coef_T9_array[30] = 3349.189941;
    calib_coef_T9_array[31] = 2858.685059;
    calib_coef_T9_array[32] = 3435.631592;
    calib_coef_T9_array[33] = 4162.787598;
    calib_coef_T9_array[34] = 3714.517090;
    calib_coef_T9_array[35] = 4235.436523;
    calib_coef_T9_array[36] = 3208.158936;
    calib_coef_T9_array[37] = 4060.614014;
    calib_coef_T9_array[38] = 3780.815186;
    calib_coef_T9_array[39] = 4295.027832;

    calib_coef_T9_array[50] = 4319.851562;
    calib_coef_T9_array[51] = 3262.509766;
    calib_coef_T9_array[52] = 2610.631348;
    calib_coef_T9_array[53] = 3673.422119;
    calib_coef_T9_array[54] = 3584.770508;
    calib_coef_T9_array[55] = 3766.756592;
    calib_coef_T9_array[56] = 3278.939941;
    calib_coef_T9_array[57] = 4442.687012;
    calib_coef_T9_array[58] = 3928.084473;
    calib_coef_T9_array[59] = 2919.450195;

    calib_coef_T9_array[60] = 3507.651611;
    calib_coef_T9_array[61] = 3602.729248;
    calib_coef_T9_array[62] = 3349.394287;
    calib_coef_T9_array[63] = 3651.153564;
    calib_coef_T9_array[64] = 3868.163330;
    calib_coef_T9_array[65] = 3543.306885;
    calib_coef_T9_array[66] = 3412.467773;
    calib_coef_T9_array[67] = 3497.564209;
    calib_coef_T9_array[68] = 3029.434326;
    calib_coef_T9_array[69] = 3965.165527;

    calib_coef_T9_array[70] = 3517.516602;
    calib_coef_T9_array[71] = 3199.363525;
    calib_coef_T9_array[72] = 2721.109619;
    calib_coef_T9_array[73] = 2820.814941;
    calib_coef_T9_array[74] = 2871.791992;
    calib_coef_T9_array[75] = 3983.321777;
    calib_coef_T9_array[76] = 3707.728760;
    calib_coef_T9_array[77] = 3065.500000;
    calib_coef_T9_array[78] = 2597.196045;
    calib_coef_T9_array[79] = 2321.904297;

    calib_coef_T9_array[80] = 2723.503174;
    calib_coef_T9_array[81] = 3056.055908;
    calib_coef_T9_array[82] = 3661.230713;
    calib_coef_T9_array[83] = 3421.083984;
    calib_coef_T9_array[84] = 3725.012939;
    calib_coef_T9_array[85] = 3706.827881;
    calib_coef_T9_array[86] = 4448.343262;
    calib_coef_T9_array[87] = 4067.153564;
    calib_coef_T9_array[88] = 4756.215820;
    calib_coef_T9_array[89] = 4056.215820;
    /***

    //80 GeV 800 mV

    //pulling to 800mv like mod 5 for one pass
    for(Int_t i=0; i<90; i++) calib_coef_T9_array[i] = calib_coef_T9_array[i]/4079.049561*2478.856201;

    calib_coef_T9_array[40] = 2077.986816;
    calib_coef_T9_array[41] = 1731.056274;
    calib_coef_T9_array[42] = 1766.335938;
    calib_coef_T9_array[43] = 2043.689819;
    calib_coef_T9_array[44] = 2107.519531;
    calib_coef_T9_array[45] = 2478.856201;
    calib_coef_T9_array[46] = 1976.296631;
    calib_coef_T9_array[47] = 2428.256592;
    calib_coef_T9_array[48] = 2376.947021;
    calib_coef_T9_array[49] = 2589.365967;

    calib_coef_T9_array[0] = 2251.089600;
    calib_coef_T9_array[1] = 2472.985107;
    calib_coef_T9_array[2] = 2220.180664;
    calib_coef_T9_array[3] = 2294.024658;
    calib_coef_T9_array[4] = 2430.959717;
    calib_coef_T9_array[5] = 2481.102295;
    calib_coef_T9_array[6] = 2452.880127;
    calib_coef_T9_array[7] = 2893.937012;
    calib_coef_T9_array[8] = 2990.160889;
    calib_coef_T9_array[9] = 3580.145020;

    calib_coef_T9_array[10] = 2250.230713;
    calib_coef_T9_array[11] = 2299.533203;
    calib_coef_T9_array[12] = 2301.289795;
    calib_coef_T9_array[13] = 1831.456177;
    calib_coef_T9_array[14] = 2395.627197;
    calib_coef_T9_array[15] = 3179.364502;
    calib_coef_T9_array[16] = 2638.551758;
    calib_coef_T9_array[17] = 2416.980957;
    calib_coef_T9_array[18] = 2832.224121;
    calib_coef_T9_array[19] = 3552.782959;

    calib_coef_T9_array[30] = 1953.187012;
    calib_coef_T9_array[31] = 1474.862671;
    calib_coef_T9_array[32] = 1983.566895;
    calib_coef_T9_array[33] = 2651.727539;
    calib_coef_T9_array[34] = 2217.219482;
    calib_coef_T9_array[35] = 2391.450928;
    calib_coef_T9_array[36] = 2000.;
    calib_coef_T9_array[37] = 2430.164551;
    calib_coef_T9_array[38] = 2317.196289;
    calib_coef_T9_array[39] = 2519.206787;

    calib_coef_T9_array[50] = 2497.048096;
    calib_coef_T9_array[51] = 1902.256104;
    calib_coef_T9_array[52] = 1576.358643;
    calib_coef_T9_array[53] = 1957.367676;
    calib_coef_T9_array[54] = 2048.049561;
    calib_coef_T9_array[55] = 2215.332520;
    calib_coef_T9_array[56] = 2024.519897;
    calib_coef_T9_array[57] = 2748.440430;
    calib_coef_T9_array[58] = 2337.822998;
    calib_coef_T9_array[59] = 1644.787231;

    calib_coef_T9_array[60] = 2221.199463;
    calib_coef_T9_array[61] = 2148.634521;
    calib_coef_T9_array[62] = 2005.275879;
    calib_coef_T9_array[63] = 2324.186523;
    calib_coef_T9_array[64] = 2274.048340;
    calib_coef_T9_array[65] = 2153.391846;
    calib_coef_T9_array[66] = 2027.595947;
    calib_coef_T9_array[67] = 2143.840820;
    calib_coef_T9_array[68] = 1834.046265;
    calib_coef_T9_array[69] = 2444.438721;

    calib_coef_T9_array[70] = 2106.303467;
    calib_coef_T9_array[71] = 1948.849365;
    calib_coef_T9_array[72] = 1596.788940;
    calib_coef_T9_array[73] = 1612.078003;
    calib_coef_T9_array[74] = 1669.131104;
    calib_coef_T9_array[75] = 2564.228271;
    calib_coef_T9_array[76] = 2239.625000;
    calib_coef_T9_array[77] = 1851.608887;
    calib_coef_T9_array[78] = 1497.772583;
    calib_coef_T9_array[79] = 1415.049438;

    calib_coef_T9_array[80] = 1637.750244;
    calib_coef_T9_array[81] = 1845.295044;
    calib_coef_T9_array[82] = 2182.785156;
    calib_coef_T9_array[83] = 2145.856934;
    calib_coef_T9_array[84] = 2209.808105;
    calib_coef_T9_array[85] = 2423.619629;
    calib_coef_T9_array[86] = 2785.085205;
    calib_coef_T9_array[87] = 2571.097412;
    calib_coef_T9_array[88] = 2935.900879;
    calib_coef_T9_array[89] = 2935.900879;


    /***/

    //150Gev
    /***
    calib_coef_T9_array[40] = 3496.662354;
    calib_coef_T9_array[41] = 2783.387695;
    calib_coef_T9_array[42] = 2977.737549;
    calib_coef_T9_array[43] = 3461.675049;
    calib_coef_T9_array[44] = 3808.150146;
    calib_coef_T9_array[45] = 4079.049561;
    calib_coef_T9_array[46] = 3373.295654;
    calib_coef_T9_array[47] = 4122.693848;
    calib_coef_T9_array[48] = 4152.904785;
    calib_coef_T9_array[49] = 4640.179688;
    /***/

#else
    //new pu calib
    Float_t calib_coef_T9_array[90] = {4009.666016, 4603.011719, 4424.025879, 3926.287598, 4216.288574, 4496.131836, 4681.085449, 5621.026367, 5047.591797, 5295.909668, 5295.909668, 5295.909668, 5295.909668, 5295.909668, 5295.909668, 5295.909668, 5295.909668, 5295.909668, 5295.909668, 5295.909668, 3969.387451, 4316.051758, 3500.682373, 3936.688965, 3429.063965, 5022.426758, 4624.035156, 4605.725586, 3156.980957, 5065.471680, 4215.802246, 3466.624512, 4398.199707, 5099.606445, 4203.921387, 4794.827637, 4131.833984, 5503.022949, 5291.612305, 5301.497070, 4177.590332, 3145.001709, 3199.139404, 3788.510254, 3691.781494, 4397.065918, 3930.106201, 5203.907715, 5276.475098, 5211.631348, 4205.083496, 3483.996826, 2803.398926, 3695.864502, 3254.567383, 3857.709229, 3448.938965, 4605.855469, 4021.261475, 3601.481445, 4078.700195, 5138.793945, 4705.346680, 5018.810547, 4882.077637, 4970.685059, 5186.351074, 5344.783203, 4943.187012, 5944.669434, 3701.396729, 4355.359863, 4384.017090, 3871.524902, 4085.748047, 5214.237793, 5585.659668, 5221.013672, 4801.246582, 6505.290039, 4061.235352, 3888.816650, 3947.761475, 4175.480957, 4391.697754, 5817.223145, 6412.469238, 6368.103027, 7234.639648, 6660.306641};

    calib_coef_T9_array[10] = 4897.950195;
    calib_coef_T9_array[11] = 5637.398438;
    calib_coef_T9_array[12] = 5470.589844;
    calib_coef_T9_array[13] = 4896.302734;
    calib_coef_T9_array[14] = 5422.473145;
    calib_coef_T9_array[15] = 6915.911621;
    calib_coef_T9_array[16] = 10000.520874;
    calib_coef_T9_array[17] = 5719.597168;
    calib_coef_T9_array[18] = 7259.367676;
    calib_coef_T9_array[19] = 6948.030762;

#endif


#else
    Float_t calib_coef_T10_array[90] = {3750.386230, 4107.941406, 4032.491699, 3338.545166, 3703.039307, 3454.145264, 3853.958008, 4173.772461, 4240.171387, 4351.082031, 3736.944336, 4038.289551, 4063.821533, 3795.328125, 4031.833496, 5049.434082, 4147.233398, 4030.191162, 4886.523438, 5593.804688, 3679.116943, 3821.051514, 3277.684570, 3141.619141, 2952.343750, 3958.292480, 3881.645996, 3860.563477, 3978.328369, 4074.865234, 3135.794678, 2510.445312, 3259.782471, 3800.258789, 3272.616699, 3083.790527, 3035.256836, 3925.172119, 3637.881104, 3873.479248, 3478.527344, 2724.174072, 2672.928711, 3071.266846, 2822.518311, 3443.162109, 3016.805664, 3357.406738, 3429.935547, 3217.072021, 3705.182617, 2893.320312, 2298.411133, 2910.281982, 2618.656982, 2924.402588, 2579.128174, 3177.066162, 2744.399902, 2441.229248, 3859.330322, 4033.951416, 3813.346680, 3808.161621, 3880.612793, 4018.428467, 3932.306152, 4131.979492, 3811.412842, 4553.260254, 3188.107178, 3462.794922, 3475.235596, 3210.017090, 3120.811279, 3891.150146, 3923.279053, 3691.352783, 3223.310791, 3790.024902, 2815.613525, 3013.375732, 3151.308838, 2975.021484, 2879.842285, 3869.081299, 4689.863281, 4333.566406, 4607.885254, 4554.228027};
    //muons (T9)
    //new muons sel
    Float_t calib_coef_T9_array[90] = {4016.685059, 4548.220703, 4533.544922, 3960.995605, 4228.141602, 4507.627930, 4607.290039, 5559.662109, 4936.161133, 5249.986328, 5194.199707, 5447.122070, 5270.594238, 4908.106934, 5443.701172, 6840.478027, 5447.122070, 5346.302246, 7001.436035, 7392.856445, 4324.713379, 4603.953613, 3637.657715, 3834.876221, 3718.951416, 4868.484863, 4662.941406, 4964.414551, 3000.734375, 5223.291992, 4192.548340, 3559.809326, 4380.727051, 5121.876465, 4205.121094, 4685.333984, 4521.349609, 5594.321777, 5457.538086, 5278.439941, 4242.205566, 3051.235596, 3167.618408, 4039.746826, 3766.430664, 4513.228516, 3864.343750, 5274.463867, 5342.384766, 5214.681641, 4488.872559, 3633.140869, 2826.454346, 3728.137939, 3479.913330, 3946.219727, 3608.963379, 4625.166016, 4006.901123, 3607.464844, 4211.373047, 4891.500488, 4765.157227, 5098.345703, 5005.500000, 5253.560547, 5371.885254, 5285.877930, 4872.158691, 5874.807617, 3672.299072, 4395.668945, 4440.736328, 3863.770264, 4247.858887, 5388.792480, 5582.619141, 5380.933105, 4843.354004, 6403.968262, 4167.159668, 3793.900879, 4243.923340, 4346.011230, 4349.192871, 5630.628418, 6728.820801, 6289.883301, 6904.313965, 6621.583984};
#endif


    //new calc
    Float_t calib_coef_ampl_T10_array[90] = {200.471603, 215.764893, 226.955078, 212.315994, 230.487213, 212.197586, 224.576553, 246.946121, 247.099152, 262.913116, 213.194183, 223.392029, 225.070160, 213.218170, 241.452072, 299.922699, 248.450836, 247.351379, 291.933777, 330.276337, 191.780273, 201.651993, 201.812912, 189.486191, 194.366287, 239.287048, 232.134949, 230.872375, 242.536392, 238.257904, 176.971954, 147.096771, 179.328094, 217.694244, 199.056091, 207.920456, 186.982101, 227.677872, 212.974655, 219.923813, 189.923950, 157.565842, 152.450272, 179.497330, 189.236938, 194.319626, 181.146591, 200.922821, 192.094574, 196.915222, 199.471680, 153.815872, 130.998322, 170.429062, 163.441971, 185.529358, 161.362183, 196.335052, 177.144012, 155.423599, 198.712845, 221.408707, 217.704681, 250.863342, 254.118484, 257.822662, 242.022552, 256.466492, 224.670593, 275.155426, 179.054077, 196.764359, 198.615036, 196.534821, 191.926285, 235.370453, 237.044159, 225.597366, 194.884888, 238.239639, 163.631912, 171.173828, 187.013260, 187.239243, 180.540833, 239.299438, 280.550354, 262.352966, 266.339417, 265.785797};
    Float_t calib_coef_ampl_T9_array[90] = {232.691406, 262.082367, 266.503937, 241.833206, 259.266602, 269.235413, 288.878510, 323.034119, 284.813416, 316.296478, 308.854736, 323.810089, 379., 271.676910, 325.455536, 388.588287, 323.810089, 336.027405, 346.734680, 343.023254, 237.743622, 268.915497, 227.444061, 236.013535, 233.079681, 306.161438, 284.648621, 291.062500, 242.138229, 313.401581, 242.379059, 214.327454, 257.003204, 285.368652, 259.762238, 286.908020, 271.866791, 315.525146, 300.674103, 313.222748, 238.745834, 197.049164, 188.014725, 224.936966, 233.155807, 259.335327, 236.290604, 307.241882, 303.014008, 326.783508, 259.309570, 211.543671, 174.950256, 229.268921, 219.737259, 243.409988, 221.817673, 271.565826, 257.721069, 221.560532, 258.795959, 284.669312, 293.541168, 319.305328, 316.949677, 328.740051, 336.828369, 321.163544, 289.490692, 355.841400, 219.615906, 261.189728, 281.931732, 242.165527, 254.818085, 307.594757, 345.644501, 317.103394, 270.742279, 385.803223, 203.196716, 239.619339, 247.644638, 264.285858, 256.917786, 330.045898, 392.319122, 473., 452., 399.749634};

    //NA61 mod3 fast fee ample
    calib_coef_ampl_T9_array[20] = 498.064148;
    calib_coef_ampl_T9_array[21] = 520.698425;
    calib_coef_ampl_T9_array[22] = 353.545349;
    calib_coef_ampl_T9_array[23] = 426.865967;
    calib_coef_ampl_T9_array[24] = 416.466278;
    calib_coef_ampl_T9_array[25] = 668.676697;
    calib_coef_ampl_T9_array[26] = 544.710449;
    calib_coef_ampl_T9_array[27] = 632.407654;
    calib_coef_ampl_T9_array[28] = 655.024414;
    calib_coef_ampl_T9_array[29] = 636.509888;

    //fit ample calibratin
    calib_coef_ampl_T9_array[20] = 497.776520;
    calib_coef_ampl_T9_array[21] = 516.433411;
    calib_coef_ampl_T9_array[22] = 354.628143;
    calib_coef_ampl_T9_array[23] = 428.311646;
    calib_coef_ampl_T9_array[24] = 422.706085;
    calib_coef_ampl_T9_array[25] = 667.778870;
    calib_coef_ampl_T9_array[26] = 549.802185;
    calib_coef_ampl_T9_array[27] = 643.173889;
    calib_coef_ampl_T9_array[28] = 661.671326;
    calib_coef_ampl_T9_array[29] = 646.650940;




    // old HV board
    Float_t calib_coef_oldHV_mean_array[90] = {0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 5930.000000, 6350.000000, 12860.000000, 5510.000000, 8450.000000, 9290.000000, 6140.000000, 5930.000000, 8240.000000, 9080.000000, 9080.000000, 9080.000000, 9080.000000, 9080.000000, 9080.000000, 9080.000000, 9080.000000, 9080.000000, 9080.000000, 9080.000000, 2150.000000, 8870.000000, 4250.000000, 10550.000000, 7610.000000, 10550.000000, 3410.000000, 5930.000000, 5930.000000, 8030.000000, 4316.175293, 3037.225342, 3129.188477, 4115.401367, 3827.396484, 4481.039551, 4242.491699, 5154.722656, 4933.730469, 5325.365234, 5325.365234, 5325.365234, 5325.365234, 5325.365234, 5325.365234, 5325.365234, 5325.365234, 5325.365234, 5325.365234, 5325.365234, 5325.365234, 5325.365234, 5325.365234, 5325.365234, 5325.365234, 5325.365234, 5325.365234, 5325.365234, 5325.365234, 5325.365234, 2570.000000, 3410.000000, 3830.000000, 9710.000000, 9710.000000, 11390.000000, 2990.000000, 11810.000000, 7190.000000, 8030.000000, 8030.000000, 8030.000000, 8030.000000, 8030.000000, 8030.000000, 8030.000000, 8030.000000, 8030.000000, 8030.000000, 8030.000000};
    
    //temperature measurements
    //Float_t calib_coef_T9_array[50] = {0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 4172.578125, 3324.564209, 3014.468750, 4065.339111, 4110.683105, 4544.701172, 4326.299316, 5402.776367, 5183.249023, 5967.391113, 5967.391113, 5967.391113, 5967.391113, 5967.391113, 5967.391113, 5967.391113, 5967.391113, 5967.391113, 5967.391113, 5967.391113, 0,0,0,0,0,0,0,0,0,0};
    
    //HV08
    Float_t calib_coef_HV_array[90] = {0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 6350.000000, 6350.000000, 4670.000000, 4250.000000, 4250.000000, 7190.000000, 2990.000000, 3830.000000, 14330.000000, 12650.000000, 12650.000000, 12650.000000, 12650.000000, 12650.000000, 12650.000000, 12650.000000, 12650.000000, 12650.000000, 12650.000000, 12650.000000, 12650.000000, 12650.000000, 12650.000000, 12650.000000, 12650.000000, 12650.000000, 12650.000000, 12650.000000, 12650.000000, 12650.000000, 2745.109619, 1849.835449, 2128.256836, 2510.589600, 2597.570068, 3022.615723, 2644.125488, 3480.636475, 3457.375977, 3303.817139, 4250.000000, 7190.000000, 2570.000000, 4670.000000, 5090.000000, 6770.000000, 2990.000000, 3830.000000, 5930.000000, 2990.000000, 2990.000000, 2990.000000, 2990.000000, 2990.000000, 2990.000000, 2990.000000, 2990.000000, 2990.000000, 2990.000000, 2990.000000, 2990.000000, 2990.000000, 2990.000000, 2990.000000, 2990.000000, 2990.000000, 2990.000000, 2990.000000, 2990.000000, 2990.000000, 2990.000000, 4670.000000, 5930.000000, 3830.000000, 5510.000000, 6770.000000, 7610.000000, 8450.000000, 8870.000000, 12230.000000};
    
    //HV05
    //Float_t calib_coef_HV_array[90] = {0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 3206.940430, 2405.530273, 2530.250244, 3018.796875, 3118.817871, 3451.507812, 3044.342285, 3989.795410, 3898.813965, 4009.492432, 4009.492432, 4009.492432, 4009.492432, 4009.492432, 4009.492432, 4009.492432, 4009.492432, 4009.492432, 4009.492432, 4009.492432, 4009.492432, 4009.492432, 4009.492432, 4009.492432, 4009.492432, 4009.492432, 4009.492432, 4009.492432, 4009.492432, 4009.492432, 4009.492432, 4009.492432, 4009.492432, 4009.492432, 4009.492432, 4009.492432, 4009.492432, 4009.492432, 4009.492432, 4009.492432, 4009.492432, 4009.492432, 4009.492432, 4009.492432, 4009.492432, 4009.492432, 4009.492432, 4009.492432, 4009.492432, 4009.492432};
    
    //for(Int_t i=0; i<90; i++) calib_coef_T9_array[i] = ((i>=40)&&(i<=49))? calib_coef_HV_array[i] : calib_coef_T9_array[i];
    //for(Int_t i=0; i<90; i++) calib_coef_T9_array[i] = ((i>=40)&&(i<=49))? calib_coef_oldHV_mean_array[i] : calib_coef_T9_array[i];
    
    Float_t *calib_coef_array = (is_time_sel)? calib_coef_T10_array : calib_coef_T9_array;
    Float_t *calib_coef_ampl_array = (is_time_sel)? calib_coef_ampl_T10_array : calib_coef_ampl_T9_array;

    




    // Readme reader ///////////////////////////////////////////////////////////////////////
    printf("Opening info file %s\n", (source_path + run_file_name + ".txt").Data());
    Readme_reader *readme_reader_ptr = new Readme_reader("");
    if (readme_reader_ptr->SetInfoFile(source_path + run_file_name +".txt") < 0) return;
    readme_reader_ptr->Print();
    
    Int_t total_modules = readme_reader_ptr->Get_module_num();
    Int_t total_channels = readme_reader_ptr->Get_total_ch();
    ////////////////////////////////////////////////////////////////////////////////////////
    
    
    
    
    
    // Initialisation /////////////////////////////////////////////////////////////////////
    TString hist_comment = readme_reader_ptr->Get_run_comment();
    
    TObjArray *result_array = new TObjArray;
    result_array->SetName(readme_reader_ptr->Get_run_name().Data());
    result_array->SetOwner();
    
    TObjArray *commhist_array = new TObjArray;
    commhist_array->SetName("calo_results");
    commhist_array->SetOwner();
    result_array->Add(commhist_array);
    
    Double_t mean, sigma;
    TF1 *tfunc_ptr = NULL;
    TH1 *th1_hist_ptr = NULL;
    TH2 *th2_hist_ptr = NULL;
    TCanvas *canv_ptr = NULL;
    
    event_data_struct *event_struct = new event_data_struct[total_channels];
    for(Int_t ch = 0; ch < total_channels; ch++)
    {
	data_tree->SetBranchAddress((event_struct+ch)->GetChName(ch).Data(), (event_struct+ch));
    }
    
    MAIN_signal PMT1signal, PMT2signal;
    if(is_time_sel)
    {
	data_tree->SetBranchAddress("main_PMT01", &PMT1signal);
	data_tree->SetBranchAddress("main_PMT02", &PMT2signal);
    }
    
    
    Float_t *calib_calc_coef_mean = new Float_t[total_channels];
    Float_t *calib_calc_coef_sigma = new Float_t[total_channels];
    
    
    Int_t beam_to_module_tcells = readme_reader_ptr->Get_module_info()[beam_to_module_N-1].total_cells;
    TString beam_to_module_name = readme_reader_ptr->Get_module_info()[beam_to_module_N-1].comment;
    
    printf("total enteries: %i\n", data_tree->GetEntries());
    
    std::ofstream log_out ((result_path + result_file_name + ".log").Data());
    ////////////////////////////////////////////////////////////////////////////////////////
    
    
    
    
    // Result file /////////////////////////////////////////////////////////////////////////
    TFile *result_file = new TFile((result_path + result_file_name + ".root").Data(), "RECREATE");
    result_file->cd();
    printf("Result file opened: %s\n\n\n", (result_path + result_file_name + ".root").Data());
    ////////////////////////////////////////////////////////////////////////////////////////
    
    
    // /afs/cern.ch/sw/lcg/app/releases/ROOT/5.34.36/x86_64-slc6-gcc48-opt/root/bin
    // source ./thisroot.sh
    // Histogramms /////////////////////////////////////////////////////////////////////////
    Float_t range_mult = 1.;

    th1_hist_ptr = new TH1F("puleup_map", Form("Pule-up statistic (events with pu) [%s]; ch", hist_comment.Data() ),
                            total_channels, 0, total_channels);
    commhist_array->Add(th1_hist_ptr);

    th1_hist_ptr = new TH1F("part_time", Form("Particles start TOF %s [%.1f, %.1f]; ps", hist_comment.Data(), part_time_sel_val[0], part_time_sel_val[1]),
            2000, -10000, 10000);
    commhist_array->Add(th1_hist_ptr);

    th1_hist_ptr = new TH1F("part_time_rej", Form("Particles rejected TOF %s [%.1f, %.1f]; ps", hist_comment.Data(), part_time_sel_val[0], part_time_sel_val[1]),
            2000, -10000, 10000);
    commhist_array->Add(th1_hist_ptr);

    th2_hist_ptr = new TH2F("TOF_ampl", Form("PMT amplitudeted %s; mV; mV", hist_comment.Data()),
                            1000, 0, 1000, 1000, 0, 1000);
    commhist_array->Add(th2_hist_ptr);

    th2_hist_ptr = new TH2F("TOF_ampl_sel", Form("PMT amplitudeted %s selected; mV; mV", hist_comment.Data()),
                            1000, 0, 1000, 1000, 0, 1000);
    commhist_array->Add(th2_hist_ptr);

    th1_hist_ptr = new TH1F(Form("Calo_trace"),
                            Form("Calo trace; cell; MeV;"),
                            beam_to_module_tcells, 0, beam_to_module_tcells);
    commhist_array->Add(th1_hist_ptr);

    th1_hist_ptr = new TH1F(Form("Calo_trace_ex_mod5"),
                            Form("Calo trace; cell; MeV;"),
                            beam_to_module_tcells, 0, beam_to_module_tcells);
    commhist_array->Add(th1_hist_ptr);

    th2_hist_ptr = new TH2F(Form("beammod%i_module_halfversus", beam_to_module_N),
                            Form("Summ charge firs vs last cells module at beam%i %s; summ first; summ last;",beam_to_module_N, hist_comment.Data()),
                            250, -10, 300, 250, -10, 200);
    commhist_array->Add(th2_hist_ptr);
    
    th2_hist_ptr = new TH2F(Form("beammod%i_module_halfversus_sel", beam_to_module_N),
                            Form("Summ charge firs vs last cells module at beam%i %s; summ first; summ last;",beam_to_module_N, hist_comment.Data()),
                            250, -10, 300, 250, -10, 200);
    commhist_array->Add(th2_hist_ptr);
    
    th2_hist_ptr = new TH2F(Form("beammod%i_module_electrons", beam_to_module_N),
                            Form("(Summ charge)/(Charge at cell 1) module at beam%i %s; Summ energy all cell; Energy (cell1 /all cells);",beam_to_module_N, hist_comment.Data()),
                            max_energy_bin, range_mult*min_energy, range_mult*max_energy, 200, 0, 1);
    commhist_array->Add(th2_hist_ptr);
    
    th2_hist_ptr = new TH2F(Form("beammod%i_module_electrons_sel", beam_to_module_N),
                            Form("(Summ charge)/(Charge at cell 1) module at beam%i %s; Summ energy all cell; Energy (cell1 /all cells);",beam_to_module_N, hist_comment.Data()),
                            max_energy_bin, range_mult*min_energy, range_mult*max_energy, 200, 0, 1);
    commhist_array->Add(th2_hist_ptr);
    
    
    th2_hist_ptr = new TH2F(Form("beammod%i_module_testsel", beam_to_module_N),
                            Form("(Summ 1:2) vs (Summ 3:10) module at beam%i %s; summ cells 3:10; summ cells 1:2;",beam_to_module_N, hist_comment.Data()),
                            max_energy_bin, min_energy, max_energy, max_energy_bin, min_energy, max_energy);
    commhist_array->Add(th2_hist_ptr);
    

    th1_hist_ptr = new TH1F(Form("allnotcentrmod_cell_1_energy_summ"),
                            Form("All module exept central cell 1 evergy sum; MeV"),
                            max_energy_bin, min_energy, max_energy);
    commhist_array->Add(th1_hist_ptr);

    
    for(Int_t icell = 0; icell < beam_to_module_tcells; icell++)
    {
	th1_hist_ptr = new TH1F(Form("calo_energy_first_%i",icell+1), Form("Calo energy first %i cells; MeV", icell+1),
	                        max_energy_bin, range_mult*min_energy, range_mult*max_energy);
	commhist_array->Add(th1_hist_ptr);
    }
    
    for(Int_t module_iter = 0; module_iter < total_modules; module_iter++)
    {
	Int_t total_cells = readme_reader_ptr->Get_module_info()[module_iter].total_cells;
	TString module_name = readme_reader_ptr->Get_module_info()[module_iter].comment;
	TObjArray *module_data_array = new TObjArray();
	module_data_array->SetName(module_name.Data());
	module_data_array->SetOwner();
	result_array->Add(module_data_array);

	Bool_t IsBeamAtThisModule = (module_iter+1 == beam_to_module_N);

	
	th1_hist_ptr = new TH1F(Form("%s_module_trace",module_name.Data()),
	                        Form("Module trace %s %s; section#; Amplitude [MeV/events];", module_name.Data(), hist_comment.Data()),
	                        total_cells, 0, total_cells);
	module_data_array->Add(th1_hist_ptr);
	
	Float_t curr_mult = IsBeamAtThisModule?range_mult:1.;

	TObjArray *chargecell_array = new TObjArray;
	chargecell_array->SetName("cell_charge");
	chargecell_array->SetOwner();
	module_data_array->Add(chargecell_array);
	
	TObjArray *calibcell_array = new TObjArray;
	calibcell_array->SetName("cell_calib");
	calibcell_array->SetOwner();
	module_data_array->Add(calibcell_array);
	
	TObjArray *firsncells_array = new TObjArray;
	firsncells_array->SetName("first_n_cell");
	firsncells_array->SetOwner();
	module_data_array->Add(firsncells_array);
	
	th2_hist_ptr = new TH2F(Form("%s_module_halfversus_calib", module_name.Data()),
	                        Form("Summ charge firs vs last cells %s %s; summ first; summ last;", module_name.Data(), hist_comment.Data()),
	                        250, -10, 200, 250, -10, 200);
	calibcell_array->Add(th2_hist_ptr);
	
	
	th2_hist_ptr = new TH2F(Form("%s_module_halfversus_sel_calib",module_name.Data()),
	                        Form("Summ charge firs vs last cells [selection] %s %s; summ first, MeV; summ last, MeV;", module_name.Data(), hist_comment.Data()),
	                        250, -10, 200, 250, -10, 200);
	calibcell_array->Add(th2_hist_ptr);
	
	
	for(Int_t icell = 0; icell < total_cells; icell++)
	{
	    th1_hist_ptr = new TH1F(Form("%s_cell_%i_energy",module_name.Data(), icell),
	                            Form("mod %i cell %i; MeV", module_iter, icell),
	                            max_energy_bin, curr_mult*min_energy, curr_mult*max_energy);
	    chargecell_array->Add(th1_hist_ptr);
	    
	    th1_hist_ptr = new TH1F(Form("%s_cell_%i_charge_muons",module_name.Data(), icell), "temp",
	                            max_charge_bin, ((IsNoiseCalib)?-5000.: min_charge),((IsNoiseCalib)?5000.:  max_charge));
	    calibcell_array->Add(th1_hist_ptr);
	    
	    th1_hist_ptr = new TH1F(Form("%s_module_energy_first_%i",module_name.Data(), icell+1),
	                            Form("Total module energy %s [first %i cell] %s; MeV", module_name.Data(), icell, hist_comment.Data()),
	                            max_energy_bin, curr_mult*min_energy, curr_mult*max_energy);
	    firsncells_array->Add(th1_hist_ptr);

	    th2_hist_ptr = new TH2F(Form("%s_cell_%i_amplcharge",module_name.Data(), icell+1),
	                            Form("Amplitude vs charge [module %s cell %i] %s; MeV", module_name.Data(), icell, hist_comment.Data()),
	                            max_energy_bin, curr_mult*min_energy, curr_mult*max_energy, max_energy_bin, curr_mult*min_energy, curr_mult*max_energy);
	    calibcell_array->Add(th2_hist_ptr);
	}
	
    }
    ////////////////////////////////////////////////////////////////////////////////////////
    
    
    
    
    
    
    
    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////  CALIBRATION  /////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    printf("Muons Calibration ==================================\n");
    for(Int_t entry = 0; entry <= data_tree->GetEntries(); entry++)
    {
	data_tree->GetEntry(entry);

	Float_t PMT_TOF = (PMT2signal.Time_pol1 - PMT1signal.Time_pol1)*1000.;
	Bool_t Is_TOF_in_reg = !is_time_sel||((PMT_TOF >= -200.)&&(PMT_TOF <= 300.));
#if Is_T10_2
	//Is_TOF_in_reg = !is_time_sel||((PMT_TOF >= -1800.)&&(PMT_TOF <= -1600.));
	//Is_TOF_in_reg = !is_time_sel||((PMT_TOF >= -2200.)&&(PMT_TOF <= -1800.)); //muons
	if(Is_T10_fastfee) Is_TOF_in_reg = !is_time_sel||((PMT_TOF >= -7000.)&&(PMT_TOF <= -6600.)); //muons fast fee
#endif

	if(Calib_TOF_selection)
	    if(!Is_TOF_in_reg)
		continue;

	for(Int_t module_iter = 0; module_iter < total_modules; module_iter++)
	{
	    TString module_name = readme_reader_ptr->Get_module_info()[module_iter].comment;
	    Int_t total_cells = readme_reader_ptr->Get_module_info()[module_iter].total_cells;
	    


	    Float_t module_charge_first_summ = 0;
	    Float_t module_charge_last_summ = 0;
	    Int_t n_cell_signal = 0;
	    
	    // Muon selection ===========================================================
	    for(Int_t cell_iter = 0; cell_iter < total_cells; cell_iter++)
	    {
		Int_t tree_ch = readme_reader_ptr->GetContCh_from_CellN(module_iter, cell_iter);
		Float_t charge_cell = (IsUseFitSignals?event_struct[tree_ch].FIT_integral_in_gate : event_struct[tree_ch].integral_in_gate);
		Float_t energy_cell = charge_cell / calib_coef_array[tree_ch] * mev_in_mip;
		UChar_t time_cell = event_struct[tree_ch].time_max_in_gate;
		Bool_t is_signal_in_gate = (time_cell > signal_time_gate[0])&&(time_cell < signal_time_gate[1]);
		Bool_t is_energy_in_range = (energy_cell > 0.2*mev_in_mip);
		Float_t PMT_TOF = (PMT2signal.Time_pol1 - PMT1signal.Time_pol1)*1000.;

		Float_t Amplitude_sig = event_struct[tree_ch].MAX_in_gate - event_struct[tree_ch].zero_level;
		Float_t Amplitude_FIT = event_struct[tree_ch].FIT_MAX_in_gate - event_struct[tree_ch].FIT_zero_level;
		Float_t Amplitude = IsUseFitSignals?Amplitude_sig:Amplitude_sig;

		Float_t energy_cell_ampl = Amplitude / calib_coef_ampl_array[tree_ch] * mev_in_mip;

		Float_t energy_value = IsUseAmplitudes?energy_cell_ampl:energy_cell;

		Bool_t is_no_pileup = (event_struct[tree_ch].zero_level_RMS < 20)
		        //        &&((event_struct[tree_ch].MAX_out_gate - event_struct[tree_ch].zero_level) < 150)
		        //        &&(event_struct[tree_ch].integral_in_gate/Amplitude < 20.)
		        ;
		Bool_t is_pule_up = event_struct[tree_ch].is_pile_up;

		Bool_t is_puleup_desidion = is_pule_up || (event_struct[tree_ch].zero_level_RMS > 30.);
		//if(Amplitude < 80.) is_puleup_desidion = false;
		if(charge_cell < 1500.) is_puleup_desidion = false;

		//		if(Is_T10_2)
		//		    if(tree_ch == 0)
		//			is_puleup_desidion = false;


		//if(is_puleup_desidion) continue;
		//		if(!is_no_pileup) continue;
		//		if(!is_signal_in_gate) continue;
		//		if(!is_energy_in_range) continue;

		n_cell_signal++;
		
		//printf("[cell%i: %i sel %i] ",cell_iter, charge_cell, is_charge_in_range);
		//		if(!is_charge_in_range) continue;
		

		
		if(cell_iter < 5)
		{
		    module_charge_first_summ += energy_value;
		}
		else
		{
		    module_charge_last_summ += energy_value;
		}
	    }// cell
	    
	    Bool_t all_cells_on = (n_cell_signal == total_cells);

	    Bool_t muon_selection = TMath::Abs(module_charge_first_summ - module_charge_last_summ) < muoncut_cmp;
	    muon_selection = muon_selection &&(module_charge_first_summ > muoncutfirst_min);
	    muon_selection = muon_selection &&(module_charge_first_summ < muoncutfirst_max);
	    muon_selection = muon_selection &&(module_charge_last_summ > muoncutlast_min);
	    muon_selection = muon_selection &&(module_charge_last_summ < muoncutlast_max);

	    Float_t muon_sel_r = 10., muon_sel_x = 25., muon_sel_y = 25.;
	    //Float_t muon_sel_r = 20., muon_sel_x = 30., muon_sel_y = 30.;
	    if(IsUseAmplitudes)
	    {
		muon_sel_r = 10.; muon_sel_x = 30.; muon_sel_y = 30.;
	    }

	    muon_selection = ( (module_charge_first_summ - muon_sel_x)*(module_charge_first_summ - muon_sel_x) +
	                       (module_charge_last_summ - muon_sel_y)*(module_charge_last_summ - muon_sel_y) ) <
	            muon_sel_r * muon_sel_r;

	    muon_selection = muon_selection && all_cells_on;
	    // ==========================================================================
	    
	    
	    // Histogramm draw ==========================================================
	    th2_hist_ptr = ((TH2*)(gDirectory->FindObjectAny(Form("%s_module_halfversus_calib",module_name.Data()) )));
	    th2_hist_ptr->Fill(module_charge_first_summ, module_charge_last_summ);

	    if(muon_selection)
	    {
		th2_hist_ptr = ((TH2*)(gDirectory->FindObjectAny(Form("%s_module_halfversus_sel_calib",module_name.Data()) )));
		th2_hist_ptr->Fill(module_charge_first_summ, module_charge_last_summ);
	    }
	    
	    if(muon_selection || IsNoiseCalib)
	    {
		for(Int_t cell_iter = 0; cell_iter < total_cells; cell_iter++)
		{
		    Int_t tree_ch = readme_reader_ptr->GetContCh_from_CellN(module_iter, cell_iter);
		Float_t charge_cell = (IsUseFitSignals?event_struct[tree_ch].FIT_integral_in_gate : event_struct[tree_ch].integral_in_gate);
		    Float_t energy_cell = charge_cell / calib_coef_array[tree_ch] * mev_in_mip;
		    UChar_t time_cell = event_struct[tree_ch].time_max_in_gate;
		    Bool_t is_signal_in_gate = (time_cell > signal_time_gate[0])&&(time_cell < signal_time_gate[1]);
		    Bool_t is_energy_in_range = (energy_cell > 0.2*mev_in_mip);
		    Float_t PMT_TOF = (PMT2signal.Time_pol1 - PMT1signal.Time_pol1)*1000.;

			Float_t Amplitude_sig = event_struct[tree_ch].MAX_in_gate - event_struct[tree_ch].zero_level;
			Float_t Amplitude_FIT = event_struct[tree_ch].FIT_MAX_in_gate - event_struct[tree_ch].FIT_zero_level;
			Float_t Amplitude = IsUseFitSignals?Amplitude_sig:Amplitude_sig;
		    Float_t energy_cell_ampl = Amplitude / calib_coef_ampl_array[tree_ch] * mev_in_mip;

		    Float_t energy_value = IsUseAmplitudes?energy_cell_ampl:energy_cell;


		    Bool_t is_pule_up = event_struct[tree_ch].is_pile_up;

		    Bool_t is_puleup_desidion = is_pule_up || (event_struct[tree_ch].zero_level_RMS > 30.);
		    //if(Amplitude < 200.) is_puleup_desidion = false;
		    if(charge_cell < 1500.) is_puleup_desidion = false;


		    // !!! if(is_puleup_desidion) continue;

#if IsNewCalculation
		    Bool_t is_no_pileup =
		            (event_struct[tree_ch].zero_level_RMS < 30);
		    //        &&((event_struct[tree_ch].MAX_out_gate - event_struct[tree_ch].zero_level) < 150);
		    //		    is_no_pileup =
		    //		            ((event_struct[tree_ch].MAX_out_gate - event_struct[tree_ch].zero_level) < pileup_trhd);

		    //if(!is_no_pileup) continue;
#endif



		    if(!IsNoiseCalib)
		    {
			//if(!is_signal_in_gate) continue;
			//if(!is_energy_in_range) continue;
		    }
		    
		    //printf("[cell%i: %i sel %i] ",cell_iter, charge_cell, is_charge_in_range);
		    //		if(!is_charge_in_range) continue;
		    
		    th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny( Form("%s_cell_%i_charge_muons",module_name.Data(), cell_iter) )));
		    th1_hist_ptr->Fill( IsUseAmplitudes ? Amplitude : charge_cell );
		}// cell
		
	    }
	    // ==========================================================================
	    
	}// module iter
    } //entery iter
    
    
    // Histogramm fitting =======================================================
    
    for(Int_t module_iter = 0; module_iter < total_modules; module_iter++)
    {
	Int_t total_cells = readme_reader_ptr->Get_module_info()[module_iter].total_cells;
	TString module_name = readme_reader_ptr->Get_module_info()[module_iter].comment;
	
	for(Int_t cell_iter = 0; cell_iter < total_cells; cell_iter++)
	{
	    th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny( Form("%s_cell_%i_charge_muons",module_name.Data(), cell_iter) )));
	    Double_t X_min, X_max;
	    if(IsNoiseCalib)
	    {
		X_min = -5000.;
		X_max = 5000.;
	    } else if (IsUseAmplitudes)
	    {
		X_min = 0.;
		X_max = 1000.;
	    }
	    else if(Is_T10_2)
	    {
		X_min = 400.;
		X_max = 7000.;
	    }
	    else
	    {
		X_min = 1000.;
		X_max = 10000.;
	    }
	    //FitHistogrammSmartInMaxPeak(th1_hist_ptr, mean, sigma, 1.5, ((IsNoiseCalib)?-5000.:1000.), ((IsNoiseCalib)?5000.: 10000.));
	    FitHistogrammSmartInMaxPeak(th1_hist_ptr, mean, sigma, 3., X_min, X_max);
	    th1_hist_ptr->SetTitle( Form("Muon ch %i mod %i Cell%i [mean %.0f, sigma %.0f]",
	                                 readme_reader_ptr->GetContCh_from_CellN(module_iter, cell_iter), module_iter, cell_iter, mean, sigma) );
	    Double_t new_X_min = mean-1.5*sigma;
	    Double_t new_X_max = mean+1.5*sigma;
	    //new_X_min = 1000.;
	    //FitHistogrammInRMSRange(th1_hist_ptr, mean, sigma, 2.0, new_X_min, new_X_max);
	    FitHistogrammInRMSRange(th1_hist_ptr, mean, sigma, 3., new_X_min, new_X_max);

	    //printf("muonfit:%s:%i:%.1f:%.1f\n", module_name.Data(), cell_iter, mean, sigma);
	    calib_calc_coef_mean[readme_reader_ptr->GetContCh_from_CellN(module_iter, cell_iter)] = mean;
	    calib_calc_coef_sigma[readme_reader_ptr->GetContCh_from_CellN(module_iter, cell_iter)] = sigma;
	}
    }
    // ==========================================================================
    
    // Printing reaults =========================================================
    log_out<<Form("\n\n======================================================\n");
    log_out<<Form("%s : %s\n", run_file_name.Data(), readme_reader_ptr->Get_run_comment().Data());
    log_out<<Form("Calibration info:\nch : module : cell : board : bd_ch : muon_mean : muon_sigma : calib_coeff\n");
    for(Int_t ch = 0; ch < total_channels; ch++)
    {
	Int_t board, bd_ch, module, cell;
	readme_reader_ptr->GetBoardCh_from_ContCh(ch, board, bd_ch);
	readme_reader_ptr->GetCellN_from_ContCh(ch, module, cell);
	Float_t calib_coeff = (IsGetCurrCalib&&(module+1 == beam_to_module_N)) ? calib_calc_coef_mean[ch] : calib_coef_array[ch];
	log_out<<Form("%03i : %02i : %02i : %02i : %02i : %5.1f : %5.1f : %5.1f\n",
	              ch, module, cell, board, bd_ch,
	              calib_calc_coef_mean[ch], calib_calc_coef_sigma[ch], calib_coeff);
    }
    log_out<<Form("\n======================================================\n\n");
    
    printf("Float_t calib_coef_array[%i] = {", total_channels);
    for(Int_t ch = 0; ch < total_channels-1; ch++)
	printf("%f, ", calib_calc_coef_mean[ch]);
    printf("%f};\n",calib_calc_coef_mean[total_channels-1]);
    
    log_out<<Form("Float_t calib_coef_mean_array[%i] = {", total_channels);
    for(Int_t ch = 0; ch < total_channels-1; ch++)
	log_out<<Form("%f, ", calib_calc_coef_mean[ch]);
    log_out<<Form("%f};\n",calib_calc_coef_mean[total_channels-1]);
    
    log_out<<Form("Float_t calib_coef_sigma_array[%i] = {", total_channels);
    for(Int_t ch = 0; ch < total_channels-1; ch++)
	log_out<<Form("%f, ", calib_calc_coef_sigma[ch]);
    log_out<<Form("%f};\n",calib_calc_coef_sigma[total_channels-1]);
    
    
    log_out<<Form("====================================================\n\n\n");
    for(Int_t ch = 0; ch < total_channels-1; ch++)
	log_out<<Form("calib_coef_mean_array[%i] = %f;\n", ch,  calib_calc_coef_mean[ch]);
    log_out<<Form("====================================================\n\n\n");
    // ==========================================================================
    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    
    
    
    
    
    
    /******/
    
    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////  ANALYSIS  ////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    printf("Analisys ==================================\n");


    Int_t energy_events_collected = 0;
    Float_t *calo_energy_summ = new Float_t[beam_to_module_tcells];
    
    
    for(Int_t entry = 0; entry <= data_tree->GetEntries(); entry++)
    {
	data_tree->GetEntry(entry);
	

	Float_t PMT_TOF = (PMT2signal.Time_pol1 - PMT1signal.Time_pol1)*1000.;
	Bool_t Is_TOF_in_reg = (!is_time_sel)||((PMT_TOF >= part_time_sel_val[0])&&(PMT_TOF <= part_time_sel_val[1]));
	Bool_t Is_TOF_Ampl_in_range = (!is_time_sel)||((PMT1signal.Amplitude > 240.)&&(PMT1signal.Amplitude < 400.)&&
	                                               (PMT2signal.Amplitude > 340.)&&(PMT2signal.Amplitude < 600.));

	Is_TOF_Ampl_in_range = (!is_time_sel)||( ((PMT1signal.Amplitude - 323.)*(PMT1signal.Amplitude - 323.)+
	                                          (PMT2signal.Amplitude - 466.)*(PMT2signal.Amplitude - 466.))<TOF_ampl_rad*TOF_ampl_rad );
	if(Is_T10_2)
	{
	    Is_TOF_Ampl_in_range = (!is_time_sel)||( ((PMT1signal.Amplitude - 300.)*(PMT1signal.Amplitude - 300.)+
	                                              (PMT2signal.Amplitude - 300.)*(PMT2signal.Amplitude - 300.))<TOF_ampl_rad*TOF_ampl_rad );
	}

	Bool_t Is_RMS_zero_level_correct_all_ch = true;


	// Pile-up selection ================================================
	Bool_t is_ev_pileup_good_beammod = 1, is_ev_pileup_good_allcells = 1;
	Bool_t is_new_nopileup_beammod = true, is_new_nopileup_allcells = true;

#if IsNewCalculation
	for(Int_t module_iter = 0; module_iter < total_modules; module_iter++)
	{
	    Bool_t IsBeamAtThisModule = (module_iter+1 == beam_to_module_N);
	    TString module_name = readme_reader_ptr->Get_module_info()[module_iter].comment;
	    Int_t total_cells = readme_reader_ptr->Get_module_info()[module_iter].total_cells;

	    Float_t module_energy_summ = 0;

	    for(Int_t cell_iter = 0; cell_iter < total_cells; cell_iter++)
	    {
		Int_t tree_ch = readme_reader_ptr->GetContCh_from_CellN(module_iter, cell_iter);
		TString channel_name = event_data_struct::GetChName(tree_ch);

		//Float_t charge_cell = (Float_t)event_struct[tree_ch].integral_in_gate;
		//Float_t charge_cell = (Float_t)event_struct[tree_ch].integral_in_gate;
		Float_t charge_cell = (IsUseFitSignals?event_struct[tree_ch].FIT_integral_in_gate : event_struct[tree_ch].integral_in_gate);
		Float_t Amplitude_sig = event_struct[tree_ch].MAX_in_gate - event_struct[tree_ch].zero_level;
		Float_t Amplitude_FIT = event_struct[tree_ch].FIT_MAX_in_gate - event_struct[tree_ch].FIT_zero_level;
		Float_t Amplitude = IsUseFitSignals?Amplitude_sig:Amplitude_sig;

		Float_t calib_coeff = (IsGetCurrCalib&&IsBeamAtThisModule) ? calib_calc_coef_mean[tree_ch] : calib_coef_array[tree_ch];
		Float_t energy_cell = charge_cell / calib_coeff * mev_in_mip;
		UChar_t time_cell = event_struct[tree_ch].time_max_in_gate;
		Bool_t is_signal_in_gate = (time_cell > signal_time_gate[0])&&(time_cell < signal_time_gate[1]);
		Float_t curr_pedestal_trah = is_custom_ped_trsh?
		            (calib_noise_mean_array[tree_ch] + custom_ped_nsgm*calib_noise_sigma_array[tree_ch])/calib_coeff :
		            pedestal_cut_mip;
		//Bool_t is_energy_in_range = (energy_cell > curr_pedestal_trah*mev_in_mip)||(!IsPedestal_rejection);

		//Float_t Amplitude = event_struct[tree_ch].MAX_in_gate - event_struct[tree_ch].zero_level;



		Float_t ch_ampl_ratio = Amplitude/event_struct->integral_in_gate;
		Bool_t is_no_pileup =
		        (event_struct[tree_ch].zero_level_RMS < 30) &&
		        ((event_struct[tree_ch].MAX_out_gate - event_struct[tree_ch].zero_level) < pileup_trhd);
		//		        ((ch_ampl_ratio > .04) || (Amplitude > 1000.));
		is_ev_pileup_good_allcells = is_ev_pileup_good_allcells && is_no_pileup;
		if(module_iter == beam_to_module_N-1) is_ev_pileup_good_beammod = is_ev_pileup_good_beammod && is_no_pileup;
	    }
	}
#endif

	for(Int_t module_iter = 0; module_iter < total_modules; module_iter++)
	{
	    Bool_t IsBeamAtThisModule = (module_iter+1 == beam_to_module_N);
	    Int_t total_cells = readme_reader_ptr->Get_module_info()[module_iter].total_cells;


	    for(Int_t cell_iter = 0; cell_iter < total_cells; cell_iter++)
	    {
		Int_t tree_ch = readme_reader_ptr->GetContCh_from_CellN(module_iter, cell_iter);

		//Float_t charge_cell = (Float_t)event_struct[tree_ch].integral_in_gate;
		//Float_t charge_cell = (Float_t)event_struct[tree_ch].integral_in_gate;
		Float_t charge_cell = (IsUseFitSignals?event_struct[tree_ch].FIT_integral_in_gate : event_struct[tree_ch].integral_in_gate);
		Float_t Amplitude_sig = event_struct[tree_ch].MAX_in_gate - event_struct[tree_ch].zero_level;
		Float_t Amplitude_FIT = event_struct[tree_ch].FIT_MAX_in_gate - event_struct[tree_ch].FIT_zero_level;
		Float_t Amplitude = IsUseFitSignals?Amplitude_sig:Amplitude_sig;

		Float_t calib_coeff = (IsGetCurrCalib&&IsBeamAtThisModule) ? calib_calc_coef_mean[tree_ch] : calib_coef_array[tree_ch];
		Float_t energy_cell = charge_cell / calib_coeff * mev_in_mip;
		UChar_t time_cell = event_struct[tree_ch].time_max_in_gate;
		Bool_t is_signal_in_gate = (time_cell > signal_time_gate[0])&&(time_cell < signal_time_gate[1]);
		Float_t curr_pedestal_trah = is_custom_ped_trsh?
		            (calib_noise_mean_array[tree_ch] + custom_ped_nsgm*calib_noise_sigma_array[tree_ch])/calib_coeff :
		            pedestal_cut_mip;
		//Bool_t is_energy_in_range = (energy_cell > curr_pedestal_trah*mev_in_mip)||(!IsPedestal_rejection);

		//Float_t Amplitude = event_struct[tree_ch].MAX_in_gate - event_struct[tree_ch].zero_level;
		Float_t ch_ampl_ratio = Amplitude/event_struct->integral_in_gate;
		Bool_t is_pule_up = event_struct[tree_ch].is_pile_up;

		Bool_t is_puleup_desidion = is_pule_up || (event_struct[tree_ch].zero_level_RMS > 30.);
		if(Amplitude < 250.) is_puleup_desidion = false;
		//if(charge_cell < 1500.) is_puleup_desidion = false;


		//noise 5gev/c runs
		if(!is_time_sel)
		{
		    if( (tree_ch == 41) || (tree_ch == 63) || (tree_ch == 79) || (tree_ch == 80) || (tree_ch == 89) )
		    {
			if(charge_cell < 1500.) is_puleup_desidion = false;
		    }
		}

		//noise all runs
		//		if(!is_time_sel)
		//		{
		//		    if( (tree_ch == 63) )
		//		    {
		//			is_puleup_desidion = false;
		//		    }
		//		}


		if(is_puleup_desidion)
		{
		    th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny( Form("puleup_map") )));
		    th1_hist_ptr->Fill(tree_ch);
		}



		if(module_iter == exclude_module) continue;

		is_new_nopileup_allcells =
		        is_new_nopileup_allcells && (!is_puleup_desidion);
		if(IsBeamAtThisModule) is_new_nopileup_beammod =
		        is_new_nopileup_beammod && (!is_puleup_desidion);
	    }
	}


	//if(!is_ev_pileup_good_allcells) continue;
	//if(!is_ev_pileup_good_beammod) continue;
	//if(!is_new_nopileup_beammod) continue;
	//if(!is_new_nopileup_allcells) continue;
	// ==========================================================================


	// Selection for beam module ================================================
	Bool_t is_all_cells_nonoize, muon_selection, e_selection, all_cells_on;
	{
	    Float_t module_charge_sqsumm = 0;
	    Float_t module_charge_first_summ = 0;
	    Float_t module_charge_last_summ = 0;
	    Float_t module_charge_cell0 = 0;
	    Float_t module_charge_cell_all = 0;
	    Int_t n_cell_signal = 0;
	    
	    Float_t module_first_n_cells = 0;
	    Float_t module_other_n_cells = 0;
	    Int_t n_first_cells = 2;
	    

	    for(Int_t cell_iter = 0; cell_iter < beam_to_module_tcells; cell_iter++)
	    {
		Int_t tree_ch = readme_reader_ptr->GetContCh_from_CellN(beam_to_module_N-1, cell_iter);
		Float_t charge_cell = (IsUseFitSignals?event_struct[tree_ch].FIT_integral_in_gate : event_struct[tree_ch].integral_in_gate);
		Float_t Amplitude_sig = event_struct[tree_ch].MAX_in_gate - event_struct[tree_ch].zero_level;
		Float_t Amplitude_FIT = event_struct[tree_ch].FIT_MAX_in_gate - event_struct[tree_ch].FIT_zero_level;
		Float_t Amplitude = IsUseFitSignals?Amplitude_sig:Amplitude_sig;
		//Float_t charge_cell = event_struct[tree_ch].integral_in_gate;
		Float_t calib_coeff = (IsGetCurrCalib) ? calib_calc_coef_mean[tree_ch] : calib_coef_array[tree_ch];
		Float_t energy_cell = charge_cell / calib_coeff * mev_in_mip;
		UChar_t time_cell = event_struct[tree_ch].time_max_in_gate;
		Bool_t is_signal_in_gate = (time_cell > signal_time_gate[0])&&(time_cell < signal_time_gate[1]);
		Bool_t is_energy_in_range = (energy_cell > 0.5*mev_in_mip);
		Float_t PMT_TOF = (PMT2signal.Time_pol1 - PMT1signal.Time_pol1)*1000.;
		Float_t RMS_zero_level = event_struct[tree_ch].RMS_out_gate;
		//Float_t Amplitude = event_struct[tree_ch].MAX_in_gate - event_struct[tree_ch].zero_level;
		Float_t energy_cell_ampl = Amplitude / calib_coef_ampl_array[tree_ch] * mev_in_mip;
		Float_t energy_value = IsUseAmplitudes?energy_cell_ampl:energy_cell;

		Is_RMS_zero_level_correct_all_ch = Is_RMS_zero_level_correct_all_ch && (RMS_zero_level < RMS_zero_level_trsh);


//		if(!is_signal_in_gate) continue;
//		if(!is_energy_in_range) continue;
		
		n_cell_signal++;

		//printf("[cell%i: %i sel %i] ",cell_iter, charge_cell, is_charge_in_range);
		//		if(!is_charge_in_range) continue;
		
		if(cell_iter < n_module_noize_rej) module_charge_sqsumm += (Float_t)energy_cell*(Float_t)energy_cell;
		
		if(cell_iter == 0) module_charge_cell0 = energy_value;
		module_charge_cell_all += energy_value;

		if(cell_iter < 5)
		{
		    module_charge_first_summ += energy_value;
		}
		else
		{
		    module_charge_last_summ += energy_value;
		}
		
		if(cell_iter < n_first_cells)
		{
		    module_first_n_cells += energy_value;
		}
		else
		{
		    module_other_n_cells += energy_value;
		}
		
	    }// cell
	    
	    all_cells_on = (n_cell_signal == beam_to_module_tcells);
	    e_selection = (module_charge_cell0/module_charge_cell_all) > 0.85;
	    is_all_cells_nonoize = module_charge_sqsumm > TMath::Sqrt(n_module_noize_rej)*mev_in_mip*0.85;

	    muon_selection = TMath::Abs(module_charge_first_summ - module_charge_last_summ) < muoncut_cmp;
	    muon_selection = muon_selection &&(module_charge_first_summ > muoncutfirst_min);
	    muon_selection = muon_selection &&(module_charge_first_summ < muoncutfirst_max);
	    muon_selection = muon_selection &&(module_charge_last_summ > muoncutlast_min);
	    muon_selection = muon_selection &&(module_charge_last_summ < muoncutlast_max);
	    //	    muon_selection = muon_selection && all_cells_on;

	    Float_t muon_sel_r = 20., muon_sel_x = 27., muon_sel_y = 27.;
	    if(IsUseAmplitudes)
	    {
		muon_sel_r = 20.; muon_sel_x = 30.; muon_sel_y = 30.;
	    }

	    muon_selection = ( (module_charge_first_summ - muon_sel_x)*(module_charge_first_summ - muon_sel_x) +
	                       (module_charge_last_summ - muon_sel_y)*(module_charge_last_summ - muon_sel_y) ) <
	            muon_sel_r * muon_sel_r;
	    muon_selection = muon_selection && all_cells_on;


	    if(e_selection||true)
	    {
		th2_hist_ptr = ((TH2*)(gDirectory->FindObjectAny( Form("beammod%i_module_testsel", beam_to_module_N) )));
		th2_hist_ptr->Fill(module_other_n_cells, module_first_n_cells);
	    }
	    
	    if(muon_selection)
	    {
		th2_hist_ptr = ((TH2*)(gDirectory->FindObjectAny( Form("beammod%i_module_halfversus_sel", beam_to_module_N) )));
		th2_hist_ptr->Fill(module_charge_first_summ, module_charge_last_summ);
		
	    }
	    else
	    {
		th2_hist_ptr = ((TH2*)(gDirectory->FindObjectAny( Form("beammod%i_module_halfversus", beam_to_module_N) )));
		th2_hist_ptr->Fill(module_charge_first_summ, module_charge_last_summ);
	    }
	    
	    if(e_selection)
	    {
		th2_hist_ptr = ((TH2*)(gDirectory->FindObjectAny( Form("beammod%i_module_electrons", beam_to_module_N) )));
		th2_hist_ptr->Fill(module_charge_cell_all, module_charge_cell0/module_charge_cell_all);
	    }
	    else
	    {
		th2_hist_ptr = ((TH2*)(gDirectory->FindObjectAny( Form("beammod%i_module_electrons_sel", beam_to_module_N) )));
		th2_hist_ptr->Fill(module_charge_cell_all, module_charge_cell0/module_charge_cell_all);
	    }

	    if(is_time_sel)
	    {
		if(Is_TOF_in_reg)
		{
		    th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny("part_time")));
		    th1_hist_ptr->Fill(PMT_TOF);
		}
		else
		{
		    th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny("part_time_rej")));
		    th1_hist_ptr->Fill(PMT_TOF);
		}
	    }


	    if(Is_TOF_Ampl_in_range)
	    {
		th2_hist_ptr = ((TH2*)(gDirectory->FindObjectAny( Form("TOF_ampl_sel") )));
		th2_hist_ptr->Fill(PMT1signal.Amplitude, PMT2signal.Amplitude);
	    }
	    th2_hist_ptr = ((TH2*)(gDirectory->FindObjectAny( Form("TOF_ampl") )));
	    th2_hist_ptr->Fill(PMT1signal.Amplitude, PMT2signal.Amplitude);

	    
	}
	// ==========================================================================
	
	// Multi hit selection ======================================================
	Float_t summ_first_cells = 0.;
	for(Int_t module_iter = 0; module_iter < total_modules; module_iter++)
	{
	    Bool_t IsBeamAtThisModule = (module_iter+1 == beam_to_module_N);
	    TString module_name = readme_reader_ptr->Get_module_info()[module_iter].comment;
	    Int_t total_cells = readme_reader_ptr->Get_module_info()[module_iter].total_cells;

	    if(IsBeamAtThisModule) continue;

	    Int_t cell_iter = 0;

	    Int_t tree_ch = readme_reader_ptr->GetContCh_from_CellN(module_iter, cell_iter);
	    TString channel_name = event_data_struct::GetChName(tree_ch);

	    //Float_t charge_cell = (Float_t)event_struct[tree_ch].integral_in_gate;
		Float_t charge_cell = (IsUseFitSignals?event_struct[tree_ch].FIT_integral_in_gate : event_struct[tree_ch].integral_in_gate);
		Float_t Amplitude_sig = event_struct[tree_ch].MAX_in_gate - event_struct[tree_ch].zero_level;
		Float_t Amplitude_FIT = event_struct[tree_ch].FIT_MAX_in_gate - event_struct[tree_ch].FIT_zero_level;
		Float_t Amplitude = IsUseFitSignals?Amplitude_sig:Amplitude_sig;

	    //Float_t charge_cell = (Float_t)event_struct[tree_ch].integral_in_gate;
	    Float_t calib_coeff = (IsGetCurrCalib&&IsBeamAtThisModule) ? calib_calc_coef_mean[tree_ch] : calib_coef_array[tree_ch];
	    Float_t energy_cell = charge_cell / calib_coeff * mev_in_mip;
	    UChar_t time_cell = event_struct[tree_ch].time_max_in_gate;
	    Bool_t is_signal_in_gate = (time_cell > signal_time_gate[0])&&(time_cell < signal_time_gate[1]);
	    Float_t curr_pedestal_trah = is_custom_ped_trsh?
	                (calib_noise_mean_array[tree_ch] + custom_ped_nsgm*calib_noise_sigma_array[tree_ch])/calib_coeff :
	                pedestal_cut_mip;
	    //Bool_t is_energy_in_range = (energy_cell > curr_pedestal_trah*mev_in_mip)||(!IsPedestal_rejection);
	    //Float_t Amplitude = event_struct[tree_ch].MAX_in_gate - event_struct[tree_ch].zero_level;
	    Float_t energy_cell_ampl = Amplitude / calib_coef_ampl_array[tree_ch] * mev_in_mip;
	    Float_t energy_value = IsUseAmplitudes?energy_cell_ampl:energy_cell;


	    summ_first_cells  += energy_value;
	}

	Bool_t is_multihit_first_cells = summ_first_cells > 2.*mev_in_mip;
	th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny(Form("allnotcentrmod_cell_1_energy_summ"))));
	th1_hist_ptr->Fill(summ_first_cells);

	// ==========================================================================


	if(muon_selection) continue;

		if( IsElectrons )
		    if( e_selection ) continue;
	//        if( e_selection ) continue;



	if(Is_MultiHit_selection)
	    if(is_multihit_first_cells) continue;

	//if(!is_all_cells_nonoize) continue;
	//if(!Is_RMS_zero_level_correct_all_ch) continue;



	if(!Is_TOF_Ampl_in_range) continue;
	if(!Is_TOF_in_reg) continue;

	energy_events_collected++;
	for(Int_t icell = 0; icell < 10; icell++)calo_energy_summ[icell] = 0.;




	// PASS 1 Analysis ==========================================================
	for(Int_t module_iter = 0; module_iter < total_modules; module_iter++)
	{

	    if(module_iter == exclude_module) continue;

	    Bool_t IsBeamAtThisModule = (module_iter+1 == beam_to_module_N);
	    TString module_name = readme_reader_ptr->Get_module_info()[module_iter].comment;
	    Int_t total_cells = readme_reader_ptr->Get_module_info()[module_iter].total_cells;
	    
	    Float_t module_energy_summ = 0;
	    
	    for(Int_t cell_iter = 0; cell_iter < total_cells; cell_iter++)
	    {
		Int_t tree_ch = readme_reader_ptr->GetContCh_from_CellN(module_iter, cell_iter);
		TString channel_name = event_data_struct::GetChName(tree_ch);
		
		//Float_t charge_cell = (Float_t)event_struct[tree_ch].integral_in_gate;
		Float_t charge_cell = (IsUseFitSignals?event_struct[tree_ch].FIT_integral_in_gate : event_struct[tree_ch].integral_in_gate);
		Float_t Amplitude_sig = event_struct[tree_ch].MAX_in_gate - event_struct[tree_ch].zero_level;
		Float_t Amplitude_FIT = event_struct[tree_ch].FIT_MAX_in_gate - event_struct[tree_ch].FIT_zero_level;
		Float_t Amplitude = IsUseFitSignals?Amplitude_sig:Amplitude_sig;

		//Float_t charge_cell = (Float_t)event_struct[tree_ch].integral_in_gate;
		Float_t calib_coeff = (IsGetCurrCalib&&IsBeamAtThisModule) ? calib_calc_coef_mean[tree_ch] : calib_coef_array[tree_ch];
		//printf("calib_coef_mean_array[%i] = %f;\n", tree_ch,  calib_coeff);

		Float_t energy_cell = charge_cell / calib_coeff * mev_in_mip;
		UChar_t time_cell = event_struct[tree_ch].time_max_in_gate;
		Bool_t is_signal_in_gate = (time_cell > signal_time_gate[0])&&(time_cell < signal_time_gate[1]);
		Float_t curr_pedestal_trah = is_custom_ped_trsh?
		            (calib_noise_mean_array[tree_ch] + custom_ped_nsgm*calib_noise_sigma_array[tree_ch])/calib_coeff :
		            pedestal_cut_mip;
		//Bool_t is_energy_in_range = (energy_cell > curr_pedestal_trah*mev_in_mip)||(!IsPedestal_rejection);
		//Float_t Amplitude = event_struct[tree_ch].MAX_in_gate - event_struct[tree_ch].zero_level;
		Float_t energy_cell_ampl = Amplitude / calib_coef_ampl_array[tree_ch] * mev_in_mip;
		Float_t energy_value = IsUseAmplitudes?energy_cell_ampl:energy_cell;

		if(IsPedestal_rejection)
		    if(energy_value < 2.5) energy_value = 0.0;
		//3.0 - 128+-32; 2.5 - 133MeV; 2.0 - 140; 1.5 - 157; 1.0 - 186+-34;

		//selection applying
		//if(!is_signal_in_gate) continue;
		///if( !is_energy_in_range ) continue;

		th2_hist_ptr = ((TH2*)(gDirectory->FindObjectAny( Form("%s_cell_%i_amplcharge",module_name.Data(), cell_iter+1) )));
		th2_hist_ptr->Fill(energy_cell_ampl,energy_cell);


		module_energy_summ += energy_value;
		th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny( Form("%s_module_energy_first_%i",module_name.Data(), cell_iter+1) )));
		th1_hist_ptr->Fill(module_energy_summ);

		//		if(IsBeamAtThisModule)
		//		if(cell_iter == total_cells-1)
		//		    if(module_energy_summ < 20.)
		//		    {
		//			printf("event %i: mod energy: %f\n", entry, module_energy_summ);
		//			event_struct[readme_reader_ptr->GetContCh_from_CellN(beam_to_module_N-1, 0)].Print();
		//		    }
		
		
		th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny(Form("%s_cell_%i_energy",module_name.Data(), cell_iter))));
		th1_hist_ptr->Fill(energy_value);
		

		Double_t bin_content;
		th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny( Form("%s_module_trace",module_name.Data()) )));
		bin_content = th1_hist_ptr->GetBinContent(cell_iter+1);
		bin_content += energy_value;
		th1_hist_ptr->SetBinContent(cell_iter+1, bin_content);

		th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny( Form("Calo_trace") )));
		bin_content = th1_hist_ptr->GetBinContent(cell_iter+1);
		bin_content += energy_value;
		th1_hist_ptr->SetBinContent(cell_iter+1, bin_content);

		if(!IsBeamAtThisModule)
		{
		    th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny( Form("Calo_trace_ex_mod5") )));
		    bin_content = th1_hist_ptr->GetBinContent(cell_iter+1);
		    bin_content += energy_value;
		    th1_hist_ptr->SetBinContent(cell_iter+1, bin_content);
		}

		calo_energy_summ[cell_iter] += energy_value;
		
	    }// cell
	}// module
	// ==========================================================================
	
	Float_t calo_energy_summ_ = 0.;
	for(Int_t icell = 0; icell < beam_to_module_tcells; icell++)
	{
	    calo_energy_summ_ += calo_energy_summ[icell];
	    th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny(  Form("calo_energy_first_%i",icell+1)  )));
	    if(calo_energy_summ_ != 0)
		th1_hist_ptr->Fill(calo_energy_summ_);
	}



    } //entry
    delete[] calo_energy_summ;
    
    
    
    //Fit histogramms ============================================================
    
    th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny( Form("%s_module_energy_first_%i",beam_to_module_name.Data(), beam_to_module_tcells) )));
    FitHistogrammSmartInMaxPeak(th1_hist_ptr, mean, sigma, 1.0,0,1000);
    th1_hist_ptr->SetTitle( Form("Beam module energy [mean %.0f, sigma %.0f, res %.1f%%]; MeV", mean, sigma, (sigma/mean*100.)) );

    for(Int_t cell_iter = 0; cell_iter < beam_to_module_tcells; cell_iter++)
    {
	th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny( Form("calo_energy_first_%i",cell_iter+1) )));
	FitHistogrammSmartInMaxPeak(th1_hist_ptr, mean, sigma, 1.0,200,10000);
	th1_hist_ptr->SetTitle( Form("Full Calo first %i [mean %.0f, sigma %.0f, res %.1f%%]; MeV",cell_iter+1, mean, sigma, (sigma/mean*100.)) );
    }



    th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny( Form("Calo_trace") )));
    for(Int_t cell_iter = 0; cell_iter < beam_to_module_tcells; cell_iter++)
    {
	Double_t bin_content = th1_hist_ptr->GetBinContent(cell_iter+1);
	bin_content = bin_content / (Double_t)energy_events_collected;
	th1_hist_ptr->SetBinContent(cell_iter+1, bin_content);
    }

    th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny( Form("Calo_trace_ex_mod5") )));
    for(Int_t cell_iter = 0; cell_iter < beam_to_module_tcells; cell_iter++)
    {
	Double_t bin_content = th1_hist_ptr->GetBinContent(cell_iter+1);
	bin_content = bin_content / (Double_t)energy_events_collected;
	th1_hist_ptr->SetBinContent(cell_iter+1, bin_content);
    }



    for(Int_t module_iter = 0; module_iter < total_modules; module_iter++)
    {
	Int_t total_cells = readme_reader_ptr->Get_module_info()[module_iter].total_cells;
	TString module_name = readme_reader_ptr->Get_module_info()[module_iter].comment;
	
	
	th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny( Form("%s_module_trace",module_name.Data()) )));
	for(Int_t cell_iter = 0; cell_iter < total_cells; cell_iter++)
	{
	    //	    th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny(Form("%s_cell_%i_energy",module_name.Data(), cell_iter) )));
	    //	    FitHistogrammSmartInMaxPeak(th1_hist_ptr, mean, sigma, 1.0,0,1000);
	    Double_t bin_content = th1_hist_ptr->GetBinContent(cell_iter+1);
	    bin_content = bin_content / (Double_t)energy_events_collected;
	    th1_hist_ptr->SetBinContent(cell_iter+1, bin_content);
	}
    }
    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    
    
    
    
    
    ////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////// Drawing canvas ///////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    printf("Drawing canvas ==================================\n");

    TObjArray *canv_array = new TObjArray;
    canv_array->SetName("result_canvas");
    canv_array->SetOwner();
    result_array->Add(canv_array);
    
    TObjArray *canv_array_pdf = new TObjArray;
    canv_array_pdf->SetName("result_canvas");
    
    TLatex *lable = new TLatex;
    
    // Muon Calibration ==========================================================
    // Title canvas -----------------------------------
    printf("canvas muon calibration ==================================\n");

    canv_ptr = new TCanvas(Form("CalibTitle"));
    canv_array_pdf->Add(canv_ptr);
    
    lable->SetTextAlign(12);
    
    lable->SetTextSize(0.09); lable->DrawLatex(0.1,0.7,readme_reader_ptr->Get_run_comment().Data());
    lable->SetTextSize(0.08); lable->DrawLatex(0.1,0.6,"Muons Calibration");
    // ------------------------------------------------
    
    for(Int_t module_iter = (Is_Calibration?0:beam_to_module_N-1);
        module_iter < (Is_Calibration?total_modules:beam_to_module_N); module_iter++)
    {
	Int_t total_cells = readme_reader_ptr->Get_module_info()[module_iter].total_cells;
	TString module_name = readme_reader_ptr->Get_module_info()[module_iter].comment;
	
	canv_ptr = new TCanvas(Form("%s_muons", module_name.Data()), Form("%s", module_name.Data()));
	canv_ptr->DivideSquare(total_cells+3);
	canv_array->Add(canv_ptr);
	canv_array_pdf->Add(canv_ptr);
	
	canv_ptr->cd(1);
	lable->SetTextSize(0.07); lable->DrawLatex(0.1,0.8,"Calib Spectra");
	lable->SetTextSize(0.06); lable->DrawLatex(0.1,0.7,readme_reader_ptr->Get_run_comment().Data());
	lable->SetTextSize(0.06); lable->DrawLatex(0.1,0.6,module_name.Data());
	if(IsGetCurrCalib&&(module_iter+1 == beam_to_module_N))
	{
	    lable->SetTextSize(0.06); lable->DrawLatex(0.1,0.5,"* This coeffs used");
	}
	
	gStyle->SetTitleSize(0.1,"t");
	
	canv_ptr->cd(2)->SetLogz(1);
	th2_hist_ptr = ((TH2*)(gDirectory->FindObjectAny(Form("%s_module_halfversus_calib",module_name.Data()) )));
	th2_hist_ptr->Draw("colz");
	
	canv_ptr->cd(3);
	th2_hist_ptr = ((TH2*)(gDirectory->FindObjectAny(Form("%s_module_halfversus_sel_calib",module_name.Data()) )));
	th2_hist_ptr->Draw("colz");
	
	
	for(Int_t cell_iter = 0; cell_iter < total_cells; cell_iter++)
	{
	    canv_ptr->cd(cell_iter +4);
	    th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny( Form("%s_cell_%i_charge_muons",module_name.Data(), cell_iter) )));
	    th1_hist_ptr->SetTitle( Form("Ch %i mod %i Cell%i",
	                                 readme_reader_ptr->GetContCh_from_CellN(module_iter, cell_iter), module_iter, cell_iter) );
	    
	    th1_hist_ptr->Draw();
	}
    }
    //============================================================================
    
#if(!Is_Calibration)
    // Charge summ spectra =======================================================
    // Title canvas -----------------------------------
    printf("canvas charhe summ spectra ==================================\n");

    canv_ptr = new TCanvas(Form("ChargeSummTitle"));
    canv_array_pdf->Add(canv_ptr);
    
    lable->SetTextAlign(12);
    
    lable->SetTextSize(0.09); lable->DrawLatex(0.1,0.7,readme_reader_ptr->Get_run_comment().Data());
    lable->SetTextSize(0.08); lable->DrawLatex(0.1,0.6,"Charge summ first n cells");
    // ------------------------------------------------
    
    // Full calo canvas -------------------------------
    canv_ptr = new TCanvas(Form("ChargeSummFull"));
    canv_ptr->DivideSquare(beam_to_module_tcells+7);
    canv_array->Add(canv_ptr);
    canv_array_pdf->Add(canv_ptr);
    
    canv_ptr->cd(1);
    lable->SetTextSize(0.07); lable->DrawLatex(0.1,0.8,"Full Calo first n cells");
    lable->SetTextSize(0.06); lable->DrawLatex(0.1,0.7,readme_reader_ptr->Get_run_comment().Data());
    
    lable->SetTextSize(0.06); lable->DrawLatex(0.1,0.6,Form("IsPedestal_rejection: %i", IsPedestal_rejection));
    lable->SetTextSize(0.06); lable->DrawLatex(0.1,0.5,Form("IsElectrons: %i", IsElectrons));
    
    
    
    gStyle->SetTitleSize(0.1,"t");
    
    
    canv_ptr->cd(2)->SetLogz(1);
    th2_hist_ptr = ((TH2*)(gDirectory->FindObjectAny( Form("beammod%i_module_halfversus", beam_to_module_N) )));
    th2_hist_ptr->SetTitle("No Muons selection; summ first; summ last;");
    th2_hist_ptr->Draw("colz");
    
    canv_ptr->cd(3)->SetLogz(1);
    th2_hist_ptr = ((TH2*)(gDirectory->FindObjectAny( Form("beammod%i_module_halfversus_sel", beam_to_module_N) )));
    th2_hist_ptr->SetTitle("Muons selection; summ first; summ last;");
    th2_hist_ptr->Draw("colz");
    
    canv_ptr->cd(4)->SetLogz(1);
    th2_hist_ptr = ((TH2*)(gDirectory->FindObjectAny( Form("beammod%i_module_electrons", beam_to_module_N) )));
    th2_hist_ptr->SetTitle("Electrons selection;Ecell;Cell0/Ecell");
    th2_hist_ptr->Draw("colz");
    
    canv_ptr->cd(5)->SetLogz(1);
    th2_hist_ptr = ((TH2*)(gDirectory->FindObjectAny( Form("beammod%i_module_electrons_sel", beam_to_module_N) )));
    th2_hist_ptr->SetTitle("No Electrons selection;Ecell;Cell0/Ecell");
    th2_hist_ptr->Draw("colz");
    
    canv_ptr->cd(6)->SetLogz(1);
    th2_hist_ptr = ((TH2*)(gDirectory->FindObjectAny( Form("beammod%i_module_testsel", beam_to_module_N) )));
    //th2_hist_ptr->SetTitle("No Electrons selection;Ecell;Cell0/Ecell");
    th2_hist_ptr->Draw("colz");
    
    //    canv_ptr->cd(6)->SetLogz(0);
    //    th2_hist_ptr = ((TH2*)(gDirectory->FindObjectAny( Form("Calo_trace") )));
    //    //th2_hist_ptr->SetTitle("No Electrons selection;Ecell;Cell0/Ecell");
    //    th2_hist_ptr->Draw("");
    
    
    for(Int_t cell_iter = 0; cell_iter < beam_to_module_tcells; cell_iter++)
    {
	canv_ptr->cd(cell_iter +7)->SetLogy(0);
	th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny(Form("calo_energy_first_%i",cell_iter+1) )));
	tfunc_ptr = th1_hist_ptr->GetFunction(fit_funk_name);
	if(tfunc_ptr)
	{
	    mean = tfunc_ptr->GetParameter(1);
	    sigma - tfunc_ptr->GetParameter(2);
	    th1_hist_ptr->SetTitle( Form("First %i cells [%.0f +- %.0f, res %.1f%%]; MeV",cell_iter+1, mean, sigma, (sigma/mean*100.)) );
	}
	th1_hist_ptr->Draw();
    }
    
    
    // ------------------------------------------------
    
    for(Int_t module_iter = 0; module_iter < total_modules; module_iter++)
    {
	Int_t total_cells = readme_reader_ptr->Get_module_info()[module_iter].total_cells;
	TString module_name = readme_reader_ptr->Get_module_info()[module_iter].comment;
	
	canv_ptr = new TCanvas(Form("%s_summ_charge", module_name.Data()), Form("%s", module_name.Data()));
	canv_ptr->DivideSquare(total_cells+1);
	canv_array->Add(canv_ptr);
	canv_array_pdf->Add(canv_ptr);
	
	canv_ptr->cd(1);
	lable->SetTextSize(0.07); lable->DrawLatex(0.1,0.8,"Charge summ");
	lable->SetTextSize(0.06); lable->DrawLatex(0.1,0.7,readme_reader_ptr->Get_run_comment().Data());
	lable->SetTextSize(0.06); lable->DrawLatex(0.1,0.6,module_name.Data());
	
	gStyle->SetTitleSize(0.1,"t");
	
	for(Int_t cell_iter = 0; cell_iter < total_cells; cell_iter++)
	{
	    canv_ptr->cd(cell_iter +2)->SetLogy(0);
	    th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny(Form("%s_module_energy_first_%i",module_name.Data(), cell_iter+1) )));
	    //	    if((module_iter != beam_to_module_N-1)&&(cell_iter != total_cells-1))
	    //		th1_hist_ptr->SetTitle( Form("mod %i first %i cell;Mev", module_iter, cell_iter+1) );
	    
	    th1_hist_ptr->Draw();
	}
    }
    //============================================================================
    
    
    // Charge spectra ============================================================
    // Title canvas -----------------------------------
    printf("charge spectra canvas ==================================\n");

    canv_ptr = new TCanvas(Form("ChargeTitle"));
    canv_array_pdf->Add(canv_ptr);
    
    lable->SetTextAlign(12);
    
    lable->SetTextSize(0.09); lable->DrawLatex(0.1,0.7,readme_reader_ptr->Get_run_comment().Data());
    lable->SetTextSize(0.08); lable->DrawLatex(0.1,0.6,"Charge at cells");
    // ------------------------------------------------
    
    for(Int_t module_iter = 0; module_iter < total_modules; module_iter++)
    {
	Int_t total_cells = readme_reader_ptr->Get_module_info()[module_iter].total_cells;
	TString module_name = readme_reader_ptr->Get_module_info()[module_iter].comment;
	
	canv_ptr = new TCanvas(Form("%s_charge", module_name.Data()), Form("%s", module_name.Data()));
	canv_ptr->DivideSquare(total_cells+2);
	canv_array->Add(canv_ptr);
	canv_array_pdf->Add(canv_ptr);
	
	canv_ptr->cd(1);
	lable->SetTextSize(0.07); lable->DrawLatex(0.1,0.8,"Charge Spectra");
	lable->SetTextSize(0.06); lable->DrawLatex(0.1,0.7,readme_reader_ptr->Get_run_comment().Data());
	lable->SetTextSize(0.06); lable->DrawLatex(0.1,0.6,module_name.Data());
	
	gStyle->SetTitleSize(0.03,"t");
	
	canv_ptr->cd(2);
	th2_hist_ptr = ((TH2*)(gDirectory->FindObjectAny(Form("%s_module_trace",module_name.Data()) )));
	th2_hist_ptr->SetTitle(Form("Module trace [%s]; section#; Amplitude [MeV/events]", module_name.Data()));
	th2_hist_ptr->Draw("colz");
	
	
	for(Int_t cell_iter = 0; cell_iter < total_cells; cell_iter++)
	{
	    canv_ptr->cd(cell_iter +3)->SetLogy(1);
	    th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny(Form("%s_cell_%i_energy",module_name.Data(), cell_iter) )));
	    th1_hist_ptr->SetTitle( Form("Ch %i mod %i Cell%i",
	                                 readme_reader_ptr->GetContCh_from_CellN(module_iter, cell_iter), module_iter, cell_iter) );
	    


	    th1_hist_ptr->Draw();
	}
    }
    //============================================================================


    // Extra charge canvas =======================================================
//    printf("extra charge canvas ==================================\n");
//
//    for(Int_t module_iter = 4; module_iter < 5; module_iter++)
//    {
//	Int_t total_cells = readme_reader_ptr->Get_module_info()[module_iter].total_cells;
//	TString module_name = readme_reader_ptr->Get_module_info()[module_iter].comment;
//
//	canv_ptr = new TCanvas(Form("%s_charge_extra", module_name.Data()), Form("%s", module_name.Data()));
//	canv_ptr->Divide(3,3);
//	canv_array->Add(canv_ptr);
//
//	for(Int_t cell_iter = 0; cell_iter < total_cells-1; cell_iter++)
//	{
//	    TVirtualPad *pad = canv_ptr->cd(cell_iter +1);
//	    th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny(Form("%s_cell_%i_energy",module_name.Data(), cell_iter) )));
//	    if(th1_hist_ptr)
//	      {
//	    th1_hist_ptr->SetTitle( Form("mod %i section# %i;Mev",
//	                                 module_iter, cell_iter) );
//	    //th1_hist_ptr->GetXaxis()->SetRangeUser(-50., 400.);
//	    //gStyle->SetTitleSize(15,"t");
//	    gStyle->SetTitleFontSize(15);
//	    gStyle->SetLabelSize(0.5,"X");
//	    th1_hist_ptr->Draw();
//	    //	    TPaveText *pt = (TPaveText *)(pad->GetPrimitive("title"));
//	    //	    if(pt) pt->SetTextSize(0.5);
//	    //	    pad->Modified();
//	      }
//	}
//    }
    //============================================================================

    // Extra summ charge canvas =======================================================
//    printf("Extra summ charge canvas ==================================\n");
//
//    for(Int_t module_iter = 4; module_iter < 5; module_iter++)
//    {
//	Int_t total_cells = readme_reader_ptr->Get_module_info()[module_iter].total_cells;
//	TString module_name = readme_reader_ptr->Get_module_info()[module_iter].comment;
//
//	canv_ptr = new TCanvas(Form("%s_summ_charge_extra", module_name.Data()), Form("%s", module_name.Data()));
//	canv_ptr->Divide(3,3);
//	canv_array->Add(canv_ptr);
//	gStyle->SetTitleSize(0.1,"t");
//
//	for(Int_t cell_iter = 0; cell_iter < total_cells-1; cell_iter++)
//	{
//	    canv_ptr->cd(cell_iter +1)->SetLogy(0);
//	    th1_hist_ptr = ((TH1*)(gDirectory->FindObjectAny(Form("%s_module_energy_first_%i",module_name.Data(), cell_iter+1) )));
//	    th1_hist_ptr->SetTitle( Form("mod %i summ first %i sections;Mev", module_iter, cell_iter+1) );
//	    //th1_hist_ptr->GetXaxis()->SetRangeUser(-25., 300.);
//	    th1_hist_ptr->Draw();
//	}
//    }
    //============================================================================


    // Ampl - Charge ============================================================
    // Title canvas -----------------------------------
    printf("Ampl - Charge canvas ==================================\n");

    canv_ptr = new TCanvas(Form("AmplCharge"));
    canv_array_pdf->Add(canv_ptr);

    lable->SetTextAlign(12);

    lable->SetTextSize(0.09); lable->DrawLatex(0.1,0.7,readme_reader_ptr->Get_run_comment().Data());
    lable->SetTextSize(0.06); lable->DrawLatex(0.1,0.6,"Energy calculated by Amplitude vs by Charge");
    // ------------------------------------------------

    for(Int_t module_iter = 0; module_iter < total_modules; module_iter++)
    {
	Int_t total_cells = readme_reader_ptr->Get_module_info()[module_iter].total_cells;
	TString module_name = readme_reader_ptr->Get_module_info()[module_iter].comment;
	Bool_t IsBeamAtThisModule = (module_iter+1 == beam_to_module_N);

	Float_t curr_mult = IsBeamAtThisModule?range_mult:1.;


	canv_ptr = new TCanvas(Form("%s_amplcharge", module_name.Data()), Form("%s", module_name.Data()));
	canv_ptr->DivideSquare(total_cells);
	canv_array->Add(canv_ptr);
	canv_array_pdf->Add(canv_ptr);


	for(Int_t cell_iter = 0; cell_iter < total_cells; cell_iter++)
	{
	    canv_ptr->cd(cell_iter +1)->SetLogy(0);
	    th2_hist_ptr = ((TH2*)(gDirectory->FindObjectAny( Form("%s_cell_%i_amplcharge",module_name.Data(), cell_iter+1) )));
	    TProfile *profile = th2_hist_ptr->ProfileX();

	    profile->SetTitle( Form("Ch %i mod %i Cell%i;Mev",
	                            readme_reader_ptr->GetContCh_from_CellN(module_iter, cell_iter), module_iter, cell_iter) );

	    profile->GetXaxis()->SetRangeUser(curr_mult*min_energy, curr_mult*max_energy);
	    profile->GetYaxis()->SetRangeUser(curr_mult*min_energy, curr_mult*max_energy);

	    //	    if(module_iter == 4)
	    //	    {
	    //		profile->GetXaxis()->SetRangeUser(0., 250.);
	    //		profile->GetYaxis()->SetRangeUser(0., 250.);
	    //	    }
	    //	    else
	    //	    {
	    //		profile->GetXaxis()->SetRangeUser(0., 50.);
	    //		profile->GetYaxis()->SetRangeUser(0., 50.);
	    //	    }

	    profile->Draw();
	}
    }
    //============================================================================

    
#endif
    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    
    printf("saving results ==================================\n");

    
    ////////////////////////////////////////////////////////////////////////////////////////
    if(1)
      {
    for(Int_t i = 0; i < canv_array_pdf->GetLast(); i++)
    {
	((TCanvas*)canv_array_pdf->At(i))->SaveAs(Form("%s.pdf(",   (result_path + result_file_name + "").Data() ));
    }
    ((TCanvas*)canv_array_pdf->At(canv_array_pdf->GetLast()))->SaveAs(Form("%s.pdf)",  (result_path + result_file_name + "").Data() ));
      }
    ////////////////////////////////////////////////////////////////////////////////////////
    
    
    ////////////////////////////////////////////////////////////////////////////////////////
    SaveArrayStructInFile(result_array, result_file);
    result_file->Close();
    delete result_file;
    
    printf("file %s was writed\n", (result_path + result_file_name + ".root").Data());
    gROOT->ProcessLine( Form(".cp calo_test_analisys_T9.c %s",(result_path + result_file_name + "_code.c").Data()) );
    ////////////////////////////////////////////////////////////////////////////////////////
    //delete result_array;
    
    
}







