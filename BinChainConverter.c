/*
 * Convert list of binary file to root
 *
 * 1) select including data from CAEN by USECAEN 0 or 1
 *   * external lib digdataprocessing needed for CAEN data
 * 2) select path to slow control to log_file_name
 *   * slow control log reading in not tested
 * 3) select signal gate by gate_beg, gate_end use use_var_gate = true
 * 4) use result_file_prefix for file name comment
 * 5) set destination path by result_path
 *   * if result_path = "void", root file will be at
 *     same folder as binary file
 * 6) fill files_names with list of files to convert
 * 7) run macro root -l -b -q BinToTreeDataConverter.c+
 *
 *
 *
 * Dmitry Finogeev
 * dmitry-finogeev@yandex.ru
 */

#define USECAEN 1
#define IsLocal 0

#define IsFITwaveform 0
#define IsFITwaveformQA 0 //launch root -l -b if turned on

#include <fstream>
#include <iostream>
#include <TNtuple.h>
#include <TFile.h>
#include <Rtypes.h>
#include <vector>
#include <TString.h>
#include <TROOT.h>

#include "Readme_reader.h"
#include "event_data_struct.h"
#include "bindataformat.h"
//#include "BinToTreeDataConverter.c"

#if USECAEN
#include "../digdataprocessing/DigMonitor.cpp"
#include "../digdataprocessing/DigVariables.cpp"
#include "../digdataprocessing/DigData.cpp"
#include "../digdataprocessing/DigDataRecipe.cpp"
#include "../digdataprocessing/DigEvent.cpp"
#include "../digdataprocessing/DigSignals.cpp"
#include "../digdataprocessing/DigChannel.cpp"
#include "../digdataprocessing/DigDataAnalysis.cpp"
#endif


// scp -r runna61@na61pc006.cern.ch:/home/runna61/testbeam_t9_afs_work/22.11.2017_mod5_shutter1_in_shutter2_out /eos/user/m/morozovs/testbeam_T9_Nov2017/
// scp -r /eos/user/m/morozovs/testbeam_T9_Nov2017/ runna61@na61pc008.cern.ch:/eos/user/m/morozovs/testbeam_T9_Nov2017


TString BinToTreeDataConverter(TString run_name, Int_t TotEv = 0, TString source_path = "", TString result_path = "void")
{
    ////////////////////////////////////////////////////////////////////////////////////////

#if IsFITwaveform
    Bool_t IsFIT = true;
#else
    Bool_t IsFIT = false;
#endif

#if IsFITwaveformQA
    ///  |             mode            |                    condition                    |    argument   |
    ///  |           default           |            first events_to_check_fit            |       -       |
    ///  |       neg_fit_integral      |             FIT_integral_in_gate < 0            |       -       |
    ///  |   diff_fit_int_and_wf_int   |  FIT_integral_in_gate - integral_in_gate > 10%  |      [2]      |
    ///  |          saturation         |           integral_in_gate > 600 000            |      [3]      |
    ///  |       integral_region       |      200 000 < integral_in_gate < 250 000       |    [4],[5]    |
    ///  |          chi_square         |             FIT_Chi_square > 80 000             |      [6]      |
    ///  |           time_half         |                FIT_time_half > 80               |      [7]      |

//how to: TString FIT_QA_mode = "diff_fit_int_and_wf_int, saturation";
 
    TString FIT_QA_mode = "diff_fit_int_and_wf_int";
    Int_t events_to_check_fit = 20;
    Int_t channel_to_check_fit = -1; //if -1 not used
    TObjArray *check_fit_arr = new TObjArray;
    Int_t fitQA_arg_arr[] = {events_to_check_fit, channel_to_check_fit, 10, 600000, 20000, 30000, 80000, 80};
#else
    TString FIT_QA_mode = "false";
    TObjArray *check_fit_arr = NULL;
    Int_t *fitQA_arg_arr = NULL;
#endif

	// INR na61 host
    TString log_file_name = IsLocal ? 
			//"/media/nikolay/ADATA_HD650/INR_data/variousPrograms/slowControl.csv":
			//"/run/media/runna61/ADATA_HD650/INR_data/variousPrograms/slowControl.csv"; 
			"/media/nikolay/ADATA_HD650/INR_data/variousPrograms/nica_slowControl_TC/slowControl.csv":
			"/home/runna61/nica_slowControl_TC/slowControl.csv";
			//"/run/media/runna61/ADATA_HD650/INR_data/variousPrograms/nica_slowControl_TC/slowControl.csv";

    TString result_file_prefix = "";
    Bool_t use_var_gate = true; //use_var_gate =  true: use this values, else val from run_info

    //T10 18
//        Int_t gate_beg = 70;
//	Int_t gate_end = 120;

    //T10 cosmic
    //        Int_t gate_beg = 40;
    //	Int_t gate_end = 130;

    // T9 , NA61
//    Int_t gate_beg = 60;
//    Int_t gate_end = 120;

    // T9, T10, NA61 fast fee
    Int_t gate_beg = 50;
    Int_t gate_end = 110;

    // INR cosmic CBM
//    Int_t gate_beg = 40;
//    Int_t gate_end = 100;

    ////////////////////////////////////////////////////////////////////////////////////////

    TString response;

    TString result_file_name;
    Bool_t save_at_same_folder = (result_path == "void");
    result_file_name =  save_at_same_folder ?
                source_path + run_name + "/" + run_name + result_file_prefix + ".root":
                result_path + run_name + result_file_prefix + ".root";


    Readme_reader *readme_reader_ptr = new Readme_reader(source_path);
    Int_t is_readme_opened = readme_reader_ptr->Set_runfolder_name(run_name);
    Int_t total_channels = readme_reader_ptr->Get_total_ch();
    Int_t total_boards = readme_reader_ptr->Get_boards_num();
    readme_reader_ptr->Print();
    //return;

    if(is_readme_opened < 0)
    {
	response = Form("readme file %s not opened", readme_reader_ptr->Get_info_file_path().Data());
	printf("%s\n",response.Data());
	return response;
    }


    Log_file_reader *Log_reader_ptr = new Log_file_reader(log_file_name);

#if USECAEN
    printf("Using CAEN processing\n");
    GlobalProcessStatus *globalProcessState = new GlobalProcessStatus();
    DigData *DigData_ptr = new DigData(globalProcessState);
    DigDataRecipe *DataRecipe_ptr = DigData_ptr->GetDataRecipe();
    DataRecipe_ptr->GetMainDetault()->minAmplitude = 0.; // set minimal amplitude no ampl selection)\n
    Int_t Is_DigDataConfigured = DataRecipe_ptr->Configure((source_path + run_name).Data()); //open configuration file, and configure data processing parameters\n
    if(Is_DigDataConfigured)
    {
	DigData_ptr->OpenData();
	DataRecipe_ptr->PrintOut();
    }
#endif

    event_log_data_struct *event_log_struct = new event_log_data_struct[total_boards];
    event_data_struct *event_struct = new event_data_struct[total_channels];








    //create result root file, create tree
    TFile *result_file = new TFile(result_file_name.Data(), "RECREATE");

    TTree *data_tree = new TTree("adc64_data", "adc64_data");
    for(Int_t ch = 0; ch < total_channels; ch++)
	(event_struct+ch)->CreateBranch(data_tree, ch);

    Int_t *log_search_index = new Int_t[total_boards];
    for(Int_t brd = 0; brd < total_boards; brd++)
    {
	(event_log_struct+brd)->CreateBranch(data_tree, readme_reader_ptr->Get_boardid(brd));
	log_search_index[brd] = 0;
    }
	
	Long64_t timestamp_first_ent0, timestamp_last_ent0;
	Long64_t timestamp_first_ent1, timestamp_last_ent1;

	
	
#if USECAEN
    if(Is_DigDataConfigured)
	DigData_ptr->GetEvent(0)->SetTreeParam(data_tree);
#endif


    //opening source file
    BinDataReader *bin_reader_arr = new BinDataReader[readme_reader_ptr->Get_boards_num()];

    Int_t events_to_process = 0, events_in_dig = 0;
    if(TotEv != 0) events_to_process = TotEv;



#if USECAEN
    if(Is_DigDataConfigured)
    {
	events_in_dig =  DigData_ptr->GetEvent(0)->GetChannel(0)->GetTotalEvents();
	printf("Events in Dig file %i\n", events_in_dig);
    }
#endif


    for(Int_t ifile = 0; ifile < total_boards; ifile++)
    {
	printf("\nLoading file (for baord %i) %s\n", ifile, readme_reader_ptr->Get_file_name(ifile).Data());
	bin_reader_arr[ifile].SetFile(readme_reader_ptr->Get_file_name(ifile));
	Int_t events_in_curr_bin = bin_reader_arr[ifile].events_total;


	if ( (events_to_process == 0) || (events_to_process > events_in_curr_bin) )
	    events_to_process = events_in_curr_bin;

	if((events_in_dig != 0)&&(events_in_curr_bin != events_in_dig))
	{
	    response = Form("Waring different number of events in bin[%i] and dig[%i] files",
	                    events_in_curr_bin,  events_in_dig);
	    printf("%s\n",response.Data());
	    //return response;
	}
    }

    //if((events_in_dig !=0) && (events_to_process > events_in_dig)) events_to_process = events_in_dig;


    // first events ADC sync
#if !USECAEN //for caen not ready

    printf("\nEvent syncronisation at ADC files ===========\n");
    //reading each board first event
    Int_t *first_boards_events = new Int_t[total_boards];
    for(Int_t ifile = 0; ifile < total_boards; ifile++)
    {
	Int_t board_ser = readme_reader_ptr->adc_info_arr[ifile].get_hex_name();
	Int_t reader_result = bin_reader_arr[ifile].ReadEvent(0, board_ser);//nevent - not used, next event readed
	if(reader_result != 1)
	{
	    response =   Form("Error reading board %i while event ADC sync\n", board_ser);
	    return response;
	}

	first_boards_events[ifile] = bin_reader_arr[ifile].Event_Num;
    }

    //found max event in boards
    Bool_t all_equals = true;
    Int_t max_ev_board = 0;

    printf("First event number all files\n");
    for(Int_t ifile = 0; ifile < total_boards; ifile++)
    {
	printf("board %x [%i] fevent %i\n",
	       readme_reader_ptr->adc_info_arr[ifile].get_hex_name(), ifile,
	       first_boards_events[ifile]);

	if(first_boards_events[0] != first_boards_events[ifile]) all_equals = false;
	if(first_boards_events[max_ev_board] < first_boards_events[ifile]) max_ev_board = ifile;
    }
    printf("All events equals: %i; board with max ev: %x [%i]\n",
           all_equals, readme_reader_ptr->adc_info_arr[max_ev_board].get_hex_name(), max_ev_board);

    //reading event at each board pulling to max ev
    if(!all_equals)
    {
	for(Int_t ifile = 0; ifile < total_boards; ifile++)
	{
	    Int_t board_ser = readme_reader_ptr->adc_info_arr[ifile].get_hex_name();
	    Int_t nev_to_read = first_boards_events[max_ev_board] - first_boards_events[ifile];
	    printf("board %x event to read %i\n",
	           readme_reader_ptr->adc_info_arr[ifile].get_hex_name(),
	           nev_to_read);

	    for(Int_t iev_sync = 0; iev_sync < nev_to_read; iev_sync++)
	    {
		Int_t reader_result = bin_reader_arr[ifile].ReadEvent(0, board_ser);//nevent - not used, next event readed
		if(reader_result != 1)
		{
		    response =   Form("Error reading board %i while event ADC sync\n", board_ser);
		    return response;
		}
	    }


	}
    }

    delete[] first_boards_events;

    printf("================================\n");


#endif

    //return "test";



    printf("\n\n###############\nEvent to process %i\n", events_to_process);
    //return;
    //reading events
    time_t start_time = time(NULL);
    Int_t last_readed;

    for(Int_t nevent = 1; nevent <= (readdebug?10:events_to_process); nevent++)
    {

	Int_t reader_result;
	for(Int_t ifile = 0; ifile < total_boards; ifile++)
	{
	    Int_t board_ser = readme_reader_ptr->adc_info_arr[ifile].get_hex_name();
	    reader_result = bin_reader_arr[ifile].ReadEvent(nevent, board_ser);//nevent - not used, next event readed
	    if(reader_result != 1) break;

	    if(bin_reader_arr[0].Event_Num != bin_reader_arr[ifile].Event_Num)
	    {
		printf("ERROR event sync in files:\n");
		printf("event requested: %i\n", nevent);
		printf("event readed at file %i: %i\n",0, bin_reader_arr[0].Event_Num);
		printf("event readed at file %i: %i\n",ifile, bin_reader_arr[ifile].Event_Num);
		printf("===\n");
	    }


	}


	if(reader_result == 0)
	{
	    printf("EOF reached\n");
	    break;
	}
	if(reader_result == -1)
	{
	    printf("File converting error\n");
	    break;
	}

	last_readed = bin_reader_arr[0].Event_Num;

	for(Int_t channel = 0; channel < total_channels; channel++)
	{
	    Int_t ch_num, ch_board, module_i, cell_i;
	    readme_reader_ptr->GetBoardCh_from_ContCh(channel, ch_board, ch_num);
	    readme_reader_ptr->GetCellN_from_ContCh(channel, module_i, cell_i);

	    if(!use_var_gate)
	    {
		gate_beg = readme_reader_ptr->Get_module_info()[module_i].sgate_min;
		gate_end = readme_reader_ptr->Get_module_info()[module_i].sgate_max;
	    }

	    Float_t ampl_scale = readme_reader_ptr->Get_module_info()[module_i].ampl_scale;

	    //printf("calc ch%i, brd%i, ch_num%i\n", channel,ch_board, ch_num);

	    ch_num--; //in info file map channel 1-128; in file 0-127

	    if(readdebug)   printf("\n\nCall waveform calculator: Event %i channel %i board %i ch_board %i\n", nevent, channel, ch_board, ch_num);

	    bin_reader_arr[ch_board].Calculate_waveform(event_struct[channel], ch_num, gate_beg, gate_end, ampl_scale, IsFIT, FIT_QA_mode, check_fit_arr, fitQA_arg_arr);

	    if(readdebug){event_struct[channel].Print();  printf("\n");}


	}


	Long64_t search_timestamp = bin_reader_arr->TimeStamp_date*1000;
	search_timestamp += bin_reader_arr->TimeStamp_ns*0.000001;
	search_timestamp = (bin_reader_arr->TimeStamp_date<<32) + (bin_reader_arr->TimeStamp_ns);
	search_timestamp /= 1000000000.;

	
	//printf("date: %ld; ns: %ld timestamp %lld\n", bin_reader_arr->TimeStamp_date, bin_reader_arr->TimeStamp_ns, search_timestamp);

	
	
	if(nevent == 1)
	  {
	    timestamp_first_ent0 = bin_reader_arr->TimeStamp_date;
	    timestamp_first_ent1 = bin_reader_arr->TimeStamp_ns;
	  }
	if(nevent == events_to_process)
	  {
	    timestamp_last_ent0 = bin_reader_arr->TimeStamp_date;
	    timestamp_last_ent1 = bin_reader_arr->TimeStamp_ns;
	  }


	for(Int_t brd = 0; brd < total_boards; brd++)
	{
	    //	    Long64_t search_timestamp = bin_reader_arr->TimeStamp_date;
		
	    //Log_reader_ptr->Search_entry(*(event_log_struct+brd), search_timestamp, readme_reader_ptr->Get_boardid(brd), *(log_search_index+brd));
	    //Log_reader_ptr->Search_entry(*(event_log_struct+brd), search_timestamp, readme_reader_ptr->Get_module_info()->slowcntr_num, *(log_search_index+brd));
		
		//printf("found log record for event %i:/n", nevent); (event_log_struct+brd)->Print();
	}



#if USECAEN
	if(Is_DigDataConfigured)
	    DigData_ptr->GetEvent(nevent-1)->CalculateEvent();
#endif

	data_tree->Fill();



	if((nevent%10) == 0)
	{
	    time_t current_time = time(NULL);
	    Int_t proc_sec = difftime(current_time,start_time);
	    Float_t percents = (float)nevent/events_to_process;
	    Int_t time_est = (percents == 0)?0:proc_sec/percents*(1. - percents);
	    Float_t proc_rate = (float)proc_sec/nevent*1000000./60.;

	    printf("Processed events: %i [%i] (%5.1f%%); [pas %3.0dm %2.0is] [est %3.0dm %2.0is] [%.1f min [%.1fh]/1M ev]\r",
	           nevent,last_readed, (percents*100.),
	           (proc_sec/60), proc_sec%60,
	           (time_est/60), time_est%60,
	           proc_rate, (proc_rate/60.));
	    cout<<flush;
	}

    }
	


#if IsFITwaveformQA

    TString FIT_QA_result_file_name =  save_at_same_folder ?
                source_path + run_name + "/" + run_name + result_file_prefix + "_FIT_QA":
                result_path + run_name + result_file_prefix + "_FIT_QA";
    if((check_fit_arr)&&(check_fit_arr->GetLast() > 0))
    {
	for (Int_t i = 0; i < check_fit_arr->GetLast(); i++)
	{
	((TCanvas*) check_fit_arr->At (i))->SaveAs (
	(FIT_QA_result_file_name + ".pdf(").Data ());
	}
	((TCanvas*) check_fit_arr->At (check_fit_arr->GetLast ()))->SaveAs (
	(FIT_QA_result_file_name + ".pdf)").Data ());

	printf("\n%s\n", Form("Fit QA written in %s.pdf file\n", FIT_QA_result_file_name.Data()));
	delete check_fit_arr;
    }


#endif	


//	time_t time_first_en = timestamp_first_ent/1000.;
//    time_t time_last_en = timestamp_last_ent/1000.;
	
	printf("\n\n");
//	    printf("First event time: [%lld] (%s);\n",timestamp_first_ent,  asctime( localtime( &time_first_en ) ));
//	    printf("Last  event time: [%lld] (%s);\n",timestamp_last_ent,   asctime( localtime( &time_last_en ) ));
	    printf("First event time: [%x][%x];\n",timestamp_first_ent0,timestamp_first_ent1);
	    printf("Last  event time: [%x][%x];\n",timestamp_last_ent0,timestamp_last_ent1);

	

    //copy info file
    if(!save_at_same_folder)gROOT->ProcessLine( Form(".cp %s %s.txt",readme_reader_ptr->Get_info_file_path().Data(), (result_path+run_name+result_file_prefix).Data()) );


    response = Form("%i events writed in %s file\nlast readed event from file(0): %i; events in dig: %i",
                    events_to_process, result_file_name.Data(), last_readed, events_in_dig);

    data_tree->Write();
    result_file->Close();

    delete readme_reader_ptr;
    delete Log_reader_ptr;
    delete[] event_struct;
    delete[] bin_reader_arr;
    delete[] log_search_index;

    printf("%s\n",response.Data());
    return response;
}


void BinChainConverter()
{

    TString source_path = IsLocal ? 
			//"/media/nikolay/ADATA_HD650/INR_data/testbeam_T9_Nov2017/":
			//"/run/media/runna61/ADATA_HD650/INR_data/testbeam_T9_Nov2017/"; 

			"/media/nikolay/ADATA_HD650/INR_data/beam_T10_Oct_18/":
			"/run/media/runna61/ADATA_HD650/INR_data/cosmic_data_2018_quater_3/";


    TString result_path = "void"; 



    TObjArray files_names;

        //files_names.Add(new TObjString("adc64_17_09_08_13_45_protonScan_mod5_6GeV")); 
        //files_names.Add(new TObjString("26.11.2017_09_40_mod5_6GeV_shutter1_in_shutter2_out_nominalHV")); 

	//cosmic
        //files_names.Add(new TObjString("18_09_20_14_10_INR_12mod_NICA_TH200_ZS100_DSPoff"));


    //fast FEE an NA61 test beam
    /****************************

    result_path = "/mnt/disk_na61pc008/runna61/adc64_data_2018_quater1/beam_root_fit/";
    source_path = "/mnt/disk_na61pc008/runna61/adc64_data_2018_quater1/";

    files_names.Add(new TObjString("beam_T10_18_10_24_16_38_beam_5GeV_noZS"));
   // files_names.Add(new TObjString("beam_NA61_18_04_30_18_27_40GeV_mod3_noZS"));
    //files_names.Add(new TObjString("beam_NA61_18_04_30_18_37_30GeV_mod3_noZS"));
    //files_names.Add(new TObjString("beam_NA61_18_04_30_18_55_19GeV_mod3_noZS"));
    //files_names.Add(new TObjString("beam_NA61_18_04_30_19_00_13GeV_mod3_noZS"));
    //files_names.Add(new TObjString("beam_NA61_18_04_30_19_45_10GeV_mod3_noZS"));
    /****************************/


    //old CBM cosmics
    /****************************/
    //result_path = "void";
    //source_path = "/mnt/disk_data_8TB/cosmic_data_2018_quater1/";

   // files_names.Add(new TObjString("adc64_17_10_30_16_15_CBM_15_overnight_bin"));
    //files_names.Add(new TObjString("adc64_18_03_26_22_30_INR_5mod_TRSH200_ZS150_MAF8_BLC50_8M"));
    //files_names.Add(new TObjString("beam_NA61_18_04_30_18_37_30GeV_mod3_noZS"));
    //files_names.Add(new TObjString("beam_NA61_18_04_30_18_55_19GeV_mod3_noZS"));
    //files_names.Add(new TObjString("beam_NA61_18_04_30_19_00_13GeV_mod3_noZS"));
    //files_names.Add(new TObjString("beam_NA61_18_04_30_19_45_10GeV_mod3_noZS"));

    //T10 beam fast fee ADC v3
    /****************************
    result_path = "/home/runna61/adc64_data_2018_quater4/root_files/";
    source_path = "/home/runna61/adc64_data_2018_quater4/";
//    files_names.Add(new TObjString("beam_T10_18_10_24_22_35_beam_5GeV_noZS"));
//    files_names.Add(new TObjString("beam_T10_18_10_24_23_38_beam_4GeV_noZS"));
//    files_names.Add(new TObjString("beam_T10_18_10_25_00_42_beam_3GeV_noZS"));
//    files_names.Add(new TObjString("beam_T10_18_10_25_10_55_beam_2GeV_noZS"));
    /****************************/

    //T10 beam fast fee ADC v3 long cables
    /****************************/
    result_path = "/home/runna61/adc64_data_2018_quater4/root_files/";
    source_path = "/home/runna61/adc64_data_2018_quater4/";
 //   files_names.Add(new TObjString("beam_T10_18_10_25_15_09_beam_5GeV_noZS_longCable"));
    files_names.Add(new TObjString("beam_T10_18_10_25_16_40_beam_4GeV_noZS_longCable"));
 //   files_names.Add(new TObjString("beam_T10_18_10_25_17_51_beam_3GeV_noZS_longCable"));
 //   files_names.Add(new TObjString("beam_T10_18_10_25_19_27_beam_2GeV_noZS_longCable"));
    /****************************/


    TString response;
    TObjArray response_array;

    Int_t total_files = files_names.GetLast()+1;
    for(Int_t iFile=0; iFile<total_files;iFile++)
    {
	TString iFile_name = ((TObjString*)(files_names.At(iFile)))->GetString();

	printf("\n######################################\n");
	printf("############## %i of %i ##############\n", iFile+1, total_files);
	printf("######################################\n\n");

	response = BinToTreeDataConverter(iFile_name, 0, source_path, result_path);

	response_array.Add(new TObjString( response ));
    }

    printf("\n---------------------------------------------------\nChain converter summary:\n");
    for(Int_t iFile=0; iFile<total_files;iFile++)
    {
	TString iFile_name = ((TObjString*)(files_names.At(iFile)))->GetString();
	response = ((TObjString*)(response_array.At(iFile)))->GetString();
	printf("%s:\n%s\n\n", iFile_name.Data(), response.Data());
    }


}


#if USECAEN
#ifdef __CINT__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;

#pragma link C++ class DigDataRecipe+;
#pragma link C++ struct DataConfiguration+;
#pragma link C++ struct ChConfiguration+;
#pragma link C++ struct DataProcParameters+;
#pragma link C++ struct Main_Processing_Param+;
#pragma link C++ struct Fitting_Processing+;
#pragma link C++ struct Amplitude_Processing_Param+;
#pragma link C++ class ParametersFrame+;
#pragma link C++ class MyMainFrame+;
#pragma link C++ class EvSelectotrFrame+;
#endif
#endif
